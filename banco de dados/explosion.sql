-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: 25-Abr-2019 às 20:45
-- Versão do servidor: 5.7.24
-- versão do PHP: 7.2.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `explosion`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_boletos`
--

DROP TABLE IF EXISTS `tb_boletos`;
CREATE TABLE IF NOT EXISTS `tb_boletos` (
  `bol_id` int(11) NOT NULL,
  `bol_numero` varchar(30) NOT NULL,
  PRIMARY KEY (`bol_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_boletos_pagamento`
--

DROP TABLE IF EXISTS `tb_boletos_pagamento`;
CREATE TABLE IF NOT EXISTS `tb_boletos_pagamento` (
  `blp_id` int(11) NOT NULL,
  `blp_bol_id` int(11) NOT NULL,
  `blp_pag_id` int(11) NOT NULL,
  PRIMARY KEY (`blp_id`),
  KEY `blp_bol_id` (`blp_bol_id`),
  KEY `blp_pag_id` (`blp_pag_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_cartao_credito`
--

DROP TABLE IF EXISTS `tb_cartao_credito`;
CREATE TABLE IF NOT EXISTS `tb_cartao_credito` (
  `crt_id` int(255) NOT NULL AUTO_INCREMENT COMMENT 'ID do cartão',
  `crt_numero_cartao` varchar(255) NOT NULL COMMENT 'Núemro do cartão',
  `crt_bandeira` varchar(255) NOT NULL COMMENT 'Bandeira do cartão',
  `crt_nome_impresso` varchar(255) NOT NULL COMMENT 'Nome impresso no cartão',
  `crt_validade` date NOT NULL COMMENT 'Validade do cartão',
  `crt_codigo_segurança` varchar(255) NOT NULL COMMENT 'Código de segurança',
  `crt_cli_id` int(255) NOT NULL COMMENT 'ID do cliente que possui o cartão',
  PRIMARY KEY (`crt_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='Tabela armazena os dados referentes aos cartões do cliente';

--
-- Extraindo dados da tabela `tb_cartao_credito`
--

INSERT INTO `tb_cartao_credito` (`crt_id`, `crt_numero_cartao`, `crt_bandeira`, `crt_nome_impresso`, `crt_validade`, `crt_codigo_segurança`, `crt_cli_id`) VALUES
(1, '4002 8922 2225 1234', '2', 'leonardo', '2019-03-03', '159', 5);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_cartoes`
--

DROP TABLE IF EXISTS `tb_cartoes`;
CREATE TABLE IF NOT EXISTS `tb_cartoes` (
  `crt_id` int(11) NOT NULL AUTO_INCREMENT,
  `crt_numero_cartao` varchar(25) NOT NULL,
  `crt_bandeira` varchar(20) NOT NULL,
  `crt_nome_impresso` varchar(50) NOT NULL,
  `crt_validade` date NOT NULL,
  `crt_codigo_seguranca` varchar(3) NOT NULL,
  `crt_cli_id` int(11) NOT NULL,
  PRIMARY KEY (`crt_id`),
  KEY `crt_cli_id` (`crt_cli_id`)
) ENGINE=MyISAM AUTO_INCREMENT=42 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tb_cartoes`
--

INSERT INTO `tb_cartoes` (`crt_id`, `crt_numero_cartao`, `crt_bandeira`, `crt_nome_impresso`, `crt_validade`, `crt_codigo_seguranca`, `crt_cli_id`) VALUES
(1, '123456', 'Master Card', 'leonardo batista Carias', '2020-03-23', '123', 9),
(2, '123', 'Visa', '123', '2020-03-12', '123', 9),
(3, '1', 'Visa', '1', '1998-03-23', '1', 9),
(4, '1', 'Visa', '1', '1998-03-23', '1', 9),
(31, '1000 1000 1000 1000', 'Visa', 'leonardo batista Carias', '1998-03-23', '159', 9),
(32, '1234123412341234', 'Visa', 'marcelo v b c filho', '2019-05-07', '123', 10),
(33, '10000', 'Master Card', 'Leonardo', '1998-03-23', '12', 10),
(34, '1234567', 'Master Card', 'marcelo v b c filho', '1999-01-02', '234', 10),
(35, '12345654321234565432', 'PayPal', 'marcelo v b c filho', '1999-01-02', '123', 10),
(36, '123456787654321', 'Visa', 'marcelo v b c filho', '1999-01-02', '123', 10),
(37, '12345678765432', 'Master Card', 'marcelo v b c filho', '1999-01-02', '123', 10),
(38, '12345671234567', 'Master Card', 'marcelo v b c filho', '1999-01-02', '123', 10),
(39, '12345434564', 'Master Card', 'dzffsdfs', '1998-03-23', '122', 10),
(40, '12345676543212345', 'Master Card', 'marcelo v b c filho', '1999-01-02', '123', 10),
(41, '123456787654323', 'Visa', 'marcelo v b c filho', '1999-01-02', '123', 10);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_cartoes_pagamento`
--

DROP TABLE IF EXISTS `tb_cartoes_pagamento`;
CREATE TABLE IF NOT EXISTS `tb_cartoes_pagamento` (
  `ctp_id` int(11) NOT NULL,
  `ctp_pag_id` int(11) NOT NULL,
  `ctp_crt_id` int(11) NOT NULL,
  `ctp_qtd_parcelas` int(11) NOT NULL,
  PRIMARY KEY (`ctp_id`),
  KEY `ctp_pag_id` (`ctp_pag_id`),
  KEY `ctp_crt_id` (`ctp_crt_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_categorias`
--

DROP TABLE IF EXISTS `tb_categorias`;
CREATE TABLE IF NOT EXISTS `tb_categorias` (
  `cat_id` int(11) NOT NULL AUTO_INCREMENT,
  `cat_nome` varchar(30) NOT NULL,
  PRIMARY KEY (`cat_id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tb_categorias`
--

INSERT INTO `tb_categorias` (`cat_id`, `cat_nome`) VALUES
(1, 'Hardware'),
(2, 'Eletrodomestico'),
(5, 'Celulares'),
(7, 'Perifericos');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_cliente`
--

DROP TABLE IF EXISTS `tb_cliente`;
CREATE TABLE IF NOT EXISTS `tb_cliente` (
  `cli_id` int(255) NOT NULL AUTO_INCREMENT COMMENT 'ID do cliente',
  `cli_nome_completo` varchar(256) NOT NULL COMMENT 'Nome completo do cliente',
  `cli_cpf` varchar(11) NOT NULL COMMENT 'CPF do cliente',
  `cli_rg` varchar(9) NOT NULL COMMENT 'RG do cliente',
  `cli_data_nascimento` date NOT NULL COMMENT 'Data de Nascimento do Cliente',
  `cli_email` varchar(256) NOT NULL COMMENT 'E-Mail do Cliente',
  `cli_senha` varchar(25) NOT NULL COMMENT 'Senha do Cliente',
  `cli_status` varchar(20) NOT NULL COMMENT 'Status do Cliente',
  `cli_admin` int(1) NOT NULL,
  PRIMARY KEY (`cli_id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COMMENT='Tabela armazena os dados referentes ao cliente';

--
-- Extraindo dados da tabela `tb_cliente`
--

INSERT INTO `tb_cliente` (`cli_id`, `cli_nome_completo`, `cli_cpf`, `cli_rg`, `cli_data_nascimento`, `cli_email`, `cli_senha`, `cli_status`, `cli_admin`) VALUES
(5, 'Leonardo batista carias', '47251299844', '3', '1998-03-26', 'leonardo', 'leo15926', 'INATIVO', 0),
(6, 'leonardo', '47251299844', '362790462', '1998-03-23', 'leonrdo', 'leo15926', 'ATIVO', 0),
(7, 'admin', '47251299844', '362790462', '1998-03-23', 'admin', 'Admin01@', 'ATIVO', 1),
(8, 'ASDASD', '47251299844', '36222', '1998-03-23', 'asd', 'leo15926', 'ATIVO', 0),
(9, 'Leonardo batista carias', '47251299844', '362790462', '1998-03-23', 'leo', 'leo15926', 'ATIVO', 0),
(10, 'marcelo vilas boas correa filho', '38661267862', '373014405', '1999-01-02', 'marcelovbcfilho@gmail.com', 'Marcelo1@', 'ATIVO', 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_endereco`
--

DROP TABLE IF EXISTS `tb_endereco`;
CREATE TABLE IF NOT EXISTS `tb_endereco` (
  `end_id` int(255) NOT NULL AUTO_INCREMENT COMMENT 'ID de endereço',
  `end_identificacao` varchar(255) NOT NULL COMMENT 'Nome do endereço',
  `end_logradouro` varchar(255) NOT NULL COMMENT 'Nome da rua',
  `end_numero` int(255) NOT NULL COMMENT 'Número do endereço',
  `end_cep` varchar(20) NOT NULL COMMENT 'CEP do endereço',
  `end_complemento` varchar(255) NOT NULL COMMENT 'Complemento do endereço',
  `end_bairro` varchar(255) NOT NULL COMMENT 'Bairro do endereço',
  `end_cidade` varchar(255) NOT NULL COMMENT 'Cidade do endereço',
  `end_estado` varchar(10) NOT NULL COMMENT 'Estado do endereço',
  `end_cli_id` int(255) NOT NULL COMMENT 'ID do cliente que possui o endereço',
  PRIMARY KEY (`end_id`),
  KEY `end_cli_id` (`end_cli_id`)
) ENGINE=MyISAM AUTO_INCREMENT=44 DEFAULT CHARSET=utf8 COMMENT='Tabela armazena os dados referentes aos endereços do cliente';

--
-- Extraindo dados da tabela `tb_endereco`
--

INSERT INTO `tb_endereco` (`end_id`, `end_identificacao`, `end_logradouro`, `end_numero`, `end_cep`, `end_complemento`, `end_bairro`, `end_cidade`, `end_estado`, `end_cli_id`) VALUES
(7, '', 'asdadsad', 15926, '08471013', 'a', 'a', 'a', 'AC', 5),
(6, '', 'a', 263, '08673115', 'a', 'a', 'a', 'a', 6),
(8, '', 'asda', 15, 'asda', 'asda', 'asda', 'asd', 'asda', 8),
(9, '', 'asdasd', 1592, '12312-3', '', 'dsa', 'asdsad', 'AC', 8),
(10, '', 'daqsda', 1592, 'asdasd', 'axda', 'dasdas', 'dasdas', 'dasdas', 8),
(29, 'Casa', 'casa', 15, 'casa', 'casa', 'casa', 'casa', 'casa', 9),
(18, '', 'Blog', 15, 'blog', 'blog', 'blohg', 'loh', 'blog', 9),
(19, '', 'bred', 15, 'bred', 'bred', 'bred', 'bred', 'bred', 9),
(20, '', 'PAO', 15, 'PAO', 'PAO', 'PAO', 'PAO', 'PAO', 9),
(32, 'teste', 'teste', 23, 'teste', 'teste', 'teste', 'teste', 'teste', 9),
(31, 'pagamento', 'pagamento', 12, 'pagamento', 'pagamento', 'pagamento', 'pagamento', 'pagmanteo', 9),
(30, 'casa', 'casa', 15, 'casa', 'casa', 'casa', 'casa', 'casa', 9),
(33, 'Casa', 'Rua  Das Colves', 150, '08410-006', 'Casa 2 bloco 3', 'Guaianazes', 'SÃ£o Paulo', 'SÃ£o Paulo', 9),
(34, 'Trabalho', 'rua das centrais', 159, '08410-006', 'casa 2 bolo 2', 'Guaianazes', 'Cidadela', 'SP', 9),
(35, 'TESTE3', 'TESTE3', 12, 'TESTE3', 'TESTE3', 'TESTE3', 'TESTE3', 'TESTE3', 9),
(36, 'Teste3', 'Teste3', 15, 'Teste3', 'Teste3', 'Teste3', 'Teste3', 'Teste3', 9),
(37, 'TESTE4', 'TESTE4', 15, 'TESTE4', 'TESTE4', 'TESTE4', 'TESTE4', 'TESTE4', 9),
(38, 'TESTE5', 'TESTE5', 5, 'TESTE5', 'TESTE5', 'TESTE5', 'TESTE5', 'TESTE5', 9),
(39, 'Teste', 'Teste', 15, 'Teste', 'Teste', 'Teste', 'Teste', 'Tste', 9),
(40, 'Teste', 'Teste', 15, 'Tesste', 'Tester', 'Teste', 'Teste', 'Teste', 9),
(41, 'teste', 'teste', 15, 'teste', 'TESTE4', 'teste', 'teste', 'teste', 9),
(42, 'teste', 'teste', 15, 'teste', 'teste', 'teste', 'teste', 'teste', 9),
(43, 'casa sabauna', 'rua aquelino estebanes', 28, '08850140', 'casa da esquina', 'sabaÃºna', 'mogi das cruzes', 'sp', 10);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_metodos_entrega`
--

DROP TABLE IF EXISTS `tb_metodos_entrega`;
CREATE TABLE IF NOT EXISTS `tb_metodos_entrega` (
  `mte_id` int(11) NOT NULL AUTO_INCREMENT,
  `mte_nome` varchar(25) NOT NULL,
  `mte_valor` double NOT NULL,
  PRIMARY KEY (`mte_id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tb_metodos_entrega`
--

INSERT INTO `tb_metodos_entrega` (`mte_id`, `mte_nome`, `mte_valor`) VALUES
(1, 'Sedex', 15.06),
(2, 'Entrega Ninja', 26.01),
(3, 'TNT Express', 50),
(4, 'Entrega Agendada', 15.4);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_pagamentos`
--

DROP TABLE IF EXISTS `tb_pagamentos`;
CREATE TABLE IF NOT EXISTS `tb_pagamentos` (
  `pag_id` int(11) NOT NULL,
  `pag_data_pagamento` date NOT NULL,
  `pag_ped_id` int(11) NOT NULL,
  PRIMARY KEY (`pag_id`),
  KEY `pag_ped_id` (`pag_ped_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_pedidos`
--

DROP TABLE IF EXISTS `tb_pedidos`;
CREATE TABLE IF NOT EXISTS `tb_pedidos` (
  `ped_id` int(11) NOT NULL AUTO_INCREMENT,
  `ped_metodo_entrega` int(11) NOT NULL,
  `ped_cartao` text NOT NULL,
  `ped_boleto` text NOT NULL,
  `ped_valor_total` double NOT NULL,
  `ped_status` varchar(20) NOT NULL,
  `ped_data_pagamento` date NOT NULL,
  `ped_data_pedido` date NOT NULL,
  `ped_cli_id` int(11) NOT NULL,
  `ped_end_id` int(11) NOT NULL,
  PRIMARY KEY (`ped_id`),
  KEY `ped_cli_id` (`ped_cli_id`),
  KEY `ped_end_id` (`ped_end_id`),
  KEY `ped_metodo_entrega` (`ped_metodo_entrega`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tb_pedidos`
--

INSERT INTO `tb_pedidos` (`ped_id`, `ped_metodo_entrega`, `ped_cartao`, `ped_boleto`, `ped_valor_total`, `ped_status`, `ped_data_pagamento`, `ped_data_pedido`, `ped_cli_id`, `ped_end_id`) VALUES
(1, 1, '0', '39', 0, 'Processando', '2019-04-23', '2019-04-23', 10, 43),
(2, 1, '0', '40', 0, 'Processando', '2019-04-23', '2019-04-23', 10, 43),
(3, 1, '0', '41', 0, 'Processando', '2019-04-23', '2019-04-23', 10, 43),
(4, 1, '0', '41', 0, 'Processando', '2019-04-23', '2019-04-23', 10, 43);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_produtos`
--

DROP TABLE IF EXISTS `tb_produtos`;
CREATE TABLE IF NOT EXISTS `tb_produtos` (
  `pro_id` int(11) NOT NULL AUTO_INCREMENT,
  `pro_nome` varchar(40) NOT NULL,
  `pro_descricao` text NOT NULL,
  `pro_peso` float NOT NULL,
  `pro_preco` float NOT NULL,
  `pro_endereco_imagem` text NOT NULL,
  `pro_endereco_imagem_lado` text NOT NULL,
  `pro_endereco_imagem_costas` text NOT NULL,
  `pro_sub_id` int(11) NOT NULL,
  PRIMARY KEY (`pro_id`),
  KEY `pro_cat_id` (`pro_sub_id`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tb_produtos`
--

INSERT INTO `tb_produtos` (`pro_id`, `pro_nome`, `pro_descricao`, `pro_peso`, `pro_preco`, `pro_endereco_imagem`, `pro_endereco_imagem_lado`, `pro_endereco_imagem_costas`, `pro_sub_id`) VALUES
(1, 'i5 8° Geração', 'Produto Bom', 25, 499, 'images/produtos/informatica/image1.png', 'images/produtos/informatica/image1lado.png', 'images/produtos/informatica/image1costas.png', 2),
(2, 'GTX 1660 Zotac', 'Produto Bacana', 400, 800, 'images/produtos/informatica/gtx1660Principal.png', 'images/produtos/informatica/gtx1660Lado.png', 'images/produtos/informatica/gtx1660Costas.png', 2),
(3, 'Ryzen 5 1600', 'Com esse processador inovador e incrível você desfruta ao máximo o verdadeiro potencial do seu computador e desfruta da mais pura velocidade. Maximize o seu desempenho seja trabalhando, jogando, navegando ou assistindo o seu filme preferido, com esse processador você pode tudo!', 300, 730, 'images/produtos/informatica/ryzen5Principal.png', 'images/produtos/informatica/ryzen5Lado.png', 'images/produtos/informatica/ryzen5Costas.png', 2),
(4, 'Processador I9', 'Processador TOP de linha', 100, 3000, 'images/produtos/informatica/i9Principal.png', 'images/produtos/informatica/i9Lado.png', 'images/produtos/informatica/i9Costas.png', 2),
(5, 'Thread Ripper', 'Processador TOP de linha absurdo', 100, 10000, 'images/produtos/informatica/threadRipperPrincipal.png', 'images/produtos/informatica/threadRipperLado.png', 'images/produtos/informatica/threadRipperCostas.png', 2),
(6, 'DDR4 8GB', 'Memória RAM', 100, 300, 'images/produtos/informatica/ddr4Principal.png', 'images/produtos/informatica/ddr4Lado.png', 'images/produtos/informatica/ddr4Costas.png', 2),
(7, 'RTX 2080 Aorus', 'Placa Dahora', 600, 5300, 'images/produtos/informatica/rtx2080AorusPrincipal.png', 'images/produtos/informatica/rtx2080AorusLado.png', 'images/produtos/informatica/rtx2080AorusCostas.png', 2),
(8, 'RTX 2080 TI ZOTAC', 'Placa Monstra', 600, 5500, 'images/produtos/informatica/rtx2080TiZotac.png', 'images/produtos/informatica/rtx2080TiZotacLado.png', 'images/produtos/informatica/rtx2080TiZotacCostas.png', 2),
(9, 'DDR4 8GB', 'Memória RAM', 100, 300, 'images/produtos/informatica/ddr4Principal.png', 'images/produtos/informatica/ddr4Lado.png', 'images/produtos/informatica/ddr4Costas.png', 2),
(10, 'DDR4 8GB', 'Memória RAM', 100, 300, 'images/produtos/informatica/ddr4Principal.png', 'images/produtos/informatica/ddr4Lado.png', 'images/produtos/informatica/ddr4Costas.png', 2),
(11, 'DDR4 8GB', 'Memória RAM', 100, 300, 'images/produtos/informatica/ddr4Principal.png', 'images/produtos/informatica/ddr4Lado.png', 'images/produtos/informatica/ddr4Costas.png', 2),
(12, 'DDR4 8GB', 'Memória RAM', 100, 300, 'images/produtos/informatica/ddr4Principal.png', 'images/produtos/informatica/ddr4Lado.png', 'images/produtos/informatica/ddr4Costas.png', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_produtos_pedidos`
--

DROP TABLE IF EXISTS `tb_produtos_pedidos`;
CREATE TABLE IF NOT EXISTS `tb_produtos_pedidos` (
  `pp_id` int(11) NOT NULL,
  `pp_ped_id` int(11) NOT NULL,
  `pp_pro_id` int(11) NOT NULL,
  `pp_pro_valor` double DEFAULT NULL,
  `pp_pro_qtde` int(11) DEFAULT NULL,
  PRIMARY KEY (`pp_id`),
  KEY `pp_ped_id` (`pp_ped_id`),
  KEY `pp_pro_id` (`pp_pro_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_sub_categorias`
--

DROP TABLE IF EXISTS `tb_sub_categorias`;
CREATE TABLE IF NOT EXISTS `tb_sub_categorias` (
  `sub_id` int(11) NOT NULL AUTO_INCREMENT,
  `sub_nome` varchar(30) NOT NULL,
  `sub_cat_id` int(11) NOT NULL,
  PRIMARY KEY (`sub_id`),
  KEY `sub_cat_id` (`sub_cat_id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tb_sub_categorias`
--

INSERT INTO `tb_sub_categorias` (`sub_id`, `sub_nome`, `sub_cat_id`) VALUES
(1, 'Processador', 1),
(2, 'LG', 5),
(4, 'Mouse Razer', 7),
(5, 'Teclado Logitec', 7);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
