<%@page import="br.com.ecomerce.dominio.Resultado"%>
<%@page import="br.com.ecomerce.dominio.Categoria"%>
<%@page import="br.com.ecomerce.dominio.SubCategoria"%>
<%@page import="br.com.ecomerce.dominio.EntidadeDominio"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="br.com.ecomerce.dominio.Produto"%>
<%@page import="br.com.ecomerce.dominio.Carrinho"%>
<%@page import="java.util.ArrayList"%>
<%@page import="br.com.ecomerce.dominio.Cliente"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>

        <meta charset="utf-8">
        <title>Explosion | E-Commerce de Eletrônicos</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">

        <link rel="shortcut icon" href="images/favicon.ico">

        <!-- CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/css/flexslider.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/css/fancySelect.css" rel="stylesheet" media="screen, projection" />
        <link href="${pageContext.request.contextPath}/css/animate.css" rel="stylesheet" type="text/css" media="all" />
        <link href="${pageContext.request.contextPath}/css/style.css" rel="stylesheet" type="text/css" />


        <!-- FONTS -->
        <link href='http://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
        <link href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">

    </head>
    <body>

        <!-- PRELOADER -->
        <div id="preloader"><img src="${pageContext.request.contextPath}/images/preloader.gif" alt="" /></div>
        <!-- //PRELOADER -->
        <div class="preloader_hide">

            <!-- PAGE -->
            <div id="page">

                <!-- HEADER -->
                <header>

                    <!-- TOP INFO -->
                    <div class="top_info">

                        <!-- CONTAINER -->
                        <div class="container clearfix">
                            <ul class="secondary_menu">
                                <%
                                    DecimalFormat df = new DecimalFormat("0.##");
                                    Cliente cliente = null;
                                    if (session.getAttribute("cliente") != null) {
                                        cliente = (Cliente) session.getAttribute("cliente");
                                        if (cliente.getAdmin() == 1) {
                                            out.println("<li><a href='"+request.getContextPath()+"/minha-conta-admin.jsp' >Minha Conta</a></li>");
                                            out.println("<li><a>Bem Vindo " + cliente.getNomeCompleto() + "</a></li>");
                                            out.println("<li><a href='"+request.getContextPath()+"/Logout?logout=logout'>Logout</a></li>");
                                        } else {
                                            out.println("<li><a href='"+request.getContextPath()+"/minha-conta.jsp' >Minha Conta</a></li>");
                                            out.println("<li><a>Bem Vindo " + cliente.getNomeCompleto() + "</a></li>");
                                            out.println("<li><a>Carteira: R$" + df.format(cliente.getCarteira()) + "</a></li>");
                                            out.println("<li><a href='"+request.getContextPath()+"/Logout?logout=logout'>Logout</a></li>");
                                        }

                                    } else {
                                        out.println("<li><a href='"+request.getContextPath()+"/login.jsp' >Login</a></li>");
                                        out.println("<li><a href='"+request.getContextPath()+"/cadastro-cliente.jsp' >Registrar</a></li>");
                                    }
                                %>
                            </ul>
                        </div><!-- //CONTAINER -->
                    </div><!-- TOP INFO -->


                    <!-- MENU BLOCK -->
                    <div class="menu_block">

                        <!-- CONTAINER -->
                        <div class="container clearfix">

                            <!-- LOGO -->
                            <div class="logo">
                                <a href="${pageContext.request.contextPath}/index.jsp" ><img src="${pageContext.request.contextPath}/images/logo/explosionV2.png" alt="" /></a>
                            </div><!-- //LOGO -->


                            <!-- SEARCH FORM -->
                            <div class="top_search_form">
                                <a class="top_search_btn" href="javascript:void(0);" ><i class="fa fa-search"></i></a>
                                <form method="get" action="#">
                                    <input type="text" name="search" value="Search" onFocus="if (this.value == 'Search')
                                                this.value = '';" onBlur="if (this.value == '')
                                                            this.value = 'Search';" />
                                </form>
                            </div><!-- SEARCH FORM -->


                            <!-- SHOPPING BAG -->
                            <%
                                double total = 0;
                                String limiteEstoque = "";
                                Carrinho carrinho = new Carrinho();
                                if (null == session.getAttribute("carrinho")) {
                                    ArrayList<Produto> produtos = new ArrayList<Produto>();
                                    carrinho.setProdutos(produtos);
                                    session.setAttribute("carrinho", carrinho);
                            %>
                            <div class="shopping_bag">
                                <a class="shopping_bag_btn" href = "javascript:void(0);" > 
                                    <i class="fa fa-shopping-cart"></i > <p>Carrinho</p > <span> <% out.print(carrinho.getProdutos().size());%></span></a>
                                <div class="cart">
                                    <div class="cart_total">
                                        <div class="clearfix">
                                            <span class="cart_subtotal">Total: <b><% out.print("R$" + total);%></b></span>
                                        </div>
                                        <a class="btn active" href="logado/carrinho-compra.jsp">Ir ao Carrinho</a>
                                    </div>
                                </div>
                            </div>
                            <%
                            } else {

                                carrinho = (Carrinho) session.getAttribute("carrinho");
                                for (int i = 0; i < carrinho.getProdutos().size(); i++) {
                                    int id = 0;
                                    if (null != request.getParameter("OPERACAO")
                                            && null != request.getParameter("idProduto")) {
                                        id = Integer.valueOf(request.getParameter("idProduto"));
                                        if (request.getParameter("OPERACAO").equals("REMOVER")) {
                                            if (id == carrinho.getProdutos().get(i).getId()) {
                                                //index = carrinho.getProdutos().indexOf(pro);                                                            
                                                carrinho.getProdutos().remove(i);
                                                session.setAttribute("carrinho", carrinho);
                                                carrinho = (Carrinho) session.getAttribute("carrinho");
                                            }
                                        }
                                        if (request.getParameter("OPERACAO").equals("ATUALIZAR") && null != request.getParameter("txtQtd")) {
                                            int qtd = Integer.valueOf(request.getParameter("txtQtd"));
                                            if (qtd <= 0) {
                                                carrinho.getProdutos().remove(i);
                                            } else if (id == carrinho.getProdutos().get(i).getId()) {
                                                //index = carrinho.getProdutos().indexOf(pro);  
                                                if (carrinho.getProdutos().get(i).getQuantidadeEstoque() < qtd) {
                                                    limiteEstoque = "Erro! Quantidade no estoque é de apenas " + carrinho.getProdutos().get(i).getQuantidadeEstoque();
                                                } else {
                                                    carrinho.getProdutos().get(i).setQuantidadeProdutoCarrinho(qtd);
                                                    limiteEstoque = "";
                                                }
                                                session.setAttribute("carrinho", carrinho);
                                                carrinho = (Carrinho) session.getAttribute("carrinho");
                                            }

                                        }
                                    }
                                }

                            %>
                            <!-- SHOPPING BAG -->
                            <div class="shopping_bag">
                                <a class="shopping_bag_btn" href="javascript:void(0);" ><i class="fa fa-shopping-cart"></i><p>Carrinho</p><span><%out.print(carrinho.getProdutos().size());%></span></a>
                                <div class="cart">
                                    <ul class="cart-items">
                                        <%
                                            for (Produto pro : carrinho.getProdutos()) {
                                                out.print("<li class='clearfix'>");
                                                out.print("<img class='cart_item_product' src='" + request.getContextPath() + "/" + pro.getEndereco_imagem() + "'/>");
                                                out.print(pro.getQuantidadeProdutoCarrinho() + " x " + pro.getNome());
                                                out.print("<span class='cart_item_price'> R$" + pro.getPreco() + "</span>");
                                                out.print("</li>");
                                                total += pro.getPreco() * pro.getQuantidadeProdutoCarrinho();
                                            }
                                        %>
                                    </ul>
                                    <div class="cart_total">
                                        <div class="clearfix"><span class="cart_subtotal">Total: <b><% out.print("R$" + total);%></b></span></div>
                                        <a class="btn active" href="${pageContext.request.contextPath}/logado/carrinho-compra.jsp">Ir ao Carrinho</a>
                                    </div>
                                </div>
                            </div>
                            <%
                                }
                            %>
                            <!-- //SHOPPING BAG -->


                            <!-- MENU -->
                            <ul class="navmenu center">
                                <li>
                                    <a href="${pageContext.request.contextPath}/index.jsp" >Home</a>
                                </li>
                                <%
                                    Resultado resultadoCategorias = (Resultado) session.getAttribute("categorias");
                                    Categoria catg;
                                    SubCategoria sub;
                                    for (EntidadeDominio entity : resultadoCategorias.getEntidades()) {
                                        catg = (Categoria) entity;
                                %>
                                <li class='sub-menu'>
                                    <form name='categoria<%out.print(catg.getId());%>' action='${pageContext.request.contextPath}/VisualizarEstoque' method='POST' hidden>
                                        <input type='hidden' id='idCategoria' name='idCategoria' value='<% out.print(catg.getId());%>'/>
                                        <input type='hidden' id='OPERACAO' name='OPERACAO' value='VISUALIZAR'/>
                                        <input type='hidden' id='PAGINA' name='PAGINA' value='CATALOGO'/>                                        
                                    </form>
                                    <a href='javascript:categoria<%out.print(catg.getId());%>.submit();'>
                                        <%out.print(catg.getNome()); %>
                                    </a>
                                    <%
                                        if (catg.getSubCategorias().size() == 0) {
                                            out.println("</li>");
                                        } else {
                                    %>
                                    <ul class='mega_menu megamenu_col1 clearfix'>
                                        <li class='col'>
                                            <ol>
                                                <%
                                                    for (EntidadeDominio entitySub : catg.getSubCategorias()) {
                                                        sub = (SubCategoria) entitySub;
                                                %>
                                                <li>
                                                    <form name='sub<% out.print(sub.getId());%>' action='${pageContext.request.contextPath}/VisualizarEstoque' method='POST' hidden>
                                                        <input type='hidden' id='OPERACAO' name='OPERACAO' value='VISUALIZAR'/>
                                                        <input type='hidden' id='PAGINA' name='PAGINA' value='CATALOGO'/>
                                                        <input type='hidden' id='idSubCategoria' name='idSubCategoria' value='<%out.print(sub.getId());%>'/>
                                                    </form>
                                                    <a href='javascript:sub<% out.print(sub.getId());%>.submit();'><%out.print(sub.getNome());%></a>
                                                </li>
                                                <%}%>
                                            </ol>
                                        </li>
                                    </ul>
                                    <%}%>
                                </li>
                                <%
                                    }
                                %>                                                                
                            </ul><!-- //MENU -->
                        </div><!-- //MENU BLOCK -->
                    </div><!-- //CONTAINER -->
                </header><!-- //HEADER -->


                <!-- BREADCRUMBS -->
                <section class="breadcrumb parallax margbot30"></section>
                <!-- //BREADCRUMBS -->


                <!-- PAGE HEADER -->
                <section class="page_header">

                    <!-- CONTAINER -->
                    <div class="container">
                        <h3 class="pull-left"><b>Carrinho</b></h3>
                    </div><!-- //CONTAINER -->
                </section><!-- //PAGE HEADER -->


                <!-- SHOPPING BAG BLOCK -->
                <section class="shopping_bag_block">

                    <!-- CONTAINER -->
                    <div class="container">

                        <!-- ROW -->
                        <div class="row">

                            <!-- CART TABLE -->
                            <div class="col-lg-9 col-md-9 padbot40">

                                <table class="shop_table">
                                    <thead>
                                        <tr>
                                            <th class="product-thumbnail" ></th>
                                            <th class="product-name" style="text-align: center;">Item</th>
                                            <th class="product-price" style="text-align: center;">Preço</th>
                                            <th class="product-quantity" style="text-align: center;">Quantidade</th>
                                            <th class="product-subtotal" style="text-align: center;">Total</th>
                                            <th class="product-remove"></th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                        <%
                                            int qtdTotalItens = 0;
                                            if (carrinho.getProdutos().isEmpty()) {
                                                out.print("<tr class='cart_item'>");
                                                out.print("<td></td>");
                                                out.print("<td class='product-name'><a>Seu carrinho está vazio</a></td>");
                                                out.print("<td class='product-price'></td>");
                                                //out.print("<td class='product-quantity'><select class='basic'><option>"+pro.getQuantidadeProdutoCarrinho()+"</option> </select></td>");
                                                out.print("<td></td>");
                                                out.print("<td class='product-subtotal'></td>");
                                                out.print("<td class='product-remove'></td>");
                                                out.print("</tr>");

                                            } else {
                                                for (Produto pro : carrinho.getProdutos()) {
                                                    out.print("<tr class='cart_item'>");
                                                    out.print("<td class='product-thumbnail'><img src='" + request.getContextPath()
                                                            + "/" + pro.getEndereco_imagem() + "'  width='100px' /></td>");
                                                    out.print("<td class='product-name'><a>" + pro.getNome() + "</a></td>");
                                                    out.print("<td class='product-price'>R$" + pro.getPreco() + "</td>");
                                                    out.print("<td>");
                                                    out.print("<form name='atualizar' action='carrinho-compra.jsp' method='POST'>");
                                                    out.print("<input type='hidden' name='OPERACAO' id='OPERACAO' value='ATUALIZAR'/>");
                                                    out.print("<input type='text' class='input' name='txtQtd' value='" + pro.getQuantidadeProdutoCarrinho() + "' style='width:50px; text-align: center;' />");
                                                    out.print("<input type='hidden' name='idProduto' id='idProduto' value='" + pro.getId() + "'/>");
                                                    out.print("<a href='javascript:atualizar.submit()'><b>Atualizar</b></a>");
                                                    out.print("</form>");
                                                    out.print("</td>");
                                                    out.print("<td class='product-subtotal'>R$" + pro.getPreco() * pro.getQuantidadeProdutoCarrinho() + "</td>");
                                                    out.print("<td class='product-remove'>");
                                                    out.print("<form name='remover' action='carrinho-compra.jsp' method='POST'>");
                                                    out.print("<input type='hidden' name='OPERACAO' id='OPERACAO' value='REMOVER'/>");
                                                    out.print("<input type='hidden' name='idProduto' id='idProduto' value='" + pro.getId() + "'/>");
                                                    out.print("<a href='javascript:remover.submit()'><span>Remover</span> <i>X</i></a>");
                                                    out.print("</form>");
                                                    out.print("</td>");
                                                    out.print("</tr>");
                                                    qtdTotalItens += pro.getQuantidadeProdutoCarrinho();
                                                }
                                            }
                                        %>                                       
                                    </tbody>
                                </table>     
                                <%
                                    if (!limiteEstoque.equals("")) {
                                %>
                                <div class="alert alert-danger">
                                    <strong><%out.print(limiteEstoque);%></strong>
                                </div>
                                <%
                                    }
                                %>
                            </div><!-- //CART TABLE -->


                            <!-- SIDEBAR -->
                            <div id="sidebar" class="col-lg-3 col-md-3 padbot50">

                                <!-- BAG TOTALS -->
                                <div class="sidepanel widget_bag_totals your_order_block">
                                    <h3>Seu Pedido</h3>
                                    <table class="bag_total">
                                        <tr class="cart-subtotal clearfix">
                                            <th>Total de Itens</th>
                                            <td><%out.print(qtdTotalItens);%></td>
                                        </tr>                                        
                                        <tr class="total clearfix">
                                            <th>Total</th>
                                            <td><%out.print("R$" + total);%></td>
                                        </tr>
                                    </table>                                    
                                    <br>
                                    <br>
                                    <%
                                        if (carrinho.getProdutos().size() == 0) {
                                            out.print("<a class='btn active' href='" + request.getContextPath() + "/index.jsp' >Selecionar Produtos</a>");
                                        } else {
                                            out.print("<a class='btn active' href='finalizar1.jsp' >Prosseguir</a>");
                                            out.print("<a class='btn inactive' href='" + request.getContextPath() + "/index.jsp' >Voltar ao Início</a>");
                                        }
                                    %>

                                </div>
                            </div><!-- //SIDEBAR -->
                        </div><!-- //ROW -->
                    </div><!-- //CONTAINER -->
                </section><!-- //SHOPPING BAG BLOCK -->

                <!-- FOOTER -->
                <footer>
                    <!-- CONTAINER -->
                    <div class="container" data-animated='fadeInUp'>
                        <!-- ROW -->
                        <div class="row">
                            <div class="col-lg-2 col-md-2 col-sm-3 col-xs-6 col-ss-12 padbot30">
                                <h4>Contato</h4>
                                <div class="foot_address"><span>Explosion E-Commerce</span>Rua Zé das Colves, Guiana City</div>
                                <div class="foot_phone"><a href="#" >(11) 4002-8922</a></div>
                                <div class="foot_mail"><a href="#" >explosionecommerce@explosion.com</a></div>
                            </div>

                            <div class="col-lg-2 col-md-2 col-sm-3 col-xs-6 col-ss-12 padbot30">
                                <h4>Information</h4>
                                <ul class="foot_menu">
                                    <li><a href="#" >Sobre Nós</a></li>
                                    <li><a href="javascript:void(0);" >Política de Privacidade</a></li>
                                    <li><a href="contacts.html" >Contato</a></li>
                                </ul>
                            </div>

                            <div class="respond_clear_480"></div>

                            <div class="col-lg-4 col-md-4 col-sm-6 padbot30">
                                <h4>Sobre a Loja</h4>
                                <p>Nós somos uma loja renomada no mercado, vendemos os produtos mais diversificados. Pode confiar!</p>
                            </div>

                            <div class="respond_clear_768"></div>

                            <div class="col-lg-4 col-md-4 padbot30">
                                <h4>Boletim de Notícias</h4>
                                <form class="newsletter_form clearfix" action="javascript:void(0);" method="get">
                                    <input type="text" name="newsletter" value="Informe seu e-mail" onFocus="if (this.value == 'Enter E-mail & Get 10% off')
                                                this.value = '';" onBlur="if (this.value == '')
                                                            this.value = 'Enter E-mail & Get 10% off';" />
                                    <input class="btn newsletter_btn" type="submit" value="SIGN UP">
                                </form>

                                <h4>Nossas Redes Sociais</h4>
                                <div class="social">
                                    <a href="javascript:void(0);" ><i class="fa fa-twitter"></i></a>
                                    <a href="javascript:void(0);" ><i class="fa fa-facebook"></i></a>
                                    <a href="javascript:void(0);" ><i class="fa fa-google-plus"></i></a>
                                    <a href="javascript:void(0);" ><i class="fa fa-pinterest-square"></i></a>
                                    <a href="javascript:void(0);" ><i class="fa fa-tumblr"></i></a>
                                    <a href="javascript:void(0);" ><i class="fa fa-instagram"></i></a>
                                </div>
                            </div>
                        </div><!-- //ROW -->
                    </div><!-- //CONTAINER -->

                    <!-- COPYRIGHT -->
                    <div class="copyright">

                        <!-- CONTAINER -->
                        <div class="container clearfix">
                            <div class="foot_logo"><a href="${pageContext.request.contextPath}/index.jsp" ><img src="${pageContext.request.contextPath}/images/foot_logo.png" alt="" /></a></div>

                            <div class="copyright_inf">
                                <span>Explosion E-Commerce © 1950</span> |                                            
                                <a class="back_top" href="javascript:void(0);" >Voltar ao topo <i class="fa fa-angle-up"></i></a>
                            </div>
                        </div><!-- //CONTAINER -->
                    </div><!-- //COPYRIGHT -->
                </footer><!-- //FOOTER -->
            </div><!-- //PAGE -->
        </div>


        <!-- SCRIPTS -->
        <!--[if IE]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
        <!--[if IE]><html class="ie" lang="en"> <![endif]-->

        <script src="${pageContext.request.contextPath}/js/jquery.min.js" type="text/javascript"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="${pageContext.request.contextPath}/js/jquery.sticky.js" type="text/javascript"></script>
        <script src="${pageContext.request.contextPath}/js/parallax.js" type="text/javascript"></script>
        <script src="${pageContext.request.contextPath}/js/jquery.flexslider-min.js" type="text/javascript"></script>
        <script src="${pageContext.request.contextPath}/js/jquery.jcarousel.js" type="text/javascript"></script>
        <script src="${pageContext.request.contextPath}/js/fancySelect.js"></script>
        <script src="${pageContext.request.contextPath}/js/animate.js" type="text/javascript"></script>
        <script src="${pageContext.request.contextPath}/js/myscript.js" type="text/javascript"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
        <script>
                                                    if (top != self)
                                                        top.location.replace(self.location.href);
        </script>

    </body>
</html>
