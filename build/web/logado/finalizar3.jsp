
<%@page import="br.com.ecomerce.dominio.EntidadeDominio"%>
<%@page import="br.com.ecomerce.dominio.SubCategoria"%>
<%@page import="br.com.ecomerce.dominio.Categoria"%>
<%@page import="br.com.ecomerce.dominio.Resultado"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="br.com.ecomerce.dominio.Pedido"%>
<%@page import="br.com.ecomerce.dominio.MetodoEntrega"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="br.com.ecomerce.dominio.CartaoCredito"%>
<%@page import="br.com.ecomerce.dominio.Produto"%>
<%@page import="br.com.ecomerce.dominio.Carrinho"%>
<%@page import="java.util.ArrayList"%>
<%@page import="br.com.ecomerce.dominio.Cliente"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>

        <meta charset="utf-8">
        <title>Explosion | E-Commerce de Eletrônicos</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">

        <link rel="shortcut icon" href="${pageContext.request.contextPath}/images/favicon.ico">

        <!-- CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/css/flexslider.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/css/fancySelect.css" rel="stylesheet" media="screen, projection" />
        <link href="${pageContext.request.contextPath}/css/animate.css" rel="stylesheet" type="text/css" media="all" />
        <link href="${pageContext.request.contextPath}/css/style.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/css/ion.rangeSlider.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/css/ion.rangeSlider.min.css" rel="stylesheet" type="text/css" />
        <!-- FONTS -->
        <link href='http://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
        <link href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">

    </head>
    <body>

        <!-- PRELOADER -->
        <div id="preloader"><img src="${pageContext.request.contextPath}/images/preloader.gif" alt="" /></div>
        <!-- //PRELOADER -->
        <div class="preloader_hide">

            <!-- PAGE -->
            <div id="page">

                <!-- HEADER -->
                <header>

                    <!-- TOP INFO -->
                    <div class="top_info">

                        <!-- CONTAINER -->
                        <div class="container clearfix">
                            <ul class="secondary_menu">
                                <%
                                    DecimalFormat def = new DecimalFormat("0.##");
                                    Cliente cliente = null;
                                    if (session.getAttribute("cliente") != null) {
                                        cliente = (Cliente) session.getAttribute("cliente");
                                        if (cliente.getAdmin() == 1) {
                                            out.println("<li><a href='" + request.getContextPath() + "/minha-conta-admin.jsp' >Minha Conta</a></li>");
                                            out.println("<li><a>Bem Vindo " + cliente.getNomeCompleto() + "</a></li>");
                                            out.println("<li><a href='" + request.getContextPath() + "/Logout?logout=logout'>Logout</a></li>");
                                        } else {
                                            out.println("<li><a href='" + request.getContextPath() + "/minha-conta.jsp' >Minha Conta</a></li>");
                                            out.println("<li><a>Bem Vindo " + cliente.getNomeCompleto() + "</a></li>");
                                            out.println("<li><a>Carteira: R$" + def.format(cliente.getCarteira()) + "</a></li>");
                                            out.println("<li><a href='" + request.getContextPath() + "/Logout?logout=logout'>Logout</a></li>");
                                        }

                                    } else {
                                        out.println("<li><a href='" + request.getContextPath() + "/login.jsp' >Login</a></li>");
                                        out.println("<li><a href='" + request.getContextPath() + "/cadastro-cliente.jsp' >Registrar</a></li>");
                                    }

                                    Resultado resultado = (Resultado) request.getAttribute("resultado");
                                    MetodoEntrega metodoEntrega = (MetodoEntrega) resultado.getEntidades().get(0);

                                    Pedido pedido = (Pedido) session.getAttribute("pedido");
                                    pedido.setMetodoEntrega(metodoEntrega);
                                    session.setAttribute("pedido", pedido);
                                %>
                            </ul>
                        </div><!-- //CONTAINER -->
                    </div><!-- TOP INFO -->


                    <!-- MENU BLOCK -->
                    <div class="menu_block">

                        <!-- CONTAINER -->
                        <div class="container clearfix">

                            <!-- LOGO -->
                            <div class="logo">
                                <a href="${pageContext.request.contextPath}/index.jsp" ><img src="${pageContext.request.contextPath}/images/logo/explosionV2.png" alt="" /></a>
                            </div><!-- //LOGO -->

                            <!-- SEARCH FORM -->
                            <div class="top_search_form">
                                <a class="top_search_btn" href="javascript:void(0);"><i class="fa fa-search"></i></a>
                                <form action="${pageContext.request.contextPath}/VisualizarProduto" method='POST'>
                                    <input type="text" name="pesquisar" value="Pesquisar" onFocus="if (this.value == 'Pesquisar')
                                                this.value = '';" onBlur="if (this.value == '')
                                                            this.value = 'Pesquisar';"/>
                                    <input type='hidden' id='OPERACAO' name='OPERACAO' value='VISUALIZAR'/>
                                    <input type='hidden' id='PAGINA' name='PAGINA' value='PESQUISA'/>
                                </form>
                            </div><!-- SEARCH FORM -->

                            <%
                                double total = 0;
                                if (null == session.getAttribute("carrinho")) {
                                    ArrayList<Produto> produtos = new ArrayList<Produto>();
                                    Carrinho carrinho = new Carrinho();
                                    carrinho.setProdutos(produtos);
                                    session.setAttribute("carrinho", carrinho);
                            %>
                            <div class="shopping_bag">
                                <a class="shopping_bag_btn" href = "javascript:void(0);" > 
                                    <i class="fa fa-shopping-cart"></i > <p>Carrinho</p > <span> <% out.print(carrinho.getProdutos().size());%></span></a>
                                <div class="cart">
                                    <div class="cart_total">
                                        <div class="clearfix">
                                            <span class="cart_subtotal">Total: <b><% out.print("R$" + total);%></b></span>
                                        </div>
                                        <a class="btn active" href="logado/carrinho-compra.jsp">Ir ao Carrinho</a>
                                    </div>
                                </div>
                            </div>
                            <%
                            } else {

                                Carrinho carrinho = (Carrinho) session.getAttribute("carrinho");

                            %>
                            <!-- SHOPPING BAG -->
                            <div class="shopping_bag">
                                <a class="shopping_bag_btn" href="javascript:void(0);" ><i class="fa fa-shopping-cart"></i><p>Carrinho</p><span><%out.print(carrinho.getProdutos().size());%></span></a>
                                <div class="cart">
                                    <ul class="cart-items">
                                        <%
                                            for (Produto pro : carrinho.getProdutos()) {
                                                out.print("<li class='clearfix'>");
                                                out.print("<img class='cart_item_product' src='" + request.getContextPath() + "/" + pro.getEndereco_imagem() + "'/>");
                                                out.print(pro.getQuantidadeProdutoCarrinho() + " x " + pro.getNome());
                                                out.print("<span class='cart_item_price'> R$" + pro.getPreco() + "</span>");
                                                out.print("</li>");
                                                total += pro.getPreco() * pro.getQuantidadeProdutoCarrinho();
                                            }
                                        %>
                                    </ul>
                                    <div class="cart_total">
                                        <div class="clearfix"><span class="cart_subtotal">Total: <b><% out.print("R$" + total);%></b></span></div>
                                        <a class="btn active" href="${pageContext.request.contextPath}/logado/carrinho-compra.jsp">Ir ao Carrinho</a>
                                    </div>
                                </div>
                            </div>
                            <%
                                    pedido.setValorTotal(total + metodoEntrega.getValor());
                                }
                            %>

                            <!-- MENU -->
                            <ul class="navmenu center">
                                <li>
                                    <a href="${pageContext.request.contextPath}/index.jsp" >Home</a>
                                </li>
                                <%
                                    Resultado resultadoCategorias = (Resultado) session.getAttribute("categorias");
                                    Categoria catg;
                                    SubCategoria sub;
                                    for (EntidadeDominio entity : resultadoCategorias.getEntidades()) {
                                        catg = (Categoria) entity;
                                %>
                                <li class='sub-menu'>
                                    <form name='categoria<%out.print(catg.getId());%>' action='${pageContext.request.contextPath}/VisualizarEstoque' method='POST' hidden>
                                        <input type='hidden' id='idCategoria' name='idCategoria' value='<% out.print(catg.getId());%>'/>
                                        <input type='hidden' id='OPERACAO' name='OPERACAO' value='VISUALIZAR'/>
                                        <input type='hidden' id='PAGINA' name='PAGINA' value='CATALOGO'/>                                        
                                    </form>
                                    <a href='javascript:categoria<%out.print(catg.getId());%>.submit();'>
                                        <%out.print(catg.getNome()); %>
                                    </a>
                                    <%
                                        if (catg.getSubCategorias().size() == 0) {
                                            out.println("</li>");
                                        } else {
                                    %>
                                    <ul class='mega_menu megamenu_col1 clearfix'>
                                        <li class='col'>
                                            <ol>
                                                <%
                                                    for (EntidadeDominio entitySub : catg.getSubCategorias()) {
                                                        sub = (SubCategoria) entitySub;
                                                %>
                                                <li>
                                                    <form name='sub<% out.print(sub.getId());%>' action='${pageContext.request.contextPath}/VisualizarEstoque' method='POST' hidden>
                                                        <input type='hidden' id='OPERACAO' name='OPERACAO' value='VISUALIZAR'/>
                                                        <input type='hidden' id='PAGINA' name='PAGINA' value='CATALOGO'/>
                                                        <input type='hidden' id='idSubCategoria' name='idSubCategoria' value='<%out.print(sub.getId());%>'/>
                                                    </form>
                                                    <a href='javascript:sub<% out.print(sub.getId());%>.submit();'><%out.print(sub.getNome());%></a>
                                                </li>
                                                <%}%>
                                            </ol>
                                        </li>
                                    </ul>
                                    <%}%>
                                </li>
                                <%
                                    }
                                %>                                                                
                            </ul><!-- //MENU -->
                        </div><!-- //CONTAINER -->
                    </div><!-- //MENU BLOCK -->
                </header><!-- //HEADER -->


                <!-- BREADCRUMBS -->
                <section class="breadcrumb parallax margbot30"></section>
                <!-- //BREADCRUMBS -->



                <!-- PAGE HEADER -->
                <section class="page_header">

                    <!-- CONTAINER -->
                    <div class="container border0 margbot0">
                        <h3 class="pull-left"><b>Checkout</b></h3>

                        <div class="pull-right">
                            <a href="carrinho-compra.jsp" >Voltar ao Carrinho<i class="fa fa-angle-right"></i></a>
                        </div>
                    </div><!-- //CONTAINER -->
                </section><!-- //PAGE HEADER -->


                <!-- CHECKOUT PAGE -->
                <section class="checkout_page">

                    <!-- CONTAINER -->
                    <div class="container" >

                        <!-- CHECKOUT BLOCK -->
                        <div class="checkout_block">
                            <ul class="checkout_nav">
                                <li class="done_step2">1. Confirmar Endereço</li>
                                <li class="done_step">2. Método de Entrega</li>
                                <li class="active_step">3. Forma de Pagamento</li>
                                <li class="last">4. Confirmar Compra</li>
                            </ul>
                            <div class="checkout_payment clearfix" style="text-align: center;">
                                <div class="col-xs-3">
                                    <input type="radio" data-div="div1" id="cartaoExistente" name="radioButtons"  value="1" hidden checked>
                                    <label for="cartaoExistente">
                                        Selecionar Cartão Existente
                                    </label> 
                                </div>
                                <div class="col-xs-3">
                                    <input type="radio" data-div="div2" id="doisCartoesExistente" name="radioButtons" value="2" hidden>
                                    <label for="doisCartoesExistente">
                                        Pagamento com dois cartões
                                    </label> 
                                </div>
                                <div class="col-xs-3">
                                    <input type="radio" data-div="div3" id="cartaoNovo" name="radioButtons" value="3" hidden>
                                    <label for="cartaoNovo">
                                        Cadastrar Novo Cartão
                                    </label> 
                                </div>
                                <div class="col-xs-3">
                                    <input type="radio" data-div="div4" id="boleto" name="radioButtons" value="4" hidden>
                                    <label for="boleto">
                                        Pagamento com Boleto
                                    </label> 
                                </div>
                            </div>
                            <div id="div1" class="div-radiobox">
                                <form class="checkout_form clearfix " id="form-cartao" action="${pageContext.request.contextPath}/logado/finalizar4.jsp" method="POST" >                                                                        
                                    <%
                                        int cartaoContadorUnico = 1;
                                        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                                        DecimalFormat df = new DecimalFormat("0.##");
                                        if (cliente.getCartoesCredito().size() > 0) {
                                            for (CartaoCredito cartao : cliente.getCartoesCredito()) {

                                    %>

                                    <input type="radio" id="radioCartao<% out.print(cartaoContadorUnico); %>" 
                                           name="radioCartao" value="<% out.print(cartao.getId()); %>" 
                                           onclick="radioFunctionHidden('<%out.print("radio" + cartaoContadorUnico);%>')" 
                                           <% if (cartaoContadorUnico == 1) {
                                                   out.print("checked");
                                               } %> hidden>
                                    <label for="radioCartao<%out.print(cartaoContadorUnico); %>">
                                        Número Cartão: <% out.print(cartao.getNumeroCartao()); %>
                                        Código: <% out.print(cartao.getCodigoSeguranca()); %>
                                        Data Validade: <% out.print(cartao.getMes() + "/" + cartao.getAno()); %>
                                        Nome: <% out.print(cartao.getNomeImpresso()); %>
                                    </label>
                                    <div id="<%out.print("radio" + cartaoContadorUnico);%>" 
                                         <% if (cartaoContadorUnico != 1) {
                                                 out.print("style='display:none;'");
                                             }%>>
                                        <select class="basic" name="<%out.print("qtdParcelas" + cartao.getId());%>">
                                            <%
                                                for (int i = 1; i <= 12; i++) {
                                            %>
                                            <option value="<%out.print(i);%>"><%out.print(i);%> x R$<%out.print(df.format(pedido.getValorTotal() / i));%></option>
                                            <%}%>
                                        </select>                                 

                                    </div>
                                    <br>
                                    <%
                                            cartaoContadorUnico++;
                                        }
                                    %>
                                    <input type="hidden" id="ultimoRadio" value="radio1"/>
                                    <input type="hidden" id="TIPOPAGAMENTO" name="TIPOPAGAMENTO" value="UMCARTAO"/>
                                    <div class="col-xs-12">
                                        <input type="submit" class="btn-sm" value="CONTINUAR"/>
                                    </div>
                                    <%
                                    } else {
                                    %>
                                    <br>
                                    <h1> Você não possui cartões cadastrados! </h1>
                                    <%
                                        }
                                    %>

                                </form>
                            </div>
                            <div id="div2" class="div-radiobox" style="display:none;" >
                                <form class="checkout_form clearfix " action="${pageContext.request.contextPath}/logado/finalizar4.jsp" method="POST">                                    
                                    <label>Cartões Pré-Cadastrados</label>
                                    <%
                                        int cartaoContador = 1;
                                        if (cliente.getCartoesCredito().size() > 0) {

                                            for (CartaoCredito cartao : cliente.getCartoesCredito()) {
                                    %>

                                    <input class="limited" type="checkbox" id="checkCartao<% out.print(cartaoContador); %>" 
                                           name="checkboxCartao" value="<% out.print(cartao.getId()); %>" 
                                           onclick="verificaChecks('divRange')" hidden />
                                    <label for="checkCartao<% out.print(cartaoContador); %>">
                                        Número Cartão: <% out.print(cartao.getNumeroCartao()); %>
                                        Código: <% out.print(cartao.getCodigoSeguranca()); %>
                                        Data Validade: <% out.print(cartao.getMes() + "/" + cartao.getAno()); %>
                                        Nome: <% out.print(cartao.getNomeImpresso()); %>
                                    </label>                                    
                                    <%
                                            cartaoContador++;
                                        }

                                    %>   
                                    <div id="divRange" style="display:none">
                                        <input  type="text" class="js-range-slider" name="my_range" value=""
                                                data-type="double"
                                                data-min="0"
                                                data-max="<%out.print(pedido.getValorTotal());%>"
                                                data-from="0"
                                                data-to="<%out.print(pedido.getValorTotal() / 2);%>"
                                                data-grid="true"
                                                />
                                        
                                        <h4><b>Quantidade de Parcelas Cartão 1</b></h4>
                                        <div class="col-xs-2">
                                            <select class="form-control" id="parcelas1" name="qtdParcelas1">
                                                <%
                                                    for (int i = 1; i <= 12; i++) {
                                                        out.println("<option value='" + i + "'>" + i + " x " + (df.format((pedido.getValorTotal()/2) / i)) + "</option>");
                                                    }
                                                %>
                                            </select>
                                        </div>
                                        </br>
                                        </br>

                                        <input  type="text" class="js-range-slider" name="my_range2" value=""
                                                data-type="double"
                                                data-min="0"
                                                data-max="<%out.print(pedido.getValorTotal());%>"
                                                data-from="0"
                                                data-to="<%out.print(pedido.getValorTotal() / 2);%>"
                                                data-grid="true"
                                                />
                                        
                                        <h4><b>Quantidade de Parcelas Cartão 2</b></h4>
                                        <div class="col-xs-2">
                                            <select class="form-control" id="parcelas2" name="qtdParcelas2">
                                                <%
                                                    for (int i = 1; i <= 12; i++) {
                                                        out.println("<option value='" + i + "'>" + i + " x " + (df.format((pedido.getValorTotal()/2) / i)) + "</option>");
                                                    }
                                                %>
                                            </select>
                                        </div>
                                    </div>
                                    <input type="hidden" id="valorTotal" name="valorTotal" value="<%out.print(pedido.getValorTotal());%>"/>
                                    <input type="hidden" id="TIPOPAGAMENTO" name="TIPOPAGAMENTO" value="DOISCARTOES"/>
                                    <div class="col-xs-12">
                                        <input type="submit" class="btn-sm" value="CONTINUAR"/>
                                    </div>
                                    <%
                                    } else {
                                    %>
                                    <br>
                                    <h1> Você não possui cartões cadastrados! </h1>
                                    <%
                                        }
                                    %>
                                </form>
                            </div>

                            <div id="div3" class="div-radiobox" style="display:none">                                    
                                <form class="checkout_form clearfix " id="form-cadastro" action="${pageContext.request.contextPath}/SalvarCartao" method="POST">

                                    <div class="col-xs-10">
                                        <h2>Cadastrar Novo Cartão</h2>
                                    </div>

                                    <div class="col-xs-3">
                                        <label>Número do Cartão</label>
                                        <input type="text" class="input" id="txtNumeroCartao" name="txtNumeroCartao" placeholder="Número do Cartão" maxlength="16"/>
                                    </div>

                                    <div class="col-xs-1">
                                        <label>Código</label>
                                        <input type="text" class="input" id="txtCodigoSegurancaCartao" name="txtCodigoSegurancaCartao" placeholder="Código"/>
                                    </div>                                    
                                    <div class="col-xs-3">
                                        <label>Nome Impresso</label>
                                        <input type="text" class="input" id="txtNomeImpressoCartao" name="txtNomeImpressoCartao" placeholder="Nome completo no cartão"/>
                                    </div>

                                    <div class="col-xs-2">
                                        <label>Mês</label>
                                        <select class="basic" id="selectBandeiraCartao" name="selectMesCartao">                                            
                                            <option value="1">Janeiro</option>
                                            <option value="2">Fevereiro</option>
                                            <option value="3">Março</option>
                                            <option value="4">Abril</option>
                                            <option value="5">Maio</option>
                                            <option value="6">Junho</option>
                                            <option value="7">Julho</option>
                                            <option value="8">Agosto</option>
                                            <option value="9">Setembro</option>
                                            <option value="10">Outubro</option>
                                            <option value="11">Novembro</option>
                                            <option value="12">Dezembro</option>
                                        </select>
                                    </div>

                                    <div class="col-xs-2">
                                        <label>Ano</label>
                                        <select class="basic" id="selectBandeiraCartao" name="selectAnoCartao">                                            
                                            <option value="2019">2019</option>
                                            <option value="2020">2020</option>
                                            <option value="2021">2021</option>
                                            <option value="2022">2022</option>                                            
                                        </select>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class ="col-xs-2">
                                        <label>Bandeira</label>
                                        <select class="basic" id="selectBandeiraCartao" name="selectBandeiraCartao">                                            
                                            <option>Visa</option>
                                            <option>Master Card</option>
                                            <option>PayPal</option>
                                        </select>
                                    </div>
                                    <div class ="col-xs-2">
                                        <label>Quantidade de Parcelas</label>
                                        <select class="basic" name="qtdParcelas">
                                            <%
                                                for (int i = 1; i <= 12; i++) {
                                            %>
                                            <option value="<%out.print(i);%>"><%out.print(i);%> x R$<%out.print(df.format(pedido.getValorTotal() / i));%></option>
                                            <%}%>
                                        </select>    
                                    </div>
                                    <input type="hidden" id="idCliente" name="idCliente" value="<%out.print(cliente.getId());%>"/>
                                    <input type="hidden" id="OPERACAO" name="OPERACAO" value="SALVAR"/>
                                    <input type="hidden" id="PAGINA" name="PAGINA" value="FINALIZARCOMPRA"/>
                                    <input type="hidden" id="TIPOPAGAMENTO" name="TIPOPAGAMENTO" value="NOVO"/>
                                    <div class="col-xs-12">                                        
                                        <input type="submit" class="btn-sm" value="CONTINUAR"/>
                                    </div>
                                </form>
                            </div>                           
                            <div id="div4" class="div-radiobox" style="display:none">

                                <form class="checkout_form clearfix " id="form-boleto" action="${pageContext.request.contextPath}/SalvarBoleto" method="POST">                                                                    
                                    <div class="col-xs-12" style="text-align: center;">
                                        <img class="img" id="imagemBoleto" src="${pageContext.request.contextPath}/images/cartao/boleto.png" alt="" />                                    
                                    </div>
                                    <input type="hidden" id="TIPOPAGAMENTO" name="TIPOPAGAMENTO" value="BOLETO"/>
                                    <input type="hidden" id="OPERACAO" name="OPERACAO" value="SALVAR"/>
                                    <div class="col-xs-12">
                                        <input type="submit" class="btn-sm" value="CONTINUAR"/>
                                    </div>
                                </form>
                            </div>
                        </div><!-- //CHECKOUT BLOCK -->
                    </div><!-- //CONTAINER -->
                </section><!-- //CHECKOUT PAGE -->


                <!-- FOOTER -->
                <footer>

                    <!-- CONTAINER -->
                    <div class="container" data-animated='fadeInUp'>

                        <!-- ROW -->
                        <div class="row">

                            <div class="col-lg-2 col-md-2 col-sm-3 col-xs-6 col-ss-12 padbot30">
                                <h4>Contato</h4>
                                <div class="foot_address"><span>Explosion E-Commerce</span>Rua Zé das Colves, Guiana City</div>
                                <div class="foot_phone"><a href="#" >(11) 4002-8922</a></div>
                                <div class="foot_mail"><a href="#" >explosionecommerce@explosion.com</a></div>
                            </div>

                            <div class="col-lg-2 col-md-2 col-sm-3 col-xs-6 col-ss-12 padbot30">
                                <h4>Information</h4>
                                <ul class="foot_menu">
                                    <li><a href="#" >Sobre Nós</a></li>
                                    <li><a href="javascript:void(0);" >Política de Privacidade</a></li>
                                    <li><a href="contacts.html" >Contato</a></li>
                                </ul>
                            </div>

                            <div class="respond_clear_480"></div>

                            <div class="col-lg-4 col-md-4 col-sm-6 padbot30">
                                <h4>Sobre a Loja</h4>
                                <p>Nós somos uma loja renomada no mercado, vendemos os produtos mais diversificados. Pode confiar!</p>
                            </div>

                            <div class="respond_clear_768"></div>

                            <div class="col-lg-4 col-md-4 padbot30">
                                <h4>Boletim de Notícias</h4>
                                <form class="newsletter_form clearfix" action="javascript:void(0);" method="get">
                                    <input type="text" name="newsletter" value="Informe seu e-mail" onFocus="if (this.value == 'Enter E-mail & Get 10% off')
                                                this.value = '';" onBlur="if (this.value == '')
                                                            this.value = 'Enter E-mail & Get 10% off';" />
                                    <input class="btn newsletter_btn" type="submit" value="SIGN UP">
                                </form>

                                <h4>Nossas Redes Sociais</h4>
                                <div class="social">
                                    <a href="javascript:void(0);" ><i class="fa fa-twitter"></i></a>
                                    <a href="javascript:void(0);" ><i class="fa fa-facebook"></i></a>
                                    <a href="javascript:void(0);" ><i class="fa fa-google-plus"></i></a>
                                    <a href="javascript:void(0);" ><i class="fa fa-pinterest-square"></i></a>
                                    <a href="javascript:void(0);" ><i class="fa fa-tumblr"></i></a>
                                    <a href="javascript:void(0);" ><i class="fa fa-instagram"></i></a>
                                </div>
                            </div>
                        </div><!-- //ROW -->
                    </div><!-- //CONTAINER -->

                    <!-- COPYRIGHT -->
                    <div class="copyright">

                        <!-- CONTAINER -->
                        <div class="container clearfix">
                            <div class="foot_logo"><a href="${pageContext.request.contextPath}/index.jsp" ><img src="images/foot_logo.png" alt="" /></a></div>

                            <div class="copyright_inf">
                                <span>Explosion E-Commerce © 1950</span> |                                            
                                <a class="back_top" href="javascript:void(0);" >Voltar ao topo <i class="fa fa-angle-up"></i></a>
                            </div>
                        </div><!-- //CONTAINER -->
                    </div><!-- //COPYRIGHT -->
                </footer><!-- //FOOTER -->
            </div><!-- //PAGE -->
        </div>

        <!-- TOVAR MODAL CONTENT -->
        <div id="modal-body" class="clearfix">
            <div id="tovar_content"></div>
            <div class="close_block"></div>
        </div><!-- TOVAR MODAL CONTENT -->

        <!-- SCRIPTS -->
        <!--[if IE]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
        <!--[if IE]><html class="ie" lang="en"> <![endif]-->

        <script src="${pageContext.request.contextPath}/js/jquery.min.js" type="text/javascript"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="${pageContext.request.contextPath}/js/jquery.sticky.js" type="text/javascript"></script>
        <script src="${pageContext.request.contextPath}/js/parallax.js" type="text/javascript"></script>
        <script src="${pageContext.request.contextPath}/js/jquery.flexslider-min.js" type="text/javascript"></script>z
        <script src="${pageContext.request.contextPath}/js/jquery.jcarousel.js" type="text/javascript"></script>
        <script src="${pageContext.request.contextPath}/js/jqueryui.custom.min.js" type="text/javascript"></script>
        <script src="${pageContext.request.contextPath}/js/fancySelect.js"></script>
        <script src="${pageContext.request.contextPath}/js/animate.js" type="text/javascript"></script>
        <script src="${pageContext.request.contextPath}/js/myscript.js" type="text/javascript"></script>
        <script src="${pageContext.request.contextPath}/js/ion.rangeSlider.js" type="text/javascript"></script>
        <script src="${pageContext.request.contextPath}/js/ion.rangeSlider.min.js" type="text/javascript"></script>

        <script>

                                                    // Scripts para trabalhar com os checkbox
                                                    $(function () {
                                                        var MAX_SELECT = 2; // Máximo de 'input' selecionados

                                                        $('input.limited').on('change', function () {
                                                            if ($(this).siblings(':checked').length >= MAX_SELECT) {
                                                                this.checked = false;
                                                            }
                                                        });
                                                    });

                                                    function verificaChecks(p) {
                                                        var aChk = document.getElementsByName("checkboxCartao");
                                                        var cont = 0;
                                                        for (var i = 0; i < aChk.length; i++) {
                                                            if (aChk[i].checked == true) {
                                                                cont++;
                                                                console.log(i);
                                                            }
                                                        }
                                                        var x = document.getElementById(p)
                                                        if (cont >= 2) {

                                                            x.style.display = 'block';

                                                        } else {
                                                            x.style.display = 'none';

                                                        }
                                                    }

                                                    function myFunction(p) {
                                                        var x = document.getElementById(p);
                                                        if (x.style.display == 'none') {
                                                            x.style.display = 'block';
                                                        } else {
                                                            x.style.display = 'none';
                                                        }
                                                    }

                                                    function radioFunctionHidden(p) {
                                                        var x = document.getElementById(p);
                                                        var value = document.getElementById("ultimoRadio");
                                                        if (value.value === "")
                                                        {
                                                            if (x.style.display == 'none') {
                                                                x.style.display = 'block';
                                                                value.value = x.id;
                                                            }
                                                        } else
                                                        {
                                                            if (x.style.display == 'none') {
                                                                x.style.display = 'block';
                                                                var ultimoDiv = document.getElementById(value.value);
                                                                ultimoDiv.style.display = 'none';
                                                                value.value = x.id;
                                                            }
                                                        }
                                                    }

                                                    // SCRIPT PARA ESCONDER AS DIVS
                                                    var viewDivs = {
                                                        init: function () {
                                                            var that = this;

                                                            //Elements
                                                            that.inputs = $('input:radio');
                                                            that.divs = $('.div-radiobox');

                                                            //Events
                                                            that.inputs.change(function () {
                                                                that.render();
                                                            });
                                                        },

                                                        render: function () {
                                                            //input selecionado
                                                            var idDiv = this.inputs.filter(':checked').attr('data-div');

                                                            this.divs.each(function () {
                                                                var $this = $(this);
                                                                if ($this.attr('id') == idDiv) {
                                                                    $this.show();

                                                                } else {
                                                                    $this.hide();

                                                                }
                                                            })
                                                        }
                                                    }

                                                    viewDivs.init();


                                                    $("input[name='my_range']").ionRangeSlider({
                                                        onStart: function (data) {
                                                            // Realiza algo assim que a instância é criada

                                                            console.log(data.input);        // jQuery-link to inpuz
                                                            console.log(data.slider);       // jQuery-link to range sliders container
                                                            console.log(data.min);          // MIN value
                                                            console.log(data.max);          // MAX values
                                                            console.log(data.from);         // FROM value
                                                            console.log(data.from_percent); // FROM value in percent
                                                            console.log(data.from_value);   // FROM index in values array (if used)
                                                            console.log(data.to);           // TO value
                                                            console.log(data.to_percent);   // TO value in percent
                                                            console.log(data.to_value);     // TO index in values array (if used)
                                                            console.log(data.min_pretty);   // MIN prettified (if used)
                                                            console.log(data.max_pretty);   // MAX prettified (if used)
                                                            console.log(data.from_pretty);  // FROM prettified (if used)
                                                            console.log(data.to_pretty);    // TO prettified (if used)
                                                        },

                                                        onChange: function (data) {
                                                            // Called every time handle position is changed

                                                            console.log(data.from);
                                                        },

                                                        onFinish: function (data) {
                                                            // Called then action is done and mouse is released
                                                            var valorTotal = document.getElementById('valorTotal').value;
                                                            my_range2.update({
                                                                from: 0,
                                                                to: valorTotal - data.to,
                                                            });
                                                            console.log(data.to);

                                                            // PARCELAS PRIMEIRO CARTAO
                                                            var parcelas = document.getElementById('parcelas1');

                                                            for (var i = 1; i <= 12; i++) {
                                                                parcelas.remove(0);
                                                            }


                                                            for (var i = 1; i <= 12; i++) {
                                                                var option = document.createElement("option");
                                                                option.text = (i) + " x " + parseFloat(data.to / (i)).toFixed(2);
                                                                option.value = i;
                                                                parcelas.add(option);
                                                            }


                                                            console.log(parcelas);

                                                            // PARCELAS SEGUNDO CARTAO
                                                            var parcelas2 = document.getElementById('parcelas2');

                                                            for (var i = 1; i <= 12; i++) {
                                                                parcelas2.remove(0);
                                                            }


                                                            for (var i = 1; i <= 12; i++) {
                                                                var option = document.createElement("option");
                                                                option.text = (i) + " x " + parseFloat((valorTotal - data.to) / (i)).toFixed(2);
                                                                option.value = i;
                                                                parcelas2.add(option);
                                                            }


                                                            console.log(parcelas2);
                                                        },

                                                        onUpdate: function (data) {
                                                            // Called then slider is changed using Update public method

                                                            console.log(data.from_percent);
                                                        }
                                                    });

                                                    let my_range = $("input[name='my_range']").data("ionRangeSlider");

                                                    $("input[name='my_range2']").ionRangeSlider({
                                                        onStart: function (data) {
                                                            // Called right after range slider instance initialised

                                                            console.log(data.input);        // jQuery-link to input
                                                            console.log(data.slider);       // jQuery-link to range sliders container
                                                            console.log(data.min);          // MIN value
                                                            console.log(data.max);          // MAX values
                                                            console.log(data.from);         // FROM value
                                                            console.log(data.from_percent); // FROM value in percent
                                                            console.log(data.from_value);   // FROM index in values array (if used)
                                                            console.log(data.to);           // TO value
                                                            console.log(data.to_percent);   // TO value in percent
                                                            console.log(data.to_value);     // TO index in values array (if used)
                                                            console.log(data.min_pretty);   // MIN prettified (if used)
                                                            console.log(data.max_pretty);   // MAX prettified (if used)
                                                            console.log(data.from_pretty);  // FROM prettified (if used)
                                                            console.log(data.to_pretty);    // TO prettified (if used)
                                                        },

                                                        onChange: function (data) {
                                                            // Called every time handle position is changed

                                                            console.log(data.from);
                                                        },

                                                        onFinish: function (data) {
                                                            // Called then action is done and mouse is released
                                                            var valorTotal = document.getElementById('valorTotal').value;
                                                            console.log(data.to);
                                                            my_range.update({
                                                                from: 0,
                                                                to: valorTotal - data.to,
                                                            });


                                                            // PARCELAS SEGUNDO CARTAO
                                                            var parcelas2 = document.getElementById('parcelas2');

                                                            for (var i = 1; i <= 12; i++) {
                                                                parcelas2.remove(0);
                                                            }


                                                            for (var i = 1; i <= 12; i++) {
                                                                var option = document.createElement("option");
                                                                option.text = (i) + " x " + parseFloat(data.to / (i)).toFixed(2);
                                                                option.value = i;
                                                                parcelas2.add(option);
                                                            }

                                                            console.log(parcelas2);

                                                            // PARCELAS PRIMEIRO CARTAO
                                                            var parcelas = document.getElementById('parcelas1');

                                                            for (var i = 1; i <= 12; i++) {
                                                                parcelas.remove(0);
                                                            }


                                                            for (var i = 1; i <= 12; i++) {
                                                                var option = document.createElement("option");
                                                                option.text = (i) + " x " + parseFloat((valorTotal - data.to) / (i)).toFixed(2);
                                                                option.value = i;
                                                                parcelas.add(option);
                                                            }

                                                            console.log(parcelas1);
                                                        },

                                                        onUpdate: function (data) {
                                                            // Called then slider is changed using Update public method

                                                            console.log(data.from_percent);
                                                        }
                                                    });
                                                    let my_range2 = $("input[name='my_range2']").data("ionRangeSlider");

        </script>

    </body>
</html>