<%@page import="br.com.ecomerce.dominio.EntidadeDominio"%>
<%@page import="br.com.ecomerce.dominio.SubCategoria"%>
<%@page import="br.com.ecomerce.dominio.Categoria"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="br.com.ecomerce.dominio.CartaoCreditoPedido"%>
<%@page import="br.com.ecomerce.dominio.Boleto"%>
<%@page import="br.com.ecomerce.dominio.Pedido"%>
<%@page import="br.com.ecomerce.dominio.CartaoCredito"%>
<%@page import="br.com.ecomerce.dominio.Resultado"%>
<%@page import="br.com.ecomerce.dominio.Produto"%>
<%@page import="br.com.ecomerce.dominio.Carrinho"%>
<%@page import="java.util.ArrayList"%>
<%@page import="br.com.ecomerce.dominio.Cliente"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>

        <meta charset="utf-8">
        <title>Explosion | E-Commerce de Eletrônicos</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">

        <link rel="shortcut icon" heref="${pageContext.request.contextPath}/images/favicon.ico">

        <!-- CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/css/flexslider.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/css/fancySelect.css" rel="stylesheet" media="screen, projection" />
        <link href="${pageContext.request.contextPath}/css/animate.css" rel="stylesheet" type="text/css" media="all" />
        <link href="${pageContext.request.contextPath}/css/style.css" rel="stylesheet" type="text/css" />

        <!-- FONTS -->
        <link href='http://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
        <link href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">

    </head>
    <body>

        <!-- PRELOADER -->
        <div id="preloader"><img src="${pageContext.request.contextPath}/images/preloader.gif" alt="" /></div>
        <!-- //PRELOADER -->
        <div class="preloader_hide">

            <!-- PAGE -->
            <div id="page">

                <!-- HEADER -->
                <header>

                    <!-- TOP INFO -->
                    <div class="top_info">

                        <!-- CONTAINER -->
                        <div class="container clearfix">
                            <ul class="secondary_menu">
                                <%
                                    DecimalFormat def = new DecimalFormat("0.##");
                                    Cliente cliente = null;
                                    if (session.getAttribute("cliente") != null) {
                                        cliente = (Cliente) session.getAttribute("cliente");
                                        if (cliente.getAdmin() == 1) {
                                            out.println("<li><a href='" + request.getContextPath() + "/minha-conta-admin.jsp' >Minha Conta</a></li>");
                                            out.println("<li><a>Bem Vindo " + cliente.getNomeCompleto() + "</a></li>");
                                            out.println("<li><a href='" + request.getContextPath() + "/Logout?logout=logout'>Logout</a></li>");
                                        } else {
                                            out.println("<li><a href='" + request.getContextPath() + "/minha-conta.jsp' >Minha Conta</a></li>");
                                            out.println("<li><a>Bem Vindo " + cliente.getNomeCompleto() + "</a></li>");
                                            out.println("<li><a>Carteira: R$" + def.format(cliente.getCarteira()) + "</a></li>");
                                            out.println("<li><a href='" + request.getContextPath() + "/Logout?logout=logout'>Logout</a></li>");
                                        }

                                    } else {
                                        out.println("<li><a href='" + request.getContextPath() + "/login.jsp' >Login</a></li>");
                                        out.println("<li><a href='" + request.getContextPath() + "/cadastro-cliente.jsp' >Registrar</a></li>");
                                    }

                                    Pedido pedido = (Pedido) session.getAttribute("pedido");
                                    ArrayList<CartaoCreditoPedido> cartaoCreditoPedido = new ArrayList<CartaoCreditoPedido>();
                                    if (request.getParameter("TIPOPAGAMENTO").equals("UMCARTAO")) {
                                        String idCartao = request.getParameter("radioCartao");
                                        String quantidadeParcelas = "qtdParcelas" + idCartao;
                                        quantidadeParcelas = request.getParameter(quantidadeParcelas);

                                        for (CartaoCredito crt : cliente.getCartoesCredito()) {

                                            if (Integer.valueOf(idCartao) == crt.getId()) {
                                                crt.setId(Integer.valueOf(idCartao));
                                                cartaoCreditoPedido.add(new CartaoCreditoPedido(crt, Integer.valueOf(quantidadeParcelas), pedido.getValorTotal()));
                                            }
                                        }
                                        pedido.setCartoes(cartaoCreditoPedido);
                                        pedido.setTipoPagamento("CARTAO");

                                    } else if (request.getParameter("TIPOPAGAMENTO").equals("DOISCARTOES")) {
                                        String[] idCartaos = request.getParameterValues("checkboxCartao");

                                        int qtdParcelas1 = Integer.valueOf(request.getParameter("qtdParcelas1"));
                                        int qtdParcelas2 = Integer.valueOf(request.getParameter("qtdParcelas2"));
                                        int contador = 0;
                                        String my_range1 = request.getParameter("my_range");
                                        String my_range2 = request.getParameter("my_range2");
                                        for (int i = 0; i < idCartaos.length; i++) {
                                            for (CartaoCredito crt : cliente.getCartoesCredito()) {
                                                if (Integer.valueOf(idCartaos[i]) == crt.getId()) {
                                                    String[] array = new String[2];
                                                    if (contador == 0) {
                                                        array = my_range1.split(";");
                                                        if (Double.valueOf(array[1]) >= 0) {
                                                            cartaoCreditoPedido.add(new CartaoCreditoPedido(crt, qtdParcelas1, Double.valueOf(array[1])));
                                                        }
                                                    } else {
                                                        array = my_range2.split(";");
                                                        if (Double.valueOf(array[1]) >= 0) {
                                                            cartaoCreditoPedido.add(new CartaoCreditoPedido(crt, qtdParcelas2, Double.valueOf(array[1])));
                                                        }
                                                    }
                                                }
                                            }
                                            contador++;
                                        }
                                        pedido.setCartoes(cartaoCreditoPedido);
                                        pedido.setTipoPagamento("CARTAO");
                                    } else if (request.getParameter("TIPOPAGAMENTO").equals("NOVO")) {
                                        pedido.setTipoPagamento("CARTAO");
                                    } else if (request.getParameter("TIPOPAGAMENTO").equals("BOLETO")) {
                                        pedido.setTipoPagamento("BOLETO");
                                        pedido.setCartoes(new ArrayList<CartaoCreditoPedido>());
                                    }

                                    session.setAttribute("pedido", pedido);

                                %>
                            </ul>
                        </div><!-- //CONTAINER -->
                    </div><!-- TOP INFO -->


                    <!-- MENU BLOCK -->
                    <div class="menu_block">

                        <!-- CONTAINER -->
                        <div class="container clearfix">

                            <!-- LOGO -->
                            <div class="logo">
                                <a href="${pageContext.request.contextPath}/index.jsp" ><img src="${pageContext.request.contextPath}/images/logo/explosionV2.png" alt="" /></a>
                            </div><!-- //LOGO -->

                            <!-- SEARCH FORM -->
                            <div class="top_search_form">
                                <a class="top_search_btn" href="javascript:void(0);"><i class="fa fa-search"></i></a>
                                <form action="${pageContext.request.contextPath}/VisualizarProduto" method='POST'>
                                    <input type="text" name="pesquisar" value="Pesquisar" onFocus="if (this.value == 'Pesquisar')
                                                this.value = '';" onBlur="if (this.value == '')
                                                            this.value = 'Pesquisar';"/>
                                    <input type='hidden' id='OPERACAO' name='OPERACAO' value='VISUALIZAR'/>
                                    <input type='hidden' id='PAGINA' name='PAGINA' value='PESQUISA'/>
                                </form>
                            </div><!-- SEARCH FORM -->

                            <%                                
                                double total = 0;
                                Carrinho carrinho = null;
                                if (null == session.getAttribute("carrinho")) {
                                    ArrayList<Produto> produtos = new ArrayList<Produto>();
                                    carrinho = new Carrinho();
                                    carrinho.setProdutos(produtos);
                                    session.setAttribute("carrinho", carrinho);
                            %>
                            <div class="shopping_bag">
                                <a class="shopping_bag_btn" href = "javascript:void(0);" > 
                                    <i class="fa fa-shopping-cart"></i > <p>Carrinho</p > <span> <% out.print(carrinho.getProdutos().size());%></span></a>
                                <div class="cart">
                                    <div class="cart_total">
                                        <div class="clearfix">
                                            <span class="cart_subtotal">Total: <b><% out.print("R$" + total);%></b></span>
                                        </div>
                                        <a class="btn active" href="logado/carrinho-compra.jsp">Ir ao Carrinho</a>
                                    </div>
                                </div>
                            </div>
                            <%
                            } else {

                                carrinho = (Carrinho) session.getAttribute("carrinho");

                            %>
                            <!-- SHOPPING BAG -->
                            <div class="shopping_bag">
                                <a class="shopping_bag_btn" href="javascript:void(0);" ><i class="fa fa-shopping-cart"></i><p>Carrinho</p><span><%out.print(carrinho.getProdutos().size());%></span></a>
                                <div class="cart">
                                    <ul class="cart-items">
                                        <%
                                            for (Produto pro : carrinho.getProdutos()) {
                                                out.print("<li class='clearfix'>");
                                                out.print("<img class='cart_item_product' src='" + request.getContextPath()+"/"+ pro.getEndereco_imagem() + "'/>");
                                                out.print(pro.getQuantidadeProdutoCarrinho() + " x " + pro.getNome());
                                                out.print("<span class='cart_item_price'> R$" + pro.getPreco() + "</span>");
                                                out.print("</li>");
                                                total += pro.getPreco() * pro.getQuantidadeProdutoCarrinho();
                                            }
                                        %>
                                    </ul>
                                    <div class="cart_total">
                                        <div class="clearfix"><span class="cart_subtotal">Total: <b><% out.print("R$" + total);%></b></span></div>
                                        <a class="btn active" href="${pageContext.request.contextPath}/logado/carrinho-compra.jsp">Ir ao Carrinho</a>
                                    </div>
                                </div>
                            </div>
                            <%
                                }
                            %>

                            <!-- MENU -->
                            <ul class="navmenu center">
                                <li>
                                    <a href="${pageContext.request.contextPath}/index.jsp" >Home</a>
                                </li>
                                <%
                                    Resultado resultadoCategorias = (Resultado) session.getAttribute("categorias");
                                    Categoria catg;
                                    SubCategoria sub;
                                    for (EntidadeDominio entity : resultadoCategorias.getEntidades()) {
                                        catg = (Categoria) entity;
                                %>
                                <li class='sub-menu'>
                                    <form name='categoria<%out.print(catg.getId());%>' action='${pageContext.request.contextPath}/VisualizarEstoque' method='POST' hidden>
                                        <input type='hidden' id='idCategoria' name='idCategoria' value='<% out.print(catg.getId());%>'/>
                                        <input type='hidden' id='OPERACAO' name='OPERACAO' value='VISUALIZAR'/>
                                        <input type='hidden' id='PAGINA' name='PAGINA' value='CATALOGO'/>                                        
                                    </form>
                                    <a href='javascript:categoria<%out.print(catg.getId());%>.submit();'>
                                        <%out.print(catg.getNome()); %>
                                    </a>
                                    <%
                                        if (catg.getSubCategorias().size() == 0) {
                                            out.println("</li>");
                                        } else {
                                    %>
                                    <ul class='mega_menu megamenu_col1 clearfix'>
                                        <li class='col'>
                                            <ol>
                                                <%
                                                    for (EntidadeDominio entitySub : catg.getSubCategorias()) {
                                                        sub = (SubCategoria) entitySub;
                                                %>
                                                <li>
                                                    <form name='sub<% out.print(sub.getId());%>' action='${pageContext.request.contextPath}/VisualizarEstoque' method='POST' hidden>
                                                        <input type='hidden' id='OPERACAO' name='OPERACAO' value='VISUALIZAR'/>
                                                        <input type='hidden' id='PAGINA' name='PAGINA' value='CATALOGO'/>
                                                        <input type='hidden' id='idSubCategoria' name='idSubCategoria' value='<%out.print(sub.getId());%>'/>
                                                    </form>
                                                    <a href='javascript:sub<% out.print(sub.getId());%>.submit();'><%out.print(sub.getNome());%></a>
                                                </li>
                                                <%}%>
                                            </ol>
                                        </li>
                                    </ul>
                                    <%}%>
                                </li>
                                <%
                                    }
                                %>                                                                
                            </ul><!-- //MENU -->
                        </div><!-- //CONTAINER -->
                    </div><!-- //MENU BLOCK -->
                </header><!-- //HEADER -->


                <!-- BREADCRUMBS -->
                <section class="breadcrumb parallax margbot30"></section>
                <!-- //BREADCRUMBS -->



                <!-- PAGE HEADER -->
                <section class="page_header">

                    <!-- CONTAINER -->
                    <div class="container border0 margbot0">
                        <h3 class="pull-left"><b>Checkout</b></h3>

                        <div class="pull-right">
                            <a href="carrinho-compra.jsp" >Voltar ao Carrinho<i class="fa fa-angle-right"></i></a>
                        </div>
                    </div><!-- //CONTAINER -->
                </section><!-- //PAGE HEADER -->


                <!-- CHECKOUT PAGE -->
                <section class="checkout_page">

                    <!-- CONTAINER -->
                    <div class="container">

                        <!-- CHECKOUT BLOCK -->
                        <div class="checkout_block">
                            <ul class="checkout_nav">
                                <li class="done_step2">1. Confirmar Endereço</li>
                                <li class="done_step2">2. Método de Entrega</li>
                                <li class="done_step">3. Forma de Pagamento</li>
                                <li class="active_step last">4. Confirmar Compra</li>
                            </ul>
                        </div><!-- //CHECKOUT BLOCK -->

                        <!-- ROW -->
                        <div class="row">
                            <div class="col-lg-9 col-md-9 padbot60">
                                <div class="checkout_confirm_orded clearfix">
                                    <div class="checkout_confirm_orded_bordright clearfix">
                                        <div class="billing_information">
                                            <p class="checkout_title margbot10"><label>Informação de Cobrança</label></p>

                                            <div class="billing_information_content margbot40">
                                                <label> <span><% out.print(
                                                            "Nome: " + cliente.getNomeCompleto()); %></span> </label>
                                                <label> <span><% out.print(
                                                            "CEP: " + pedido.getEndereco().getCep()); %></span></label>                                                
                                                <label> <span><% out.print(
                                                            "Endereço: " + pedido.getEndereco().getLogradouro() + ", "
                                                            + pedido.getEndereco().getNumeroEndereco() + "."); %>
                                                    </span></label>
                                                <label> <span> <% out.print(
                                                            "Bairro: " + pedido.getEndereco().getBairro() + "."); %> </span></label>
                                                <label> <span> <% out.print(
                                                            "Cidade: " + pedido.getEndereco().getCidade() + "."); %> </span></label>
                                                <label> <span> <% out.print(
                                                            "Estado: " + pedido.getEndereco().getSiglaEstado() + "."); %> </span> </label>                                          

                                            </div>
                                        </div>

                                        <div class="payment_delivery">
                                            <p class="checkout_title margbot10"><label>Pagamento e Entrega</label></p>

                                            <p>
                                                <label><span>Pagamento: </span> </label>
                                                <%
                                                    if (pedido.getCartoes()
                                                            .size() == 0) {
                                                        out.println("BOLETO BANCÁRIO");
                                                    } else {
                                                        for (CartaoCreditoPedido crtPedido : pedido.getCartoes()) {
                                                %>
                                                <label>Número do Cartão: 
                                                    <%
                                                                out.println(crtPedido.getCartao().getNumeroCartao()
                                                                        + ", Código de Segurança: " + crtPedido.getCartao().getCodigoSeguranca()
                                                                        + ", Nome Impresso: " + crtPedido.getCartao().getNomeImpresso() + ".");
                                                            }
                                                        }
                                                    %>
                                                </label>
                                            </p>                                                                  

                                            <p><label>Forma de Entrega:</label> <label><% out.print(pedido.getMetodoEntrega().getNome()); %></label></p>                                                        
                                        </div>
                                    </div>

                                    <div class="checkout_confirm_orded_products">
                                        <p class="checkout_title"><label>Produtos</label></p>
                                        <ul class="cart-items">
                                            <%
                                                double produtoPedido;
                                                for (Produto pro : carrinho.getProdutos()) {
                                                    produtoPedido = 0;
                                                    out.print("<li class='clearfix'>");
                                                    out.print("<img class='cart_item_product' src='" + request.getContextPath() + "/" + pro.getEndereco_imagem() + "'/>");
                                                    out.print("<a class='cart_item_title'>" + pro.getQuantidadeProdutoCarrinho() + " x " + pro.getNome() + "</a>");
                                                    produtoPedido = pro.getPreco() * pro.getQuantidadeProdutoCarrinho();
                                                    out.print("<span class='cart_item_price'> R$" + produtoPedido + "</span>");
                                                    out.print("</li>");

                                                }
                                            %>
                                        </ul>                                                            
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-3 col-md-3 padbot60">
                                <!-- BAG TOTALS -->
                                <div class="sidepanel widget_bag_totals your_order_block">
                                    <h3>Seu Pedido</h3>
                                    <table class="bag_total">
                                        <tr class="cart-subtotal clearfix">
                                            <th>Subtotal</th>
                                            <td>R$<% out.print(total); %></td>
                                        </tr>
                                        <tr class="cart-subtotal clearfix">
                                            <th>Entrega</th>
                                            <td>R$<%out.print(pedido.getMetodoEntrega().getValor());%></td>
                                        </tr>
                                        <tr class="total clearfix">
                                            <th>Total</th>
                                            <td>R$ 
                                                <%
                                                    DecimalFormat df = new DecimalFormat("0.##");

                                                    if (pedido.getValorTotal()
                                                            - cliente.getCarteira() < 0) {
                                                        out.print(0);
                                                    } else {
                                                        out.print(df.format(pedido.getValorTotal() - cliente.getCarteira()));
                                                    }
                                                %> 
                                            </td>
                                            Caso tenha saldo na carteira ele é descontado automaticamente da venda.
                                        </tr>
                                    </table>
                                    <form name='finalizar' action='${pageContext.request.contextPath}/SalvarPedido' method='POST'>
                                        <input type="hidden" id="OPERACAO" name="OPERACAO" value="SALVAR" />
                                        <a class="btn active" href="javascript:finalizar.submit();" >Concluir</a>
                                    </form>
                                    <a class="btn inactive" href="finalizar1.jsp" >Voltar ao início</a>
                                </div><!-- //REGISTRATION FORM -->
                            </div><!-- //SIDEBAR -->
                        </div><!-- //ROW -->
                    </div><!-- //CONTAINER -->
                </section><!-- //CHECKOUT PAGE -->


                <!-- FOOTER -->
                <footer>

                    <!-- CONTAINER -->
                    <div class="container" data-animated='fadeInUp'>

                        <!-- ROW -->
                        <div class="row">

                            <div class="col-lg-2 col-md-2 col-sm-3 col-xs-6 col-ss-12 padbot30">
                                <h4>Contato</h4>
                                <div class="foot_address"><span>Explosion E-Commerce</span>Rua Zé das Colves, Guiana City</div>
                                <div class="foot_phone"><a href="#" >(11) 4002-8922</a></div>
                                <div class="foot_mail"><a href="#" >explosionecommerce@explosion.com</a></div>
                            </div>

                            <div class="col-lg-2 col-md-2 col-sm-3 col-xs-6 col-ss-12 padbot30">
                                <h4>Information</h4>
                                <ul class="foot_menu">
                                    <li><a href="#" >Sobre Nós</a></li>
                                    <li><a href="javascript:void(0);" >Política de Privacidade</a></li>
                                    <li><a href="contacts.html" >Contato</a></li>
                                </ul>
                            </div>

                            <div class="respond_clear_480"></div>

                            <div class="col-lg-4 col-md-4 col-sm-6 padbot30">
                                <h4>Sobre a Loja</h4>
                                <p>Nós somos uma loja renomada no mercado, vendemos os produtos mais diversificados. Pode confiar!</p>
                            </div>

                            <div class="respond_clear_768"></div>

                            <div class="col-lg-4 col-md-4 padbot30">
                                <h4>Boletim de Notícias</h4>
                                <form class="newsletter_form clearfix" action="javascript:void(0);" method="get">
                                    <input type="text" name="newsletter" value="Informe seu e-mail" onFocus="if (this.value == 'Enter E-mail & Get 10% off')
                                                this.value = '';" onBlur="if (this.value == '')
                                                            this.value = 'Enter E-mail & Get 10% off';" />
                                    <input class="btn newsletter_btn" type="submit" value="SIGN UP">
                                </form>

                                <h4>Nossas Redes Sociais</h4>
                                <div class="social">
                                    <a href="javascript:void(0);" ><i class="fa fa-twitter"></i></a>
                                    <a href="javascript:void(0);" ><i class="fa fa-facebook"></i></a>
                                    <a href="javascript:void(0);" ><i class="fa fa-google-plus"></i></a>
                                    <a href="javascript:void(0);" ><i class="fa fa-pinterest-square"></i></a>
                                    <a href="javascript:void(0);" ><i class="fa fa-tumblr"></i></a>
                                    <a href="javascript:void(0);" ><i class="fa fa-instagram"></i></a>
                                </div>
                            </div>
                        </div><!-- //ROW -->
                    </div><!-- //CONTAINER -->

                    <!-- COPYRIGHT -->
                    <div class="copyright">

                        <!-- CONTAINER -->
                        <div class="container clearfix">
                            <div class="foot_logo"><a heref="${pageContext.request.contextPath}/index.jsp" ><img src="images/foot_logo.png" alt="" /></a></div>

                            <div class="copyright_inf">
                                <span>Explosion E-Commerce © 1950</span> |                                            
                                <a class="back_top" href="javascript:void(0);" >Voltar ao topo <i class="fa fa-angle-up"></i></a>
                            </div>
                        </div><!-- //CONTAINER -->
                    </div><!-- //COPYRIGHT -->
                </footer><!-- //FOOTER -->
            </div><!-- //PAGE -->
        </div>

        <!-- TOVAR MODAL CONTENT -->
        <div id="modal-body" class="clearfix">
            <div id="tovar_content"></div>
            <div class="close_block"></div>
        </div><!-- TOVAR MODAL CONTENT -->

        <!-- SCRIPTS -->
        <!--[if IE]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
        <!--[if IE]><html class="ie" lang="en"> <![endif]-->

        <script src="${pageContext.request.contextPath}/js/jquery.min.js" type="text/javascript"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="${pageContext.request.contextPath}/js/jquery.sticky.js" type="text/javascript"></script>
        <script src="${pageContext.request.contextPath}/js/parallax.js" type="text/javascript"></script>
        <script src="${pageContext.request.contextPath}/js/jquery.flexslider-min.js" type="text/javascript"></script>
        <script src="${pageContext.request.contextPath}/js/jquery.jcarousel.js" type="text/javascript"></script>
        <script src="${pageContext.request.contextPath}/js/jqueryui.custom.min.js" type="text/javascript"></script>
        <script src="${pageContext.request.contextPath}/js/fancySelect.js"></script>
        <script src="${pageContext.request.contextPath}/js/animate.js" type="text/javascript"></script>
        <script src="${pageContext.request.contextPath}/js/myscript.js" type="text/javascript"></script>

    </body>
</html>