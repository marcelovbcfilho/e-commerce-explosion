<%@page import="br.com.ecomerce.dominio.Produto"%>
<%@page import="java.util.ArrayList"%>
<%@page import="br.com.ecomerce.dominio.Carrinho"%>
<%@page import="br.com.ecomerce.dominio.Cliente"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<html lang="pt">
    <head>

        <meta charset="utf-8">
        <title>Explosion | E-Commerce de Eletrônicos</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">

        <link rel="shortcut icon" href="../images/logo/explosionV1.png">

        <!-- CSS -->
        <link href="../css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="../css/flexslider.css" rel="stylesheet" type="text/css" />
        <link href="../css/fancySelect.css" rel="stylesheet" media="screen, projection" />
        <link href="../css/animate.css" rel="stylesheet" type="text/css" media="all" />
        <link href="../css/style.css" rel="stylesheet" type="text/css" />
        <link href="../css/estilo.css" rel="stylesheet" type="text/css" />

        <!-- FONTS -->
        <link href='http://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
        <link href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">

    </head>
    <body>

        <!-- PRELOADER -->
        <div id="preloader"><img src="../images/preloader.gif" alt="" /></div>
        <!-- //PRELOADER -->
        <div class="preloader_hide">

            <!-- PAGE -->
            <div id="page">

                <!-- HEADER -->
                <header>

                    <!-- TOP INFO -->
                    <div class="top_info">

                        <!-- CONTAINER -->
                        <div class="container clearfix">
                            <ul class="secondary_menu">
                                <%
                                    try {
                                        Cliente cliente = (Cliente) session.getAttribute("cliente");
                                        cliente.getStatus();
                                            out.print("<li><a href='../minha-conta.jsp' >Minha Conta</a></li>");
                                        out.println("<li><a>Bem Vindo " + cliente.getNomeCompleto() + "</a></li>");
                                        out.println("<li><a href='../Logout?logout=logout'>Logout</a></li>");
                                    } catch (Exception e) {
                                        out.print("<li><a href='../login.jsp' >Login</a></li>");
                                        out.println("<li><a href='../cadastro-cliente.jsp' >Registrar</a></li>");
                                    }
                                %>
                            </ul>
                        </div><!-- //CONTAINER -->
                    </div><!-- TOP INFO -->


                    <!-- MENU BLOCK -->
                    <div class="menu_block">

                        <!-- CONTAINER -->
                        <div class="container clearfix">

                            <!-- LOGO -->
                            <div class="logo">
                                <a href="../index.jsp" ><img src="../images/logo/explosionV2.png" alt="" /></a>
                            </div><!-- //LOGO -->


                            <!-- SEARCH FORM -->
                            <div class="top_search_form">
                                <a class="top_search_btn" href="../javascript:void(0);" ><i class="fa fa-search"></i></a>
                                <form method="get" action="#">
                                    <input type="text" name="search" value="Search" onFocus="if (this.value == 'Search')
                                                this.value = '';" onBlur="if (this.value == '')
                                                            this.value = 'Search';" />
                                </form>
                            </div><!-- SEARCH FORM -->


                            <!-- SHOPPING BAG -->
                            <%
                                double total = 0;
                                if (null == session.getAttribute("carrinho")) {
                                    ArrayList<Produto> produtos = new ArrayList<Produto>();
                                    Carrinho carrinho = new Carrinho();
                                    carrinho.setProdutos(produtos);
                                    session.setAttribute("carrinho", carrinho);
                            %>
                            <div class="shopping_bag">
                                <a class="shopping_bag_btn" href = "javascript:void(0);" > 
                                    <i class="fa fa-shopping-cart"></i > <p>Carrinho</p > <span> <% out.print(carrinho.getProdutos().size());%></span></a>
                                <div class="cart">
                                    <div class="cart_total">
                                        <div class="clearfix">
                                            <span class="cart_subtotal">Total: <b><% out.print("R$" + total);%></b></span>
                                        </div>
                                        <a class="btn active" href="logado/carrinho-compra.jsp">Ir ao Carrinho</a>
                                    </div>
                                </div>
                            </div>
                            <%
                            } else {

                                Carrinho carrinho = (Carrinho) session.getAttribute("carrinho");

                            %>
                            <!-- SHOPPING BAG -->
                            <div class="shopping_bag">
                                <a class="shopping_bag_btn" href="javascript:void(0);" ><i class="fa fa-shopping-cart"></i><p>Carrinho</p><span><%out.print(carrinho.getProdutos().size());%></span></a>
                                <div class="cart">
                                    <ul class="cart-items">
                                        <%
                                            for (Produto pro : carrinho.getProdutos()) {
                                                out.print("<li class='clearfix'>");
                                                out.print("<img class='cart_item_product' src='" + pro.getEndereco_imagem() + "'/>");
                                                out.print(pro.getQuantidadeProdutoCarrinho() + " x " + pro.getNome());
                                                out.print("<span class='cart_item_price'> R$" + pro.getPreco() + "</span>");
                                                out.print("</li>");
                                                total += pro.getPreco() * pro.getQuantidadeProdutoCarrinho();
                                            }
                                        %>
                                    </ul>
                                    <div class="cart_total">
                                        <div class="clearfix"><span class="cart_subtotal">Total: <b><% out.print("R$" + total);%></b></span></div>
                                        <a class="btn active" href="logado/carrinho-compra.jsp">Ir ao Carrinho</a>
                                    </div>
                                </div>
                            </div>
                            <%
                                }
                            %>
                            <!-- //SHOPPING BAG -->


                            <!-- MENU -->
                            <ul class="navmenu center">
                                <li>
                                    <a href="../index.jsp" >Home</a>
                                </li>
                                <%
                                    try {
                                        Cliente cliente = (Cliente) session.getAttribute("cliente");
                                        cliente.getStatus();
                                        out.println("<li class='sub-menu'><a href='../minha-conta.jsp' >Minha Conta</a>");
                                        out.println("   <!-- Opções Minha Conta -->");
                                        out.println("   <ul class='mega_menu megamenu_col1 clearfix'>");
                                        out.println("   <li class='col'>");
                                        out.println("   <ol>");
                                        out.println("       <li><a href='../minha-conta.jsp'>Minha Conta</a></li>");
                                        out.println("       <li><a href='../logado/alterar-dados.jsp' class='active'>Alterar Dados</a></li>");
                                        out.println("       <li><a href='../logado/enderecos.jsp'>Endereços</a></li>");
                                        out.println("       <li><a href='../logado/meus-pedidos.jsp'>Meus Pedidos</a></li>");
                                        out.println("       <li><a href='../logado/alterar-senha.jsp'>Alterar Senha</a></li>");
                                        out.println("       <li><a href='../logado/cartoes.jsp'>Cartões</a></li>");
                                        out.println("       <li><a href='#'>Em Construção...</a></li>");
                                        out.println("   </ol>");
                                        out.println("   </li>");
                                        out.println("   </ul><!-- Opções minha conta -->");
                                        out.println("</li>");
                                    } catch (Exception e) {
                                    }
                                %>
                                <li class="sub-menu"><a href="../javascript:void(0);" >Eletrodomésticos</a>
                                    <!-- MEGA MENU -->
                                    <ul class="mega_menu megamenu_col1 clearfix">
                                        <li class="col">
                                            <ol>
                                                <li><a href="../wo#" >Fogão</a></li>
                                                <li><a href="../wo#" >Máquina de Lavar Roupas</a></li>
                                                <li><a href="../wo#" >Micro-ondas</a></li>
                                                <li><a href="../wo#" >Televisão</a></li>
                                            </ol>
                                        </li>
                                    </ul><!-- //MEGA MENU -->
                                </li>
                                <li class="sub-menu"><a href="../javascript:void(0);" >Celulares</a>
                                    <!-- MEGA MENU -->
                                    <ul class="mega_menu megamenu_col2 clearfix">
                                        <li class="col">
                                            <ol>
                                                <li><a href="../#" >LG</a></li>
                                                <li><a href="../#" >Motorola</a></li>
                                            </ol>
                                        </li>
                                        <li class="col">
                                            <ol>
                                                <li><a href="../#" >Nokia</a></li>
                                                <li><a href="../#" >Samsung</a></li>
                                            </ol>
                                        </li>
                                    </ul><!-- //MEGA MENU -->
                                </li>
                                <li class="sub-menu"><a href="../javascript:void(0);" >Informática</a>
                                    <!-- MEGA MENU -->
                                    <ul class="mega_menu megamenu_col1 clearfix">
                                        <li class="col">
                                            <ol>
                                                <li><a href="../wo#" >Placa Mãe</a></li>
                                                <li><a href="../wo#" >Placa de Vídeo</a></li>
                                                <li><a href="../wo#" >Memória RAM</a></li>
                                                <li><a href="../wo#" >Fontes</a></li>
                                                <li><a href="../wo#" >Gabinetes</a></li>
                                            </ol>
                                        </li>
                                    </ul><!-- //MEGA MENU -->
                                </li>
                            </ul><!-- //MENU -->
                        </div><!-- //MENU BLOCK -->
                    </div><!-- //CONTAINER -->
                </header><!-- //HEADER -->

                <!-- BREADCRUMBS -->
                <section class="breadcrumb parallax margbot30"></section>
                <!-- //BREADCRUMBS -->


                <!-- TOVAR DETAILS -->
                <section class="tovar_details padbot70">

                    <!-- CONTAINER -->
                    <div class="container">

                        <!-- ROW -->
                        <div class="row">

                            <!-- SIDEBAR TOVAR DETAILS -->
                            <div class="col-lg-2 col-md-1 sidebar_tovar_details">

                            </div><!-- //SIDEBAR TOVAR DETAILS -->

                            <!-- TOVAR DETAILS WRAPPER -->
                            <div class="col-lg-9 col-md-9 tovar_details_wrapper clearfix">


                                <!-- CLEARFIX -->
                                <div class="clearfix padbot40">
                                    <div class="tovar_view_fotos clearfix">
                                        <div id="slider2" class="flexslider">
                                            <ul class="slides">
                                                <li><a href="../javascript:void(0);" ><img src="../images/produtos/informatica/image1.png" alt="" /></a></li>
                                                <li><a href="../javascript:void(0);" ><img src="../images/produtos/informatica/image1lado.png" alt="" /></a></li>
                                                <li><a href="../javascript:void(0);" ><img src="../images/produtos/informatica/image1costas.png" alt="" /></a></li>
                                            </ul>
                                        </div>
                                        <div id="carousel2" class="flexslider">
                                            <ul class="slides">
                                                <li><a href="../javascript:void(0);" ><img src="../images/produtos/informatica/image1.png" alt="" /></a></li>
                                                <li><a href="../javascript:void(0);" ><img src="../images/produtos/informatica/image1lado.png" alt="" /></a></li>
                                                <li><a href="../javascript:void(0);" ><img src="../images/produtos/informatica/image1costas.png" alt="" /></a></li>
                                            </ul>
                                        </div>
                                    </div>

                                    <div class="tovar_view_description">
                                        <div class="tovar_view_title">Core i5 8° Geração</div>
                                        <br>
                                        <div class="clearfix tovar_brend_price">
                                            <div class="pull-left tovar_view_price">R$850.00</div>
                                        </div>

                                        <div class="col-xs-10 tovar_view_btn">                                            
                                            <a class="add_bag" href="../javascript:void(0);" ><i class="fa fa-shopping-cart"></i>Adicionar no Carrinho</a>
                                        </div>                                        
                                    </div>
                                </div><!-- //CLEARFIX -->

                                <!-- TOVAR INFORMATION -->
                                <div class="tovar_information">
                                    <ul class="tabs clearfix">
                                        <li class="current">Descrição</li>
                                        <li>Informação</li>
                                        <li>Comentários</li>
                                    </ul>
                                    <div class="box visible">
                                        <p> Com esse processador inovador e incrível você desfruta ao máximo o verdadeiro potencial do seu computador e da mais pura velocidade. Maximize o seu desempenho seja trabalhando, jogando, navegando ou assistindo o seu filme preferido, com esse processador você pode tudo!</p>

                                    </div>
                                    <div class="box">
                                        <strong>Características:</strong> <br>
                                        - Marca: Intel<br>
                                        - Modelo: BX80648158400<br>
                                        <strong>Especificações:</strong><br>
                                        <strong>Essenciais:</strong><br>
                                        - Coleção de produtos: 8ª geração de processadores Intel Core i5 <br>
                                        - Codinome: Produtos com denominação anterior Coffee Lake<br>
                                        - Segmento vertical: Desktop<br>
                                        - Número do processador: i5-8400<br>
                                        - Status: Launched<br>
                                        - Data de introdução: Q4'17<br>
                                        - Litografia: 14 nm<br>


                                    </div>
                                    <div class="box">
                                        <ul class="comments">
                                            <li>
                                                <div class="clearfix">
                                                    <p class="pull-left"><strong><a href="../javascript:void(0);" >Leonardo Batista</a></strong></p>
                                                    <span class="date">20-04-2009 20:23</span>                                                    
                                                </div>
                                                <p>Bacana.</p>
                                            </li>
                                            <li>
                                                <div class="clearfix">
                                                    <p class="pull-left"><strong><a href="../javascript:void(0);" >Marcelo Good Village</a></strong></p>
                                                    <span class="date">2019-10-09 20:23</span>
                                                </div>
                                                <p>mUITO BOM RECOMENDO.</p>

                                                <ul>
                                                    <li>
                                                        <p><strong><a href="../javascript:void(0);" >Biggest Batista</a></strong></p>
                                                        <p>Melhores preços.</p>
                                                    </li>
                                                </ul>
                                            </li>
                                        </ul>

                                        <h3>Deixa seu comentário</h3>
                                        <p>Digite um texto com no máximo 200 caracteres</p>
                                        <div class="clearfix">
                                            <textarea id="review-textarea"></textarea>

                                            <input type="submit" class="dark-blue big" value="Confirmar">
                                        </div>
                                    </div>
                                </div><!-- //TOVAR INFORMATION -->
                            </div><!-- //TOVAR DETAILS WRAPPER -->
                        </div><!-- //ROW -->
                    </div><!-- //CONTAINER -->
                </section><!-- //TOVAR DETAILS -->




                <!-- NEW ARRIVALS -->
                <section class="new_arrivals padbot50">

                    <!-- CONTAINER -->
                    <div class="container">
                        <h2>Novidades</h2>

                        <!-- JCAROUSEL -->
                        <div class="jcarousel-wrapper">

                            <!-- NAVIGATION -->
                            <div class="jCarousel_pagination">
                                <a href="../javascript:void(0);" class="jcarousel-control-prev" ><i class="fa fa-angle-left"></i></a>
                                <a href="../javascript:void(0);" class="jcarousel-control-next" ><i class="fa fa-angle-right"></i></a>
                            </div><!-- //NAVIGATION -->

                            <div class="jcarousel" data-appear-top-offset='-100' data-animated='fadeInUp'>
                                <ul>
                                    <li>
                                        <!-- TOVAR -->
                                        <div class="tovar_item_new">
                                            <div class="tovar_img">
                                                <img src="../images/produtos/informatica/image1.png" alt="" />
                                                <div class="open-project-link"><a class="open-project tovar_view" href="i5-8geracao.jsp" data-url="!projects/women/1.html" >Visualizar</a></div>
                                            </div>
                                            <div class="tovar_description clearfix">
                                                <a class="tovar_title" href="../product-page.html" >Core i5 8° Geração</a>
                                                <span class="tovar_price">R$850.00</span>
                                            </div>
                                        </div><!-- //TOVAR -->
                                    </li>
                                    <li>
                                        <!-- TOVAR -->
                                        <div class="tovar_item_new">
                                            <div class="tovar_img">
                                                <img src="../images/produtos/informatica/image6.png" alt="" />
                                                <div class="open-project-link">
                                                    <a class="open-project tovar_view" href="../javascript:void(0);" data-url="!projects/women/1.html" >Visualizar</a>
                                                </div>
                                            </div>
                                            <div class="tovar_description clearfix">
                                                <a class="tovar_title" href="../product-page.html" >HD Portátil 1TB</a>
                                                <span class="tovar_price">R$150.00</span>
                                            </div>
                                        </div><!-- //TOVAR -->
                                    </li>
                                    <li>
                                        <!-- TOVAR -->
                                        <div class="tovar_item_new">
                                            <div class="tovar_img">
                                                <img src="../images/produtos/informatica/image7.png" alt="" />
                                                <div class="open-project-link">
                                                    <a class="open-project tovar_view" href="../javascript:void(0);" data-url="!projects/women/1.html" >Visualizar</a>
                                                </div>
                                            </div>
                                            <div class="tovar_description clearfix">
                                                <a class="tovar_title" href="../product-page.html" >GTX 1060 EVGA</a>
                                                <span class="tovar_price">R$1200.00</span>
                                            </div>
                                        </div><!-- //TOVAR -->
                                    </li>
                                    <li>
                                        <!-- TOVAR -->
                                        <div class="tovar_item_new">
                                            <div class="tovar_img">
                                                <img src="../images/produtos/informatica/image8.png" alt="" />
                                                <div class="open-project-link">
                                                    <a class="open-project tovar_view" href="../javascript:void(0);" data-url="!projects/women/1.html" >Visualizar</a>
                                                </div>
                                            </div>
                                            <div class="tovar_description clearfix">
                                                <a class="tovar_title" href="../product-page.html" >ASUS Zen Fone</a>
                                                <span class="tovar_price">R$800.00</span>
                                            </div>
                                        </div><!-- //TOVAR -->
                                    </li>
                                    <li>
                                        <!-- TOVAR -->
                                        <div class="tovar_item_new">
                                            <div class="tovar_img">
                                                <img src="../images/produtos/informatica/image9.png" alt="" />
                                                <div class="open-project-link">
                                                    <a class="open-project tovar_view" href="../javascript:void(0);" data-url="!projects/women/1.html" >Visualizar</a>
                                                </div>
                                            </div>
                                            <div class="tovar_description clearfix">
                                                <a class="tovar_title" href="../product-page.html" >Monitor Full HD LG</a>
                                                <span class="tovar_price">R$300.00</span>
                                            </div>
                                        </div><!-- //TOVAR -->
                                    </li>
                                    <li>
                                        <!-- TOVAR -->
                                        <div class="tovar_item_new">
                                            <div class="tovar_img">
                                                <img src="../images/produtos/informatica/image10.png" alt="" />
                                                <div class="open-project-link">
                                                    <a class="open-project tovar_view" href="../javascript:void(0);" data-url="!projects/women/1.html" >Visualizar</a>
                                                </div>
                                            </div>
                                            <div class="tovar_description clearfix">
                                                <a class="tovar_title" href="../product-page.html" >Memória Ram Hyperx</a>
                                                <span class="tovar_price">$250.00</span>
                                            </div>
                                        </div><!-- //TOVAR -->
                                    </li>
                                    <li>
                                        <!-- TOVAR -->
                                        <div class="tovar_item_new">
                                            <div class="tovar_img">
                                                <img src="../images/produtos/informatica/image11.png" alt="" />
                                                <div class="open-project-link">
                                                    <a class="open-project tovar_view" href="../javascript:void(0);" data-url="!projects/women/1.html" >Visualizar</a>
                                                </div>
                                            </div>
                                            <div class="tovar_description clearfix">
                                                <a class="tovar_title" href="../product-page.html" >Mouse e Teclado</a>
                                                <span class="tovar_price">R$180.00</span>
                                            </div>
                                        </div><!-- //TOVAR -->
                                    </li>
                                    <li>
                                        <!-- TOVAR -->
                                        <div class="tovar_item_new">
                                            <div class="tovar_img">
                                                <img src="../images/produtos/informatica/image12.png" alt="" />
                                                <div class="open-project-link">
                                                    <a class="open-project tovar_view" href="../javascript:void(0);" data-url="!projects/women/1.html" >Visualizar</a>
                                                </div>
                                            </div>
                                            <div class="tovar_description clearfix">
                                                <a class="tovar_title" href="../product-page.html" >Micro SD 16 GB + Adaptador</a>
                                                <span class="tovar_price">R$100.00</span>
                                            </div>
                                        </div><!-- //TOVAR -->
                                    </li>


                                </ul>
                            </div>
                        </div><!-- //JCAROUSEL -->
                    </div><!-- //CONTAINER -->
                </section><!-- //NEW ARRIVALS -->


                <!-- BRANDS -->
                <section class="brands_carousel">

                    <!-- CONTAINER -->
                    <div class="container">

                        <!-- JCAROUSEL -->
                        <div class="jcarousel-wrapper">

                            <!-- NAVIGATION -->


                            <!--<div class="jcarousel" data-appear-top-offset='-100' data-animated='fadeInUp'>
                                    <ul>
                                            <li><a href="../javascript:void(0);" ><img src="../images/brands/1.jpg" alt="" /></a></li>
                                            <li><a href="../javascript:void(0);" ><img src="../images/brands/2.jpg" alt="" /></a></li>
                                            <li><a href="../javascript:void(0);" ><img src="../images/brands/3.jpg" alt="" /></a></li>
                                            <li><a href="../javascript:void(0);" ><img src="../images/brands/4.jpg" alt="" /></a></li>
                                            <li><a href="../javascript:void(0);" ><img src="../images/brands/5.jpg" alt="" /></a></li>
                                            <li><a href="../javascript:void(0);" ><img src="../images/brands/6.jpg" alt="" /></a></li>
                                            <li><a href="../javascript:void(0);" ><img src="../images/brands/7.jpg" alt="" /></a></li>
                                            <li><a href="../javascript:void(0);" ><img src="../images/brands/8.jpg" alt="" /></a></li>
                                            <li><a href="../javascript:void(0);" ><img src="../images/brands/9.jpg" alt="" /></a></li>
                                            <li><a href="../javascript:void(0);" ><img src="../images/brands/10.jpg" alt="" /></a></li>
                                            <li><a href="../javascript:void(0);" ><img src="../images/brands/11.jpg" alt="" /></a></li>
                                            <li><a href="../javascript:void(0);" ><img src="../images/brands/12.jpg" alt="" /></a></li>
                                    </ul>
                            </div>-->
                        </div><!-- //JCAROUSEL -->
                    </div><!-- //CONTAINER -->
                </section><!-- //BRANDS -->





                <!-- FOOTER -->
                <footer>

                    <!-- CONTAINER -->
                    <div class="container" data-animated='fadeInUp'>

                        <!-- ROW -->
                        <div class="row">

                            <div class="col-lg-2 col-md-2 col-sm-3 col-xs-6 col-ss-12 padbot30">
                                <h4>Contato</h4>
                                <div class="foot_address"><span>Explosion E-Commerce</span>Rua Zé das Colves, Guiana City</div>
                                <div class="foot_phone"><a href="../#" >(11) 4002-8922</a></div>
                                <div class="foot_mail"><a href="../#" >explosionecommerce@explosion.com</a></div>
                            </div>

                            <div class="col-lg-2 col-md-2 col-sm-3 col-xs-6 col-ss-12 padbot30">
                                <h4>Information</h4>
                                <ul class="foot_menu">
                                    <li><a href="../#" >Sobre Nós</a></li>
                                    <li><a href="../javascript:void(0);" >Política de Privacidade</a></li>
                                    <li><a href="../contacts.html" >Contato</a></li>
                                </ul>
                            </div>

                            <div class="respond_clear_480"></div>

                            <div class="col-lg-4 col-md-4 col-sm-6 padbot30">
                                <h4>Sobre a Loja</h4>
                                <p>Nós somos uma loja renomada no mercado, vendemos os produtos mais diversificados. Pode confiar!</p>
                            </div>

                            <div class="respond_clear_768"></div>

                            <div class="col-lg-4 col-md-4 padbot30">
                                <h4>Boletim de Notícias</h4>
                                <form class="newsletter_form clearfix" action="javascript:void(0);" method="get">
                                    <input type="text" name="newsletter" value="Informe seu e-mail" onFocus="if (this.value == 'Enter E-mail & Get 10% off')
                                                this.value = '';" onBlur="if (this.value == '')
                                                            this.value = 'Enter E-mail & Get 10% off';" />
                                    <input class="btn newsletter_btn" type="submit" value="SIGN UP">
                                </form>

                                <h4>Nossas Redes Sociais</h4>
                                <div class="social">
                                    <a href="../javascript:void(0);" ><i class="fa fa-twitter"></i></a>
                                    <a href="../javascript:void(0);" ><i class="fa fa-facebook"></i></a>
                                    <a href="../javascript:void(0);" ><i class="fa fa-google-plus"></i></a>
                                    <a href="../javascript:void(0);" ><i class="fa fa-pinterest-square"></i></a>
                                    <a href="../javascript:void(0);" ><i class="fa fa-tumblr"></i></a>
                                    <a href="../javascript:void(0);" ><i class="fa fa-instagram"></i></a>
                                </div>
                            </div>
                        </div><!-- //ROW -->
                    </div><!-- //CONTAINER -->

                    <!-- COPYRIGHT -->
                    <div class="copyright">

                        <!-- CONTAINER -->
                        <div class="container clearfix">
                            <div class="foot_logo"><a href="../index.jsp" ><img src="../images/foot_logo.png" alt="" /></a></div>

                            <div class="copyright_inf">
                                <span>Explosion E-Commerce © 1950</span> |                                            
                                <a class="back_top" href="../javascript:void(0);" >Voltar ao topo <i class="fa fa-angle-up"></i></a>
                            </div>
                        </div><!-- //CONTAINER -->
                    </div><!-- //COPYRIGHT -->
                </footer><!-- //FOOTER -->
            </div><!-- //PAGE -->
        </div>


        <!-- SCRIPTS -->
        <!--[if IE]><script src="../http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
        <!--[if IE]><html class="ie" lang="en"> <![endif]-->

        <script src="../js/jquery.min.js" type="text/javascript"></script>
        <script src="../js/bootstrap.min.js" type="text/javascript"></script>
        <script src="../js/jquery.sticky.js" type="text/javascript"></script>
        <script src="../js/parallax.js" type="text/javascript"></script>
        <script src="../js/jquery.flexslider-min.js" type="text/javascript"></script>
        <script src="../js/jquery.jcarousel.js" type="text/javascript"></script>
        <script src="../js/fancySelect.js"></script>
        <script src="../js/animate.js" type="text/javascript"></script>
        <script src="../js/myscript.js" type="text/javascript"></script>
        <script>
                                                    if (top != self)
                                                        top.location.replace(self.location.href);
        </script>

    </body>
</html>
