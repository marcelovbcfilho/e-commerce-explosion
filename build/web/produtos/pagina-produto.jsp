<%@page import="br.com.ecomerce.dominio.SubCategoria"%>
<%@page import="br.com.ecomerce.dominio.Categoria"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="br.com.ecomerce.dominio.Carrinho"%>
<%@page import="java.util.ArrayList"%>
<%@page import="br.com.ecomerce.dominio.Resultado"%>
<%@page import="br.com.ecomerce.dominio.Produto"%>
<%@page import="br.com.ecomerce.dao.ProdutoDAO"%>
<%@page import="br.com.ecomerce.dominio.EntidadeDominio"%>
<%@page import="br.com.ecomerce.dominio.Cliente"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<html lang="pt">
    <head>

        <meta charset="utf-8">
        <title>Explosion | E-Commerce de Eletrônicos</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">

        <link rel="shortcut icon" href="${pageContext.request.contextPath}/images/logo/explosionV1.png">

        <!-- CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/css/flexslider.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/css/fancySelect.css" rel="stylesheet" media="screen, projection" />
        <link href="${pageContext.request.contextPath}/css/animate.css" rel="stylesheet" type="text/css" media="all" />
        <link href="${pageContext.request.contextPath}/css/style.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/css/estilo.css" rel="stylesheet" type="text/css" />

        <!-- FONTS -->
        <link href='http://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
        <link href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">

    </head>
    <body>

        <!-- PRELOADER -->

        <!-- //PRELOADER -->

        <div class="preloader_hide">

            <!-- PAGE -->
            <div id="page">

                <!-- HEADER -->
                <header>

                    <!-- TOP INFO -->
                    <div class="top_info">

                        <!-- CONTAINER -->
                        <div class="container clearfix">
                            <ul class="secondary_menu">
                                <%
                                    DecimalFormat df = new DecimalFormat("0.##");
                                    Cliente cliente = null;
                                    if (session.getAttribute("cliente") != null) {
                                        cliente = (Cliente) session.getAttribute("cliente");
                                        if (cliente.getAdmin() == 1) {
                                            out.println("<li><a href='" + request.getContextPath() + "/minha-conta-admin.jsp' >Minha Conta</a></li>");
                                            out.println("<li><a>Bem Vindo " + cliente.getNomeCompleto() + "</a></li>");
                                            out.println("<li><a href='" + request.getContextPath() + "/Logout?logout=logout'>Logout</a></li>");
                                        } else {
                                            out.println("<li><a href='" + request.getContextPath() + "/minha-conta.jsp' >Minha Conta</a></li>");
                                            out.println("<li><a>Bem Vindo " + cliente.getNomeCompleto() + "</a></li>");
                                            out.println("<li><a>Carteira: R$" + df.format(cliente.getCarteira()) + "</a></li>");
                                            out.println("<li><a href='" + request.getContextPath() + "/Logout?logout=logout'>Logout</a></li>");
                                        }

                                    } else {
                                        out.println("<li><a href='" + request.getContextPath() + "/login.jsp' >Login</a></li>");
                                        out.println("<li><a href='" + request.getContextPath() + "/cadastro-cliente.jsp' >Registrar</a></li>");
                                    }
                                %>
                            </ul>
                        </div><!-- //CONTAINER -->
                    </div><!-- TOP INFO -->


                    <!-- MENU BLOCK -->
                    <div class="menu_block">

                        <!-- CONTAINER -->
                        <div class="container clearfix">

                            <!-- LOGO -->
                            <div class="logo">
                                <a href="${pageContext.request.contextPath}/index.jsp" ><img src="${pageContext.request.contextPath}/images/logo/explosionV2.png" alt="" /></a>
                            </div><!-- //LOGO -->

                            <!-- SEARCH FORM -->
                            <div class="top_search_form">
                                <a class="top_search_btn" href="javascript:void(0);"><i class="fa fa-search"></i></a>
                                <form action="${pageContext.request.contextPath}/VisualizarProduto" method='POST'>
                                    <input type="text" name="pesquisar" value="Pesquisar" onFocus="if (this.value == 'Pesquisar')
                                                this.value = '';" onBlur="if (this.value == '')
                                                            this.value = 'Pesquisar';"/>
                                    <input type='hidden' id='OPERACAO' name='OPERACAO' value='VISUALIZAR'/>
                                    <input type='hidden' id='PAGINA' name='PAGINA' value='PESQUISA'/>
                                </form>
                            </div><!-- SEARCH FORM -->

                            <%
                                double total = 0;
                                if (null == session.getAttribute("carrinho")) {
                                    ArrayList<Produto> produtos = new ArrayList<Produto>();
                                    Carrinho carrinho = new Carrinho();
                                    carrinho.setProdutos(produtos);
                                    session.setAttribute("carrinho", carrinho);
                            %>
                            <div class="shopping_bag">
                                <a class="shopping_bag_btn" href = "javascript:void(0);" > 
                                    <i class="fa fa-shopping-cart"></i > <p>Carrinho</p > <span> <% out.print(carrinho.getProdutos().size());%></span></a>
                                <div class="cart">
                                    <div class="cart_total">
                                        <div class="clearfix">
                                            <span class="cart_subtotal">Total: <b><% out.print("R$" + total);%></b></span>
                                        </div>
                                        <a class="btn active" href="logado/carrinho-compra.jsp">Ir ao Carrinho</a>
                                    </div>
                                </div>
                            </div>
                            <%
                            } else {

                                Carrinho carrinho = (Carrinho) session.getAttribute("carrinho");

                            %>
                            <!-- SHOPPING BAG -->
                            <div class="shopping_bag">
                                <a class="shopping_bag_btn" href="javascript:void(0);" ><i class="fa fa-shopping-cart"></i><p>Carrinho</p><span><%out.print(carrinho.getProdutos().size());%></span></a>
                                <div class="cart">
                                    <ul class="cart-items">
                                        <%
                                            for (Produto pro : carrinho.getProdutos()) {
                                                out.print("<li class='clearfix'>");
                                                out.print("<img class='cart_item_product' src='" + request.getContextPath() + "/" + pro.getEndereco_imagem() + "'/>");
                                                out.print(pro.getQuantidadeProdutoCarrinho() + " x " + pro.getNome());
                                                out.print("<span class='cart_item_price'> R$" + pro.getPreco() + "</span>");
                                                out.print("</li>");
                                                total += pro.getPreco() * pro.getQuantidadeProdutoCarrinho();
                                            }
                                        %>
                                    </ul>
                                    <div class="cart_total">
                                        <div class="clearfix"><span class="cart_subtotal">Total: <b><% out.print("R$" + total);%></b></span></div>
                                        <a class="btn active" href="${pageContext.request.contextPath}/logado/carrinho-compra.jsp">Ir ao Carrinho</a>
                                    </div>
                                </div>
                            </div>
                            <%
                                }
                            %>

                            <!-- MENU -->
                            <ul class="navmenu center">
                                <li>
                                    <a href="${pageContext.request.contextPath}/index.jsp" >Home</a>
                                </li>
                                <%
                                    Resultado resultadoCategorias = (Resultado) session.getAttribute("categorias");
                                    Categoria catg;
                                    SubCategoria sub;
                                    for (EntidadeDominio entity : resultadoCategorias.getEntidades()) {
                                        catg = (Categoria) entity;
                                %>
                                <li class='sub-menu'>
                                    <form name='categoria<%out.print(catg.getId());%>' action='${pageContext.request.contextPath}/VisualizarEstoque' method='POST' hidden>
                                        <input type='hidden' id='idCategoria' name='idCategoria' value='<% out.print(catg.getId());%>'/>
                                        <input type='hidden' id='OPERACAO' name='OPERACAO' value='VISUALIZAR'/>
                                        <input type='hidden' id='PAGINA' name='PAGINA' value='CATALOGO'/>                                        
                                    </form>
                                    <a href='javascript:categoria<%out.print(catg.getId());%>.submit();'>
                                        <%out.print(catg.getNome()); %>
                                    </a>
                                    <%
                                        if (catg.getSubCategorias().size() == 0) {
                                            out.println("</li>");
                                        } else {
                                    %>
                                    <ul class='mega_menu megamenu_col1 clearfix'>
                                        <li class='col'>
                                            <ol>
                                                <%
                                                    for (EntidadeDominio entitySub : catg.getSubCategorias()) {
                                                        sub = (SubCategoria) entitySub;
                                                %>
                                                <li>
                                                    <form name='sub<% out.print(sub.getId());%>' action='${pageContext.request.contextPath}/VisualizarEstoque' method='POST' hidden>
                                                        <input type='hidden' id='OPERACAO' name='OPERACAO' value='VISUALIZAR'/>
                                                        <input type='hidden' id='PAGINA' name='PAGINA' value='CATALOGO'/>
                                                        <input type='hidden' id='idSubCategoria' name='idSubCategoria' value='<%out.print(sub.getId());%>'/>
                                                    </form>
                                                    <a href='javascript:sub<% out.print(sub.getId());%>.submit();'><%out.print(sub.getNome());%></a>
                                                </li>
                                                <%}%>
                                            </ol>
                                        </li>
                                    </ul>
                                    <%}%>
                                </li>
                                <%
                                    }
                                %>                                                                
                            </ul><!-- //MENU -->
                        </div><!-- //CONTAINER -->
                    </div><!-- //MENU BLOCK -->
                </header><!-- //HEADER -->

                <!-- BREADCRUMBS -->
                <section class="breadcrumb parallax margbot30"></section>
                <!-- //BREADCRUMBS -->


                <!-- TOVAR DETAILS -->
                <section class="tovar_details padbot70">

                    <!-- CONTAINER -->
                    <div class="container">

                        <!-- ROW -->
                        <div class="row">

                            <!-- SIDEBAR TOVAR DETAILS -->
                            <div class="col-lg-2 col-md-1 sidebar_tovar_details">

                            </div><!-- //SIDEBAR TOVAR DETAILS -->

                            <!-- TOVAR DETAILS WRAPPER -->
                            <div class="col-lg-9 col-md-9 tovar_details_wrapper clearfix">
                                <%
                                    Resultado resultado = (Resultado) request.getAttribute("resultado");
                                    Produto produto = (Produto) resultado.getEntidades().get(0);
                                %>
                                <!-- CLEARFIX -->
                                <div class="clearfix padbot40">
                                    <div class="tovar_view_fotos clearfix">
                                        <div id="slider2" class="flexslider">
                                            <ul class="slides">
                                                <%
                                                    if (!produto.getEndereco_imagem().trim().equals("") || !produto.getEndereco_imagem_lado().trim().equals("")) {

                                                %>                                                    
                                                <li><a href="javascript:void(0);" ><img src="${pageContext.request.contextPath}/<%out.print(produto.getEndereco_imagem());%>" alt="" /></a></li>
                                                <li><a href="javascript:void(0);" ><img src="${pageContext.request.contextPath}/<%out.print(produto.getEndereco_imagem_lado());%>" alt="" /></a></li>
                                                <li><a href="javascript:void(0);" ><img src="${pageContext.request.contextPath}/<%out.print(produto.getEndereco_imagem_costas());%>" alt="" /></a></li>                                                
                                                        <%
                                                        } else {
                                                        %>
                                                <li><a href="javascript:void(0);" ><img src="<%out.print(request.getContextPath() + "/images/imagemIndisponivel.png");%>" alt="" /></a></li>
                                                <li><a href="javascript:void(0);" ><img src="<%out.print(request.getContextPath() + "/images/imagemIndisponivel.png");%>" alt="" /></a></li>
                                                <li><a href="javascript:void(0);" ><img src="<%out.print(request.getContextPath() + "/images/imagemIndisponivel.png");%>" alt="" /></a></li>
                                                        <%
                                                            }
                                                        %>
                                            </ul>
                                        </div>
                                        <div id="carousel2" class="flexslider">
                                            <ul class="slides">
                                                <%
                                                    if (!produto.getEndereco_imagem().trim().equals("") || !produto.getEndereco_imagem_lado().trim().equals("")) {

                                                %>                                                    
                                                <li><a href="javascript:void(0);" ><img src="${pageContext.request.contextPath}/<%out.print(produto.getEndereco_imagem());%>" alt="" /></a></li>
                                                <li><a href="javascript:void(0);" ><img src="${pageContext.request.contextPath}/<%out.print(produto.getEndereco_imagem_lado());%>" alt="" /></a></li>
                                                <li><a href="javascript:void(0);" ><img src="${pageContext.request.contextPath}/<%out.print(produto.getEndereco_imagem_costas());%>" alt="" /></a></li>                                                
                                                        <%
                                                        } else {
                                                        %>
                                                <li><a href="javascript:void(0);" ><img src="<%out.print(request.getContextPath() + "/images/imagemIndisponivel.png");%>" alt="" /></a></li>
                                                <li><a href="javascript:void(0);" ><img src="<%out.print(request.getContextPath() + "/images/imagemIndisponivel.png");%>" alt="" /></a></li>
                                                <li><a href="javascript:void(0);" ><img src="<%out.print(request.getContextPath() + "/images/imagemIndisponivel.png");%>" alt="" /></a></li>
                                                        <%
                                                            }
                                                        %>
                                            </ul>
                                        </div>
                                    </div>

                                    <div class="tovar_view_description">
                                        <div class="tovar_view_title"><%out.print(produto.getNome());%></div>
                                        <br>
                                        <div class="clearfix tovar_brend_price">
                                            <div class="pull-left tovar_view_price">R$<%out.print(produto.getPreco());%></div>
                                        </div>

                                        <div class="col-xs-12 tovar_view_btn" >    
                                            <form action='${pageContext.request.contextPath}/VisualizarProduto' method='POST'>
                                                <input type='hidden' name='idProduto' id='idProduto' value='<%out.print(produto.getId());%>'/>
                                                <input type='hidden' id='OPERACAO' name='OPERACAO' value='VISUALIZAR'/>
                                                <input type='hidden' id='PAGINA' name='PAGINA' value='CARRINHO'/>
                                                <input class='btn btn-group' type='submit' id='' name='' value='Adicionar ao Carrinho'/>
                                            </form>
                                        </div>                                        
                                    </div>
                                </div><!-- //CLEARFIX -->

                                <!-- TOVAR INFORMATION -->
                                <div class="tovar_information">
                                    <ul class="tabs clearfix">
                                        <li class="current">Descrição</li>
                                        <li>Informação</li>                                        
                                    </ul>
                                    <div class="box visible">
                                        <p><%out.print(produto.getDescricao());%></p>

                                    </div>
                                    <div class="box">
                                        <strong>Características:</strong> <br>
                                        - Marca: Intel<br>
                                        - Modelo: BX80648158400<br>
                                        <strong>Especificações:</strong><br>
                                        <strong>Essenciais:</strong><br>
                                        - Coleção de produtos: 8ª geração de processadores Intel Core i5 <br>
                                        - Codinome: Produtos com denominação anterior Coffee Lake<br>
                                        - Segmento vertical: Desktop<br>
                                        - Número do processador: i5-8400<br>
                                        - Status: Launched<br>
                                        - Data de introdução: Q4'17<br>
                                        - Litografia: 14 nm<br>


                                    </div>

                                </div><!-- //TOVAR INFORMATION -->
                            </div><!-- //TOVAR DETAILS WRAPPER -->
                        </div><!-- //ROW -->
                    </div><!-- //CONTAINER -->
                </section><!-- //TOVAR DETAILS -->

                <!-- NEW ARRIVALS -->
                <section class="new_arrivals padbot50">

                    <!-- CONTAINER -->
                    <div class="container">
                        <h2>Novidades</h2>

                        <!-- JCAROUSEL -->
                        <div class="jcarousel-wrapper">

                            <!-- NAVIGATION -->
                            <div class="jCarousel_pagination">
                                <a href="${pageContext.request.contextPath}/javascript:void(0);" class="jcarousel-control-prev" ><i class="fa fa-angle-left"></i></a>
                                <a href="${pageContext.request.contextPath}/javascript:void(0);" class="jcarousel-control-next" ><i class="fa fa-angle-right"></i></a>
                            </div><!-- //NAVIGATION -->

                            <div class="jcarousel" data-appear-top-offset='-100' data-animated='fadeInUp'>
                                <ul>
                                    <li>
                                        <!-- TOVAR -->
                                        <div class="tovar_item_new">
                                            <div class="tovar_img">
                                                <img src="${pageContext.request.contextPath}/images/produtos/informatica/image1.png" alt="" />
                                                <div class="open-project-link"><a class="open-project tovar_view" href="i5-8geracao.jsp" data-url="!projects/women/1.html" >Visualizar</a></div>
                                            </div>
                                            <div class="tovar_description clearfix">
                                                <a class="tovar_title" href="${pageContext.request.contextPath}/product-page.html" >Core i5 8° Geração</a>
                                                <span class="tovar_price">R$850.00</span>
                                            </div>
                                        </div><!-- //TOVAR -->
                                    </li>
                                    <li>
                                        <!-- TOVAR -->
                                        <div class="tovar_item_new">
                                            <div class="tovar_img">
                                                <img src="${pageContext.request.contextPath}/images/produtos/informatica/image6.png" alt="" />
                                                <div class="open-project-link">
                                                    <a class="open-project tovar_view" href="${pageContext.request.contextPath}/javascript:void(0);" data-url="!projects/women/1.html" >Visualizar</a>
                                                </div>
                                            </div>
                                            <div class="tovar_description clearfix">
                                                <a class="tovar_title" href="${pageContext.request.contextPath}/product-page.html" >HD Portátil 1TB</a>
                                                <span class="tovar_price">R$150.00</span>
                                            </div>
                                        </div><!-- //TOVAR -->
                                    </li>
                                    <li>
                                        <!-- TOVAR -->
                                        <div class="tovar_item_new">
                                            <div class="tovar_img">
                                                <img src="${pageContext.request.contextPath}/images/produtos/informatica/image7.png" alt="" />
                                                <div class="open-project-link">
                                                    <a class="open-project tovar_view" href="${pageContext.request.contextPath}/javascript:void(0);" data-url="!projects/women/1.html" >Visualizar</a>
                                                </div>
                                            </div>
                                            <div class="tovar_description clearfix">
                                                <a class="tovar_title" href="${pageContext.request.contextPath}/product-page.html" >GTX 1060 EVGA</a>
                                                <span class="tovar_price">R$1200.00</span>
                                            </div>
                                        </div><!-- //TOVAR -->
                                    </li>
                                    <li>
                                        <!-- TOVAR -->
                                        <div class="tovar_item_new">
                                            <div class="tovar_img">
                                                <img src="${pageContext.request.contextPath}/images/produtos/informatica/image8.png" alt="" />
                                                <div class="open-project-link">
                                                    <a class="open-project tovar_view" href="${pageContext.request.contextPath}/javascript:void(0);" data-url="!projects/women/1.html" >Visualizar</a>
                                                </div>
                                            </div>
                                            <div class="tovar_description clearfix">
                                                <a class="tovar_title" href="${pageContext.request.contextPath}/product-page.html" >ASUS Zen Fone</a>
                                                <span class="tovar_price">R$800.00</span>
                                            </div>
                                        </div><!-- //TOVAR -->
                                    </li>
                                    <li>
                                        <!-- TOVAR -->
                                        <div class="tovar_item_new">
                                            <div class="tovar_img">
                                                <img src="${pageContext.request.contextPath}/images/produtos/informatica/image9.png" alt="" />
                                                <div class="open-project-link">
                                                    <a class="open-project tovar_view" href="${pageContext.request.contextPath}/javascript:void(0);" data-url="!projects/women/1.html" >Visualizar</a>
                                                </div>
                                            </div>
                                            <div class="tovar_description clearfix">
                                                <a class="tovar_title" href="${pageContext.request.contextPath}/product-page.html" >Monitor Full HD LG</a>
                                                <span class="tovar_price">R$300.00</span>
                                            </div>
                                        </div><!-- //TOVAR -->
                                    </li>
                                    <li>
                                        <!-- TOVAR -->
                                        <div class="tovar_item_new">
                                            <div class="tovar_img">
                                                <img src="${pageContext.request.contextPath}/images/produtos/informatica/image10.png" alt="" />
                                                <div class="open-project-link">
                                                    <a class="open-project tovar_view" href="${pageContext.request.contextPath}/javascript:void(0);" data-url="!projects/women/1.html" >Visualizar</a>
                                                </div>
                                            </div>
                                            <div class="tovar_description clearfix">
                                                <a class="tovar_title" href="${pageContext.request.contextPath}/product-page.html" >Memória Ram Hyperx</a>
                                                <span class="tovar_price">$250.00</span>
                                            </div>
                                        </div><!-- //TOVAR -->
                                    </li>
                                    <li>
                                        <!-- TOVAR -->
                                        <div class="tovar_item_new">
                                            <div class="tovar_img">
                                                <img src="${pageContext.request.contextPath}/images/produtos/informatica/image11.png" alt="" />
                                                <div class="open-project-link">
                                                    <a class="open-project tovar_view" href="${pageContext.request.contextPath}/javascript:void(0);" data-url="!projects/women/1.html" >Visualizar</a>
                                                </div>
                                            </div>
                                            <div class="tovar_description clearfix">
                                                <a class="tovar_title" href="${pageContext.request.contextPath}/product-page.html" >Mouse e Teclado</a>
                                                <span class="tovar_price">R$180.00</span>
                                            </div>
                                        </div><!-- //TOVAR -->
                                    </li>
                                    <li>
                                        <!-- TOVAR -->
                                        <div class="tovar_item_new">
                                            <div class="tovar_img">
                                                <img src="${pageContext.request.contextPath}/images/produtos/informatica/image12.png" alt="" />
                                                <div class="open-project-link">
                                                    <a class="open-project tovar_view" href="${pageContext.request.contextPath}/javascript:void(0);" data-url="!projects/women/1.html" >Visualizar</a>
                                                </div>
                                            </div>
                                            <div class="tovar_description clearfix">
                                                <a class="tovar_title" href="${pageContext.request.contextPath}/product-page.html" >Micro SD 16 GB + Adaptador</a>
                                                <span class="tovar_price">R$100.00</span>
                                            </div>
                                        </div><!-- //TOVAR -->
                                    </li>


                                </ul>
                            </div>
                        </div><!-- //JCAROUSEL -->
                    </div><!-- //CONTAINER -->
                </section><!-- //NEW ARRIVALS -->


                <!-- FOOTER -->
                <footer>

                    <!-- CONTAINER -->
                    <div class="container" data-animated='fadeInUp'>

                        <!-- ROW -->
                        <div class="row">

                            <div class="col-lg-2 col-md-2 col-sm-3 col-xs-6 col-ss-12 padbot30">
                                <h4>Contato</h4>
                                <div class="foot_address"><span>Explosion E-Commerce</span>Rua Zé das Colves, Guiana City</div>
                                <div class="foot_phone"><a href="${pageContext.request.contextPath}/#" >(11) 4002-8922</a></div>
                                <div class="foot_mail"><a href="${pageContext.request.contextPath}/#" >explosionecommerce@explosion.com</a></div>
                            </div>

                            <div class="col-lg-2 col-md-2 col-sm-3 col-xs-6 col-ss-12 padbot30">
                                <h4>Information</h4>
                                <ul class="foot_menu">
                                    <li><a href="${pageContext.request.contextPath}/#" >Sobre Nós</a></li>
                                    <li><a href="javascript:void(0);" >Política de Privacidade</a></li>
                                    <li><a href="${pageContext.request.contextPath}/contacts.html" >Contato</a></li>
                                </ul>
                            </div>

                            <div class="respond_clear_480"></div>

                            <div class="col-lg-4 col-md-4 col-sm-6 padbot30">
                                <h4>Sobre a Loja</h4>
                                <p>Nós somos uma loja renomada no mercado, vendemos os produtos mais diversificados. Pode confiar!</p>
                            </div>

                            <div class="respond_clear_768"></div>

                            <div class="col-lg-4 col-md-4 padbot30">
                                <h4>Boletim de Notícias</h4>
                                <form class="newsletter_form clearfix" action="javascript:void(0);" method="get">
                                    <input type="text" name="newsletter" value="Informe seu e-mail" onFocus="if (this.value == 'Enter E-mail & Get 10% off')
                                                this.value = '';" onBlur="if (this.value == '')
                                                            this.value = 'Enter E-mail & Get 10% off';" />
                                    <input class="btn newsletter_btn" type="submit" value="SIGN UP">
                                </form>

                                <h4>Nossas Redes Sociais</h4>
                                <div class="social">
                                    <a href="javascript:void(0);" ><i class="fa fa-twitter"></i></a>
                                    <a href="javascript:void(0);" ><i class="fa fa-facebook"></i></a>
                                    <a href="javascript:void(0);" ><i class="fa fa-google-plus"></i></a>
                                    <a href="javascript:void(0);" ><i class="fa fa-pinterest-square"></i></a>
                                    <a href="javascript:void(0);" ><i class="fa fa-tumblr"></i></a>
                                    <a href="javascript:void(0);" ><i class="fa fa-instagram"></i></a>
                                </div>
                            </div>
                        </div><!-- //ROW -->
                    </div><!-- //CONTAINER -->

                    <!-- COPYRIGHT -->
                    <div class="copyright">

                        <!-- CONTAINER -->
                        <div class="container clearfix">
                            <div class="foot_logo"><a href="${pageContext.request.contextPath}/index.jsp" ><img src="${pageContext.request.contextPath}/images/foot_logo.png" alt="" /></a></div>

                            <div class="copyright_inf">
                                <span>Explosion E-Commerce © 1950</span> |                                            
                                <a class="back_top" href="${pageContext.request.contextPath}/javascript:void(0);" >Voltar ao topo <i class="fa fa-angle-up"></i></a>
                            </div>
                        </div><!-- //CONTAINER -->
                    </div><!-- //COPYRIGHT -->
                </footer><!-- //FOOTER -->
            </div><!-- //PAGE -->
        </div>


        <!-- SCRIPTS -->
        <!--[if IE]><script src="${pageContext.request.contextPath}/http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
        <!--[if IE]><html class="ie" lang="en"> <![endif]-->

        <script src="${pageContext.request.contextPath}/js/jquery.min.js" type="text/javascript"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="${pageContext.request.contextPath}/js/jquery.sticky.js" type="text/javascript"></script>
        <script src="${pageContext.request.contextPath}/js/parallax.js" type="text/javascript"></script>
        <script src="${pageContext.request.contextPath}/js/jquery.flexslider-min.js" type="text/javascript"></script>
        <script src="${pageContext.request.contextPath}/js/jquery.jcarousel.js" type="text/javascript"></script>
        <script src="${pageContext.request.contextPath}/js/fancySelect.js"></script>
        <script src="${pageContext.request.contextPath}/js/animate.js" type="text/javascript"></script>
        <script src="${pageContext.request.contextPath}/js/myscript.js" type="text/javascript"></script>
        <script>
                                                    if (top != self)
                                                        top.location.replace(self.location.href);
        </script>

    </body>
</html>
