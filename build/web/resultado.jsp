
<%@page import="java.text.DecimalFormat"%>
<%@page import="br.com.ecomerce.dominio.EntidadeDominio"%>
<%@page import="br.com.ecomerce.dominio.Estoque"%>
<%@page import="br.com.ecomerce.dominio.Pedido"%>
<%@page import="br.com.ecomerce.dominio.Devolucao"%>
<%@page import="br.com.ecomerce.dominio.SubCategoria"%>
<%@page import="br.com.ecomerce.dominio.Produto"%>
<%@page import="br.com.ecomerce.dominio.Carrinho"%>
<%@page import="java.util.ArrayList"%>
<%@page import="br.com.ecomerce.dominio.Categoria"%>
<%@page import="br.com.ecomerce.dominio.Endereco"%>
<%@page import="br.com.ecomerce.dominio.Cliente"%>
<%@page import="br.com.ecomerce.dominio.CartaoCredito"%>
<%@page import="br.com.ecomerce.dominio.Resultado"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="br.com.ecomerce.dominio.Cliente"%>
<%@page import="br.com.ecomerce.dominio.Resultado"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Explosion | E-Commerce de Eletrônicos</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">

        <link rel="shortcut icon" href="images/logo/explosionV1.png">

        <!-- CSS -->
        <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="css/flexslider.css" rel="stylesheet" type="text/css" />
        <link href="css/fancySelect.css" rel="stylesheet" media="screen, projection" />
        <link href="css/animate.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/style.css" rel="stylesheet" type="text/css" />

        <!-- FONTS -->
        <link href='http://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
        <link href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">

    </head>
    <body>

        <!-- PRELOADER -->
        <div id="preloader"><img src="images/preloader.gif" alt="" /></div>
        <!-- //PRELOADER -->
        <div class="preloader_hide">

            <!-- PAGE -->
            <div id="page">

                <!-- HEADER -->
                <header>

                    <!-- TOP INFO -->
                    <div class="top_info">

                        <!-- CONTAINER -->
                        <div class="container clearfix">
                            <ul class="secondary_menu">
                                <%
                                    DecimalFormat df = new DecimalFormat("0.##");
                                    Cliente cliente = null;
                                    if (session.getAttribute("cliente") != null) {
                                        cliente = (Cliente) session.getAttribute("cliente");
                                        if (cliente.getAdmin() == 1) {
                                            out.println("<li><a href='" + request.getContextPath() + "/minha-conta-admin.jsp' >Minha Conta</a></li>");
                                            out.println("<li><a>Bem Vindo " + cliente.getNomeCompleto() + "</a></li>");
                                            out.println("<li><a href='" + request.getContextPath() + "/Logout?logout=logout'>Logout</a></li>");
                                        } else {
                                            out.println("<li><a href='" + request.getContextPath() + "/minha-conta.jsp' >Minha Conta</a></li>");
                                            out.println("<li><a>Bem Vindo " + cliente.getNomeCompleto() + "</a></li>");
                                            out.println("<li><a>Carteira: R$" + df.format(cliente.getCarteira()) + "</a></li>");
                                            out.println("<li><a href='" + request.getContextPath() + "/Logout?logout=logout'>Logout</a></li>");
                                        }

                                    } else {
                                        out.println("<li><a href='" + request.getContextPath() + "/login.jsp' >Login</a></li>");
                                        out.println("<li><a href='" + request.getContextPath() + "/cadastro-cliente.jsp' >Registrar</a></li>");
                                    }
                                %>
                            </ul>
                        </div><!-- //CONTAINER -->
                    </div><!-- TOP INFO -->


                    <!-- MENU BLOCK -->
                    <div class="menu_block">

                        <!-- CONTAINER -->
                        <div class="container clearfix">

                            <!-- LOGO -->
                            <div class="logo">
                                <a href="${pageContext.request.contextPath}/index.jsp" ><img src="${pageContext.request.contextPath}/images/logo/explosionV2.png" alt="" /></a>
                            </div><!-- //LOGO -->

                            <!-- SEARCH FORM -->
                            <div class="top_search_form">
                                <a class="top_search_btn" href="javascript:void(0);"><i class="fa fa-search"></i></a>
                                <form action="${pageContext.request.contextPath}/VisualizarProduto" method='POST'>
                                    <input type="text" name="pesquisar" value="Pesquisar" onFocus="if (this.value == 'Pesquisar')
                                                this.value = '';" onBlur="if (this.value == '')
                                                            this.value = 'Pesquisar';"/>
                                    <input type='hidden' id='OPERACAO' name='OPERACAO' value='VISUALIZAR'/>
                                    <input type='hidden' id='PAGINA' name='PAGINA' value='PESQUISA'/>
                                </form>
                            </div><!-- SEARCH FORM -->

                            <%
                                double total = 0;
                                if (null == session.getAttribute("carrinho")) {
                                    ArrayList<Produto> produtos = new ArrayList<Produto>();
                                    Carrinho carrinho = new Carrinho();
                                    carrinho.setProdutos(produtos);
                                    session.setAttribute("carrinho", carrinho);
                            %>
                            <div class="shopping_bag">
                                <a class="shopping_bag_btn" href = "javascript:void(0);" > 
                                    <i class="fa fa-shopping-cart"></i > <p>Carrinho</p > <span> <% out.print(carrinho.getProdutos().size());%></span></a>
                                <div class="cart">
                                    <div class="cart_total">
                                        <div class="clearfix">
                                            <span class="cart_subtotal">Total: <b><% out.print("R$" + total);%></b></span>
                                        </div>
                                        <a class="btn active" href="logado/carrinho-compra.jsp">Ir ao Carrinho</a>
                                    </div>
                                </div>
                            </div>
                            <%
                            } else {

                                Carrinho carrinho = (Carrinho) session.getAttribute("carrinho");

                            %>
                            <!-- SHOPPING BAG -->
                            <div class="shopping_bag">
                                <a class="shopping_bag_btn" href="javascript:void(0);" ><i class="fa fa-shopping-cart"></i><p>Carrinho</p><span><%out.print(carrinho.getProdutos().size());%></span></a>
                                <div class="cart">
                                    <ul class="cart-items">
                                        <%
                                            for (Produto pro : carrinho.getProdutos()) {
                                                out.print("<li class='clearfix'>");
                                                out.print("<img class='cart_item_product' src='" + pro.getEndereco_imagem() + "'/>");
                                                out.print(pro.getQuantidadeProdutoCarrinho() + " x " + pro.getNome());
                                                out.print("<span class='cart_item_price'> R$" + pro.getPreco() + "</span>");
                                                out.print("</li>");
                                                total += pro.getPreco() * pro.getQuantidadeProdutoCarrinho();
                                            }
                                        %>
                                    </ul>
                                    <div class="cart_total">
                                        <div class="clearfix"><span class="cart_subtotal">Total: <b><% out.print("R$" + total);%></b></span></div>
                                        <a class="btn active" href="logado/carrinho-compra.jsp">Ir ao Carrinho</a>
                                    </div>
                                </div>
                            </div>
                            <%
                                }
                            %>

                            <!-- MENU -->
                            <ul class="navmenu center">
                                <li>
                                    <a href="${pageContext.request.contextPath}/index.jsp" >Home</a>
                                </li>
                                <%
                                    Resultado resultadoCategorias = (Resultado) session.getAttribute("categorias");
                                    Categoria catg;
                                    SubCategoria sub;
                                    for (EntidadeDominio entity : resultadoCategorias.getEntidades()) {
                                        catg = (Categoria) entity;
                                %>
                                <li class='sub-menu'>
                                    <form name='categoria<%out.print(catg.getId());%>' action='${pageContext.request.contextPath}/VisualizarEstoque' method='POST' hidden>
                                        <input type='hidden' id='idCategoria' name='idCategoria' value='<% out.print(catg.getId());%>'/>
                                        <input type='hidden' id='OPERACAO' name='OPERACAO' value='VISUALIZAR'/>
                                        <input type='hidden' id='PAGINA' name='PAGINA' value='CATALOGO'/>                                        
                                    </form>
                                    <a href='javascript:categoria<%out.print(catg.getId());%>.submit();'>
                                        <%out.print(catg.getNome()); %>
                                    </a>
                                    <%
                                        if (catg.getSubCategorias().size() == 0) {
                                            out.println("</li>");
                                        } else {
                                    %>
                                    <ul class='mega_menu megamenu_col1 clearfix'>
                                        <li class='col'>
                                            <ol>
                                                <%
                                                    for (EntidadeDominio entitySub : catg.getSubCategorias()) {
                                                        sub = (SubCategoria) entitySub;
                                                %>
                                                <li>
                                                    <form name='sub<% out.print(sub.getId());%>' action='${pageContext.request.contextPath}/VisualizarEstoque' method='POST' hidden>
                                                        <input type='hidden' id='OPERACAO' name='OPERACAO' value='VISUALIZAR'/>
                                                        <input type='hidden' id='PAGINA' name='PAGINA' value='CATALOGO'/>
                                                        <input type='hidden' id='idSubCategoria' name='idSubCategoria' value='<%out.print(sub.getId());%>'/>
                                                    </form>
                                                    <a href='javascript:sub<% out.print(sub.getId());%>.submit();'><%out.print(sub.getNome());%></a>
                                                </li>
                                                <%}%>
                                            </ol>
                                        </li>
                                    </ul>
                                    <%}%>
                                </li>
                                <%
                                    }
                                %>                                                                
                            </ul><!-- //MENU -->
                        </div><!-- //CONTAINER -->
                    </div><!-- //MENU BLOCK -->
                </header><!-- //HEADER -->

                <!-- MY ACCOUNT PAGE -->
                <section class="my_account">
                    <div class="container">
                        <div class="row">
                            <%
                                Resultado resultado = (Resultado) request.getAttribute("resultado");
                                String entidade = (String) request.getAttribute("entidade");
                                String operacao = (String) request.getAttribute("operacao");

                                // Cria um atributo de acordo com o nome recebido
                                if (entidade == null) {
                                    out.println("<h1>Erro objeto não conhecido recebido.</h1>");
                                } else if (entidade.trim().equals("CLIENTE")) {
                                    cliente = (Cliente) resultado.getEntidades().get(0);

                                    if (operacao == null) {
                                        session.setAttribute("cliente", cliente);
                                        out.println("<form class='form' action='VisualizarCliente'  method='POST'>");
                                        out.println("<input type='hidden' name='idCliente' id='idCliente' value='" + cliente.getId() + "'/>");
                                        out.println("<h2>" + resultado.getMsg() + "</h2>");
                                        out.println("<input type='hidden' name='PAGINA' id='PAGINA' value='PERFIL'/>");
                                        out.println("<input type='hidden' name='OPERACAO' id='OPERACAO' value='VISUALIZAR'/>");
                                        out.println("<div class='col-xs-2'>");
                                        out.println("<input class='btn btn-group' type='submit' id='' name='' value='Perfil'/>");
                                        out.println("</div>");
                                        out.println("</form>");
                                        out.println("<a class='btn btn-group' href='logado/alterar-dados.jsp'>Voltar</a>");
                                    } else {
                                        if (operacao.trim().equals("SALVAR")) {
                                            session.setAttribute("cliente", cliente);
                                            out.println("<form class='form' action='VisualizarCliente'  method='POST'>");
                                            out.println("<input type='hidden' name='idCliente' id='idCliente' value='" + cliente.getId() + "'/>");
                                            out.println("<h2>" + resultado.getMsg() + "</h2>");
                                            out.println("<input type='hidden' name='PAGINA' id='PAGINA' value='PERFIL'/>");
                                            out.println("<input type='hidden' name='OPERACAO' id='OPERACAO' value='VISUALIZAR'/>");
                                            out.println("<div class='col-xs-2'>");
                                            out.println("<input class='btn btn-group' type='submit' id='' name='' value='Perfil'/>");
                                            out.println("</div>");
                                            out.println("</form>");
                                            out.print("<a href='cadastro-cliente.jsp' class='btn btn-group'>Voltar</a>");
                                        } else if (operacao.trim().equals("EXCLUIR")) {
                                            out.println("<form class='ui form' action='ConsultarCliente' method='POST'>");
                                            out.println("<div class='col-xs-12'>");
                                            out.println("<h2>" + resultado.getMsg() + "</h2>");
                                            out.println("</div>");
                                            out.println("<div class='col-xs-4'>");
                                            out.println("<input type='hidden' id='OPERACAO' name='OPERACAO' value='CONSULTAR'/>");
                                            out.println("<input type='submit' class='btn' id='' name='' value='Voltar a lista de Clientes'/>");
                                            out.println("</div>");
                                            out.println("</form>");
                                        }
                                    }
                                } else if (entidade.trim().equals("ENDERECO")) {
                                    Endereco endereco = (Endereco) resultado.getEntidades().get(0);

                                    out.println("<form class='form' action='VisualizarCliente'  method='POST'>");
                                    out.println("<input type='hidden' name='idCliente' id='idCliente' value='" + endereco.getCliente().getId() + "'/>");
                                    out.println("<h2>" + resultado.getMsg() + "</h2>");
                                    out.println("<input type='hidden' name='PAGINA' id='PAGINA' value='PERFIL'/>");
                                    out.println("<input type='hidden' name='OPERACAO' id='OPERACAO' value='VISUALIZAR'/>");
                                    out.println("<div class='col-xs-2'>");
                                    out.println("<input class='btn btn-group' type='submit' id='' name='' value='Perfil'/>");
                                    out.println("</div>");
                                    out.println("</form>");

                                    if (operacao == null) {
                                        out.println("<form action='VisualizarEndereco' method='POST'>");
                                        out.println("<input type='hidden' name='idEndereco' id='idEndereco' value='" + endereco.getId() + "'/>");
                                        out.println("<input type='hidden' name='OPERACAO' id='OPERACAO' value='VISUALIZAR'/>");
                                        out.println("<div class='col-xs-2'>");
                                        out.println("<input class='btn btn-group' type='submit' id='' name='' value='Voltar'/>");
                                        out.println("</div>");
                                        out.println("</form>");
                                    }

                                } else if (entidade.trim().equals("CARTAO")) {
                                    CartaoCredito cartao = (CartaoCredito) resultado.getEntidades().get(0);

                                    out.println("<form class='form' action='VisualizarCliente'  method='POST'>");
                                    out.println("<input type='hidden' name='idCliente' id='idCliente' value='" + cartao.getCliente().getId() + "'/>");
                                    out.println("<h2>" + resultado.getMsg() + "</h2>");
                                    out.println("<input type='hidden' name='PAGINA' id='PAGINA' value='PERFIL'/>");
                                    out.println("<input type='hidden' name='OPERACAO' id='OPERACAO' value='VISUALIZAR'/>");
                                    out.println("<div class='col-xs-2'>");
                                    out.println("<input class='btn btn-block' type='submit' id='' name='' value='Perfil'/>");
                                    out.println("</div>");
                                    out.println("</form>");

                                    if (operacao == null) {
                                        out.println("<form class='form' action='VisualizarCartao' method='POST'>");
                                        out.println("<input type='hidden' name='idCartao' id='idCartao' value='" + cartao.getId() + "'/>");
                                        out.println("<input type='hidden' name='OPERACAO' id='OPERACAO' value='VISUALIZAR'/>");
                                        out.println("<div class='col-xs-2'>");
                                        out.println("<input class='btn btn-block' type='submit' id='' name='' value='Voltar'/>");
                                        out.println("</div>");
                                        out.println("</form>");
                                    }
                                } else if (entidade.trim().equals("CARTAO")) {
                                    CartaoCredito cartao = (CartaoCredito) resultado.getEntidades().get(0);

                                    out.println("<form class='form' action='VisualizarCliente'  method='POST'>");
                                    out.println("<input type='hidden' name='idCliente' id='idCliente' value='" + cartao.getCliente().getId() + "'/>");
                                    out.println("<h2>" + resultado.getMsg() + "</h2>");
                                    out.println("<input type='hidden' name='PAGINA' id='PAGINA' value='PERFIL'/>");
                                    out.println("<input type='hidden' name='OPERACAO' id='OPERACAO' value='VISUALIZAR'/>");
                                    out.println("<div class='col-xs-2'>");
                                    out.println("<input class='btn btn-block' type='submit' id='' name='' value='Perfil'/>");
                                    out.println("</div>");
                                    out.println("</form>");

                                    if (operacao == null) {
                                        out.println("<form class='form' action='VisualizarCartao' method='POST'>");
                                        out.println("<input type='hidden' name='idCartao' id='idCartao' value='" + cartao.getId() + "'/>");
                                        out.println("<input type='hidden' name='OPERACAO' id='OPERACAO' value='VISUALIZAR'/>");
                                        out.println("<div class='col-xs-2'>");
                                        out.println("<input class='btn btn-block' type='submit' id='' name='' value='Voltar'/>");
                                        out.println("</div>");
                                        out.println("</form>");
                                    }
                                } else if (entidade.trim().equals("CATEGORIA")) {
                                    Categoria categoria = (Categoria) resultado.getEntidades().get(0);

                                    out.println("<form class='form' action='ConsultarCategoria'  method='POST'>");
                                    out.println("<h2>" + resultado.getMsg() + "</h2>");
                                    out.println("<input type='hidden' name='OPERACAO' id='OPERACAO' value='CONSULTAR'/>");
                                    out.println("<div class='col-xs-2'>");
                                    out.println("<input class='btn btn-block' type='submit' id='' name='' value='Voltar'/>");
                                    out.println("</div>");
                                    out.println("</form>");
                                } else if (entidade.trim().equals("SUBCATEGORIA")) {
                                    SubCategoria subCategoria = (SubCategoria) resultado.getEntidades().get(0);

                                    out.println("<form class='form' action='ConsultarSubCategoria'  method='POST'>");
                                    out.println("<h2>" + resultado.getMsg() + "</h2>");
                                    out.println("<input type='hidden' name='OPERACAO' id='OPERACAO' value='CONSULTAR'/>");
                                    out.println("<div class='col-xs-2'>");
                                    out.println("<input class='btn btn-block' type='submit' id='' name='' value='Voltar'/>");
                                    out.println("</div>");
                                    out.println("</form>");
                                } else if (entidade.trim().equals("PRODUTO")) {
                                    Produto produto = (Produto) resultado.getEntidades().get(0);

                                    out.println("<form class='form' action='ConsultarProduto'  method='POST'>");
                                    out.println("<h2>" + resultado.getMsg() + "</h2>");
                                    out.println("<input type='hidden' name='OPERACAO' id='OPERACAO' value='CONSULTAR'/>");
                                    out.println("<div class='col-xs-2'>");
                                    out.println("<input class='btn btn-block' type='submit' id='' name='' value='Voltar'/>");
                                    out.println("</div>");
                                    out.println("</form>");
                                } else if (entidade.trim().equals("DEVOLUCAO")) {
                                    Devolucao devolucao = (Devolucao) resultado.getEntidades().get(0);

                                    out.println("<form class='form' action='minha-conta.jsp'  method='POST'>");
                                    out.println("<h2>" + resultado.getMsg() + "</h2>");
                                    out.println("<div class='col-xs-2'>");
                                    out.println("<input class='btn btn-block' type='submit' id='' name='' value='Voltar'/>");
                                    out.println("</div>");
                                    out.println("</form>");
                                } else if (entidade.trim().equals("PEDIDO")) {
                                    Pedido pedido = (Pedido) resultado.getEntidades().get(0);
                                    
                                    if (operacao.equals("ALTERAR")) {                                         
                                        out.println("<form action='"+request.getContextPath()+"/VisualizarPedido' method='POST'>");
                                        out.println("<h2>" + resultado.getMsg() + "</h2>");
                                        out.println("<input type='hidden' id='OPERACAO' name='OPERACAO' value='VISUALIZAR'/>");
                                        out.println("<input type='hidden' id='PESQUISA' name='PESQUISA' value='PEDIDOSADM'/>");
                                        out.println("<input type='hidden' id='PAGINA' name='PAGINA' value='PEDIDOS'/>");
                                        out.println("<div class='col-xs-2'>");
                                        out.println("<input class='btn btn-block' type='submit' id='' name='' value='Voltar'/>");
                                        out.println("</div>");
                                        out.println("</form>");
                                    } else {
                                        out.println("<form class='form' action='index.jsp'  method='POST'>");
                                        out.println("<h2>" + resultado.getMsg() + "</h2>");
                                        out.println("<div class='col-xs-2'>");
                                        out.println("<input class='btn btn-block' type='submit' id='' name='' value='Voltar'/>");
                                        out.println("</div>");
                                        out.println("</form>");
                                    }
                                    
                                } else if (entidade.trim().equals("ESTOQUE")) {
                                    Estoque estoque = (Estoque) resultado.getEntidades().get(0);
                                    out.println("<form class='form' action='" + request.getContextPath() + "/VisualizarEstoque'  method='POST'>");
                                    out.println("<h2>" + resultado.getMsg() + "</h2>");
                                    out.println("<div class='col-xs-2'>");
                                    out.println("<input type='hidden' id='OPERACAO' name='OPERACAO' value='VISUALIZAR'/>");
                                    out.println("<input type='hidden' id='PAGINA' name='PAGINA' value='ESTOQUE'/>");
                                    out.println("<input class='btn btn-block' type='submit' id='' name='' value='Voltar'/>");
                                    out.println("</div>");
                                    out.println("</form>");
                                }
                            %>
                        </div>
                    </div>
                </section>
                <br/>
                <br/>

                <!-- FOOTER -->
                <footer>

                    <!-- CONTAINER -->
                    <div class="container" data-animated='fadeInUp'>

                        <!-- ROW -->
                        <div class="row">

                            <div class="col-lg-2 col-md-2 col-sm-3 col-xs-6 col-ss-12 padbot30">
                                <h4>Contato</h4>
                                <div class="foot_address"><span>Explosion E-Commerce</span>Rua Zé das Colves, Guiana City</div>
                                <div class="foot_phone"><a href="#" >(11) 4002-8922</a></div>
                                <div class="foot_mail"><a href="#" >explosionecommerce@explosion.com</a></div>
                            </div>

                            <div class="col-lg-2 col-md-2 col-sm-3 col-xs-6 col-ss-12 padbot30">
                                <h4>Information</h4>
                                <ul class="foot_menu">
                                    <li><a href="#" >Sobre Nós</a></li>
                                    <li><a href="javascript:void(0);" >Política de Privacidade</a></li>
                                    <li><a href="contacts.html" >Contato</a></li>
                                </ul>
                            </div>

                            <div class="respond_clear_480"></div>

                            <div class="col-lg-4 col-md-4 col-sm-6 padbot30">
                                <h4>Sobre a Loja</h4>
                                <p>Nós somos uma loja renomada no mercado, vendemos os produtos mais diversificados. Pode confiar!</p>
                            </div>

                            <div class="respond_clear_768"></div>

                            <div class="col-lg-4 col-md-4 padbot30">
                                <h4>Boletim de Notícias</h4>
                                <form class="newsletter_form clearfix" action="javascript:void(0);" method="get">
                                    <input type="text" name="newsletter" value="Informe seu e-mail" onFocus="if (this.value == 'Enter E-mail & Get 10% off')
                                                this.value = '';" onBlur="if (this.value == '')
                                                            this.value = 'Enter E-mail & Get 10% off';" />
                                    <input class="btn newsletter_btn" type="submit" value="SIGN UP">
                                </form>

                                <h4>Nossas Redes Sociais</h4>
                                <div class="social">
                                    <a href="javascript:void(0);" ><i class="fa fa-twitter"></i></a>
                                    <a href="javascript:void(0);" ><i class="fa fa-facebook"></i></a>
                                    <a href="javascript:void(0);" ><i class="fa fa-google-plus"></i></a>
                                    <a href="javascript:void(0);" ><i class="fa fa-pinterest-square"></i></a>
                                    <a href="javascript:void(0);" ><i class="fa fa-tumblr"></i></a>
                                    <a href="javascript:void(0);" ><i class="fa fa-instagram"></i></a>
                                </div>
                            </div>
                        </div><!-- //ROW -->
                    </div><!-- //CONTAINER -->

                    <!-- COPYRIGHT -->
                    <div class="copyright">

                        <!-- CONTAINER -->
                        <div class="container clearfix">
                            <div class="foot_logo"><a href="index.jsp" ><img src="images/foot_logo.png" alt="" /></a></div>

                            <div class="copyright_inf">
                                <span>Explosion E-Commerce © 1950</span> |                                            
                                <a class="back_top" href="javascript:void(0);" >Voltar ao topo <i class="fa fa-angle-up"></i></a>
                            </div>
                        </div><!-- //CONTAINER -->
                    </div><!-- //COPYRIGHT -->
                </footer><!-- //FOOTER -->
            </div><!-- //PAGE -->
        </div>


        <!-- SCRIPTS -->
        <!--[if IE]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
        <!--[if IE]><html class="ie" lang="en"> <![endif]-->

        <script src="js/jquery.min.js" type="text/javascript"></script>
        <script src="js/bootstrap.min.js" type="text/javascript"></script>
        <script src="js/jquery.sticky.js" type="text/javascript"></script>
        <script src="js/parallax.js" type="text/javascript"></script>
        <script src="js/jquery.flexslider-min.js" type="text/javascript"></script>
        <script src="js/jquery.jcarousel.js" type="text/javascript"></script>
        <script src="js/fancySelect.js"></script>
        <script src="js/animate.js" type="text/javascript"></script>
        <script src="js/myscript.js" type="text/javascript"></script>
        <script>
                                                    if (top != self)
                                                        top.location.replace(self.location.href);
        </script>

    </body>
</html>
