
package br.com.ecomerce.controladora;

import br.com.ecomerce.dao.*;
import br.com.ecomerce.dominio.*;
import br.com.ecomerce.web.command.AlterarCommand;
import br.com.ecomerce.web.command.ConsultarCommand;
import br.com.ecomerce.web.command.ExcluirCommand;
import br.com.ecomerce.web.command.ICommand;
import br.com.ecomerce.web.command.SalvarCommand;
import br.com.ecomerce.web.command.VisualizarCommand;
import br.com.ecomerce.web.viewHelper.AnaliseGraficoVH;
import br.com.ecomerce.web.viewHelper.BoletoVH;
import br.com.ecomerce.web.viewHelper.CartaoVH;
import br.com.ecomerce.web.viewHelper.CategoriaVH;
import br.com.ecomerce.web.viewHelper.ClienteVH;
import br.com.ecomerce.web.viewHelper.DevolucaoVH;
import br.com.ecomerce.web.viewHelper.EnderecoVH;
import br.com.ecomerce.web.viewHelper.EstoqueVH;
import br.com.ecomerce.web.viewHelper.IViewHelper;
import br.com.ecomerce.web.viewHelper.MetodoEntregaVH;
import br.com.ecomerce.web.viewHelper.PedidoVH;
import br.com.ecomerce.web.viewHelper.ProdutoVH;
import br.com.ecomerce.web.viewHelper.SenhaVH;
import br.com.ecomerce.web.viewHelper.SubCategoriaVH;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
//import org.jboss.weld.servlet.SessionHolder;

public class Servlet extends HttpServlet {

    private Map<String, ICommand> commands;
    private Map<String, IViewHelper> vhs;

    public Servlet() {
        super();
        // adicionando commands ao mapa
        commands = new HashMap<String, ICommand>();
        commands.put("SALVAR", new SalvarCommand());
        commands.put("ALTERAR", new AlterarCommand());
        commands.put("CONSULTAR", new ConsultarCommand());
        commands.put("EXCLUIR", new ExcluirCommand());
        commands.put("VISUALIZAR", new VisualizarCommand());
        commands.put("LOGIN", new VisualizarCommand());

        // adicionando vhs ao mapa
        vhs = new HashMap<String, IViewHelper>();

        // Cliente
        vhs.put("/e-commerce-explosion/SalvarCliente", new ClienteVH());
        vhs.put("/e-commerce-explosion/AlterarCliente", new ClienteVH());
        vhs.put("/e-commerce-explosion/ConsultarCliente", new ClienteVH());
        vhs.put("/e-commerce-explosion/ExcluirCliente", new ClienteVH());
        vhs.put("/e-commerce-explosion/VisualizarCliente", new ClienteVH());
        vhs.put("/e-commerce-explosion/Logando", new ClienteVH());

        // Senha cliente
        vhs.put("/e-commerce-explosion/AlterarSenha", new SenhaVH());

        // Endereco
        vhs.put("/e-commerce-explosion/SalvarEndereco", new EnderecoVH());
        vhs.put("/e-commerce-explosion/AlterarEndereco", new EnderecoVH());
        vhs.put("/e-commerce-explosion/ConsultarEndereco", new EnderecoVH());
        vhs.put("/e-commerce-explosion/ExcluirEndereco", new EnderecoVH());
        vhs.put("/e-commerce-explosion/VisualizarEndereco", new EnderecoVH());

        // Cartão
        vhs.put("/e-commerce-explosion/SalvarCartao", new CartaoVH());
        vhs.put("/e-commerce-explosion/AlterarCartao", new CartaoVH());
        vhs.put("/e-commerce-explosion/ConsultarCartao", new CartaoVH());
        vhs.put("/e-commerce-explosion/ExcluirCartao", new CartaoVH());
        vhs.put("/e-commerce-explosion/VisualizarCartao", new CartaoVH());

        // Produto
        vhs.put("/e-commerce-explosion/SalvarProduto", new ProdutoVH());
        vhs.put("/e-commerce-explosion/AlterarProduto", new ProdutoVH());
        vhs.put("/e-commerce-explosion/ConsultarProduto", new ProdutoVH());
        vhs.put("/e-commerce-explosion/ExcluirProduto", new ProdutoVH());
        vhs.put("/e-commerce-explosion/VisualizarProduto", new ProdutoVH());

        // Categoria
        vhs.put("/e-commerce-explosion/SalvarCategoria", new CategoriaVH());
        vhs.put("/e-commerce-explosion/AlterarCategoria", new CategoriaVH());
        vhs.put("/e-commerce-explosion/ConsultarCategoria", new CategoriaVH());
        vhs.put("/e-commerce-explosion/ExcluirCategoria", new CategoriaVH());
        vhs.put("/e-commerce-explosion/VisualizarCategoria", new CategoriaVH());

        // SubCategoria
        vhs.put("/e-commerce-explosion/SalvarSubCategoria", new SubCategoriaVH());
        vhs.put("/e-commerce-explosion/AlterarSubCategoria", new SubCategoriaVH());
        vhs.put("/e-commerce-explosion/ConsultarSubCategoria", new SubCategoriaVH());
        vhs.put("/e-commerce-explosion/ExcluirSubCategoria", new SubCategoriaVH());
        vhs.put("/e-commerce-explosion/VisualizarSubCategoria", new SubCategoriaVH());
        
        // Boleto
        vhs.put("/e-commerce-explosion/SalvarBoleto", new BoletoVH());
        vhs.put("/e-commerce-explosion/AlterarBoleto", new BoletoVH());
        vhs.put("/e-commerce-explosion/ConsultarBoleto", new BoletoVH());
        vhs.put("/e-commerce-explosion/ExcluirBoleto", new BoletoVH());
        vhs.put("/e-commerce-explosion/VisualizarBoleto", new BoletoVH());
        
        // Pedido
        vhs.put("/e-commerce-explosion/SalvarPedido", new PedidoVH());
        vhs.put("/e-commerce-explosion/AlterarPedido", new PedidoVH());
        vhs.put("/e-commerce-explosion/ConsultarPedido", new PedidoVH());
        vhs.put("/e-commerce-explosion/ExcluirPedido", new PedidoVH());
        vhs.put("/e-commerce-explosion/VisualizarPedido", new PedidoVH());
        
        // Devolucao
        vhs.put("/e-commerce-explosion/SalvarDevolucao", new DevolucaoVH());
        vhs.put("/e-commerce-explosion/AlterarDevolucao", new DevolucaoVH());
        vhs.put("/e-commerce-explosion/ConsultarDevolucao", new DevolucaoVH());
        vhs.put("/e-commerce-explosion/ExcluirDevolucao", new DevolucaoVH());
        vhs.put("/e-commerce-explosion/VisualizarDevolucao", new DevolucaoVH());
        
        // MetodoEntrega
        vhs.put("/e-commerce-explosion/SalvarMetodoEntrega", new MetodoEntregaVH());
        vhs.put("/e-commerce-explosion/AlterarMetodoEntrega", new MetodoEntregaVH());
        vhs.put("/e-commerce-explosion/ConsultarMetodoEntrega", new MetodoEntregaVH());
        vhs.put("/e-commerce-explosion/ExcluirMetodoEntrega", new MetodoEntregaVH());
        vhs.put("/e-commerce-explosion/VisualizarMetodoEntrega", new MetodoEntregaVH());
        
        // Estoque
        vhs.put("/e-commerce-explosion/SalvarEstoque", new EstoqueVH());
        vhs.put("/e-commerce-explosion/AlterarEstoque", new EstoqueVH());
        vhs.put("/e-commerce-explosion/ConsultarEstoque", new EstoqueVH());
        vhs.put("/e-commerce-explosion/ExcluirEstoque", new EstoqueVH());
        vhs.put("/e-commerce-explosion/VisualizarEstoque", new EstoqueVH());
        
        // AnaliseGrafico
        vhs.put("/e-commerce-explosion/SalvarAnaliseGrafico", new AnaliseGraficoVH());
        vhs.put("/e-commerce-explosion/AlterarAnaliseGrafico", new AnaliseGraficoVH());
        vhs.put("/e-commerce-explosion/ConsultarAnaliseGrafico", new AnaliseGraficoVH());
        vhs.put("/e-commerce-explosion/ExcluirAnaliseGrafico", new AnaliseGraficoVH());
        vhs.put("/e-commerce-explosion/VisualizarAnaliseGrafico", new AnaliseGraficoVH());
    }

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            if (request.getParameter("logout").equals("logout")) {
                HttpSession sessao = request.getSession(false);
                sessao.invalidate();
                response.sendRedirect("login.jsp");
            }            
        } catch (Exception e) {
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String uri = request.getRequestURI();

        String OPERACAO = request.getParameter("OPERACAO");

        Resultado resultado = null;

        IViewHelper vh = vhs.get(uri);

        EntidadeDominio entidade = vh.getEntidade(request);

        resultado = commands.get(OPERACAO).executar(entidade);

        vh.setView(resultado, request, response);

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
