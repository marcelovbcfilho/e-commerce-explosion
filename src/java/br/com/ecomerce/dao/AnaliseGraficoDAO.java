/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.ecomerce.dao;

import br.com.ecomerce.dominio.AnaliseGrafico;
import br.com.ecomerce.dominio.EntidadeDominio;
import br.com.ecomerce.dominio.Produto;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Leonardo Japa
 */
public class AnaliseGraficoDAO extends AbstractDAO {

    public AnaliseGraficoDAO(Connection connection, String table, String idTable) {
        super(connection, table, idTable);
    }

    // Contrutor 2: Recebe aoenas os dados da tabela e da coluna do banco de dados.
    public AnaliseGraficoDAO(String table, String idTable) {
        super(table, idTable);
    }

    public AnaliseGraficoDAO() {
        super("tb_analise_grafico", "ang_id");
    }

    @Override
    public void salvar(EntidadeDominio entidadeDominio) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void alterar(EntidadeDominio entidadeDominio) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<EntidadeDominio> consultar(EntidadeDominio entidadeDominio) {
        List<EntidadeDominio> analises = new ArrayList<EntidadeDominio>();
        ArrayList<Produto> produtos = new ArrayList<Produto>();
        AnaliseGrafico analiseGrafico = (AnaliseGrafico) entidadeDominio;
        SimpleDateFormat dataFormatada = new SimpleDateFormat("dd-MM-yyyy");        
        ResultSet rsAnalise;

        openConnection();

        PreparedStatement pst = null;
        StringBuilder sql = new StringBuilder();

        if (analiseGrafico.getTipoAnalise() == 1) {
            sql.append("SELECT SUM(pp_pro_qtde) AS registroDado, ped_data_pagamento AS registroDia ");
            sql.append("FROM tb_pedidos INNER JOIN tb_produtos_pedidos on tb_pedidos.ped_id = tb_produtos_pedidos.pp_ped_id ");
            sql.append("INNER JOIN tb_produtos ON tb_produtos_pedidos.pp_pro_id = tb_produtos.pro_id ");
            sql.append("WHERE pp_pro_id = ? AND ped_data_pagamento ");
            sql.append("BETWEEN '" + new java.sql.Date(analiseGrafico.getDataInicio().getTime()) + "' AND '" + new java.sql.Date(analiseGrafico.getDataFim().getTime()) + "' ");
            sql.append("GROUP BY ped_data_pagamento; ");
        } else if (analiseGrafico.getTipoAnalise() == 2) {
            sql.append("SELECT SUM(pp_pro_valor*pp_pro_qtde) AS registroDado, ped_data_pagamento AS registroDia ");
            sql.append("FROM tb_pedidos INNER JOIN tb_produtos_pedidos on tb_pedidos.ped_id = tb_produtos_pedidos.pp_ped_id ");
            sql.append("INNER JOIN tb_produtos ON tb_produtos_pedidos.pp_pro_id = tb_produtos.pro_id ");
            sql.append("WHERE pp_pro_id = ? AND ped_data_pagamento ");
            sql.append("BETWEEN '" + new java.sql.Date(analiseGrafico.getDataInicio().getTime()) + "' AND '" + new java.sql.Date(analiseGrafico.getDataFim().getTime()) + "' ");
            sql.append("GROUP BY ped_data_pagamento; ");
        }

        try {

            while (analiseGrafico.getProdutos().size() > 0) {

                pst = connection.prepareStatement(sql.toString());
                pst.setInt(1, analiseGrafico.getProdutos().get(0).getId());
                rsAnalise = pst.executeQuery();
                                
                AnaliseGrafico analise = new AnaliseGrafico();
                analise.setProdutos(new ArrayList<Produto>());
                Date aux = new Date(analiseGrafico.getDataInicio().getTime());                

                analise.setPeriodos(new ArrayList<String>());
                analise.setDados(new ArrayList<String>());
                analise.setTipoAnalise(analiseGrafico.getTipoAnalise());

                while (rsAnalise.next()) {
                    while (aux.getTime() < rsAnalise.getDate("registroDia").getTime()) {
                        analise.getPeriodos().add(dataFormatada.format(aux));
                        aux.setDate(aux.getDate() + 1);
                        analise.getDados().add("0");
                    }
                    analise.getDados().add(rsAnalise.getString("registroDado"));
                    aux.setDate(aux.getDate() + 1);
                    analise.getPeriodos().add(dataFormatada.format(rsAnalise.getDate("registroDia")));
                }

                while (aux.getTime() <= analiseGrafico.getDataFim().getTime()) {
                    analise.getPeriodos().add(dataFormatada.format(aux));
                    aux.setDate(aux.getDate() + 1);
                    analise.getDados().add("0");

                }

                ResultSet rsProduto;
                PreparedStatement pstProduto = null;
                StringBuilder sqlProduto = new StringBuilder();
                sqlProduto.append("SELECT * ");
                sqlProduto.append("FROM tb_produtos");
                sqlProduto.append(" WHERE pro_id = " + analiseGrafico.getProdutos().get(0).getId());

                try {
                    pstProduto = connection.prepareStatement(sqlProduto.toString());
                    rsProduto = pstProduto.executeQuery();
                    while (rsProduto.next()) {
                        Produto produto = new Produto();
                        produto.setId(rsProduto.getInt("pro_id"));
                        produto.setNome(rsProduto.getString("pro_nome"));
                        analise.getProdutos().add(produto);
                    }

                } catch (SQLException ex2) {
                    ex2.printStackTrace();
                }

                analiseGrafico.getProdutos().remove(0);
                analises.add(analise);
            }

        } catch (SQLException ex) {
            ex.printStackTrace();

        }
        return analises;
    }

}
