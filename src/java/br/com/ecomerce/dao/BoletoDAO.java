package br.com.ecomerce.dao;

import br.com.ecomerce.dominio.EntidadeDominio;
import java.sql.Connection;
import java.util.List;

/**
 *
 * @author Marcelo Vilas Boas Correa Filho
 */
public class BoletoDAO extends AbstractDAO{

    public BoletoDAO(Connection connection, String table, String idTable) {
        super(connection, table, idTable);
    }
    
    public BoletoDAO(String table, String idTable) {
        super(table, idTable);
    }

    public BoletoDAO() {
        super("tb_boleto", "blt_id");
    }

    @Override
    public void salvar(EntidadeDominio entidadeDominio) {
        return;
    }

    @Override
    public void alterar(EntidadeDominio entidadeDominio) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<EntidadeDominio> consultar(EntidadeDominio entidadeDominio) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
