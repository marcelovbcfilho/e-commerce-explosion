package br.com.ecomerce.dao;

import br.com.ecomerce.dominio.CartaoCredito;
import br.com.ecomerce.dominio.Cliente;
import br.com.ecomerce.dominio.EntidadeDominio;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class CartaoCreditoDAO extends AbstractDAO {

    public CartaoCreditoDAO(Connection connection, String table, String idTable) {
        super(connection, table, idTable);
    }

    public CartaoCreditoDAO(String table, String idTable) {
        super(table, idTable);
    }

    public CartaoCreditoDAO() {
        super("tb_cartoes", "crt_id");
    }

    @Override
    public void salvar(EntidadeDominio entidadeDominio) {
        CartaoCredito cc = (CartaoCredito) entidadeDominio;
        if (connection == null) {
            openConnection();
        }
        PreparedStatement pst = null;
        StringBuilder sql = new StringBuilder();

        sql.append("INSERT INTO " + table + " (crt_numero_cartao, crt_bandeira, crt_nome_impresso,");
        sql.append(" crt_mes, crt_ano, crt_codigo_seguranca, crt_cli_id) ");
        sql.append(" VALUES (?, ?, ?, ?, ?, ?, ?);");
        try {
            pst = connection.prepareStatement(sql.toString(), Statement.RETURN_GENERATED_KEYS);

            pst.setString(1, cc.getNumeroCartao());
            pst.setString(2, cc.getBandeira());
            pst.setString(3, cc.getNomeImpresso());
            pst.setInt(4, Integer.valueOf(cc.getMes()));
            pst.setInt(5, cc.getAno());
            pst.setString(6, cc.getCodigoSeguranca());
            pst.setInt(7, cc.getCliente().getId());
            pst.executeUpdate();

            ResultSet rs = pst.getGeneratedKeys();
            int idCartaoCredito = 0;
            if (rs.next()) {
                idCartaoCredito = rs.getInt(1);
            }
            cc.setId(idCartaoCredito);
            connection.close();

            connection = null;
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    // Recebe um Cliente e atualiza todos os telefones que o Cliente possui.
    @Override
    public void alterar(EntidadeDominio entidadeDominio) {
        CartaoCredito cartao = (CartaoCredito) entidadeDominio;

        openConnection();

        PreparedStatement pst = null;
        StringBuilder sql = new StringBuilder();

        sql.append("UPDATE " + table + " SET crt_numero_cartao = ?, crt_bandeira = ?, crt_nome_impresso = ?,");
        sql.append(" crt_mes = ?, crt_ano = ?, crt_codigo_seguranca = ?");
        sql.append(" WHERE " + idtable + " = ?;");
        try {
            pst = connection.prepareStatement(sql.toString());

            pst.setString(1, cartao.getNumeroCartao());
            pst.setString(2, cartao.getBandeira());
            pst.setString(3, cartao.getNomeImpresso());
            pst.setInt(4, Integer.valueOf(cartao.getMes()));
            pst.setInt(5, cartao.getAno());
            pst.setString(6, cartao.getCodigoSeguranca());

            pst.setInt(7, cartao.getId());
            pst.executeUpdate();

            connection.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    // Consulta um cartai com base no ID de cartao recebido por parametro
    @Override
    public List<EntidadeDominio> consultar(EntidadeDominio entidadeDominio) {
        CartaoCredito crt = (CartaoCredito) entidadeDominio;
        List<EntidadeDominio> cartoes = new ArrayList<EntidadeDominio>();
        CartaoCredito cartao;
        ResultSet rs;

        openConnection();

        PreparedStatement pst = null;
        StringBuilder sql = new StringBuilder();

        if (crt.getId() == 0 && crt.getCliente().getId() == 0) {
            sql.append("SELECT * ");
            sql.append("FROM " + table);
        } else if (crt.getId() != 0 && crt.getCliente().getId() == 0) {
            sql.append("SELECT * ");
            sql.append("FROM " + table);
            sql.append(" WHERE " + idtable + " = " + crt.getId());
        } else if (crt.getId() == 0 && crt.getCliente().getId() != 0) {
            sql.append("SELECT * ");
            sql.append("FROM " + table);
            sql.append(" WHERE crt_cli_id = " + crt.getCliente().getId());
        } else if (crt.getId() != 0 && crt.getCliente().getId() != 0) {
            sql.append("SELECT * ");
            sql.append("FROM " + table);
            sql.append(" WHERE " + idtable + " = " + crt.getId());
        }

        try {
            pst = connection.prepareStatement(sql.toString());

            rs = pst.executeQuery();

            while (rs.next()) {
                String mes="";
                if(rs.getInt("crt_mes") < 10)
                {
                    mes = "0"+rs.getInt("crt_mes");
                }else{
                    mes = rs.getString("crt_mes");
                }
                
                cartao = new CartaoCredito(rs.getString("crt_numero_cartao"), rs.getString("crt_bandeira"), rs.getString("crt_nome_impresso"), mes, rs.getInt("crt_ano"), rs.getString("crt_codigo_seguranca"));
                Cliente cliente = new Cliente();
                cliente.setId(rs.getInt("crt_cli_id"));
                cartao.setCliente(cliente);
                cartao.setId(rs.getInt("crt_id"));
                cartoes.add(cartao);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return cartoes;
    }
}
