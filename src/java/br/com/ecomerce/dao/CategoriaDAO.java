package br.com.ecomerce.dao;

import br.com.ecomerce.dominio.Categoria;
import br.com.ecomerce.dominio.EntidadeDominio;
import br.com.ecomerce.dominio.SubCategoria;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.sql.Statement;

public class CategoriaDAO extends AbstractDAO {

    public CategoriaDAO(Connection connection, String table, String idTable) {
        super(connection, table, idTable);
    }

    public CategoriaDAO(String table, String idTable) {
        super(table, idTable);
    }

    public CategoriaDAO() {
        super("tb_categorias", "cat_id");
    }

    @Override
    public void salvar(EntidadeDominio entidadeDominio) {
        Categoria cat = (Categoria) entidadeDominio;
        if (connection == null) {
            openConnection();
        }
        PreparedStatement pst = null;
        StringBuilder sql = new StringBuilder();

        sql.append("INSERT INTO " + table + " (cat_nome)");
        sql.append(" VALUES (?);");
        try {
            pst = connection.prepareStatement(sql.toString(), Statement.RETURN_GENERATED_KEYS);

            pst.setString(1, cat.getNome());
            pst.executeUpdate();

            ResultSet rs = pst.getGeneratedKeys();
            int idCategoria = 0;
            if (rs.next()) {
                idCategoria = rs.getInt(1);
            }
            cat.setId(idCategoria);
            connection.close();

            connection = null;
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void alterar(EntidadeDominio entidadeDominio) {
        Categoria categoria = (Categoria) entidadeDominio;

        openConnection();

        PreparedStatement pst = null;
        StringBuilder sql = new StringBuilder();

        sql.append("UPDATE " + table + " SET cat_nome = ?");
        sql.append(" WHERE " + idtable + " = ?;");
        try {
            pst = connection.prepareStatement(sql.toString());

            pst.setString(1, categoria.getNome());
            pst.setInt(2, categoria.getId());
            pst.executeUpdate();

            connection.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public List<EntidadeDominio> consultar(EntidadeDominio entidadeDominio) {
        Categoria catg = (Categoria) entidadeDominio;
        List<EntidadeDominio> categorias = new ArrayList<EntidadeDominio>();
        Categoria categoria;
        ResultSet rs;

        openConnection();

        PreparedStatement pst = null;
        StringBuilder sql = new StringBuilder();
        if (catg.getId() == 0) {
            sql.append("SELECT * ");
            sql.append("FROM " + table);
        }else 
        {
            sql.append("SELECT * ");
            sql.append("FROM " + table);
            sql.append(" WHERE " + idtable + " = " + catg.getId());
        }

        try {
            pst = connection.prepareStatement(sql.toString());

            rs = pst.executeQuery();

            while (rs.next()) {
                categoria = new Categoria(rs.getString("cat_nome"));

                categoria.setId(rs.getInt("cat_id"));

                StringBuilder sqlSub = new StringBuilder();
                ResultSet rsSub;
                
                sqlSub.append("SELECT * ");
                sqlSub.append("FROM tb_sub_categorias");
                sqlSub.append(" WHERE sub_cat_id = " + categoria.getId());                

                try {
                    pst = connection.prepareStatement(sqlSub.toString());
                    
                    rsSub = pst.executeQuery();

                    while (rsSub.next()) {
                        SubCategoria sub = new SubCategoria();
                        sub.setId(rsSub.getInt("sub_id"));
                        sub.setNome(rsSub.getString("sub_nome"));                        
                        
                        categoria.getSubCategorias().add(sub);
                    }
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
                
                
                categorias.add(categoria);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return categorias;
    }

}
