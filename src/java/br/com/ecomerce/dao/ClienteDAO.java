package br.com.ecomerce.dao;

import br.com.ecomerce.dao.AbstractDAO;
import br.com.ecomerce.dominio.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;


public class ClienteDAO extends AbstractDAO {

    // Contrutor 1: Recebe uma conexão já aberta e os dados da tabela e coluna de banco de dados.
    public ClienteDAO(Connection connection, String table, String idTable) {
        super(connection, table, idTable);
    }

    // Contrutor 2: Recebe aoenas os dados da tabela e da coluna do banco de dados.
    public ClienteDAO(String table, String idTable) {
        super(table, idTable);
    }

    public ClienteDAO() {
        super("tb_cliente", "cli_id");
    }

    // Salva os dados de cliente.
    @Override
    public void salvar(EntidadeDominio entidadeDominio) {
        openConnection();

        PreparedStatement pst = null;
        Cliente cliente = (Cliente) entidadeDominio;
        EnderecoDAO enderecosDAO = new EnderecoDAO();
        CartaoCreditoDAO cartoesDAO = new CartaoCreditoDAO();
        StringBuilder sql = new StringBuilder();

        sql.append("INSERT INTO " + table + "(cli_nome_completo, cli_cpf, ");
        sql.append("cli_rg, cli_data_nascimento, cli_email, cli_senha, cli_status, cli_carteira, cli_admin) ");
        sql.append("VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)");

        try {
            pst = connection.prepareStatement(sql.toString(), Statement.RETURN_GENERATED_KEYS);		// retorna as chaves primárias geradas
            pst.setString(1, cliente.getNomeCompleto());
            pst.setString(2, cliente.getCpf());
            pst.setString(3, cliente.getRg());
            pst.setDate(4, new java.sql.Date(cliente.getDataNascimento().getTime()));
            pst.setString(5, cliente.getEmail());
            pst.setString(6, cliente.getSenha().getSenha());
            pst.setString(7, cliente.getStatus());
            pst.setDouble(8, cliente.getCarteira());
            pst.setInt(9, 0);
            pst.executeUpdate();

            ResultSet rs = pst.getGeneratedKeys();  // Gera os id automaticamente
            int idCliente = 0;
            if (rs.next()) {
                idCliente = rs.getInt(1);
            }
            cliente.setId(idCliente);

            for (Endereco endereco : cliente.getEnderecos()) {
                endereco.setCliente(cliente);
                enderecosDAO.salvar(endereco);
            }
            // Fecha a conexão
            connection.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void alterar(EntidadeDominio entidadeDominio) {
        Cliente cliente;
        openConnection();
        try {
            cliente = (Cliente) entidadeDominio;
        } catch (ClassCastException e) {
            Senha senha = (Senha) entidadeDominio;
            cliente = new Cliente();
            cliente.setSenha(senha);
            cliente.setId(senha.getId());
            cliente.setStatus("ATIVO");
        }
        
        PreparedStatement pst = null;
        StringBuilder sql = new StringBuilder();

        // Ativa o cliente
        if (cliente.getStatus().trim().equals("INATIVO")) {
            sql.append("UPDATE " + table + " SET ");
            sql.append("cli_status = ? ");
            sql.append("WHERE " + idtable + " = " + cliente.getId());

            try {
                pst = connection.prepareStatement(sql.toString());

                pst.setString(1, cliente.getStatus());

                pst.executeUpdate();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }

            // Altera a senha do cliente
        } else if (cliente.getSenha().getSenha() != null) {
            sql.append("UPDATE " + table + " SET ");
            sql.append("cli_senha = ? ");
            sql.append("WHERE " + idtable + " = " + cliente.getId());

            try {
                pst = connection.prepareStatement(sql.toString());

                pst.setString(1, cliente.getSenha().getSenha());

                pst.executeUpdate();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }

            // Altera todos os dados de cliente
        } else {
            sql.append("UPDATE " + table + " SET ");
            sql.append("cli_nome_completo=?, ");
            sql.append("cli_cpf=?, ");
            sql.append("cli_rg=?, ");
            sql.append("cli_data_nascimento=?, ");
            sql.append("cli_email=?, ");
            sql.append("cli_status=? ");
            sql.append("WHERE " + idtable + " = " + cliente.getId() + ";");

            try {
                pst = connection.prepareStatement(sql.toString());
                pst.setString(1, cliente.getNomeCompleto());
                pst.setString(2, cliente.getCpf());
                pst.setString(3, cliente.getRg());
                pst.setDate(4, new java.sql.Date(cliente.getDataNascimento().getTime()));
                pst.setString(5, cliente.getEmail());
                pst.setString(6, cliente.getStatus());
                pst.executeUpdate();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        openConnection();

    }

    @Override
    public List<EntidadeDominio> consultar(EntidadeDominio entidadeDominio) {
        List<EntidadeDominio> clientes = new ArrayList<EntidadeDominio>();
        Cliente cliente = (Cliente) entidadeDominio;
        EnderecoDAO enderecosDAO = new EnderecoDAO();
        CartaoCreditoDAO cartoesDAO = new CartaoCreditoDAO();

        ResultSet rs;

        openConnection();

        PreparedStatement pst = null;
        StringBuilder sql = new StringBuilder();

        if (cliente.getSenha() != null && cliente.getEmail() != null) {
            sql.append("SELECT * ");
            sql.append("FROM " + table);
            sql.append(" WHERE cli_email = '" + cliente.getEmail() + "' AND cli_senha = '" + cliente.getSenha().getSenha() + "';");
        } else if (cliente.getId() == 0 && cliente.getNomeCompleto().equals("")) {
            sql.append("SELECT * ");
            sql.append("FROM " + table);
        } else if (cliente.getId() != 0 && cliente.getNomeCompleto().equals("")) {
            sql.append("SELECT * ");
            sql.append("FROM " + table);
            sql.append(" WHERE " + idtable + " = " + cliente.getId());
        } else if(cliente.getNomeCompleto().equals("ValidaEmail"))
        {
            sql.append("SELECT * ");
            sql.append("FROM " + table);
            sql.append(" WHERE cli_email = '" + cliente.getEmail() + "';");
        }
        else if (cliente.getId() == 0 && !cliente.getNomeCompleto().equals("")) {
            sql.append("SELECT * ");
            sql.append("FROM " + table);
            sql.append(" WHERE cli_nome like '%" + cliente.getNomeCompleto() + "%'");
        } 
        try {
            pst = connection.prepareStatement(sql.toString());

            rs = pst.executeQuery();

            cliente = null;

            while (rs.next()) {
                Senha senha = new Senha(rs.getString("cli_senha"));
                cliente = new Cliente(rs.getString("cli_nome_completo"), rs.getString("cli_cpf"), rs.getString("cli_rg"), new java.util.Date(rs.getDate("cli_data_nascimento").getTime()), rs.getString("cli_email"), senha, rs.getString("cli_status"), rs.getInt("cli_admin"));
                cliente.setCarteira(rs.getDouble("cli_carteira"));
                cliente.setId(rs.getInt("cli_id"));
                ArrayList<Endereco> enderecos = new ArrayList<Endereco>();
                Endereco endereco = new Endereco();
                endereco.setCliente(cliente);
                for (EntidadeDominio e : enderecosDAO.consultar(endereco)) {
                    enderecos.add((Endereco) e);
                }

                ArrayList<CartaoCredito> cartoes = new ArrayList<CartaoCredito>();
                CartaoCredito cartao = new CartaoCredito();
                cartao.setCliente(cliente);
                for (EntidadeDominio c : cartoesDAO.consultar(cartao)) {
                    cartoes.add((CartaoCredito) c);
                }

                // Salvando as consultas de endereco e cartao no cliente
                cliente.setEnderecos(enderecos);
                cliente.setCartoesCredito(cartoes);

                clientes.add(cliente);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return clientes;
    }

    // Inativa o cliente
    @Override
    public void excluir(EntidadeDominio entidadeDominio) {
        Cliente cliente = (Cliente) entidadeDominio;
        cliente.setStatus("INATIVO");
        alterar(cliente);
    }
}
