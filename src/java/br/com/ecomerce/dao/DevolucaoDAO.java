package br.com.ecomerce.dao;

import br.com.ecomerce.dominio.Cliente;
import br.com.ecomerce.dominio.Devolucao;
import br.com.ecomerce.dominio.EntidadeDominio;
import br.com.ecomerce.dominio.Produto;
import br.com.ecomerce.dominio.ProdutosPedido;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class DevolucaoDAO extends AbstractDAO {

    public DevolucaoDAO(Connection connection, String table, String idTable) {
        super(connection, table, idTable);
    }

    public DevolucaoDAO(String table, String idTable) {
        super(table, idTable);
    }

    public DevolucaoDAO() {
        super("tb_devolucoes", "dev_id");
    }

    @Override
    public void salvar(EntidadeDominio entidadeDominio) {
        openConnection();

        PreparedStatement pst = null;
        Devolucao devolucao = (Devolucao) entidadeDominio;
        ProdutoDAO produtoDAO = new ProdutoDAO();
        StringBuilder sql = new StringBuilder();

        // Consulta todos os produtos a serem devolvidos para atualizar o estoque
        for (Produto prod : devolucao.getProdutos()) {
            sql.append("UPDATE tb_produtos_pedidos SET pp_pro_qtde=? WHERE pp_pro_id=? AND pp_ped_id=?");
            try {
                pst = connection.prepareStatement(sql.toString(), Statement.RETURN_GENERATED_KEYS);		// retorna as chaves primárias geradas
                pst.setInt(1, prod.getQuantidadeEstoque());
                pst.setInt(2, prod.getId());
                pst.setInt(3, Integer.valueOf(devolucao.getNumeroPedidoDevolucao()));

                pst.executeUpdate();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        sql = new StringBuilder();

        sql.append("INSERT INTO " + table + "(dev_status, dev_ped_id, dev_data_solicitacao, dev_cli_id) ");
        sql.append("VALUES (?, ?, ?, ?)");

        try {
            pst = connection.prepareStatement(sql.toString(), Statement.RETURN_GENERATED_KEYS);		// retorna as chaves primárias geradas
            pst.setString(1, devolucao.getStatus());
            pst.setString(2, devolucao.getNumeroPedidoDevolucao());
            pst.setDate(3, new java.sql.Date(devolucao.getDataSolicitacao().getTime()));
            pst.setInt(4, devolucao.getCliente().getId());

            pst.executeUpdate();

            ResultSet rs = pst.getGeneratedKeys();  // Gera os id automaticamente
            int idDevolucao = 0;
            if (rs.next()) {
                idDevolucao = rs.getInt(1);
            }
            devolucao.setId(idDevolucao);

            for (Produto produto : devolucao.getProdutos()) {
                sql = new StringBuilder();
                sql.append("INSERT INTO tb_produtos_devolucao (pdv_pro_id, pdv_dev_id, pdv_pro_valor, pdv_pro_qtde)");
                sql.append(" VALUES (?, ?, ?, ?)");
                try {
                    pst = connection.prepareStatement(sql.toString());		// retorna as chaves primárias geradas
                    pst.setInt(1, produto.getId());
                    pst.setInt(2, idDevolucao);
                    pst.setDouble(3, produto.getPreco());
                    pst.setInt(4, produto.getQuantidadeProdutoCarrinho());

                    pst.executeUpdate();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            // Fecha a conexão
            connection.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void alterar(EntidadeDominio entidadeDominio) {
        Devolucao devolucao = (Devolucao) entidadeDominio;
        
        String status = devolucao.getStatus();
        devolucao = (Devolucao) consultar(devolucao).get(0);
        devolucao.setStatus(status);
        double valorCarteira = 0;
        openConnection();

        PreparedStatement pst = null;
        StringBuilder sql = new StringBuilder();

        sql.append("UPDATE " + table + " SET dev_status = ?");
        sql.append(" WHERE " + idtable + " = ?;");
        try {
            pst = connection.prepareStatement(sql.toString());

            pst.setString(1, devolucao.getStatus());
            pst.setInt(2, devolucao.getId());
            pst.executeUpdate();

            // Altera o status de cada produto do pedido para não ser possível solicitar a devolução 2 vezes
            PreparedStatement pstPP;
            for (Produto prdDevolucao : devolucao.getProdutos()) {
                pstPP = null;
                sql = new StringBuilder();

                sql.append("UPDATE tb_estoque SET est_quantidade_estoque =est_quantidade_estoque + ?");
                sql.append(" WHERE est_pro_id = ?;");
                try {
                    pstPP = connection.prepareStatement(sql.toString());

                    pstPP.setInt(1, prdDevolucao.getQuantidadeProdutoCarrinho());
                    pstPP.setInt(2, prdDevolucao.getId());
                    pstPP.executeUpdate();

                    valorCarteira += prdDevolucao.getPreco();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }

                pstPP = null;
                sql = new StringBuilder();

                sql.append("UPDATE tb_produtos_pedidos SET pp_status = ?");
                sql.append(" WHERE pp_ped_id  = ? AND pp_pro_id = ?;");
                try {
                    pstPP = connection.prepareStatement(sql.toString());

                    pstPP.setString(1, "TROCA");
                    pstPP.setInt(2, Integer.valueOf(devolucao.getNumeroPedidoDevolucao()));
                    pstPP.setInt(3, prdDevolucao.getId());
                    pstPP.executeUpdate();

                    valorCarteira += prdDevolucao.getPreco();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }

            // Atualiza o valor da carteira do usuaário
            pstPP = null;
            sql = new StringBuilder();

            sql.append("UPDATE tb_cliente SET cli_carteira = ?");
            sql.append(" WHERE cli_id = " + devolucao.getCliente().getId() + ";");
            try {
                pstPP = connection.prepareStatement(sql.toString());

                pstPP.setDouble(1, devolucao.getCliente().getCarteira() + valorCarteira);
                pstPP.executeUpdate();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }

            connection.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public List<EntidadeDominio> consultar(EntidadeDominio entidadeDominio) {
        Devolucao devolucao = (Devolucao) entidadeDominio;
        List<EntidadeDominio> devolucoes = new ArrayList<EntidadeDominio>();
        ProdutoDAO produtoDAO = new ProdutoDAO();
        ClienteDAO clienteDAO = new ClienteDAO();
        Cliente cliente = new Cliente();
        ArrayList<Produto> produtos;
        Devolucao devolucaoTemp;
        Produto produto;
        ResultSet rs;

        openConnection();

        PreparedStatement pst = null;
        StringBuilder sql = new StringBuilder();
        if (devolucao.getCliente() == null) {
            if (devolucao.getId() == 0) {
                sql.append("SELECT * ");
                sql.append("FROM " + table);
            } else {
                sql.append("SELECT * ");
                sql.append("FROM " + table);
                sql.append(" WHERE " + idtable + " = " + devolucao.getId());
            }
        } else {
            sql.append("SELECT * ");
            sql.append("FROM " + table);
            sql.append(" WHERE dev_cli_id = " + devolucao.getCliente().getId());
        }

        try {
            pst = connection.prepareStatement(sql.toString());

            rs = pst.executeQuery();

            while (rs.next()) {
                devolucao = new Devolucao();
                produtos = new ArrayList<Produto>();

                devolucao.setId(rs.getInt("dev_id"));
                devolucao.setStatus(rs.getString("dev_status"));
                devolucao.setNumeroPedidoDevolucao(rs.getString("dev_ped_id"));
                devolucao.setDataSolicitacao(rs.getDate("dev_data_solicitacao"));

                if (devolucao.getCliente() == null) {
                    cliente.setId(rs.getInt("dev_cli_id"));
                    cliente.setNomeCompleto("");
                    cliente = (Cliente) clienteDAO.consultar(cliente).get(0);
                    devolucao.setCliente(cliente);
                }

                // Executa consulta sobre os produtos da devolução
                ResultSet rsProduto;

                openConnection();

                pst = null;
                sql = new StringBuilder();
                sql.append("SELECT * ");
                sql.append("FROM tb_produtos_devolucao");
                sql.append(" WHERE pdv_dev_id = " + devolucao.getId());

                try {
                    pst = connection.prepareStatement(sql.toString());

                    rsProduto = pst.executeQuery();

                    while (rsProduto.next()) {
                        produto = new Produto();

                        produto.setId(rsProduto.getInt("pdv_pro_id"));
                        produto = (Produto) produtoDAO.consultar(produto).get(0);
                        produto.setPreco((float) rsProduto.getDouble("pdv_pro_valor"));
                        produto.setQuantidadeProdutoCarrinho(rsProduto.getInt("pdv_pro_qtde"));

                        produtos.add(produto);
                    }
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }

                devolucao.setProdutos(produtos);
                devolucoes.add(devolucao);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return devolucoes;
    }

}
