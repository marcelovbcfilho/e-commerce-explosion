package br.com.ecomerce.dao;

import br.com.ecomerce.dominio.Cliente;
import br.com.ecomerce.dominio.Endereco;
import br.com.ecomerce.dominio.EntidadeDominio;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;


public class EnderecoDAO extends AbstractDAO{

    public EnderecoDAO(Connection connection, String table, String idTable) {
        super(connection, table, idTable);
    }

    public EnderecoDAO(String table, String idTable) {
        super(table, idTable);
    }

    public EnderecoDAO() {
        super("tb_endereco", "end_id");
    }

    // Recebe uma EntidadeDominio cria um Cliente e salva todos os enderecos que esse Cliente tem
    @Override
    public void salvar(EntidadeDominio entidadeDominio) {
        Endereco endereco = (Endereco)entidadeDominio;
        openConnection();
        
        PreparedStatement pst = null;
        StringBuilder sql = new StringBuilder();

        sql.append("INSERT INTO " + table + "(end_identificacao, end_logradouro, end_numero, end_cep,");
        sql.append(" end_complemento, end_bairro, end_cidade, end_estado, end_cli_id) ");
        sql.append(" VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)");	
        try {
            pst = connection.prepareStatement(sql.toString(), Statement.RETURN_GENERATED_KEYS);

            pst.setString(1, endereco.getIdentificacao());
            pst.setString(2, endereco.getLogradouro());
            pst.setString(3, endereco.getNumeroEndereco());
            pst.setString(4, endereco.getCep());
            pst.setString(5, endereco.getComplemento());
            pst.setString(6, endereco.getBairro());
            pst.setString(7, endereco.getCidade());
            pst.setString(8, endereco.getSiglaEstado());
            pst.setInt(9, endereco.getCliente().getId());
            pst.executeUpdate();		

            ResultSet rs = pst.getGeneratedKeys();
            int idEndereco = 0;
            if(rs.next()){
                idEndereco = rs.getInt(1);
            }
            endereco.setId(idEndereco);
            connection.close();
            
            connection = null;
        } catch (SQLException ex) {
            ex.printStackTrace();	
        }
    }

    // Altera um endereco com base no endereco recebido por parametro
    @Override
    public void alterar(EntidadeDominio entidadeDominio) {
        Endereco endereco = (Endereco) entidadeDominio;
        
        openConnection();
        
        PreparedStatement pst = null;
        StringBuilder sql = new StringBuilder();

        sql.append("UPDATE " + table + " SET end_identificacao=?, end_logradouro = ?, end_numero = ?, end_cep = ?,");
        sql.append(" end_complemento = ?, end_bairro = ?, end_cidade = ?, end_estado = ?");
        sql.append(" WHERE " + idtable + " = " + endereco.getId() + ";");
        try {
            pst = connection.prepareStatement(sql.toString());

            pst.setString(1, endereco.getIdentificacao());
            pst.setString(2, endereco.getLogradouro());
            pst.setString(3, endereco.getNumeroEndereco());
            pst.setString(4, endereco.getCep());
            pst.setString(5, endereco.getComplemento());
            pst.setString(6, endereco.getBairro());
            pst.setString(7, endereco.getCidade());
            pst.setString(8, endereco.getSiglaEstado());
            
            pst.executeUpdate();		

            connection.close();					
        } catch (SQLException ex) {
                ex.printStackTrace();	
        }
    }

    // Consulta enderecos
    @Override
    public List<EntidadeDominio> consultar(EntidadeDominio entidadeDominio) {
        Endereco end = (Endereco) entidadeDominio;
        List<EntidadeDominio> enderecos = new ArrayList<EntidadeDominio>();
        Endereco endereco;
        ResultSet rs;
        
        openConnection();
        
        PreparedStatement pst = null;
        StringBuilder sql = new StringBuilder();
        
        if(end.getId() == 0 && end.getCliente().getId() == 0){
            sql.append("SELECT * ");
            sql.append("FROM " + table);
        }else if(end.getId() != 0 && end.getCliente().getId() == 0){
            sql.append("SELECT * ");
            sql.append("FROM " + table);
            sql.append(" WHERE " + idtable + " = " + end.getId());
        }else if(end.getId() == 0 && end.getCliente().getId() != 0){
            sql.append("SELECT * ");
            sql.append("FROM " + table);
            sql.append(" WHERE end_cli_id = " + end.getCliente().getId());
        }else if(end.getId() != 0 && end.getCliente().getId() != 0){
            sql.append("SELECT * ");
            sql.append("FROM " + table);
            sql.append(" WHERE " + idtable + " = " + end.getId());
        }
        
        try {
            pst = connection.prepareStatement(sql.toString());
            
            rs = pst.executeQuery();		

            while(rs.next()){
                endereco = new Endereco(rs.getString("end_identificacao"), rs.getString("end_logradouro"), rs.getString("end_numero"), rs.getString("end_complemento"), rs.getString("end_bairro"), rs.getString("end_cidade"), rs.getString("end_estado"), rs.getString("end_cep"));
                Cliente cliente = new Cliente();
                cliente.setId(rs.getInt("end_cli_id"));
                endereco.setCliente(cliente);
                endereco.setId(rs.getInt("end_id"));
                enderecos.add(endereco);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();	
        }
        return enderecos;
    }
}
