package br.com.ecomerce.dao;

import br.com.ecomerce.dominio.Categoria;
import br.com.ecomerce.dominio.EntidadeDominio;
import br.com.ecomerce.dominio.Estoque;
import br.com.ecomerce.dominio.Produto;
import br.com.ecomerce.dominio.SubCategoria;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class EstoqueDAO extends AbstractDAO {

    public EstoqueDAO(Connection connection, String table, String idTable) {
        super(connection, table, idTable);
    }

    public EstoqueDAO(String table, String idTable) {
        super(table, idTable);
    }

    public EstoqueDAO() {
        super("tb_estoque", "est_id");
    }

    @Override
    public void salvar(EntidadeDominio entidadeDominio) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void alterar(EntidadeDominio entidadeDominio) {
        Estoque estoque = (Estoque) entidadeDominio;

        openConnection();

        PreparedStatement pst = null;
        StringBuilder sql = new StringBuilder();

        if (estoque.getProduto().getNome().equals("STATUS")) {
            sql.append("UPDATE " + table + " SET est_status = ?");
            sql.append(" WHERE " + idtable + " = ?;");
        } else {
            sql.append("UPDATE " + table + " SET est_quantidade_estoque = ?");
            sql.append(" WHERE " + idtable + " = ?;");
        }
        try {
            pst = connection.prepareStatement(sql.toString());

            if (estoque.getProduto().getNome().equals("STATUS")) {
                pst.setBoolean(1, estoque.isStatus());
                pst.setInt(2, estoque.getId());
            }else{
                pst.setInt(1, estoque.getQuantidadeEstoque());
                pst.setInt(2, estoque.getId());
            }
            pst.executeUpdate();

            connection.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public List<EntidadeDominio> consultar(EntidadeDominio entidadeDominio) {
        Estoque estoque = (Estoque) entidadeDominio;
        List<EntidadeDominio> estoques = new ArrayList<EntidadeDominio>();

        // Objetos utlizados
        Produto produto;
        SubCategoria sub;
        Categoria categoria;

        ResultSet rs;

        openConnection();

        PreparedStatement pst = null;
        StringBuilder sql = new StringBuilder();

        if (estoque.getProduto().getSubCategoria().getCategoria().getId() != 0) {
            sql.append("SELECT * FROM " + table);
            sql.append(" INNER JOIN tb_produtos ON tb_estoque.est_pro_id = tb_produtos.pro_id");
            sql.append(" INNER JOIN tb_sub_categorias ON tb_produtos.pro_sub_id = tb_sub_categorias.sub_id");
            sql.append(" INNER JOIN tb_categorias ON tb_sub_categorias.sub_cat_id = tb_categorias.cat_id");
            sql.append(" WHERE cat_id = " + estoque.getProduto().getSubCategoria().getCategoria().getId() + " AND est_status = true");
            estoques.add(estoque);
        } else if (estoque.getProduto().getSubCategoria().getId() != 0) {
            sql.append("SELECT * FROM " + table);
            sql.append(" INNER JOIN tb_produtos ON tb_estoque.est_pro_id = tb_produtos.pro_id");
            sql.append(" INNER JOIN tb_sub_categorias ON tb_produtos.pro_sub_id = tb_sub_categorias.sub_id");
            sql.append(" INNER JOIN tb_categorias ON tb_sub_categorias.sub_cat_id = tb_categorias.cat_id");
            sql.append(" WHERE sub_id = " + estoque.getProduto().getSubCategoria().getId() + " AND est_status = true");
            estoques.add(estoque);
        } else if (estoque.getProduto().getId() != 0) {
            sql.append("SELECT * FROM " + table);
            sql.append(" INNER JOIN tb_produtos ON tb_estoque.est_pro_id = tb_produtos.pro_id");
            sql.append(" INNER JOIN tb_sub_categorias ON tb_produtos.pro_sub_id = tb_sub_categorias.sub_id");
            sql.append(" INNER JOIN tb_categorias ON tb_sub_categorias.sub_cat_id = tb_categorias.cat_id");
            sql.append(" WHERE est_pro_id = " + estoque.getProduto().getId());
        } else {
            sql.append("SELECT * FROM " + table);
            sql.append(" INNER JOIN tb_produtos ON tb_estoque.est_pro_id = tb_produtos.pro_id");
            sql.append(" INNER JOIN tb_sub_categorias ON tb_produtos.pro_sub_id = tb_sub_categorias.sub_id");
            sql.append(" INNER JOIN tb_categorias ON tb_sub_categorias.sub_cat_id = tb_categorias.cat_id");
            sql.append(" ORDER BY pro_nome DESC");

        }

        try {
            pst = connection.prepareStatement(sql.toString());

            rs = pst.executeQuery();

            while (rs.next()) {
                categoria = new Categoria(rs.getString("cat_nome"));
                categoria.setId(rs.getInt("cat_id"));

                sub = new SubCategoria(rs.getString("sub_nome"));
                sub.setId(rs.getInt("sub_id"));
                sub.setCategoria(categoria);

                produto = new Produto(rs.getString("pro_nome"), rs.getString("pro_descricao"), rs.getFloat("pro_peso"), rs.getFloat("pro_preco"), rs.getString("pro_endereco_imagem"), rs.getString("pro_endereco_imagem_lado"), rs.getString("pro_endereco_imagem_costas"), sub);
                produto.setId(rs.getInt("pro_id"));

                estoque = new Estoque(produto, rs.getInt("est_quantidade_estoque"));
                estoque.setId(rs.getInt("est_id"));
                estoque.setStatus(rs.getBoolean("est_status"));
                estoques.add(estoque);
            }

        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return estoques;
    }

}
