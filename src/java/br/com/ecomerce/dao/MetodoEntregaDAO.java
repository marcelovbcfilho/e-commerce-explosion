package br.com.ecomerce.dao;

import br.com.ecomerce.dominio.EntidadeDominio;
import br.com.ecomerce.dominio.MetodoEntrega;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class MetodoEntregaDAO extends AbstractDAO{

    public MetodoEntregaDAO(Connection connection, String table, String idTable) {
        super(connection, table, idTable);
    }
    
    // Contrutor 2: Recebe aoenas os dados da tabela e da coluna do banco de dados.
    public MetodoEntregaDAO(String table, String idTable) {
        super(table, idTable);
    }

    public MetodoEntregaDAO() {
        super("tb_metodos_entrega", "mte_id");
    }

    @Override
    public void salvar(EntidadeDominio entidadeDominio) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void alterar(EntidadeDominio entidadeDominio) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<EntidadeDominio> consultar(EntidadeDominio entidadeDominio) {
        MetodoEntrega metodoEntrega = (MetodoEntrega) entidadeDominio;
        List<EntidadeDominio> metodosEntrega = new ArrayList<EntidadeDominio>();
        MetodoEntrega met = new MetodoEntrega();
        ResultSet rs;

        openConnection();

        PreparedStatement pst = null;
        StringBuilder sql = new StringBuilder();
        if (metodoEntrega.getId() == 0) {
            sql.append("SELECT * ");
            sql.append("FROM " + table);
        }else 
        {
            sql.append("SELECT * ");
            sql.append("FROM " + table);
            sql.append(" WHERE " + idtable + " = " + metodoEntrega.getId());
        }

        try {
            pst = connection.prepareStatement(sql.toString());

            rs = pst.executeQuery();
            
            while (rs.next()) {
                met = new MetodoEntrega(rs.getString("mte_nome"), rs.getDouble("mte_valor"));

                met.setId(rs.getInt("mte_id"));
                metodosEntrega.add(met);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return metodosEntrega;

    }
    
}
