package br.com.ecomerce.dao;

import br.com.ecomerce.dominio.CartaoCredito;
import br.com.ecomerce.dominio.CartaoCreditoPedido;
import br.com.ecomerce.dominio.Cliente;
import br.com.ecomerce.dominio.Endereco;
import br.com.ecomerce.dominio.EntidadeDominio;
import br.com.ecomerce.dominio.MetodoEntrega;
import br.com.ecomerce.dominio.Pedido;
import br.com.ecomerce.dominio.Produto;
import br.com.ecomerce.dominio.ProdutosPedido;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class PedidoDAO extends AbstractDAO {

    public PedidoDAO(Connection connection, String table, String idTable) {
        super(connection, table, idTable);
    }

    // Contrutor 2: Recebe aoenas os dados da tabela e da coluna do banco de dados.
    public PedidoDAO(String table, String idTable) {
        super(table, idTable);
    }

    public PedidoDAO() {
        super("tb_pedidos", "ped_id");
    }

    @Override
    public void salvar(EntidadeDominio entidadeDominio) {
        openConnection();

        PreparedStatement pst = null;
        Pedido pedido = (Pedido) entidadeDominio;
        ProdutoDAO produtoDAO = new ProdutoDAO();
        StringBuilder sql = new StringBuilder();

        sql.append("INSERT INTO " + table + "(ped_metodo_entrega, ped_boleto, ");
        sql.append("ped_valor_total, ped_status, ped_data_pagamento, ped_data_pedido, ped_cli_id, ped_end_id) ");
        sql.append("VALUES (?, ?, ?, ?, ?, ?, ?, ?)");

        try {
            pst = connection.prepareStatement(sql.toString(), Statement.RETURN_GENERATED_KEYS);		// retorna as chaves primárias geradas
            pst.setInt(1, pedido.getMetodoEntrega().getId());
            if (pedido.getTipoPagamento().trim().equals("BOLETO")) {
                pst.setString(2, "1000010000010000");

            } else {
                pst.setString(2, "0");
                // pst.setInt(3, pedido.getCartao().getId());
            }
            pst.setDouble(3, pedido.getValorTotal());
            pst.setString(4, pedido.getStatus());
            pst.setDate(5, new java.sql.Date(pedido.getDataPagamento().getTime()));
            pst.setDate(6, new java.sql.Date(pedido.getDataPedido().getTime()));
            pst.setInt(7, pedido.getCliente().getId());
            pst.setInt(8, pedido.getEndereco().getId());
            pst.executeUpdate();

            ResultSet rs = pst.getGeneratedKeys();  // Gera os id automaticamente
            int idPedido = 0;
            if (rs.next()) {
                idPedido = rs.getInt(1);
            }
            pedido.setId(idPedido);

            for (CartaoCreditoPedido crtPedido : pedido.getCartoes()) {
                sql = new StringBuilder();
                sql.append("INSERT INTO tb_cartoes_pagamento (ctp_ped_id, ctp_crt_id, ctp_qtd_parcelas, ctp_valor_pago)");
                sql.append(" VALUES (?, ?, ?, ?)");

                try {
                    pst = connection.prepareStatement(sql.toString());		// retorna as chaves primárias geradas
                    pst.setInt(1, idPedido);
                    pst.setInt(2, crtPedido.getCartao().getId());
                    pst.setInt(3, crtPedido.getParcelas());
                    if (pedido.getCartoes().size() > 1) {
                        pst.setDouble(4, crtPedido.getValorPago());
                    } else {
                        pst.setDouble(4, pedido.getValorTotal());
                    }

                    pst.executeUpdate();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }

            for (Produto produto : pedido.getProdutos().getProdutos()) {
                sql = new StringBuilder();
                sql.append("INSERT INTO tb_produtos_pedidos (pp_ped_id, pp_pro_id, pp_pro_valor, pp_pro_qtde, pp_status) VALUES (?, ?, ?, ?, ?)");
                try {
                    pst = connection.prepareStatement(sql.toString(), Statement.RETURN_GENERATED_KEYS);		// retorna as chaves primárias geradas
                    pst.setInt(1, pedido.getId());
                    pst.setInt(2, produto.getId());
                    pst.setDouble(3, produto.getPreco());
                    pst.setInt(4, produto.getQuantidadeProdutoCarrinho());
                    pst.setString(5, "");

                    pst.executeUpdate();
                } catch (SQLException e) {
                    e.printStackTrace();
                }

                sql = new StringBuilder();
                if ((produto.getQuantidadeEstoque() - produto.getQuantidadeProdutoCarrinho()) <= 0) {
                    sql.append("UPDATE tb_estoque SET est_quantidade_estoque = 0 WHERE est_pro_id = '" + produto.getId() + "';");
                } else {
                    sql.append("UPDATE tb_estoque SET est_quantidade_estoque = " + (produto.getQuantidadeEstoque() - produto.getQuantidadeProdutoCarrinho()) + " WHERE est_pro_id = '" + produto.getId() + "';");
                }
                try {
                    pst = connection.prepareStatement(sql.toString(), Statement.RETURN_GENERATED_KEYS);		// retorna as chaves primárias geradas                    
                    pst.executeUpdate();

                } catch (SQLException e) {
                    e.printStackTrace();
                }

                // Atualiza a carteira do cliente
                sql = new StringBuilder();
                sql.append("UPDATE `tb_cliente` SET `cli_carteira`= " + pedido.getCliente().getCarteira() + " WHERE cli_id = '" + pedido.getCliente().getId() + "';");
                try {
                    pst = connection.prepareStatement(sql.toString());
                    pst.executeUpdate();

                } catch (SQLException e) {
                    e.printStackTrace();
                }

            }
            // Fecha a conexão
            connection.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void alterar(EntidadeDominio entidadeDominio) {
        openConnection();
        Pedido pedido = (Pedido) entidadeDominio;

        PreparedStatement pst = null;
        StringBuilder sql = new StringBuilder();

        // Ativa o cliente
        sql.append("UPDATE " + table + " SET ");
        sql.append("ped_status = ? ");
        sql.append("WHERE " + idtable + " = " + pedido.getId());

        try {
            pst = connection.prepareStatement(sql.toString());

            pst.setString(1, pedido.getStatus());

            pst.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }

        openConnection();
    }

    @Override
    public List<EntidadeDominio> consultar(EntidadeDominio entidadeDominio) {
        List<EntidadeDominio> pedidos = new ArrayList<EntidadeDominio>();
        ArrayList<Produto> produtos = new ArrayList<Produto>();
        Pedido pedido = (Pedido) entidadeDominio;
        ClienteDAO clienteDAO = new ClienteDAO();
        ProdutoDAO produtoDAO = new ProdutoDAO();
        EnderecoDAO enderecosDAO = new EnderecoDAO();
        CartaoCreditoDAO cartoesDAO = new CartaoCreditoDAO();
        MetodoEntregaDAO metodoEntregaDao = new MetodoEntregaDAO();

        ResultSet rsPedidos;

        openConnection();

        PreparedStatement pst = null;
        StringBuilder sql = new StringBuilder();

        if (pedido.getCliente().getNomeCompleto().equals("meuspedidos")) {
            sql.append("SELECT * ");
            sql.append("FROM " + table);
            sql.append(" WHERE ped_cli_id = '" + pedido.getCliente().getId() + "'");
            sql.append(" GROUP BY ped_id DESC;");
        } else if (pedido.getId() != 0) {
            sql.append("SELECT * ");
            sql.append("FROM " + table);
            sql.append(" WHERE ped_id = '" + pedido.getId() + "';");
        } else {
            sql.append("SELECT * ");
            sql.append("FROM " + table);
            sql.append(" GROUP BY ped_id DESC;");
        }
        try {
            pst = connection.prepareStatement(sql.toString());

            rsPedidos = pst.executeQuery();

            Cliente cliente = new Cliente();

            ProdutosPedido produtosPedido = new ProdutosPedido();

            while (rsPedidos.next()) {
                // Objetos reecriados para não sobreescrever os valores anteriores
                Endereco endereco = new Endereco();
                MetodoEntrega metodoEntrega = new MetodoEntrega();
                pedido = new Pedido();

                pedido.setId(rsPedidos.getInt("ped_id"));
                pedido.setNumeroBoleto(rsPedidos.getString("ped_boleto"));
                if (pedido.getNumeroBoleto().equals("0")) {
                    pedido.setTipoPagamento("Cartão de Crédito");
                } else {
                    pedido.setTipoPagamento("Boleto");
                }
                pedido.setValorTotal(rsPedidos.getDouble("ped_valor_total"));
                pedido.setStatus(rsPedidos.getString("ped_status"));
                if (rsPedidos.getDate("ped_data_pagamento") == null) {

                } else {
                    pedido.setDataPagamento(new java.util.Date(rsPedidos.getDate("ped_data_pagamento").getTime()));
                }
                pedido.setDataPedido(new java.util.Date(rsPedidos.getDate("ped_data_pedido").getTime()));

                // Setando os ids para consultas futuras
                endereco.setId(rsPedidos.getInt("ped_end_id"));
                metodoEntrega.setId(rsPedidos.getInt("ped_metodo_entrega"));

                // Criando cliente para consulta
                cliente.setId(rsPedidos.getInt("ped_cli_id"));
                StringBuilder sqlCliente = new StringBuilder();
                ResultSet rsCliente;

                sqlCliente.append("SELECT * ");
                sqlCliente.append("FROM tb_cliente WHERE cli_id = '" + cliente.getId() + "'");

                try {
                    pst = connection.prepareStatement(sqlCliente.toString());
                    rsCliente = pst.executeQuery();
                    if (rsCliente.next()) {
                        cliente.setNomeCompleto(rsCliente.getString("cli_nome_completo"));
                        cliente.setCpf(rsCliente.getString("cli_cpf"));
                        cliente.setRg(rsCliente.getString("cli_rg"));
                        cliente.setDataNascimento(new java.util.Date(rsCliente.getDate("cli_data_nascimento").getTime()));
                        cliente.setEmail(rsCliente.getString("cli_email"));
                        cliente.setStatus(rsCliente.getString("cli_status"));
                        cliente.setAdmin(rsCliente.getInt("cli_admin"));
                        pedido.setCliente(cliente);
                    }

                } catch (SQLException ex) {
                    ex.printStackTrace();

                }

                // Criando MetodoEntrega para consulta                
                StringBuilder sqlMetodoEntrega = new StringBuilder();
                ResultSet rsMetodo;
                sqlMetodoEntrega.append("SELECT * ");
                sqlMetodoEntrega.append("FROM tb_metodos_entrega WHERE mte_id = " + metodoEntrega.getId());
                try {
                    pst = connection.prepareStatement(sqlMetodoEntrega.toString());
                    rsMetodo = pst.executeQuery();

                    if (rsMetodo.next()) {
                        metodoEntrega.setNome(rsMetodo.getString("mte_nome"));
                        metodoEntrega.setValor(rsMetodo.getDouble("mte_valor"));
                    }
                } catch (SQLException ex) {
                    ex.printStackTrace();

                }

                pedido.setMetodoEntrega(metodoEntrega);

                // Criando endereco para consulta                
                StringBuilder sqlEndereco = new StringBuilder();
                ResultSet rsEndereco;

                sqlEndereco.append("SELECT * ");
                sqlEndereco.append("FROM tb_endereco");
                sqlEndereco.append(" WHERE end_id = '" + endereco.getId() + "';");

                try {
                    pst = connection.prepareStatement(sqlEndereco.toString());
                    rsEndereco = pst.executeQuery();

                    if (rsEndereco.next()) {
                        endereco.setIdentificacao(rsEndereco.getString("end_identificacao"));
                        endereco.setLogradouro(rsEndereco.getString("end_logradouro"));
                        endereco.setNumeroEndereco(rsEndereco.getString("end_numero"));
                        endereco.setComplemento(rsEndereco.getString("end_complemento"));
                        endereco.setBairro(rsEndereco.getString("end_bairro"));
                        endereco.setCidade(rsEndereco.getString("end_cidade"));
                        endereco.setSiglaEstado(rsEndereco.getString("end_estado"));
                        endereco.setCep(rsEndereco.getString("end_cep"));
                    }
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }

                pedido.setEndereco(endereco);

                // Criando cartao para consulta                
                StringBuilder sqlCartao = new StringBuilder();
                ResultSet rsCartao;

                sqlCartao.append("SELECT ctp_id, ctp_ped_id, ctp_crt_id, ctp_qtd_parcelas, ctp_valor_pago, ");
                sqlCartao.append("crt_id, crt_numero_cartao, crt_bandeira, crt_nome_impresso, crt_mes, crt_ano,  ");
                sqlCartao.append("crt_codigo_seguranca, crt_cli_id FROM tb_cartoes_pagamento ");
                sqlCartao.append("INNER JOIN tb_cartoes ON tb_cartoes_pagamento.ctp_crt_id = tb_cartoes.crt_id ");
                sqlCartao.append("WHERE ctp_ped_id = " + pedido.getId() + ";");

                ArrayList<CartaoCreditoPedido> cartaoCreditoPedido = new ArrayList<CartaoCreditoPedido>();
                try {
                    pst = connection.prepareStatement(sqlCartao.toString());
                    rsCartao = pst.executeQuery();

                    while (rsCartao.next()) {
                        CartaoCredito cartao = new CartaoCredito();
                        cartao.setNumeroCartao(rsCartao.getString("crt_numero_cartao"));
                        cartao.setBandeira(rsCartao.getString("crt_bandeira"));
                        cartao.setNomeImpresso(rsCartao.getString("crt_nome_impresso"));
                        cartao.setMes(rsCartao.getString("crt_mes"));
                        cartao.setAno(rsCartao.getInt("crt_ano"));
                        cartao.setCodigoSeguranca(rsCartao.getString("crt_codigo_seguranca"));
                        cartaoCreditoPedido.add(new CartaoCreditoPedido(cartao, rsCartao.getInt("ctp_qtd_parcelas"),
                                rsCartao.getDouble("ctp_valor_pago")));
                    }
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
                pedido.setCartoes(cartaoCreditoPedido);
                // Criando produtos para salvar na tabela ProdutosPedido
                StringBuilder sqlProdutos = new StringBuilder();
                ResultSet rsProdutos;

                int qtdeProdutoPedido;
                float valorOriginalProduto;

                sqlProdutos.append("SELECT * ");
                sqlProdutos.append("FROM tb_produtos_pedidos");
                sqlProdutos.append(" INNER JOIN tb_produtos ON tb_produtos_pedidos.pp_pro_id = tb_produtos.pro_id");
                sqlProdutos.append(" WHERE pp_ped_id = " + pedido.getId());

                try {
                    pst = connection.prepareStatement(sqlProdutos.toString());

                    rsProdutos = pst.executeQuery();

                    // Verifica a quantidade de produtos que o produto tinha
                    while (rsProdutos.next()) {
                        Produto produto = new Produto();
                        produto.setId(rsProdutos.getInt("pro_id"));
                        qtdeProdutoPedido = rsProdutos.getInt("pp_pro_qtde");
                        valorOriginalProduto = rsProdutos.getFloat("pp_pro_valor");

                        // Atualiza o preço do produto, pois ele pode ter sido alterado depois da venda, e salva a quantidade de cada                       
                        produto.setQuantidadeProdutoCarrinho(qtdeProdutoPedido);
                        produto.setPreco(valorOriginalProduto);

                        produto.setNome(rsProdutos.getString("pro_nome"));
                        produto.setDescricao(rsProdutos.getString("pro_descricao"));
                        produto.setPeso(rsProdutos.getFloat("pro_peso"));
                        produto.setEndereco_imagem(rsProdutos.getString("pro_endereco_imagem"));
                        produto.setEndereco_imagem_lado(rsProdutos.getString("pro_endereco_imagem_lado"));
                        produto.setEndereco_imagem_costas(rsProdutos.getString("pro_endereco_imagem_costas"));
                        produto.setStatus(rsProdutos.getString("pp_status"));

                        produtos.add(produto);
                    }
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
                produtosPedido.setProdutos(produtos);
                pedido.setProdutos(produtosPedido);
                pedidos.add(pedido);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return pedidos;
    }

}
