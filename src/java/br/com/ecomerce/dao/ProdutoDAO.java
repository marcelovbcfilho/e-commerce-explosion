package br.com.ecomerce.dao;

import br.com.ecomerce.dao.AbstractDAO;
import br.com.ecomerce.dominio.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class ProdutoDAO extends AbstractDAO {

    // Contrutor 1: Recebe uma conexão já aberta e os dados da tabela e coluna de banco de dados.
    public ProdutoDAO(Connection connection, String table, String idTable) {
        super(connection, table, idTable);
    }

    // Contrutor 2: Recebe aoenas os dados da tabela e da coluna do banco de dados.
    public ProdutoDAO(String table, String idTable) {
        super(table, idTable);
    }

    public ProdutoDAO() {
        super("tb_produtos", "pro_id");
    }

    // Salva os dados do produto.
    @Override
    public void salvar(EntidadeDominio entidadeDominio) {
        openConnection();

        PreparedStatement pst = null;
        Produto produto = (Produto) entidadeDominio;
        SubCategoriaDAO subcategoriaDAO = new SubCategoriaDAO();
        StringBuilder sql = new StringBuilder();

        sql.append("INSERT INTO " + table + "(pro_nome, pro_descricao, pro_peso, pro_preco, pro_endereco_imagem, ");
        sql.append("pro_endereco_imagem_lado, pro_endereco_imagem_costas, pro_sub_id, pro_qtd_estoque) ");
        sql.append("VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)");

        try {
            pst = connection.prepareStatement(sql.toString(), Statement.RETURN_GENERATED_KEYS);		// retorna as chaves primárias geradas
            pst.setString(1, produto.getNome());
            pst.setString(2, produto.getDescricao());
            pst.setDouble(3, produto.getPeso());
            pst.setDouble(4, produto.getPreco());
            pst.setString(5, produto.getEndereco_imagem());
            pst.setString(6, produto.getEndereco_imagem_lado());
            pst.setString(7, produto.getEndereco_imagem_costas());
            pst.setInt(8, produto.getSubCategoria().getId());
            pst.setInt(9, produto.getQuantidadeEstoque());
            pst.executeUpdate();

            ResultSet rs = pst.getGeneratedKeys();  // Gera os id automaticamente
            int idProduto = 0;
            if (rs.next()) {
                idProduto = rs.getInt(1);
            }
            produto.setId(idProduto);

            sql = new StringBuilder();
            sql.append("INSERT INTO tb_estoque (est_pro_id, est_quantidade_estoque, est_status) VALUES (?, ?, ?)");
            try {
                pst = connection.prepareStatement(sql.toString(), Statement.RETURN_GENERATED_KEYS);		// retorna as chaves primárias geradas
                pst.setInt(1, idProduto);
                pst.setInt(2, produto.getQuantidadeEstoque());
                pst.setBoolean(3, true);

                pst.executeUpdate();
            } catch (SQLException e) {
                e.printStackTrace();
            }

            // Fecha a conexão
            connection.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void alterar(EntidadeDominio entidadeDominio) {
        openConnection();

        PreparedStatement pst = null;
        Produto produto = (Produto) entidadeDominio;
        SubCategoriaDAO subcategoriaDAO = new SubCategoriaDAO();
        StringBuilder sql = new StringBuilder();

        sql.append("UPDATE " + table + "SET pro_nome=?, pro_descricao=?, pro_peso=?, pro_preco=?, pro_endereco_imagem=?, ");
        sql.append("pro_endereco_imagem_lado=?, pro_endereco_imagem_costas=?, pro_sub_id=? ");
        sql.append("WHERE pro_id=?");

        try {
            pst = connection.prepareStatement(sql.toString(), Statement.RETURN_GENERATED_KEYS);		// retorna as chaves primárias geradas
            pst.setString(1, produto.getNome());
            pst.setString(2, produto.getDescricao());
            pst.setDouble(3, produto.getPeso());
            pst.setDouble(4, produto.getPreco());
            pst.setString(5, produto.getEndereco_imagem());
            pst.setString(6, produto.getEndereco_imagem_lado());
            pst.setString(7, produto.getEndereco_imagem_costas());
            pst.setInt(8, produto.getSubCategoria().getId());
            pst.setInt(9, produto.getId());
            pst.executeUpdate();

            // Fecha a conexão
            connection.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<EntidadeDominio> consultar(EntidadeDominio entidadeDominio) {
        Produto pro = (Produto) entidadeDominio;
        List<EntidadeDominio> produtos = new ArrayList<EntidadeDominio>();
        Produto produto;
        SubCategoria sub = new SubCategoria();
        ResultSet rs;

        openConnection();

        PreparedStatement pst = null;
        StringBuilder sql = new StringBuilder();

        if (pro.getId() != 0) {
            sql.append("SELECT * ");
            sql.append("FROM " + table);
            sql.append(" WHERE " + idtable + " = " + pro.getId());
        } else if (null != pro.getNome() || null != pro.getDescricao()) {
            sql.append("SELECT * ");
            sql.append("FROM " + table);
            sql.append(" INNER JOIN tb_sub_categorias ON tb_produtos.pro_sub_id = tb_sub_categorias.sub_id");
            sql.append(" INNER JOIN tb_categorias ON tb_sub_categorias.sub_cat_id = tb_categorias.cat_id");
            sql.append(" WHERE  pro_nome like '%" + pro.getNome() + "%' OR pro_descricao like '%" + pro.getNome() + "%'"
                    + " OR sub_nome like '%" + pro.getNome() + "%' OR cat_nome like '%" + pro.getNome() + "%'");
            produtos.add(pro);
        } else if (pro.getSubCategoria().getId() != 0) {
            sql.append("SELECT * ");
            sql.append("FROM " + table);
            sql.append(" WHERE  pro_sub_id = " + pro.getSubCategoria().getId());
            sub.setId(pro.getSubCategoria().getId());
            produtos.add(pro);
        } else if (pro.getSubCategoria().getCategoria().getId() != 0) {
            sql.append("SELECT * ");
            sql.append("FROM " + table);
            sql.append(" INNER JOIN tb_sub_categorias ON tb_produtos.pro_sub_id = tb_sub_categorias.sub_id");
            sql.append(" INNER JOIN tb_categorias ON tb_sub_categorias.sub_cat_id = tb_categorias.cat_id");
            sql.append(" WHERE  cat_id = " + pro.getSubCategoria().getCategoria().getId());
            sub.setId(pro.getSubCategoria().getCategoria().getId());
            produtos.add(pro);
        } else if (pro.getId() == 0 && pro.getSubCategoria().getId() == 0) {
            sql.append("SELECT * ");
            sql.append("FROM " + table);
        }

        try {
            pst = connection.prepareStatement(sql.toString());

            rs = pst.executeQuery();

            while (rs.next()) {

                produto = new Produto(rs.getString("pro_nome"), rs.getString("pro_descricao"), rs.getFloat("pro_peso"), rs.getFloat("pro_preco"), rs.getString("pro_endereco_imagem"), rs.getString("pro_endereco_imagem_lado"), rs.getString("pro_endereco_imagem_costas"), sub);
                produto.setQuantidadeProdutoCarrinho(1);
                produto.setId(rs.getInt("pro_id"));

                StringBuilder sqlSub = new StringBuilder();
                ResultSet rsEstoque;

                sqlSub.append("SELECT * ");
                sqlSub.append("FROM tb_estoque");
                sqlSub.append(" WHERE est_pro_id = " + produto.getId());

                try {
                    pst = connection.prepareStatement(sqlSub.toString());

                    rsEstoque = pst.executeQuery();

                    while (rsEstoque.next()) {
                        produto.setQuantidadeEstoque(rsEstoque.getInt("est_quantidade_estoque"));
                    }
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
                produtos.add(produto);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return produtos;
    }

    @Override
    public void excluir(EntidadeDominio entidadeDominio) {

    }
}
