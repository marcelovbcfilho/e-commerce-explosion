package br.com.ecomerce.dao;

import br.com.ecomerce.dominio.Categoria;
import br.com.ecomerce.dominio.EntidadeDominio;
import br.com.ecomerce.dominio.SubCategoria;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class SubCategoriaDAO extends AbstractDAO {

    public SubCategoriaDAO(Connection connection, String table, String idTable) {
        super(connection, table, idTable);
    }

    public SubCategoriaDAO(String table, String idTable) {
        super(table, idTable);
    }

    public SubCategoriaDAO() {
        super("tb_sub_categorias", "sub_id");
    }

    @Override
    public void salvar(EntidadeDominio entidadeDominio) {
        openConnection();

        PreparedStatement pst = null;
        SubCategoria subCategoria = (SubCategoria) entidadeDominio;

        StringBuilder sql = new StringBuilder();

        sql.append("INSERT INTO " + table + " (sub_nome, sub_cat_id) ");
        sql.append("VALUES (?, ?)");

        try {
            pst = connection.prepareStatement(sql.toString(), Statement.RETURN_GENERATED_KEYS);		// retorna as chaves primárias geradas
            pst.setString(1, subCategoria.getNome());
            pst.setInt(2, subCategoria.getCategoria().getId());
            pst.executeUpdate();

            ResultSet rs = pst.getGeneratedKeys();  // Gera os id automaticamente
            int idSubCategoria = 0;
            if (rs.next()) {
                idSubCategoria = rs.getInt(1);
            }
            subCategoria.setId(idSubCategoria);

            // Fecha a conexão
            connection.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void alterar(EntidadeDominio entidadeDominio) {
        openConnection();

        PreparedStatement pst = null;
        SubCategoria subCategoria = (SubCategoria) entidadeDominio;

        StringBuilder sql = new StringBuilder();

        sql.append("UPDATE " + table + " SET sub_nome=?, sub_cat_id=?");
        sql.append("WHERE " + idtable + " = ?");

        try {
            pst = connection.prepareStatement(sql.toString(), Statement.RETURN_GENERATED_KEYS);		// retorna as chaves primárias geradas
            pst.setString(1, subCategoria.getNome());
            pst.setInt(2, subCategoria.getCategoria().getId());
            pst.setInt(3, subCategoria.getId());
            pst.executeUpdate();

            // Fecha a conexão
            connection.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<EntidadeDominio> consultar(EntidadeDominio entidadeDominio) {
        List<EntidadeDominio> subCategorias = new ArrayList<EntidadeDominio>();
        CategoriaDAO CategoriaDAO = new CategoriaDAO();
        SubCategoria subcategoria = (SubCategoria) entidadeDominio;
        Categoria categoria = new Categoria();
        ResultSet rs;

        openConnection();

        PreparedStatement pst = null;
        StringBuilder sql = new StringBuilder();

        // Verifica se consulta por id ou seleciona todas
        if (subcategoria.getId() != 0) {
            sql.append("SELECT * ");
            sql.append("FROM " + table);
            sql.append(" WHERE " + idtable + " = " + subcategoria.getId());
        } else if (subcategoria.getId() == 0) {
            sql.append("SELECT * ");
            sql.append("FROM " + table);
        }

        try {
            pst = connection.prepareStatement(sql.toString());

            rs = pst.executeQuery();

            while (rs.next()) {
                subcategoria = new SubCategoria(rs.getString("sub_nome"));
                subcategoria.setId(rs.getInt("sub_id"));
                // Consulta a categoria relacioanda a subcategoria atual
                categoria.setId(rs.getInt("sub_cat_id"));
                for (EntidadeDominio cat : CategoriaDAO.consultar(categoria)) {
                    categoria = (Categoria) cat;
                    subcategoria.setCategoria(categoria);
                }
                subCategorias.add(subcategoria);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return subCategorias;
    }

}
