package br.com.ecomerce.dominio;

/**
 *
 * @author Marcelo Vilas Boas Correa Filho
 */
public class Boleto extends EntidadeDominio{
    private String numeroBoleto;

    public Boleto(String numeroBoleto) {
        this.numeroBoleto = numeroBoleto;
    }

    public Boleto() {
    }

    public String getNumeroBoleto() {
        return numeroBoleto;
    }

    public void setNumeroBoleto(String numeroBoleto) {
        this.numeroBoleto = numeroBoleto;
    }
}
