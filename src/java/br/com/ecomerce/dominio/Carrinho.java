package br.com.ecomerce.dominio;

import java.util.ArrayList;

public class Carrinho extends EntidadeDominio{
    private ArrayList<Produto> produtos;

    public Carrinho(ArrayList<Produto> produtos) {
        this.produtos = produtos;
    }
   
    public Carrinho(){
        
    }
    
    public ArrayList<Produto> getProdutos() {
        return produtos;
    }

    public void setProdutos(ArrayList<Produto> produtos) {
        this.produtos = produtos;
    }
    
    public void adicionarItem(Produto produto)
    {
        produtos.add(produto);
    }
        
}
