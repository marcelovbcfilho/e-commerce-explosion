package br.com.ecomerce.dominio;

import java.util.Date;


public class CartaoCredito extends EntidadeDominio{

    private String numeroCartao;
    private String bandeira;
    private String nomeImpresso;
    private String mes;
    private int ano;
    private String codigoSeguranca;
    private Cliente cliente;

    public CartaoCredito(String numeroCartao, String bandeira, String nomeImpresso, String mes, int ano, String codigoSeguranca, Cliente cliente) {
        this.numeroCartao = numeroCartao;
        this.bandeira = bandeira;
        this.nomeImpresso = nomeImpresso;
        this.mes = mes;
        this.ano = ano;
        this.codigoSeguranca = codigoSeguranca;
        this.cliente = cliente;
    }
    
    // COnstroi cartão sem o cliente
    public CartaoCredito(String numeroCartao, String bandeira, String nomeImpresso, String mes, int ano, String codigoSeguranca) {
        this.numeroCartao = numeroCartao;
        this.bandeira = bandeira;
        this.nomeImpresso = nomeImpresso;
        this.mes = mes;
        this.ano = ano;
        this.codigoSeguranca = codigoSeguranca;
    }
    
    public CartaoCredito() {
        
    }

    public String getCodigoSeguranca() {
        return codigoSeguranca;
    }

    public void setCodigoSeguranca(String codigoSeguranca) {
        this.codigoSeguranca = codigoSeguranca;
    }

    public String getNumeroCartao() {
        return numeroCartao;
    }

    public void setNumeroCartao(String numeroCartao) {
        this.numeroCartao = numeroCartao;
    }

    public String getMes() {
        return mes;
    }

    public void setMes(String mes) {
        this.mes = mes;
    }

    public int getAno() {
        return ano;
    }

    public void setAno(int ano) {
        this.ano = ano;
    }

    public String getNomeImpresso() {
        return nomeImpresso;
    }

    public void setNomeImpresso(String nomeImpresso) {
        this.nomeImpresso = nomeImpresso;
    }

    public String getBandeira() {
        return bandeira;
    }

    public void setBandeira(String bandeira) {
        this.bandeira = bandeira;
    }

    public Cliente getCliente(){
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }
}
