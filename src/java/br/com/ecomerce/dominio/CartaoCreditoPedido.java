package br.com.ecomerce.dominio;

public class CartaoCreditoPedido extends EntidadeDominio {

    private CartaoCredito cartao;
    private int parcelas;
    private double valorPago;

    public CartaoCreditoPedido(CartaoCredito cartao, int parcelas, double valorPago) {
        this.cartao = cartao;
        this.parcelas = parcelas;
        this.valorPago = valorPago;
    }

    public CartaoCreditoPedido() {
    }

    public CartaoCreditoPedido(int parcelas) {
        this.parcelas = parcelas;
    }

    public CartaoCredito getCartao() {
        return cartao;
    }

    public void setCartao(CartaoCredito cartao) {
        this.cartao = cartao;
    }

    public int getParcelas() {
        return parcelas;
    }

    public void setParcelas(int parcelas) {
        this.parcelas = parcelas;
    }

    public double getValorPago() {
        return valorPago;
    }

    public void setValorPago(double valorPago) {
        this.valorPago = valorPago;
    }

}
