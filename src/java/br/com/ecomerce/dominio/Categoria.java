package br.com.ecomerce.dominio;

import java.util.ArrayList;

public class Categoria extends EntidadeDominio{
    private String nome;
    private ArrayList<SubCategoria> subCategorias;
    
    public Categoria(String nome){
        this.nome = nome;
        subCategorias = new ArrayList<SubCategoria>();
    }
    
    public Categoria(){
        
    }
    
    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public ArrayList<SubCategoria> getSubCategorias() {
        return subCategorias;
    }

    public void setSubCategorias(ArrayList<SubCategoria> subCategorias) {
        this.subCategorias = subCategorias;
    }
    
}
