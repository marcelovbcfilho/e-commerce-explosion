package br.com.ecomerce.dominio;

import java.util.ArrayList;
import java.util.Date;

public class Cliente extends EntidadeDominio {

    private String nomeCompleto;
    private String cpf;
    private String rg;
    private Date dataNascimento;
    private String email;
    private String status;
    private Senha senha;
    private double carteira;
    private ArrayList<Endereco> enderecos;
    private ArrayList<CartaoCredito> cartoesCredito;
    private int admin;

    
    // Endereços e cartões para criar cliente
    public Cliente(String nomeCompleto, String cpf, String rg, Date dataNascimento, String email, Senha senha, String status, ArrayList<Endereco> enderecos, ArrayList<CartaoCredito> cartoesCredito) {
        this.nomeCompleto = nomeCompleto;
        this.cpf = cpf;
        this.rg = rg;
        this.dataNascimento = dataNascimento;
        this.email = email;
        this.senha = senha;
        this.status = status;
        this.enderecos = enderecos;
        this.cartoesCredito = cartoesCredito;
    }
    
    // Constroi cliente sem o cartão e o endereco
    public Cliente(String nomeCompleto, String cpf, String rg, Date dataNascimento, String email, Senha senha, String status, int admin) {
        this.dataNascimento = dataNascimento;
        this.email = email;
        this.status = status;
        this.cpf = cpf;
        this.rg = rg;
        this.nomeCompleto = nomeCompleto;
        this.senha = senha;
        this.admin = admin;
    }

    public Cliente() {
    }

    public Date getDataNascimento() {
        return dataNascimento;
    }

    public void setDataNascimento(Date dataNascimento) {
        this.dataNascimento = dataNascimento;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getNomeCompleto() {
        return nomeCompleto;
    }

    public void setNomeCompleto(String nomeCompleto) {
        this.nomeCompleto = nomeCompleto;
    }

    public Senha getSenha() {
        return senha;
    }
    
    public void setSenha(Senha senha) {
        this.senha = senha;
    }

    public ArrayList<Endereco> getEnderecos() {
        return enderecos;
    }

    public void setEnderecos(ArrayList<Endereco> enderecos) {
        this.enderecos = enderecos;
    }

    public ArrayList<CartaoCredito> getCartoesCredito() {
        return cartoesCredito;
    }

    public void setCartoesCredito(ArrayList<CartaoCredito> cartoesCredito) {
        this.cartoesCredito = cartoesCredito;
    }

    public String getRg() {
        return rg;
    }

    public void setRg(String rg) {
        this.rg = rg;
    }
    
    public int getAdmin() {
        return admin;
    }

    public void setAdmin(int admin) {
        this.admin = admin;
    }

    public double getCarteira() {
        return carteira;
    }

    public void setCarteira(double carteira) {
        this.carteira = carteira;
    }

    
    
}
