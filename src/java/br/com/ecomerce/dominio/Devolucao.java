package br.com.ecomerce.dominio;

import java.util.ArrayList;
import java.util.Date;

public class Devolucao extends EntidadeDominio {

    private String status;
    private ArrayList<Produto> produtos;
    private String numeroPedidoDevolucao;
    private Date dataSolicitacao;
    private Cliente cliente;

    public Devolucao() {

    }

    public Devolucao(String status, ArrayList<Produto> produtos, String numeroDevolucao) {
        this.status = status;
        this.produtos = produtos;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public ArrayList<Produto> getProdutos() {
        return produtos;
    }

    public void setProdutos(ArrayList<Produto> produtos) {
        this.produtos = produtos;
    }

    public Date getDataSolicitacao() {
        return dataSolicitacao;
    }

    public void setDataSolicitacao(Date dataSolicitacao) {
        this.dataSolicitacao = dataSolicitacao;
    }

    public String getNumeroPedidoDevolucao() {
        return numeroPedidoDevolucao;
    }

    public void setNumeroPedidoDevolucao(String numeroPedidoDevolucao) {
        this.numeroPedidoDevolucao = numeroPedidoDevolucao;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }
}
