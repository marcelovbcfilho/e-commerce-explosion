package br.com.ecomerce.dominio;

public class Endereco extends EntidadeDominio{
 
    private String identificacao;
    private String logradouro;
    private String numeroEndereco;
    private String cep;
    private String complemento;
    private String bairro;
    private String cidade;
    private String siglaEstado;
    private Cliente cliente;

    public Endereco(String identificacao, String logradouro, String numeroEndereco, String complemento, String bairro, String cidade, String siglaEstado, String cep, Cliente cliente) {
        this.identificacao = identificacao;
        this.logradouro = logradouro;
        this.numeroEndereco = numeroEndereco;
        this.complemento = complemento;
        this.bairro = bairro;
        this.cidade = cidade;
        this.siglaEstado = siglaEstado;
        this.cep = cep;
        this.cliente = cliente;
    }

    // Constroi endereço sem o cliente
    public Endereco(String identificacao, String logradouro, String numeroEndereco, String complemento, String bairro, String cidade, String siglaEstado, String cep) {
        this.identificacao = identificacao;
        this.logradouro = logradouro;
        this.numeroEndereco = numeroEndereco;
        this.complemento = complemento;
        this.bairro = bairro;
        this.cidade = cidade;
        this.siglaEstado = siglaEstado;
        this.cep = cep;
    }
    
    public Endereco() {
        
    }

    public String getIdentificacao() {
        return identificacao;
    }

    public void setIdentificacao(String identificacao) {
        this.identificacao = identificacao;
    }

    public String getLogradouro() {
        return logradouro;
    }

    public void setLogradouro(String logradouro) {
        this.logradouro = logradouro;
    }

    public String getNumeroEndereco() {
        return numeroEndereco;
    }

    public void setNumeroEndereco(String numeroEndereco) {
        this.numeroEndereco = numeroEndereco;
    }

    public String getComplemento() {
        return complemento;
    }

    public void setComplemento(String complemento) {
        this.complemento = complemento;
    }

    public String getBairro() {
        return bairro;
    }

    public void setBairro(String bairro) {
        this.bairro = bairro;
    }

    public String getCidade() {
        return cidade;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    public String getSiglaEstado() {
        return siglaEstado;
    }

    public void setSiglaEstado(String siglaEstado) {
        this.siglaEstado = siglaEstado;
    }

    public String getCep() {
        return cep;
    }

    public void setCep(String cep) {
        this.cep = cep;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

}