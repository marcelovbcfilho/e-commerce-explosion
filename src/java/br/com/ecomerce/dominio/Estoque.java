
package br.com.ecomerce.dominio;

public class Estoque extends EntidadeDominio{
    private Produto produto;
    private int quantidadeEstoque;
    private boolean status;
    public Estoque() {
    }

    public Estoque(Produto produto, int quantidadeEstoque) {
        this.produto = produto;
        this.quantidadeEstoque = quantidadeEstoque;
    }
    
    public Produto getProduto() {
        return produto;
    }

    public void setProduto(Produto produto) {
        this.produto = produto;
    }

    public int getQuantidadeEstoque() {
        return quantidadeEstoque;
    }

    public void setQuantidadeEstoque(int quantidadeEstoque) {
        this.quantidadeEstoque = quantidadeEstoque;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }
    
    
}
