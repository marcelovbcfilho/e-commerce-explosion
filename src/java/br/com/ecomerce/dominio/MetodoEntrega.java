
package br.com.ecomerce.dominio;


public class MetodoEntrega extends EntidadeDominio{
    private String nome;
    private double valor;

    public MetodoEntrega(String nome, double valor) {
        this.nome = nome;
        this.valor = valor;
    }

    public MetodoEntrega() {
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }
}
