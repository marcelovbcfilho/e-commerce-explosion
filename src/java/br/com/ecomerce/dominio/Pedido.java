package br.com.ecomerce.dominio;

import java.util.ArrayList;
import java.util.Date;

public class Pedido extends EntidadeDominio {

    private String tipoPagamento;
    private String numeroBoleto;
    private ArrayList<CartaoCreditoPedido> cartoes;
    private double valorTotal;
    private Date dataPagamento;
    private Date dataPedido;
    private String status;
    private Cliente cliente;
    private Endereco endereco;
    private ProdutosPedido produtos;
    private MetodoEntrega metodoEntrega;

    public Pedido() {

    }
    
    // Construtor para pedido no cartao
    public Pedido(String tipoPagamento, ArrayList<CartaoCreditoPedido> cartoes, double valorTotal, Date dataPagamento, Date dataPedido, String status, Cliente cliente, Endereco endereco, ProdutosPedido produtos, MetodoEntrega metodoEntrega) {
        this.tipoPagamento = tipoPagamento;
        this.cartoes = cartoes;
        this.valorTotal = valorTotal;
        this.dataPagamento = dataPagamento;
        this.dataPedido = dataPedido;
        this.status = status;
        this.cliente = cliente;
        this.endereco = endereco;
        this.produtos = produtos;
        this.metodoEntrega = metodoEntrega;
    }   
    
    // Construtor para pedido no boleto
    public Pedido(String tipoPagamento, String numeroBoleto, ProdutosPedido produtos, double valorTotal, Date dataPagamento, Date dataPedido, String status, Cliente cliente, Endereco endereco, MetodoEntrega metodoEntrega) {
        this.tipoPagamento = tipoPagamento;
        this.numeroBoleto = numeroBoleto;
        this.produtos = produtos;
        this.valorTotal = valorTotal;
        this.dataPagamento = dataPagamento;
        this.dataPedido = dataPedido;
        this.status = status;
        this.cliente = cliente;
        this.endereco = endereco;
        this.metodoEntrega = metodoEntrega;
    }

    public String getTipoPagamento() {
        return tipoPagamento;
    }

    public void setTipoPagamento(String tipoPagamento) {
        this.tipoPagamento = tipoPagamento;
    }

    public double getValorTotal() {
        return valorTotal;
    }

    public void setValorTotal(double valorTotal) {
        this.valorTotal = valorTotal;
    }

    public Date getDataPagamento() {
        return dataPagamento;
    }

    public void setDataPagamento(Date dataPagamento) {
        this.dataPagamento = dataPagamento;
    }

    public Date getDataPedido() {
        return dataPedido;
    }

    public void setDataPedido(Date dataPedido) {
        this.dataPedido = dataPedido;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Endereco getEndereco() {
        return endereco;
    }

    public void setEndereco(Endereco endereco) {
        this.endereco = endereco;
    }

    public MetodoEntrega getMetodoEntrega() {
        return metodoEntrega;
    }

    public void setMetodoEntrega(MetodoEntrega metodoEntrega) {
        this.metodoEntrega = metodoEntrega;
    }

    public String getNumeroBoleto() {
        return numeroBoleto;
    }

    public void setNumeroBoleto(String numeroBoleto) {
        this.numeroBoleto = numeroBoleto;
    }

    public ArrayList<CartaoCreditoPedido> getCartoes() {
        return cartoes;
    }

    public void setCartoes(ArrayList<CartaoCreditoPedido> cartoes) {
        this.cartoes = cartoes;
    }   

    public ProdutosPedido getProdutos() {
        return produtos;
    }

    public void setProdutos(ProdutosPedido produtos) {
        this.produtos = produtos;
    }

}
