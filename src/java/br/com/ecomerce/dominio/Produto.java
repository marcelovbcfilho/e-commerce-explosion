package br.com.ecomerce.dominio;

public class Produto extends EntidadeDominio {

    private String nome;
    private String descricao;
    private double peso;
    private double preco;
    private String status;
    private int quantidadeEstoque;    
    private String endereco_imagem;
    private String endereco_imagem_lado;
    private String endereco_imagem_costas;
    SubCategoria subCategoria;
    private int quantidadeProdutoCarrinho;
    
    public Produto(String nome, String descricao, double peso, double preco, String endereco_imagem, String endereco_imagem_lado, String endereco_imagem_costas, SubCategoria subCategoria) {
        this.nome = nome;
        this.descricao = descricao;
        this.peso = peso;
        this.preco = preco;
        this.endereco_imagem = endereco_imagem;
        this.endereco_imagem_lado = endereco_imagem_lado;
        this.endereco_imagem_costas = endereco_imagem_costas;
        this.subCategoria = subCategoria;
    }

    public Produto(String nome, String descricao, double peso, double preco, String endereco_imagem, String endereco_imagem_lado, String endereco_imagem_costas) {
        this.nome = nome;
        this.descricao = descricao;
        this.peso = peso;
        this.preco = preco;
        this.endereco_imagem = endereco_imagem;
        this.endereco_imagem_lado = endereco_imagem_lado;
        this.endereco_imagem_costas = endereco_imagem_costas;
    }

    public Produto() {

    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public double getPeso() {
        return peso;
    }

    public void setPeso(float peso) {
        this.peso = peso;
    }

    public double getPreco() {
        return preco;
    }

    public void setPreco(float preco) {
        this.preco = preco;
    }

    public int getQuantidadeEstoque() {
        return quantidadeEstoque;
    }

    public void setQuantidadeEstoque(int quantidadeEstoque) {
        this.quantidadeEstoque = quantidadeEstoque;
    }
    
    public String getEndereco_imagem() {
        return endereco_imagem;
    }

    public void setEndereco_imagem(String endereco_imagem) {
        this.endereco_imagem = endereco_imagem;
    }

    public String getEndereco_imagem_lado() {
        return endereco_imagem_lado;
    }

    public void setEndereco_imagem_lado(String endereco_imagem_lado) {
        this.endereco_imagem_lado = endereco_imagem_lado;
    }

    public String getEndereco_imagem_costas() {
        return endereco_imagem_costas;
    }

    public void setEndereco_imagem_costas(String endereco_imagem_costas) {
        this.endereco_imagem_costas = endereco_imagem_costas;
    }

    public SubCategoria getSubCategoria() {
        return subCategoria;
    }

    public void setSubCategoria(SubCategoria subCategoria) {
        this.subCategoria = subCategoria;
    }

    public int getQuantidadeProdutoCarrinho() {
        return quantidadeProdutoCarrinho;
    }

    public void setQuantidadeProdutoCarrinho(int quantidadeProdutoCarrinho) {
        this.quantidadeProdutoCarrinho = quantidadeProdutoCarrinho;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
    
}
