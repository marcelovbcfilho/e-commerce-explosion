package br.com.ecomerce.dominio;

import java.util.ArrayList;

public class ProdutosPedido extends EntidadeDominio{
    private ArrayList<Produto> produtos;

    public ProdutosPedido(ArrayList<Produto> produtos) {
        this.produtos = produtos;
    }
    
    public ProdutosPedido() {
        
    }
    
    public ArrayList<Produto> getProdutos() {
        return produtos;
    }

    public void setProdutos(ArrayList<Produto> produtos) {
        this.produtos = produtos;
    }
}
