
package br.com.ecomerce.dominio;


public class SubCategoria extends EntidadeDominio{
    private String nome;
    private Categoria categoria; 

    public SubCategoria()
    {
        
    }

    public SubCategoria(String nome) {
        this.nome = nome;
    }
    
    public SubCategoria(String nome, Categoria categoria) {
        this.nome = nome;
        this.categoria = categoria;
    }
    
    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Categoria getCategoria() {
        return categoria;
    }

    public void setCategoria(Categoria categoria) {
        this.categoria = categoria;
    }
   
    
    
}

