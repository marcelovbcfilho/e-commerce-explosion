package br.com.ecomerce.filtro;

import br.com.ecomerce.dominio.Cliente;
import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class FiltroAutorizacao implements Filter {

    public void doFilter(ServletRequest requestOriginal, ServletResponse responseOriginal,
            FilterChain chain)
            throws IOException, ServletException {
        // Transforma as requests em HttpServletRequest
        HttpServletRequest request = (HttpServletRequest) requestOriginal;
        // Transforma as response em HttpServletRequest
        HttpServletResponse response = (HttpServletResponse) responseOriginal;
        // Recupera a session para verificar se existe um cliente logado
        HttpSession session = request.getSession();

        if (request.getRequestURI().startsWith(request.getContextPath() + "/logado/logadoAdmin")) {
            if (session.getAttribute("cliente") == null) {
                // para pastas anteriores
                response.sendRedirect(response.encodeRedirectURL(request.getContextPath() + "/login.jsp"));
                // para o mesmo local
                // request.getRequestDispatcher("/login.jsp").forward(requestOriginal, responseOriginal);
            } else {
                Cliente cliente = (Cliente) session.getAttribute("cliente");
                if (cliente.getAdmin() == 0) {
                    response.sendRedirect(response.encodeRedirectURL(request.getContextPath() + "/login.jsp"));
                } else {
                    chain.doFilter(requestOriginal, responseOriginal);
                }
            }
        } else if (request.getRequestURI().startsWith(request.getContextPath() + "/logado")) {
            if (session.getAttribute("cliente") == null) {
                // para pastas anteriores
                response.sendRedirect(response.encodeRedirectURL(request.getContextPath() + "/login.jsp"));
                // para o mesmo local
                // request.getRequestDispatcher("/login.jsp").forward(requestOriginal, responseOriginal);
            } else {
                Cliente cliente = (Cliente) session.getAttribute("cliente");
                if (cliente.getAdmin() == 0) {
                    chain.doFilter(requestOriginal, responseOriginal);
                } else {
                    response.sendRedirect(response.encodeRedirectURL(request.getContextPath() + "/minha-conta-admin.jsp"));
                }
            }
        } else {
            response.sendRedirect(response.encodeRedirectURL(request.getContextPath() + "/login.jsp"));
        }
    }

    public void destroy() {
    }

    public void init(FilterConfig filterConfig) {
    }

}
