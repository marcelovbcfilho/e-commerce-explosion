package br.com.ecomerce.filtro;

import br.com.ecomerce.dao.CategoriaDAO;
import br.com.ecomerce.dominio.Categoria;
import br.com.ecomerce.dominio.EntidadeDominio;
import br.com.ecomerce.dominio.Resultado;
import br.com.ecomerce.web.command.ConsultarCommand;
import br.com.ecomerce.web.viewHelper.CategoriaVH;
import java.io.IOException;
import java.util.ArrayList;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class FiltroCategoria implements Filter {

    public void doFilter(ServletRequest requestOriginal, ServletResponse responseOriginal,
            FilterChain chain)
            throws IOException, ServletException {
        // Transforma as requests em HttpServletRequest
        HttpServletRequest request = (HttpServletRequest) requestOriginal;
        // Transforma as response em HttpServletRequest
        HttpServletResponse response = (HttpServletResponse) responseOriginal;
        // Recupera a session para verificar se existe um cliente logado
        HttpSession session = request.getSession();
        if (session.getAttribute("categorias") != null) {
            chain.doFilter(requestOriginal, responseOriginal);
        } else {
            Resultado resultado = null;
            CategoriaVH vhcat = new CategoriaVH();
            ConsultarCommand cCommand = new ConsultarCommand();
            EntidadeDominio ent = vhcat.getEntidade(request);
            resultado = cCommand.executar(ent);
            vhcat.setView(resultado, request, response);
        }
    }

    public void destroy() {
    }

    public void init(FilterConfig filterConfig) {
    }

}
