package br.com.ecomerce.negocio;

import br.com.ecomerce.dominio.EntidadeDominio;


public interface IStrategy {
	public String processar(EntidadeDominio entidade);
}

