
package br.com.ecomerce.negocio.cartao;

import br.com.ecomerce.dominio.CartaoCredito;
import br.com.ecomerce.dominio.EntidadeDominio;
import br.com.ecomerce.negocio.IStrategy;

public class ValidaCartao implements IStrategy{

    @Override
    public String processar(EntidadeDominio entidade) {
        CartaoCredito cartao = (CartaoCredito) entidade;
        StringBuilder msg = new StringBuilder();
        
        if(cartao.getBandeira().trim().equals("")){
            msg.append("A bandeira do cartão é obrigatório.");
        }
        
        if(cartao.getCodigoSeguranca().trim().equals("")){
            msg.append("O código de segurança é obrigatório.");
        }
        
        if(cartao.getNomeImpresso().trim().equals("")){
            msg.append("O nome impresso no cartão é obrigatório.");
        }
        
        if(cartao.getNumeroCartao().trim().equals("")){
            msg.append("O número do cartão é obrigatório.");
        }
        
        if(cartao.getMes().toString().trim().equals("")){
            msg.append("O mês da validade é obrigatório");
        }
        if(cartao.getAno() == 0){
            msg.append("O ano da validade é obrigatório");
        }
        
        return msg.toString();
    }
    
}
