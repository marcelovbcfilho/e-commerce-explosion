package br.com.ecomerce.negocio.categoria;

import br.com.ecomerce.dominio.Categoria;
import br.com.ecomerce.dominio.EntidadeDominio;
import br.com.ecomerce.negocio.IStrategy;
import br.com.ecomerce.negocio.IStrategy;
import java.text.ParseException;
import java.util.Date;


public class ValidaDadosCategoria implements IStrategy {

    @Override
    public String processar(EntidadeDominio entidade) {
        Categoria categoria = (Categoria) entidade;
        StringBuilder msg = new StringBuilder();

        if (categoria.getNome().equals("") || categoria.getNome() == null) {
            msg.append("<p>O nome da categoria é obrigatória.</p>");
        }

        return msg.toString();
    }

}
