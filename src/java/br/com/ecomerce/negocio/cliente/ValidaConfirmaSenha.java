
package br.com.ecomerce.negocio.cliente;

import br.com.ecomerce.dominio.Cliente;
import br.com.ecomerce.dominio.EntidadeDominio;
import br.com.ecomerce.negocio.IStrategy;


public class ValidaConfirmaSenha implements IStrategy {

    @Override
    public String processar(EntidadeDominio entidade) {
        Cliente cliente = (Cliente)entidade;
        if(!(cliente.getSenha().getSenha().equals(cliente.getSenha().getConfirmaSenha()))) {
             return "As duas senhas devem coincidir";
        }
        return null;
    }
    
}
