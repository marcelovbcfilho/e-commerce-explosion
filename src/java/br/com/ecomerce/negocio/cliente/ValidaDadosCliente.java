
package br.com.ecomerce.negocio.cliente;

import br.com.ecomerce.dominio.Cliente;
import br.com.ecomerce.dominio.EntidadeDominio;
import br.com.ecomerce.negocio.IStrategy;
import java.text.ParseException;
import java.util.Date;


public class ValidaDadosCliente implements IStrategy{

    @Override
    public String processar(EntidadeDominio entidade) {
        Cliente cliente = (Cliente) entidade;
        StringBuilder msg = new StringBuilder();

        if(cliente.getDataNascimento() == new Date(0L) || cliente.getDataNascimento() == null) {
            msg.append("Data inválida.\n");
        }

        if(cliente.getNomeCompleto().trim().equals("") || cliente.getNomeCompleto()== null){
            msg.append("O Nome precisa ser preenchido.\n");
        }

        if(cliente.getEmail().trim().equals("") || cliente.getEmail() == null){
            msg.append("O E-mail precisa ser preenchido.\n");
        }

        if(cliente.getCpf().trim().equals("") || cliente.getCpf() == null){
            msg.append("O CPF precisa ser preenchido.\n");
        }   
        
        return msg.toString();
    }
    
}
