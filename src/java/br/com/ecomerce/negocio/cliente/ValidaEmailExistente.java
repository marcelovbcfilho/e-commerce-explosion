
package br.com.ecomerce.negocio.cliente;

import br.com.ecomerce.dao.ClienteDAO;
import br.com.ecomerce.dominio.Cliente;
import br.com.ecomerce.dominio.EntidadeDominio;
import br.com.ecomerce.negocio.IStrategy;

public class ValidaEmailExistente implements IStrategy{

    @Override
    public String processar(EntidadeDominio entidade) {
        Cliente cliente = (Cliente) entidade;
        StringBuilder msg = new StringBuilder();
        ClienteDAO clienteDAO = new ClienteDAO();
        Cliente clienteConsulta = new Cliente();
        clienteConsulta.setEmail(cliente.getEmail());
        clienteConsulta.setNomeCompleto("ValidaEmail");
        if(clienteDAO.consultar(clienteConsulta).size() > 0)
        {
            msg.append("Este email já está cadastrado no sistema.");
        }
        
        return msg.toString();
    }
    
}
