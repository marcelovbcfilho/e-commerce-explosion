
package br.com.ecomerce.negocio.cliente;

import br.com.ecomerce.dominio.Cliente;
import br.com.ecomerce.dominio.EntidadeDominio;
import br.com.ecomerce.negocio.IStrategy;
import br.com.ecomerce.negocio.endereco.ValidaEndereco;

public class ValidaExistenciaEndereco implements IStrategy {

    @Override
    public String processar(EntidadeDominio entidade) {
        Cliente cliente = (Cliente) entidade;
        StringBuilder msg = new StringBuilder();
        ValidaEndereco v = new ValidaEndereco();
        if(!v.processar(cliente.getEnderecos().get(0)).trim().equals("")) {
            msg.append("Erros endereço: ");
            msg.append(v.processar(cliente.getEnderecos().get(0)));
        }
        
        return msg.toString();
    }
    
}
