
package br.com.ecomerce.negocio.cliente;

import br.com.ecomerce.dao.ClienteDAO;
import br.com.ecomerce.dominio.Cliente;
import br.com.ecomerce.dominio.EntidadeDominio;
import br.com.ecomerce.dominio.Senha;
import br.com.ecomerce.negocio.IStrategy;


public class ValidaTrocaSenha implements IStrategy {

    @Override
    public String processar(EntidadeDominio entidade) {
        Senha senha = (Senha) entidade;
        Cliente clienteID = new Cliente();
        clienteID.setId(senha.getId());
        clienteID.setNomeCompleto("");
        if (!senha.getSenhaTemp().equals("") || senha.getSenhaTemp() != null) {
            Cliente senhaAntiga;
            
            // Consulta se a senha antiga é igual à senha nova
            ClienteDAO dao = new ClienteDAO();
            senhaAntiga = (Cliente) dao.consultar(clienteID).get(0);
            if (!senha.getSenhaTemp().equals(senhaAntiga.getSenha().getSenha())) {
                return "A senha atual está incorreta.";
            }
            
            // Verifica se a senha e a Confirmação da Senha são iguais
            if (!(senha.getSenha().equals(senha.getConfirmaSenha()))) {
                return "As duas senhas devem coincidir";
            }
            return null;
        } else {
            return "A senha original é obrigatória";
        }
    }

}
