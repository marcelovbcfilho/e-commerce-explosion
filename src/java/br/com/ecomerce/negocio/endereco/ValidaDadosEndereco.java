
package br.com.ecomerce.negocio.endereco;

import br.com.ecomerce.dominio.Endereco;
import br.com.ecomerce.dominio.EntidadeDominio;
import br.com.ecomerce.negocio.IStrategy;

public class ValidaDadosEndereco implements IStrategy {

    @Override
    public String processar(EntidadeDominio entidade) {
        Endereco endereco = (Endereco) entidade;

        if (endereco.getIdentificacao().trim().equals("") || endereco.getIdentificacao() == null) {
            return "Identificação do endereço inválido";
        }

        if (endereco.getLogradouro().trim().equals("") || endereco.getLogradouro() == null) {
            return "Logradouro do endereço inválido";
        }
        if (endereco.getNumeroEndereco().trim().equals("") || endereco.getNumeroEndereco() == null) {
            return "Número do endereço inválido";
        }
        if (endereco.getComplemento().trim().equals("") || endereco.getComplemento() == null) {
            return ("Complemento do endereço inválido.\n");
        }
        if (endereco.getBairro().trim().equals("") || endereco.getBairro() == null) {
            return ("Bairro do endereço inválido.\n");
        }
        if (endereco.getCidade().trim().equals("") || endereco.getCidade() == null) {
            return ("Cidade do endereço inválido.\n");
        }
        if (endereco.getSiglaEstado().trim().equals("S") || endereco.getSiglaEstado() == null) {
            return "Estado do endereço inválido.";
        }
        if (endereco.getCep().trim().equals("") || endereco.getCep() == null) {
            return ("CEP inválida.\n");
        }
        return null;
    }

}
