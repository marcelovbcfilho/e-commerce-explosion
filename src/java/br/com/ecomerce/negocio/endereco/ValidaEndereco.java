
package br.com.ecomerce.negocio.endereco;

import br.com.ecomerce.dominio.Endereco;
import br.com.ecomerce.dominio.EntidadeDominio;
import br.com.ecomerce.negocio.IStrategy;

public class ValidaEndereco implements IStrategy{

    @Override
    public String processar(EntidadeDominio entidade) {
        // Validando Endereço
        Endereco endereco = (Endereco) entidade;
        StringBuilder msg = new StringBuilder();

        // Validando estado
        if (endereco.getSiglaEstado().trim().equals("")) {
            msg.append("O Estado é obrigatório.");
        }

        // Validando Cidade
        if (endereco.getCidade().trim().equals("")) {
            msg.append("A Cidade é obrigatório.");
        }

        // Validando Bairro
        if (endereco.getBairro().trim().equals("")) {
            msg.append("O Bairro é obrigatório.");
        }
        // Validando demais dados de endereço
        if (endereco.getLogradouro().trim().equals("")) {
            msg.append("O logradouro é obrigatório.");
        }

        if (endereco.getNumeroEndereco().trim().equals("")) {
            msg.append("O número do endereço é obrigatório.");
        }

        if (endereco.getCep().trim().equals("")) {
            msg.append("O CEP é obrigatório.");
        }
        
        
        return msg.toString();
    }
}
