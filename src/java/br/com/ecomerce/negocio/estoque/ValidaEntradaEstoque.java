package br.com.ecomerce.negocio.estoque;

import br.com.ecomerce.dominio.EntidadeDominio;
import br.com.ecomerce.dominio.Estoque;
import br.com.ecomerce.negocio.IStrategy;

public class ValidaEntradaEstoque implements IStrategy {

    @Override
    public String processar(EntidadeDominio entidade) {
        Estoque estoque = (Estoque) entidade;
        StringBuilder msg = new StringBuilder();
        
        if (estoque.getQuantidadeEstoque() < 0) {

            msg.append("A quantidade em estoque não pode ser negativa.");
        }

        return msg.toString();
    }

}
