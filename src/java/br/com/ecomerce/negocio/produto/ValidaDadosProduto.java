
package br.com.ecomerce.negocio.produto;

import br.com.ecomerce.dominio.EntidadeDominio;
import br.com.ecomerce.dominio.Produto;
import br.com.ecomerce.negocio.IStrategy;
import br.com.ecomerce.negocio.IStrategy;
import java.text.ParseException;
import java.util.Date;

public class ValidaDadosProduto implements IStrategy {

    @Override
    public String processar(EntidadeDominio entidade) {
        Produto produto = (Produto) entidade;
        StringBuilder msg = new StringBuilder();

        if (produto.getNome().equals("") || produto.getNome() == null) {
            msg.append("<p>O nome do produto é obrigatório.</p>");
        }

        if (produto.getDescricao().equals("") || produto.getDescricao() == null) {
            msg.append("<p>A descrição do produto é obrigatório.</p>");
        }

        if (produto.getPeso() == 0.0) {
            msg.append("<p>O peso do produto é obrigatório.</p>");
        }

        if (produto.getPreco() == 0.0) {
            msg.append("<p>O preço do produto é obrigatório.</p>");
        }

        if (produto.getEndereco_imagem().equals("") || produto.getEndereco_imagem() == null) {
            msg.append("<p>A imagem principal do produto é obrigatório.</p>");
        }

        if (produto.getEndereco_imagem_lado().equals("") || produto.getEndereco_imagem_lado() == null) {
            msg.append("<p>A imagem de lado do produto é obrigatório.</p>");
        }

        if (produto.getEndereco_imagem_costas().equals("") || produto.getEndereco_imagem_costas() == null) {
            msg.append("<p>A imagem de costas do produto é obrigatório.</p>");
        }

        if (produto.getSubCategoria().getId() == 0) {
            msg.append("<p>A subcategoria do produto é obrigatório.</p>");
        }

        if (produto.getSubCategoria().getCategoria().getId() == 0) {
            msg.append("<p>A categoria do produto é obrigatório.</p>");
        }

        return msg.toString();
    }

}
