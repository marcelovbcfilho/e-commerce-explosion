
package br.com.ecomerce.negocio.subcategoria;

import br.com.ecomerce.dominio.EntidadeDominio;
import br.com.ecomerce.dominio.SubCategoria;
import br.com.ecomerce.negocio.IStrategy;
import br.com.ecomerce.negocio.IStrategy;
import java.text.ParseException;
import java.util.Date;

public class ValidaDadosSubCategoria implements IStrategy {

    @Override
    public String processar(EntidadeDominio entidade) {
        SubCategoria subCategoria = (SubCategoria) entidade;
        StringBuilder msg = new StringBuilder();

        if (subCategoria.getCategoria().getId() == 0) {
            msg.append("<p>Toda sub-categoria deve estar ligada a uma categoria.</p>");
        }

        if (subCategoria.getNome().equals("") || subCategoria.getNome() == null) {
            msg.append("<p>O nome da sub-categoria é obrigatória.</p>");
        }

        return msg.toString();
    }

}
