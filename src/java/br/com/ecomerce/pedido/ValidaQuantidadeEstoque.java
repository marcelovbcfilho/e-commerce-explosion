package br.com.ecomerce.pedido;

import br.com.ecomerce.dao.EstoqueDAO;
import br.com.ecomerce.dao.ProdutoDAO;
import br.com.ecomerce.dominio.Categoria;
import br.com.ecomerce.dominio.EntidadeDominio;
import br.com.ecomerce.dominio.Estoque;
import br.com.ecomerce.dominio.Pedido;
import br.com.ecomerce.dominio.Produto;
import br.com.ecomerce.dominio.SubCategoria;
import br.com.ecomerce.negocio.IStrategy;

public class ValidaQuantidadeEstoque implements IStrategy {

    @Override
    public String processar(EntidadeDominio entidade) {
        Pedido pedido = (Pedido) entidade;
        StringBuilder msg = new StringBuilder();
        EstoqueDAO estoqueDAO = new EstoqueDAO();
        int quantidadeRestante = 0;        
        
        for (Produto produto : pedido.getProdutos().getProdutos()) {            
            Estoque produtoEstoque = new Estoque();              
            produtoEstoque.setProduto(produto);
            produtoEstoque.getProduto().setSubCategoria(new SubCategoria());
            produtoEstoque.getProduto().getSubCategoria().setCategoria(new Categoria());
            produtoEstoque = (Estoque) estoqueDAO.consultar(produtoEstoque).get(0);
            quantidadeRestante = produtoEstoque.getQuantidadeEstoque() - produto.getQuantidadeProdutoCarrinho();
            if (quantidadeRestante < 0) {
                msg.append("<p>A quantidade "+produto.getNome()+" excedeu a quantidade restante no estoque de "+ produto.getQuantidadeEstoque() + ".</p>");
            }
        }

        return msg.toString();
    }

}
