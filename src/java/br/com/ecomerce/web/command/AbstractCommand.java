
package br.com.ecomerce.web.command;

import br.com.ecomerce.web.fachada.Fachada;
import br.com.ecomerce.web.fachada.IFachada;


public abstract class AbstractCommand implements ICommand {
    protected IFachada fachada = new Fachada();
}
