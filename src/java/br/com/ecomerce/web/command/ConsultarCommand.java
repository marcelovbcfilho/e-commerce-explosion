
package br.com.ecomerce.web.command;

import br.com.ecomerce.dominio.EntidadeDominio;
import br.com.ecomerce.dominio.Resultado;


public class ConsultarCommand extends AbstractCommand {

    @Override
    public Resultado executar(EntidadeDominio entidadeDominio) {
        return fachada.consultar(entidadeDominio);
    }
    
}
