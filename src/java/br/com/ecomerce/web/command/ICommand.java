
package br.com.ecomerce.web.command;

import br.com.ecomerce.dominio.EntidadeDominio;
import br.com.ecomerce.dominio.Resultado;


public interface ICommand {
    public Resultado executar(EntidadeDominio entidadeDominio);
}
