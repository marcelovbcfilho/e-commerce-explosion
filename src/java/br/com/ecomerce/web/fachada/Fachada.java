package br.com.ecomerce.web.fachada;

import br.com.ecomerce.dao.AnaliseGraficoDAO;
import br.com.ecomerce.dao.BoletoDAO;
import br.com.ecomerce.dao.CartaoCreditoDAO;
import br.com.ecomerce.dao.CategoriaDAO;
import br.com.ecomerce.dao.ClienteDAO;
import br.com.ecomerce.dao.DevolucaoDAO;
import br.com.ecomerce.dao.EnderecoDAO;
import br.com.ecomerce.dao.EstoqueDAO;
import br.com.ecomerce.dao.IDAO;
import br.com.ecomerce.dao.MetodoEntregaDAO;
import br.com.ecomerce.dao.PedidoDAO;
import br.com.ecomerce.dao.ProdutoDAO;
import br.com.ecomerce.dao.SubCategoriaDAO;
import br.com.ecomerce.dominio.AnaliseGrafico;
import br.com.ecomerce.dominio.Boleto;
import br.com.ecomerce.dominio.CartaoCredito;
import br.com.ecomerce.dominio.Categoria;
import br.com.ecomerce.dominio.Cliente;
import br.com.ecomerce.dominio.Devolucao;
import br.com.ecomerce.dominio.Endereco;
import br.com.ecomerce.dominio.EntidadeDominio;
import br.com.ecomerce.dominio.Estoque;
import br.com.ecomerce.dominio.MetodoEntrega;
import br.com.ecomerce.dominio.Pedido;
import br.com.ecomerce.dominio.Produto;
import br.com.ecomerce.dominio.Resultado;
import br.com.ecomerce.dominio.Senha;
import br.com.ecomerce.dominio.SubCategoria;
import br.com.ecomerce.negocio.cliente.CriptografaSenha;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import br.com.ecomerce.negocio.*;
import br.com.ecomerce.negocio.IStrategy;
import br.com.ecomerce.negocio.cartao.ValidaCartao;
import br.com.ecomerce.negocio.categoria.ValidaDadosCategoria;
import br.com.ecomerce.negocio.cliente.ValidaCPF;
import br.com.ecomerce.negocio.cliente.ValidaConfirmaSenha;
import br.com.ecomerce.negocio.cliente.ValidaDadosCliente;
import br.com.ecomerce.negocio.cliente.ValidaEmailExistente;
import br.com.ecomerce.negocio.cliente.ValidaExistenciaEndereco;
import br.com.ecomerce.negocio.cliente.ValidaPadraoSenha;
import br.com.ecomerce.negocio.cliente.ValidaSenha;
import br.com.ecomerce.negocio.cliente.ValidaTrocaSenha;
import br.com.ecomerce.negocio.endereco.ValidaDadosEndereco;
import br.com.ecomerce.negocio.endereco.ValidaEndereco;
import br.com.ecomerce.negocio.estoque.ValidaEntradaEstoque;
import br.com.ecomerce.negocio.produto.ValidaDadosProduto;
import br.com.ecomerce.negocio.subcategoria.ValidaDadosSubCategoria;
import br.com.ecomerce.pedido.ValidaQuantidadeEstoque;
import java.sql.SQLException;

public class Fachada implements IFachada {

    private Map<String, IDAO> daos;

    // Mapa Macro, com TODAS as regras de negócio
    // Observe: ele é um mapa, de um mapa. E o mapa "menor" tem a lista de strategys
    private Map<String, Map<String, List<IStrategy>>> regrasNegocio;
    private StringBuilder sb = new StringBuilder();
    private Resultado resultado;

    public Fachada() {
        // instanciando mapas de daos e regras de negócio macro
        daos = new HashMap<String, IDAO>();
        // Instanciando o mapa macro;
        regrasNegocio = new HashMap<String, Map<String, List<IStrategy>>>();

        // adicionando todas os daos ao hash de daos
        daos.put(Cliente.class.getName(), new ClienteDAO());
        daos.put(Senha.class.getName(), new ClienteDAO());
        daos.put(Endereco.class.getName(), new EnderecoDAO());
        daos.put(CartaoCredito.class.getName(), new CartaoCreditoDAO());
        daos.put(Produto.class.getName(), new ProdutoDAO());
        daos.put(SubCategoria.class.getName(), new SubCategoriaDAO());
        daos.put(Categoria.class.getName(), new CategoriaDAO());
        daos.put(Boleto.class.getName(), new BoletoDAO());
        daos.put(Pedido.class.getName(), new PedidoDAO());
        daos.put(Devolucao.class.getName(), new DevolucaoDAO());
        daos.put(MetodoEntrega.class.getName(), new MetodoEntregaDAO());
        daos.put(Estoque.class.getName(), new EstoqueDAO());
        daos.put(AnaliseGrafico.class.getName(), new AnaliseGraficoDAO());

        // --------------------- Hash Cliente ------------------------------//
        // Criando RNs de cliente
        ValidaDadosCliente validaDadosCliente = new ValidaDadosCliente();
        ValidaSenha vSenha = new ValidaSenha();
        ValidaCPF validaCpf = new ValidaCPF();
        ValidaPadraoSenha vPadraoSenha = new ValidaPadraoSenha();
        CriptografaSenha criptografaSenha = new CriptografaSenha();
        ValidaConfirmaSenha validaConfirmaSenha = new ValidaConfirmaSenha();
        ValidaExistenciaEndereco validaExistenciaEnd = new ValidaExistenciaEndereco();
        ValidaEmailExistente validaEmailExistente = new ValidaEmailExistente();
        
        // Criando lista de RNs do Cliente Salvar
        List<IStrategy> rnsClienteSalvar = new ArrayList<IStrategy>();
        rnsClienteSalvar.add(validaDadosCliente);
        rnsClienteSalvar.add(validaCpf);
        rnsClienteSalvar.add(validaExistenciaEnd);
        rnsClienteSalvar.add(vSenha);
        rnsClienteSalvar.add(validaConfirmaSenha);
        rnsClienteSalvar.add(vPadraoSenha);
        rnsClienteSalvar.add(criptografaSenha);
        rnsClienteSalvar.add(validaEmailExistente);
        
        // Criando lista de RNs do Cliente Alterar
        List<IStrategy> rnsClienteAlterar = new ArrayList<IStrategy>();
        rnsClienteAlterar.add(validaDadosCliente);
        rnsClienteAlterar.add(validaCpf);

        // Criando mapa de Operação Cliente, RNs da operação de Cliente.
        Map<String, List<IStrategy>> mapaCliente = new HashMap<String, List<IStrategy>>();

        // Adiconando lista de RNs para cada operação de cliente
        mapaCliente.put("SALVAR", rnsClienteSalvar);
        mapaCliente.put("ALTERAR", rnsClienteAlterar);

        regrasNegocio.put(Cliente.class.getName(), mapaCliente);

        // ------------------------ Hash Senha -------------------------------//
        // Criando RNs de senha
        List<IStrategy> rnsSenhaAlterar = new ArrayList<IStrategy>();
        ValidaTrocaSenha vTrocaSenha = new ValidaTrocaSenha();
        rnsSenhaAlterar.add(vTrocaSenha);

        // Criando mapa de Operação Senha, RNs da operação de Senha.
        Map<String, List<IStrategy>> mapaSenha = new HashMap<String, List<IStrategy>>();

        // Adiconando lista de RNs para cada operação de cliente
        mapaSenha.put("ALTERAR", rnsSenhaAlterar);
        regrasNegocio.put(Senha.class.getName(), mapaSenha);

        // --------------------------- Hash Endereco -------------------------//
        // Criando RNs de endereço
        ValidaEndereco vEndereco = new ValidaEndereco();

        // Criando lista de RNs do endereço Salvar
        List<IStrategy> rnsEnderecoSalvar = new ArrayList<IStrategy>();
        rnsEnderecoSalvar.add(vEndereco);

        // Criando lista de RNs do endereço Alterar
        List<IStrategy> rnsEnderecoAlterar = new ArrayList<IStrategy>();
        rnsEnderecoAlterar.add(vEndereco);

        // Criando mapa de Operação Endereco, RNs da operação de Endereco.
        Map<String, List<IStrategy>> mapaEndereco = new HashMap<String, List<IStrategy>>();

        // Adiconando lista de RNs para cada operação de Endereco
        mapaEndereco.put("SALVAR", rnsEnderecoSalvar);
        mapaEndereco.put("ALTERAR", rnsEnderecoAlterar);

        regrasNegocio.put(Endereco.class.getName(), mapaEndereco);

        // ------------------- Hash Cartão de Crédito ------------------------//
        // Criando RNs de cartão
        ValidaCartao vCartao = new ValidaCartao();

        // Criando lista de RNs de cartao Salvar
        List<IStrategy> rnsCartaoCreditoSalvar = new ArrayList<IStrategy>();
        rnsCartaoCreditoSalvar.add(vCartao);

        // Criando lista de RNs de cartao Alterar
        List<IStrategy> rnsCartaoCreditoAlterar = new ArrayList<IStrategy>();
        rnsCartaoCreditoAlterar.add(vCartao);

        // Criando mapa de Operação CartaoCredito, RNs da operação de CartaoCredito.
        Map<String, List<IStrategy>> mapaCartaoCredito = new HashMap<String, List<IStrategy>>();

        // Adiconando lista de RNs para cada operação de Endereco
        mapaCartaoCredito.put("SALVAR", rnsCartaoCreditoSalvar);
        mapaCartaoCredito.put("ALTERAR", rnsCartaoCreditoAlterar);

        regrasNegocio.put(CartaoCredito.class.getName(), mapaCartaoCredito);

        // ------------------------ Hash Produtos -----------------------------//
        // Criando RNs de Produto
        //ValidaDadosProduto vDadosProduto = new ValidaDadosProduto();

        // Criando lista de RNs do Produto Salvar
        List<IStrategy> rnsProdutoSalvar = new ArrayList<IStrategy>();
        //rnsProdutoSalvar.add(vDadosProduto);

        // Criando lista de RNs do Produto Alterar
        List<IStrategy> rnsProdutoAlterar = new ArrayList<IStrategy>();
        //rnsProdutoAlterar.add(vDadosProduto);

        // Criando mapa de Operação Produto, RNs da operação de Produto.
        Map<String, List<IStrategy>> mapaProduto = new HashMap<String, List<IStrategy>>();

        // Adiconando lista de RNs para cada operação de Endereco
        mapaProduto.put("SALVAR", rnsProdutoSalvar);
        mapaProduto.put("ALTERAR", rnsProdutoAlterar);

        regrasNegocio.put(Produto.class.getName(), mapaProduto);

        // -------------------------- Hash Categoria --------------------------//
        // Criando lista de RNs do CategoriaSalvar
        List<IStrategy> rnsCategoriaSalvar = new ArrayList<IStrategy>();
        ValidaDadosCategoria vDadosCategoria = new ValidaDadosCategoria();
        rnsCategoriaSalvar.add(vDadosCategoria);

        // Criando lista de RNs do CategoriaAlterar
        List<IStrategy> rnsCategoriaAlterar = new ArrayList<IStrategy>();
        rnsCategoriaAlterar.add(vDadosCategoria);

        // Criando mapa de Operação Categoria, RNs da operação de categoria.
        Map<String, List<IStrategy>> mapaCategoria = new HashMap<String, List<IStrategy>>();

        // Adiconando lista de RNs para cada operação de Categoria
        mapaCategoria.put("SALVAR", rnsCategoriaSalvar);
        mapaCategoria.put("ALTERAR", rnsCategoriaAlterar);

        regrasNegocio.put(Categoria.class.getName(), mapaCategoria);

        // --------------------------- Hash Sub-Categoria --------------------//
        // Criando lista de RNs do SubCategoriaSalvar
        List<IStrategy> rnsSubCategoriaSalvar = new ArrayList<IStrategy>();
        ValidaDadosSubCategoria vDadosSubCategoria = new ValidaDadosSubCategoria();
        rnsSubCategoriaSalvar.add(vDadosSubCategoria);

        // Criando lista de RNs do SubCategoriaAlterar
        List<IStrategy> rnsSubCategoriaAlterar = new ArrayList<IStrategy>();
        rnsSubCategoriaAlterar.add(vDadosSubCategoria);

        // Criando mapa de Operação Categoria, RNs da operação de categoria.
        Map<String, List<IStrategy>> mapaSubCategoria = new HashMap<String, List<IStrategy>>();

        // Adiconando lista de RNs para cada operação de Categoria
        mapaSubCategoria.put("SALVAR", rnsSubCategoriaSalvar);
        mapaSubCategoria.put("ALTERAR", rnsSubCategoriaAlterar);

        regrasNegocio.put(SubCategoria.class.getName(), mapaSubCategoria);

        // ------------------------- Hash Pedido -----------------------------//
        // Criando lista de RNs do Pedido Salvar              
        List<IStrategy> rnsPedidoSalvar = new ArrayList<IStrategy>();
        ValidaQuantidadeEstoque vQtdEstoque = new ValidaQuantidadeEstoque();

        rnsPedidoSalvar.add(vQtdEstoque);
        // Criando lista de RNs do Pedido Alterar
        List<IStrategy> rnsPedidoAlterar = new ArrayList<IStrategy>();

        // Criando mapa de Operação Pedido, RNs da operação de Pedido.
        Map<String, List<IStrategy>> mapaPedido = new HashMap<String, List<IStrategy>>();

        // Adiconando lista de RNs para cada operação de edido
        mapaPedido.put("SALVAR", rnsPedidoSalvar);
        mapaPedido.put("ALTERAR", rnsPedidoAlterar);

        regrasNegocio.put(Pedido.class.getName(), mapaPedido);

        // ------------------------- Hash Devolucao -----------------------------//
        // Criando lista de RNs do Devolucao Salvar
        List<IStrategy> rnsDevolucaoSalvar = new ArrayList<IStrategy>();

        // Criando lista de RNs do Devolucao Alterar
        List<IStrategy> rnsDevolucaoAlterar = new ArrayList<IStrategy>();

        // Criando mapa de Operação Devolucao, RNs da operação de Pedido.
        Map<String, List<IStrategy>> mapaDevolucao = new HashMap<String, List<IStrategy>>();

        // Adiconando lista de RNs para cada operação de Devolucao
        mapaDevolucao.put("SALVAR", rnsDevolucaoSalvar);
        mapaDevolucao.put("ALTERAR", rnsDevolucaoAlterar);

        regrasNegocio.put(Devolucao.class.getName(), mapaDevolucao);

        // ------------------------- Hash Boleto -----------------------------//
        // Criando lista de RNs do Boleto Salvar
        List<IStrategy> rnsBoletoSalvar = new ArrayList<IStrategy>();

        // Criando lista de RNs do Boleto Alterar
        List<IStrategy> rnsBoletoAlterar = new ArrayList<IStrategy>();

        // Criando mapa de Operação Boleto, RNs da operação de Boleto.
        Map<String, List<IStrategy>> mapaBoleto = new HashMap<String, List<IStrategy>>();

        // Adiconando lista de RNs para cada operação de Boleto
        mapaBoleto.put("SALVAR", rnsBoletoSalvar);
        mapaBoleto.put("ALTERAR", rnsBoletoAlterar);

        regrasNegocio.put(Boleto.class.getName(), mapaBoleto);

        // ------------------------- Hash MetodoEntrega -----------------------------//
        // Criando lista de RNs do MetodoEntrega Salvar
        List<IStrategy> rnsMetodoEntregaSalvar = new ArrayList<IStrategy>();

        // Criando lista de RNs do MetodoEntrega Alterar
        List<IStrategy> rnsMetodoEntregaAlterar = new ArrayList<IStrategy>();

        // Criando mapa de Operação MetodoEntrega, RNs da operação de MetodoEntrega.
        Map<String, List<IStrategy>> mapaMetodoEntrega = new HashMap<String, List<IStrategy>>();

        // Adiconando lista de RNs para cada operação de Boleto
        mapaMetodoEntrega.put("SALVAR", rnsMetodoEntregaSalvar);
        mapaMetodoEntrega.put("ALTERAR", rnsMetodoEntregaAlterar);

        regrasNegocio.put(MetodoEntrega.class.getName(), mapaMetodoEntrega);

        // ------------------------- Hash Estoque -----------------------------//
        // Criando lista de RNs do Estoque Salvar
        List<IStrategy> rnsEstoqueSalvar = new ArrayList<IStrategy>();

        // Criando lista de RNs do Estoque Alterar
        List<IStrategy> rnsEstoqueAlterar = new ArrayList<IStrategy>();
        ValidaEntradaEstoque vEntradaEstoque = new ValidaEntradaEstoque();

        rnsEstoqueAlterar.add(vEntradaEstoque);
        // Criando mapa de Operação Estoque, RNs da operação de Estoque.
        Map<String, List<IStrategy>> mapaEstoque = new HashMap<String, List<IStrategy>>();

        // Adiconando lista de RNs para cada operação de Estoque
        mapaEstoque.put("SALVAR", rnsEstoqueSalvar);
        mapaEstoque.put("ALTERAR", rnsEstoqueAlterar);

        regrasNegocio.put(Estoque.class.getName(), mapaEstoque);
        
        // ------------------------- Hash AnaliseGrafico -----------------------------//
        // Criando lista de RNs do AnaliseGrafico Salvar
        List<IStrategy> rnsAnaliseGraficoSalvar = new ArrayList<IStrategy>();

        // Criando lista de RNs do AnaliseGrafico Alterar
        List<IStrategy> rnsAnaliseGraficoAlterar = new ArrayList<IStrategy>();
        

        // Criando mapa de Operação Estoque, RNs da operação de AnaliseGrafico.
        Map<String, List<IStrategy>> mapaAnaliseGrafico = new HashMap<String, List<IStrategy>>();

        // Adiconando lista de RNs para cada operação de AnaliseGrafico
        mapaAnaliseGrafico.put("SALVAR", rnsAnaliseGraficoSalvar);
        mapaAnaliseGrafico.put("ALTERAR", rnsAnaliseGraficoAlterar);

        regrasNegocio.put(AnaliseGrafico.class.getName(), mapaAnaliseGrafico);
        // ---------------------- Fim Regras de negócio ----------------------//
    }

    private void executarRegras(EntidadeDominio entidade, List<IStrategy> rnsEntidade) {
        for (IStrategy rn : rnsEntidade) {
            String msg = rn.processar(entidade);
            if (msg != null) {
                sb.append(msg);
            }
        }
    }

    @Override
    public Resultado salvar(EntidadeDominio entidade) {
        resultado = new Resultado();
        sb.setLength(0);
        String nmClasse = entidade.getClass().getName();

        Map<String, List<IStrategy>> mapaEntidade = regrasNegocio.get(nmClasse);
        List<IStrategy> rnsEntidade = mapaEntidade.get("SALVAR");

        executarRegras(entidade, rnsEntidade);

        if (sb.length() == 0) {
            IDAO dao = daos.get(nmClasse);
            dao.salvar(entidade);
            resultado.addEntidade(entidade);
        } else {
            resultado.addEntidade(entidade);
            resultado.setMsg(sb.toString());
        }

        return resultado;

    }

    @Override
    public Resultado alterar(EntidadeDominio entidade) {
        resultado = new Resultado();
        sb.setLength(0);
        String nmClasse = entidade.getClass().getName();

        Map<String, List<IStrategy>> mapaEntidade = regrasNegocio.get(nmClasse);
        List<IStrategy> rnsEntidade = mapaEntidade.get("ALTERAR");

        executarRegras(entidade, rnsEntidade);

        if (sb.length() == 0) {
            IDAO dao = daos.get(nmClasse);
            dao.alterar(entidade);
            resultado.addEntidade(entidade);
        } else {
            resultado.addEntidade(entidade);
            resultado.setMsg(sb.toString());
        }

        return resultado;
    }

    @Override
    public Resultado excluir(EntidadeDominio entidade) {
        resultado = new Resultado();
        String nmClasse = entidade.getClass().getName();

        IDAO dao = daos.get(nmClasse);
        resultado.addEntidade(entidade);
        dao.excluir(entidade);

        return resultado;
    }

    @Override
    public Resultado consultar(EntidadeDominio entidade) {
        resultado = new Resultado();
        String nmClasse = entidade.getClass().getName();

        IDAO dao = daos.get(nmClasse);
        resultado.setEntidades(dao.consultar(entidade));

        return resultado;

    }

    @Override
    public Resultado visualizar(EntidadeDominio entidade) {
        resultado = new Resultado();
        String nmClasse = entidade.getClass().getName();

        IDAO dao = daos.get(nmClasse);
        resultado.setEntidades(dao.consultar(entidade));
        return resultado;
    }
}
