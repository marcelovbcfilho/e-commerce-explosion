package br.com.ecomerce.web.viewHelper;

import br.com.ecomerce.dominio.AnaliseGrafico;
import br.com.ecomerce.dominio.EntidadeDominio;
import br.com.ecomerce.dominio.Produto;
import br.com.ecomerce.dominio.Resultado;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class AnaliseGraficoVH implements IViewHelper {

    @Override
    public EntidadeDominio getEntidade(HttpServletRequest request) {
        AnaliseGrafico analiseGrafico = new AnaliseGrafico();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        if (request.getParameter("OPERACAO").trim().equals("VISUALIZAR")) {
            String dataInicio = request.getParameter("dataInicio");
            String dataFim = request.getParameter("dataFim");            
            int tipoAnalise = Integer.valueOf(request.getParameter("selectTipoAnalise"));

            if (tipoAnalise == 1 || tipoAnalise == 2) {
                String idProdutos[] = request.getParameterValues("checkProduto");

                ArrayList<Produto> produtos = new ArrayList<Produto>();

                for (int i = 0; i < idProdutos.length; i++) {
                    Produto produto = new Produto();
                    produto.setId(Integer.valueOf(idProdutos[i]));
                    produtos.add(produto);
                }
                
                analiseGrafico.setTipoAnalise(tipoAnalise);
                analiseGrafico.setProdutos(produtos);

            } else {                
                analiseGrafico.setTipoAnalise(tipoAnalise);
            }
            Date dataInicioDate = new Date();
            Date dataFimDate = new Date();
            try {
                dataInicioDate = sdf.parse(dataInicio);
                dataFimDate = sdf.parse(dataFim);
            } catch (ParseException ex) {
                Logger.getLogger(AnaliseGraficoVH.class.getName()).log(Level.SEVERE, null, ex);
            }
            analiseGrafico.setDataInicio(dataInicioDate);
            analiseGrafico.setDataFim(dataFimDate);
            return analiseGrafico;
        }
        return null;
    }

    @Override
    public void setView(Resultado resultado, HttpServletRequest request, HttpServletResponse response) throws IOException {
        AnaliseGrafico analiseGrafico;
        if (null == resultado.getEntidades().get(0)) {
            analiseGrafico = null;
        } else {
            analiseGrafico = (AnaliseGrafico) resultado.getEntidades().get(0);
        }
        RequestDispatcher d = null;
        PrintWriter out = response.getWriter();
        String operacao = request.getParameter("OPERACAO");

        if (resultado.getMsg() == null && operacao.equals("VISUALIZAR")) {
            request.setAttribute("resultado", resultado);

            //d = request.getRequestDispatcher("logado/logadoAdmin/analise-grafico.jsp");
            d = request.getRequestDispatcher("logado/logadoAdmin/analise-grafico.jsp");
            try {
                d.forward(request, response);
            } catch (ServletException ex) {
                Logger.getLogger(AnaliseGraficoVH.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }

}
