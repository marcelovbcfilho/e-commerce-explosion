package br.com.ecomerce.web.viewHelper;

import br.com.ecomerce.dominio.Boleto;
import br.com.ecomerce.dominio.EntidadeDominio;
import br.com.ecomerce.dominio.Resultado;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class BoletoVH implements IViewHelper{

    @Override
    public EntidadeDominio getEntidade(HttpServletRequest request) {
        if (request.getParameter("OPERACAO").trim().equals("SALVAR")){
            
            return new Boleto();
        }
        return null;
    }

    @Override
    public void setView(Resultado resultado, HttpServletRequest request, HttpServletResponse response) throws IOException {
        Boleto boleto = (Boleto) resultado.getEntidades().get(0);
        RequestDispatcher d = null;
        PrintWriter out = response.getWriter();
        String operacao = request.getParameter("OPERACAO");

        if (operacao.equals("SALVAR")) {
            request.setAttribute("resultado", resultado);
            request.setAttribute("operacao", "SALVAR");
            request.setAttribute("entidade", "ENDERECO");

            if (resultado.getMsg() == null) {
                d = request.getRequestDispatcher("logado/finalizar4.jsp");
            } else {
                resultado.setMsg("<h1>" + resultado.getMsg() + "</h1>");
                d = request.getRequestDispatcher("resultado.jsp");
            }

            try {
                d.forward(request, response);
            } catch (ServletException ex) {
                Logger.getLogger(EnderecoVH.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        if (operacao.equals("ALTERAR")) {
            
        }

        if (resultado.getMsg() == null && operacao.equals("VISUALIZAR")) {
            
        }

        if (operacao.equals("EXCLUIR")) {
            
        }
    }
    
}
