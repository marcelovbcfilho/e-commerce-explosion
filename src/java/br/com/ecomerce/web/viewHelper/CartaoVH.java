package br.com.ecomerce.web.viewHelper;

import br.com.ecomerce.controladora.Servlet;
import br.com.ecomerce.dominio.CartaoCredito;
import br.com.ecomerce.dominio.CartaoCreditoPedido;
import br.com.ecomerce.dominio.Cliente;
import br.com.ecomerce.dominio.EntidadeDominio;
import br.com.ecomerce.dominio.Pedido;
import br.com.ecomerce.dominio.Resultado;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class CartaoVH implements IViewHelper {

    @Override
    public EntidadeDominio getEntidade(HttpServletRequest request) {
        HttpSession session = request.getSession();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

        if (request.getParameter("OPERACAO").trim().equals("SALVAR")) {
            String bandeiraCartao = request.getParameter("selectBandeiraCartao");
            String codigoSeguranca = request.getParameter("txtCodigoSegurancaCartao");
            String numeroCartao = request.getParameter("txtNumeroCartao");
            String nomeImpresso = request.getParameter("txtNomeImpressoCartao");
            String mes = request.getParameter("selectMesCartao");
            int ano = Integer.valueOf(request.getParameter("selectAnoCartao"));

            if (bandeiraCartao.trim().equals("S") || bandeiraCartao == null) {
                bandeiraCartao = "";
            }

            if (codigoSeguranca.trim().equals("") || codigoSeguranca == null) {
                codigoSeguranca = "";
            }

            if (numeroCartao.trim().equals("") || numeroCartao == null) {
                numeroCartao = "";
            }

            if (nomeImpresso.trim().equals("") || nomeImpresso == null) {
                nomeImpresso = "";
            }

            if (mes.trim().equals("") || nomeImpresso == null) {
                mes = "";
            }

            // Criando Cartão de Crédito
            CartaoCredito cartao = new CartaoCredito(numeroCartao, bandeiraCartao, nomeImpresso, mes, ano, codigoSeguranca);
            Cliente cliente = new Cliente();
            cliente.setId(Integer.valueOf(request.getParameter("idCliente")));
            cartao.setCliente(cliente);
            cartao.setId(0);

            if (request.getParameter("PAGINA").equals("FINALIZARCOMPRA")) {
                Pedido pedido = (Pedido) session.getAttribute("pedido");
                ArrayList<CartaoCreditoPedido> cartoes = new ArrayList<>();
                String qtdParcelas = request.getParameter("qtdParcelas");
                cartoes.add(new CartaoCreditoPedido(cartao, Integer.valueOf(request.getParameter("qtdParcelas")), pedido.getValorTotal()));
                pedido.setCartoes(cartoes);
                session.setAttribute("pedido", pedido);
            }

            return cartao;
        }

        if (request.getParameter("OPERACAO").trim().equals("ALTERAR")) {
            String bandeiraCartao = request.getParameter("selectBandeiraCartao");
            String codigoSeguranca = request.getParameter("txtCodigoSegurancaCartao");
            String numeroCartao = request.getParameter("txtNumeroCartao");
            String nomeImpresso = request.getParameter("txtNomeImpressoCartao");
            String mes = request.getParameter("selectMesCartao");
            int ano = Integer.valueOf(request.getParameter("selectAnoCartao"));

            if (bandeiraCartao.trim().equals("S") || bandeiraCartao == null) {
                bandeiraCartao = "";
            }

            if (codigoSeguranca.trim().equals("") || codigoSeguranca == null) {
                codigoSeguranca = "";
            }

            if (numeroCartao.trim().equals("") || numeroCartao == null) {
                numeroCartao = "";
            }

            if (nomeImpresso.trim().equals("") || nomeImpresso == null) {
                nomeImpresso = "";
            }

            if (mes.trim().equals("") || nomeImpresso == null) {
                mes = "";
            }

            // Criando Cartão de Crédito
            CartaoCredito cartao = new CartaoCredito(numeroCartao, bandeiraCartao, nomeImpresso, mes, ano, codigoSeguranca);
            cartao.setId(Integer.valueOf(request.getParameter("idCartao")));
            Cliente cliente = new Cliente();
            cliente.setId(Integer.valueOf(request.getParameter("idCliente")));
            cartao.setCliente(cliente);

            return cartao;
        }

        if (request.getParameter("OPERACAO").trim().equals("EXCLUIR")) {
            CartaoCredito cartao = new CartaoCredito();
            Cliente cliente = new Cliente();
            cliente.setId(Integer.valueOf(request.getParameter("idCliente")));
            cartao.setCliente(cliente);
            cartao.setId(Integer.valueOf(request.getParameter("idCartao")));
            return cartao;
        }

        if (request.getParameter("OPERACAO").trim().equals("VISUALIZAR")) {
            String pagina = request.getParameter("PAGINA");
            // Criando cartão
            CartaoCredito cartao = new CartaoCredito();
            Cliente cliente = new Cliente();
            cliente.setId(0);
            cartao.setCliente(cliente);
            if (pagina == null) {
                cartao.setId(Integer.valueOf(request.getParameter("idCartao")));
            } else {
                String id = request.getParameter("radioCartao");
                if (id == null) {
                    cartao.setId(0);
                } else {
                    cartao.setId(Integer.valueOf(id));
                }
            }
            return cartao;
        }
        return null;
    }

    @Override
    public void setView(Resultado resultado, HttpServletRequest request, HttpServletResponse response) throws IOException {
        CartaoCredito cartao = (CartaoCredito) resultado.getEntidades().get(0);
        RequestDispatcher d = null;
        PrintWriter out = response.getWriter();
        String operacao = request.getParameter("OPERACAO");

        if (operacao.equals("SALVAR")) {
            request.setAttribute("resultado", resultado);
            request.setAttribute("operacao", "SALVAR");
            request.setAttribute("entidade", "CARTAO");

            if (resultado.getMsg() == null) {
                if (request.getParameter("PAGINA").equals("MINHACONTA")) {
                    resultado.setMsg("<h1> Cartao com número: " + cartao.getNumeroCartao() + "</h1><br/><h1>e Nome: " + cartao.getNomeImpresso() + " salvo com sucesso!!!</h1>");
                    d = request.getRequestDispatcher("resultado.jsp");
                }

                if (request.getParameter("PAGINA").equals("FINALIZARCOMPRA")) {
                    d = request.getRequestDispatcher("logado/finalizar4.jsp");
                }
            } else {
                resultado.setMsg("<h1>" + resultado.getMsg() + "</h1>");
                d = request.getRequestDispatcher("resultado.jsp");
            }

            try {
                d.forward(request, response);
            } catch (ServletException ex) {
                Logger.getLogger(ClienteVH.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        if (operacao.equals("ALTERAR")) {
            request.setAttribute("resultado", resultado);
            request.setAttribute("entidade", "CARTAO");

            if (resultado.getMsg() == null) {
                resultado.setMsg("<h1> Cartao com número: " + cartao.getNumeroCartao() + "</h1><br/><h1>e Nome: " + cartao.getNomeImpresso() + " alterado com sucesso!!!</h1>");
            } else {
                resultado.setMsg("<h1>" + resultado.getMsg() + "</h1>");
            }

            d = request.getRequestDispatcher("resultado.jsp");
            try {
                d.forward(request, response);
            } catch (ServletException ex) {
                Logger.getLogger(ClienteVH.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        if (resultado.getMsg() == null && operacao.equals("VISUALIZAR")) {
            request.setAttribute("resultado", resultado);

            String pagina = request.getParameter("PAGINA");

            if (pagina == null) {
                d = request.getRequestDispatcher("logado/alterar-cartao.jsp");
            } else {
                d = request.getRequestDispatcher("logado/finalizar4.jsp");
            }
            try {
                d.forward(request, response);
            } catch (ServletException ex) {
                Logger.getLogger(EnderecoVH.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        if (operacao.equals("EXCLUIR")) {
            resultado.setMsg("Exclusão realizada com sucesso.");
            request.setAttribute("resultado", resultado);
            request.setAttribute("operacao", "EXCLUIR");
            request.setAttribute("entidade", "CARTAO");
            d = request.getRequestDispatcher("resultado.jsp");
            try {
                d.forward(request, response);
            } catch (ServletException ex) {
                Logger.getLogger(ClienteVH.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
