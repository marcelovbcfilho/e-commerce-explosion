package br.com.ecomerce.web.viewHelper;

import br.com.ecomerce.dominio.Categoria;
import br.com.ecomerce.dominio.EntidadeDominio;
import br.com.ecomerce.dominio.Resultado;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class CategoriaVH implements IViewHelper {

    @Override
    public EntidadeDominio getEntidade(HttpServletRequest request) {
        if(request.getParameter("OPERACAO") == null){
            Categoria cat = new Categoria("");
            cat.setId(0);
            
            return cat;
        }
        if (request.getParameter("OPERACAO").trim().equals("SALVAR")) {
            String nome = request.getParameter("txtNome");

            if (nome.trim().equals("") || nome == null) {
                nome = "";
            }

            // Criando Cartão de Crédito
            Categoria categoria = new Categoria(nome);
            categoria.setId(0);

            return categoria;
        }
        if (request.getParameter("OPERACAO").trim().equals("EXCLUIR")) {
            Categoria categoria = new Categoria();
            categoria.setId(Integer.valueOf(request.getParameter("idCategoria")));
            categoria.setNome("");
            return categoria;
        }

        if (request.getParameter("OPERACAO").trim().equals("ALTERAR")) {
            String nome = request.getParameter("txtNome");

            if (nome.trim().equals("") || nome == null) {
                nome = "";
            }

            // Criando Categoria
            Categoria categoria = new Categoria(nome);
            categoria.setId(Integer.valueOf(request.getParameter("idCategoria")));

            return categoria;
        }

        if (request.getParameter("OPERACAO").trim().equals("CONSULTAR")) {
            Categoria categoria = new Categoria();
            categoria.setId(0);
            return categoria;
        }

        if (request.getParameter("OPERACAO").trim().equals("VISUALIZAR")) {
            Categoria categoria = new Categoria();
            categoria.setId(Integer.valueOf(request.getParameter("idCategoria")));
            return categoria;
        }

        return null;
    }

    @Override
    public void setView(Resultado resultado, HttpServletRequest request, HttpServletResponse response) throws IOException {
        Categoria categoria = (Categoria) resultado.getEntidades().get(0);
        RequestDispatcher d = null;
        PrintWriter out = response.getWriter();
        String operacao = request.getParameter("OPERACAO");

        if (operacao == null) {
            HttpSession session = request.getSession();
            session.setAttribute("categorias", resultado);
            d = request.getRequestDispatcher("index.jsp");
            try {
                d.forward(request, response);
            } catch (ServletException ex) {
                Logger.getLogger(CategoriaVH.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            if (operacao.equals("CONSULTAR")) {
                request.setAttribute("resultado", resultado);
                d = request.getRequestDispatcher("/logado/logadoAdmin/categorias.jsp");
                try {
                    d.forward(request, response);
                } catch (ServletException ex) {
                    Logger.getLogger(CategoriaVH.class.getName()).log(Level.SEVERE, null, ex);
                }
            }

            if (operacao.equals("SALVAR")) {
                request.setAttribute("resultado", resultado);
                request.setAttribute("operacao", "SALVAR");
                request.setAttribute("entidade", "CATEGORIA");

                if (resultado.getMsg() == null) {
                    resultado.setMsg("<h1> Categoria: " + categoria.getNome() + " salvo com sucesso!!!</h1>");
                } else {
                    resultado.setMsg("<h1>" + resultado.getMsg() + "</h1>");
                }

                d = request.getRequestDispatcher("/resultado.jsp");
                try {
                    d.forward(request, response);
                } catch (ServletException ex) {
                    Logger.getLogger(CategoriaVH.class.getName()).log(Level.SEVERE, null, ex);
                }
            }

            if (resultado.getMsg() == null && operacao.equals("EXCLUIR")) {
                request.setAttribute("resultado", resultado);
                request.setAttribute("operacao", "EXCLUIR");
                request.setAttribute("entidade", "CATEGORIA");

                if (resultado.getMsg() == null) {
                    resultado.setMsg("<h1> Categoria: " + categoria.getNome() + " excluida com sucesso!</h1>");
                } else {
                    resultado.setMsg("<h1>" + resultado.getMsg() + "</h1>");
                }

                d = request.getRequestDispatcher("resultado.jsp");
                try {
                    d.forward(request, response);
                } catch (ServletException ex) {
                    Logger.getLogger(CategoriaVH.class.getName()).log(Level.SEVERE, null, ex);
                }
            }

            if (operacao.equals("ALTERAR")) {
                request.setAttribute("resultado", resultado);
                request.setAttribute("entidade", "CATEGORIA");

                if (resultado.getMsg() == null) {
                    resultado.setMsg("<h1> Categoria: " + categoria.getNome() + " alterado com sucesso!!!</h1>");
                } else {
                    resultado.setMsg("<h1>" + resultado.getMsg() + "</h1>");
                }

                d = request.getRequestDispatcher("resultado.jsp");
                try {
                    d.forward(request, response);
                } catch (ServletException ex) {
                    Logger.getLogger(CategoriaVH.class.getName()).log(Level.SEVERE, null, ex);
                }
            }

            if (operacao.equals("VISUALIZAR")) {
                request.setAttribute("resultado", resultado);
                d = request.getRequestDispatcher("/logado/logadoAdmin/alterar-categoria.jsp");
                try {
                    d.forward(request, response);
                } catch (ServletException ex) {
                    Logger.getLogger(CategoriaVH.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            if (operacao.equals("") || operacao == null) {
                request.setAttribute("resultado", resultado);
                d = request.getRequestDispatcher("/logado/logadoAdmin/categorias.jsp");
                try {
                    d.forward(request, response);
                } catch (ServletException ex) {
                    Logger.getLogger(CategoriaVH.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }
}
