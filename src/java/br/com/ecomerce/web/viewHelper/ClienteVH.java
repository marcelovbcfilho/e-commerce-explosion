package br.com.ecomerce.web.viewHelper;

import br.com.ecomerce.controladora.Servlet;
import br.com.ecomerce.dominio.CartaoCredito;
import br.com.ecomerce.dominio.Cliente;
import br.com.ecomerce.dominio.Endereco;
import br.com.ecomerce.dominio.EntidadeDominio;
import br.com.ecomerce.dominio.Resultado;
import br.com.ecomerce.dominio.Senha;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class ClienteVH implements IViewHelper {

    @Override
    public EntidadeDominio getEntidade(HttpServletRequest request) {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

        if (request.getParameter("OPERACAO").trim().equals("SALVAR")) {
            String identificacao = request.getParameter("txtIdentificacao");
            String logradouro = request.getParameter("txtLogradouro");
            String numeroEndereco = request.getParameter("txtNumero");
            String cep = request.getParameter("txtCep");
            String complemento = request.getParameter("txtComplemento");
            String bairro = request.getParameter("txtBairro");
            String cidade = request.getParameter("txtCidade");
            String siglaEstado = request.getParameter("selectEstado");

            Cliente cliente = new Cliente();

            // Criando Endereço 
            // Cria Pais
            if (identificacao.trim().equals("") || identificacao == null) {
                identificacao = "";
            }

            // Criando demais dados de endereço
            if (logradouro.trim().equals("") || logradouro == null) {
                logradouro = "";
            }

            if (numeroEndereco.trim().equals("") || numeroEndereco == null) {
                numeroEndereco = "";
            }

            if (cep.trim().equals("") || cep == null) {
                cep = "";
            }

            if (bairro.trim().equals("") || bairro == null) {
                bairro = "";
            }

            if (cidade.trim().equals("") || cidade == null) {
                cidade = "";
            }

            if (siglaEstado.trim().equals("S") || siglaEstado == null) {
                siglaEstado = "";
            }

            Endereco endereco = new Endereco(identificacao, logradouro, numeroEndereco, complemento, bairro, cidade, siglaEstado, cep, cliente);
            ArrayList<Endereco> enderecos = new ArrayList<Endereco>();
            enderecos.add(endereco);

            // Criando Cliente
            String nomeCompleto = request.getParameter("txtNomeCompleto");
            String cpf = request.getParameter("txtCpf");
            String rg = request.getParameter("txtRg");
            Date dataNascimento = new Date();
            try {
                dataNascimento = sdf.parse(request.getParameter("dateDataNascimento"));
            } catch (ParseException ex) {
                dataNascimento.setTime(0);
            }
            String email = request.getParameter("txtEmail");
            String senhaConfirma = request.getParameter("txtConfirmaSenha");
            String senha = request.getParameter("txtSenha");
            String status = "ATIVO";
            
            Senha senhaCliente = new Senha();

            if (dataNascimento != null) {
                cliente.setDataNascimento(dataNascimento);
            }

            if (!nomeCompleto.trim().equals("") || nomeCompleto != null) {
                cliente.setNomeCompleto(nomeCompleto);
            } else {
                cliente.setNomeCompleto("");
            }

            if (!cpf.trim().equals("") || cpf != null) {
                cliente.setCpf(cpf);
            } else {
                cliente.setCpf("");
            }

            if (!rg.trim().equals("") || rg != null) {
                cliente.setRg(rg);
            } else {
                cliente.setRg("");
            }

            if (!email.trim().equals("") || email != null) {
                cliente.setEmail(email);
            } else {
                cliente.setEmail("");
            }

            if (!senhaConfirma.equals("") || senhaConfirma != null) {
                senhaCliente.setConfirmaSenha(senhaConfirma);
            } else {
                senhaCliente.setConfirmaSenha("");
            }

            if (!senha.trim().equals("") || senha != null) {
                senhaCliente.setSenha(senha);
            } else {
                senhaCliente.setSenha("");
            }

            cliente.setSenha(senhaCliente);
            
            cliente.setStatus(status);

            // Seta o endereco do cliente
            cliente.setEnderecos(enderecos);
            cliente.setCarteira(0);
            
            return cliente;
        }

        if (request.getParameter("OPERACAO").trim().equals("ALTERAR")) {
            Cliente cliente = new Cliente();

            // Criando Cliente
            String nomeCompleto = request.getParameter("txtNomeCompleto");
            String cpf = request.getParameter("txtCpf");
            String rg = request.getParameter("txtRg");
            Date dataNascimento = new Date();
            if (request.getParameter("dateDataNascimento") == null) {
                dataNascimento.setTime(0);
            } else {
                try {
                    dataNascimento = sdf.parse(request.getParameter("dateDataNascimento"));
                } catch (ParseException ex) {
                    dataNascimento.setTime(0);
                }
            }
            String email = request.getParameter("txtEmail");
            String status = "ATIVO";
            int idCliente = Integer.valueOf(request.getParameter("idCliente"));

            if (dataNascimento != null) {
                cliente.setDataNascimento(dataNascimento);
            }

            if (!nomeCompleto.trim().equals("") || nomeCompleto != null) {
                cliente.setNomeCompleto(nomeCompleto);
            } else {
                cliente.setNomeCompleto("");
            }

            if (!cpf.trim().equals("") || cpf != null) {
                cliente.setCpf(cpf);
            } else {
                cliente.setCpf("");
            }

            if (!rg.trim().equals("") || rg != null) {
                cliente.setRg(rg);
            } else {
                cliente.setRg("");
            }

            if (!email.trim().equals("") || email != null) {
                cliente.setEmail(email);
            } else {
                cliente.setEmail("");
            }

            Senha senha = new Senha();
            senha.setSenha(null);
            
            cliente.setSenha(senha);
            
            cliente.setStatus(status);

            cliente.setId(idCliente);

            return cliente;
        }

        if (request.getParameter("OPERACAO").trim().equals("EXCLUIR")) {
            Cliente cliente = new Cliente();
            cliente.setId(Integer.valueOf(request.getParameter("idCliente")));
            cliente.setNomeCompleto("");
            return cliente;
        }

        if (request.getParameter("OPERACAO").trim().equals("CONSULTAR")) {
            String nomeCompleto = request.getParameter("txtNomeCompleto");
            Cliente cliente = new Cliente();

            cliente.setNomeCompleto("");

            return cliente;
        }

        if (request.getParameter("OPERACAO").trim().equals("VISUALIZAR")) {
            Cliente cliente = new Cliente();
            cliente.setId(Integer.valueOf(request.getParameter("idCliente")));
            cliente.setNomeCompleto("");
            return cliente;
        }

        if (request.getParameter("OPERACAO").trim().equals("LOGIN")) {
            Cliente cliente = new Cliente();
            cliente.setEmail(request.getParameter("txtEmail"));
            Senha senha = new Senha();
            senha.setSenha(request.getParameter("txtSenha"));
            cliente.setSenha(senha);
            return cliente;
        }
        return null;
    }

    @Override
    public void setView(Resultado resultado, HttpServletRequest request, HttpServletResponse response) throws IOException {
        Cliente cliente;
        
        try {
            cliente = (Cliente) resultado.getEntidades().get(0);
        } catch (Exception e) {
            cliente = null;
        }
        RequestDispatcher d = null;
        PrintWriter out = response.getWriter();
        String operacao = request.getParameter("OPERACAO");
        if (operacao.equals("CONSULTAR")) {
            request.setAttribute("resultado", resultado);
            d = request.getRequestDispatcher("listar-clientes.jsp");
            try {
                d.forward(request, response);
            } catch (ServletException ex) {
                Logger.getLogger(ClienteVH.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        if (operacao.equals("SALVAR")) {
            request.setAttribute("resultado", resultado);
            request.setAttribute("operacao", "SALVAR");
            request.setAttribute("entidade", "CLIENTE");

            if (resultado.getMsg() == null) {
                resultado.setMsg("<h1> Cliente: " + cliente.getNomeCompleto() + " salvo com sucesso!!!</h1>");
            } else {
                resultado.setMsg("<h1>" + resultado.getMsg() + "</h1>");
            }

            d = request.getRequestDispatcher("resultado.jsp");
            try {
                d.forward(request, response);
            } catch (ServletException ex) {
                Logger.getLogger(ClienteVH.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        if (operacao.equals("ALTERAR")) {
            request.setAttribute("resultado", resultado);
            request.setAttribute("entidade", "CLIENTE");

            if (resultado.getMsg() == null) {
                resultado.setMsg("<h1> Cliente: " + cliente.getNomeCompleto() + " alterado com sucesso!!!</h1>");
            } else {
                resultado.setMsg("<h1>" + resultado.getMsg() + "</h1>");
            }

            d = request.getRequestDispatcher("resultado.jsp");
            try {
                d.forward(request, response);
            } catch (ServletException ex) {
                Logger.getLogger(ClienteVH.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        if (resultado.getMsg() == null && operacao.equals("EXCLUIR")) {
            request.setAttribute("resultado", resultado);
            request.setAttribute("operacao", "EXCLUIR");
            request.setAttribute("entidade", "CLIENTE");

            if (resultado.getMsg() == null) {
                resultado.setMsg("<h1> Cliente: " + cliente.getNomeCompleto() + " desativado com sucesso!</h1>");
            } else {
                resultado.setMsg("<h1>" + resultado.getMsg() + "</h1>");
            }

            d = request.getRequestDispatcher("resultado.jsp");
            try {
                d.forward(request, response);
            } catch (ServletException ex) {
                Logger.getLogger(ClienteVH.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        if (resultado.getMsg() == null && operacao.equals("VISUALIZAR")) {
            if (request.getParameter("PAGINA").trim().equals("ALTERAR")) {
                request.setAttribute("resultado", resultado);
                d = request.getRequestDispatcher("logado/alterar-dados.jsp");
                try {
                    d.forward(request, response);
                } catch (ServletException ex) {
                    Logger.getLogger(ClienteVH.class.getName()).log(Level.SEVERE, null, ex);
                }
            } else if (request.getParameter("PAGINA").trim().equals("PERFIL")) {
                request.setAttribute("resultado", resultado);
                d = request.getRequestDispatcher("minha-conta.jsp");
                try {
                    d.forward(request, response);
                } catch (ServletException ex) {
                    Logger.getLogger(ClienteVH.class.getName()).log(Level.SEVERE, null, ex);
                }
            } else if (request.getParameter("PAGINA").trim().equals("SENHA")) {
                request.setAttribute("resultado", resultado);
                d = request.getRequestDispatcher("Alterar/alterarSenha.jsp");
                try {
                    d.forward(request, response);
                } catch (ServletException ex) {
                    Logger.getLogger(ClienteVH.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

        if (resultado.getMsg() == null && operacao.equals("LOGIN")) {
            if (request.getParameter("PAGINA").trim().equals("LOGIN")) {
                request.setAttribute("resultado", resultado);
                if (cliente == null) {
                    resultado.setMsg("Login ou senha incorretos.");
                    d = request.getRequestDispatcher("login.jsp");
                } else {
                    cliente = (Cliente) resultado.getEntidades().get(0);
                    if (cliente.getStatus().trim().equals("INATIVO")) {
                        resultado.setMsg("Conta desativada.");
                        d = request.getRequestDispatcher("login.jsp");
                    } else if (cliente.getAdmin() == 1) {
                        d = request.getRequestDispatcher("minha-conta-admin.jsp");
                    } else {
                        d = request.getRequestDispatcher("minha-conta.jsp");
                    }
                }
                try {
                    d.forward(request, response);
                } catch (ServletException ex) {
                    Logger.getLogger(ClienteVH.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }
}
