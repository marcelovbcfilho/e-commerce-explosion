package br.com.ecomerce.web.viewHelper;

import br.com.ecomerce.dominio.Cliente;
import br.com.ecomerce.dominio.Devolucao;
import br.com.ecomerce.dominio.EntidadeDominio;
import br.com.ecomerce.dominio.Pedido;
import br.com.ecomerce.dominio.Produto;
import br.com.ecomerce.dominio.ProdutosPedido;
import br.com.ecomerce.dominio.Resultado;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class DevolucaoVH implements IViewHelper {

    @Override
    public EntidadeDominio getEntidade(HttpServletRequest request) {
        HttpSession session = request.getSession();
        Cliente cliente = (Cliente) session.getAttribute("cliente");
        Devolucao devolucao = new Devolucao();
        if (request.getParameter("OPERACAO").trim().equals("SALVAR")) {

            // Dados recuperados da session temporario do pedido
            Pedido pedido = (Pedido) session.getAttribute("devolucao");

            // Pedidos a serem trocados
            String idProdutos[] = request.getParameterValues("checkProduto");
            
            // Quantidade de produtos a serem trocados
            String qtdeProdutos[] = request.getParameterValues("qtdeProduto");

            // Criando devoluções e seus dependentes
            String numeroPedidoDevolucao = pedido.getId() + "";
            ArrayList<Produto> produtos = new ArrayList<Produto>();

            // Objetos criados para formar o objeto da devolução
            for (int i = 0; i < idProdutos.length; i++) {
                for (Produto produtoTemp : pedido.getProdutos().getProdutos()) {
                    if(produtoTemp.getId() == Integer.valueOf(idProdutos[i])){
                        produtoTemp.setQuantidadeEstoque(produtoTemp.getQuantidadeProdutoCarrinho() - Integer.valueOf(qtdeProdutos[i]));
                        produtoTemp.setDescricao(qtdeProdutos[i]);
                        produtos.add(produtoTemp);
                    }
                }
            }

            // Salvando atributos de devolução
            devolucao.setDataSolicitacao(new Date());
            devolucao.setNumeroPedidoDevolucao(numeroPedidoDevolucao);
            devolucao.setCliente(cliente);
            devolucao.setProdutos(produtos);
            devolucao.setStatus("PROCESSANDO");
        }

        if (request.getParameter("OPERACAO").trim().equals("ALTERAR")) {
            int idDevolucao = Integer.valueOf(request.getParameter("idDevolucao"));

            devolucao = new Devolucao();

            devolucao.setCliente(null);
            devolucao.setId(idDevolucao);
            devolucao.setStatus("APROVADO");
        }

        if (request.getParameter("OPERACAO").trim().equals("CONSULTAR")) {
            if (cliente.getAdmin() == 1) {
                devolucao.setCliente(null);
                devolucao.setId(0);
            } else {
                devolucao.setCliente(cliente);
                devolucao.setId(0);
            }
        }

        if (request.getParameter("OPERACAO").trim().equals("VISUALIZAR")) {
            int idDevolucao = Integer.valueOf(request.getParameter("idDevolucao"));

            devolucao.setCliente(null);
            devolucao.setId(idDevolucao);
        }

        return devolucao;
    }

    @Override
    public void setView(Resultado resultado, HttpServletRequest request, HttpServletResponse response) throws IOException {
        Devolucao devolucao;
        RequestDispatcher d = null;
        HttpSession session = request.getSession();
        String operacao = request.getParameter("OPERACAO");
        Cliente cliente = (Cliente) session.getAttribute("cliente");

        if (operacao.equals("SALVAR")) {
            devolucao = (Devolucao) resultado.getEntidades().get(0);
            request.setAttribute("resultado", resultado);
            request.setAttribute("operacao", "SALVAR");
            request.setAttribute("entidade", "DEVOLUCAO");

            if (resultado.getMsg() == null) {
                resultado.setMsg("<h1> Devolucao: " + devolucao.getId() + " salvo com sucesso!!!</h1>");
            } else {
                resultado.setMsg("<h1>" + resultado.getMsg() + "</h1>");
            }

            d = request.getRequestDispatcher("resultado.jsp");
            try {
                d.forward(request, response);
            } catch (ServletException ex) {
                Logger.getLogger(ClienteVH.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        if (operacao.equals("ALTERAR")) {
            d = request.getRequestDispatcher("/minha-conta-admin.jsp");
            try {
                d.forward(request, response);
            } catch (ServletException ex) {
                Logger.getLogger(ClienteVH.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        if (operacao.equals("CONSULTAR")) {
            request.setAttribute("resultado", resultado);

            if (cliente.getAdmin() == 1) {
                d = request.getRequestDispatcher("/logado/logadoAdmin/devolucoes-solicitadas.jsp");
            } else {
                d = request.getRequestDispatcher("/logado/devolucoes.jsp");
            }

            try {
                d.forward(request, response);
            } catch (ServletException ex) {
                Logger.getLogger(ClienteVH.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        if (operacao.equals("VISUALIZAR")) {
            devolucao = (Devolucao) resultado.getEntidades().get(0);
            request.setAttribute("resultado", resultado);

            if (cliente.getAdmin() == 1) {
                d = request.getRequestDispatcher("/logado/logadoAdmin/visualizar-devolucao.jsp");
            } else {
                d = request.getRequestDispatcher("/logado/visualizar-devolucao.jsp");
            }

            try {
                d.forward(request, response);
            } catch (ServletException ex) {
                Logger.getLogger(ClienteVH.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

}
