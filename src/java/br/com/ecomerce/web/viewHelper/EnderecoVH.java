package br.com.ecomerce.web.viewHelper;

import br.com.ecomerce.dominio.Cliente;
import br.com.ecomerce.dominio.Endereco;
import br.com.ecomerce.dominio.EntidadeDominio;
import br.com.ecomerce.dominio.Resultado;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class EnderecoVH implements IViewHelper {

    @Override
    public EntidadeDominio getEntidade(HttpServletRequest request) {

        // Construindo Endereço para salvar
        if (request.getParameter("OPERACAO").trim().equals("SALVAR")) {
            String identificacao = request.getParameter("txtIdentificacao");
            String logradouro = request.getParameter("txtLogradouro");
            String numeroEndereco = request.getParameter("txtNumero");
            String cep = request.getParameter("txtCep");
            String complemento = request.getParameter("txtComplemento");
            String bairro = request.getParameter("txtBairro");
            String cidade = request.getParameter("txtCidade");
            String siglaEstado = request.getParameter("selectEstado");

            Cliente cliente = new Cliente();

            // Criando Endereço 
            // Cria Pais
            if (identificacao.trim().equals("") || identificacao == null) {
                identificacao = "";
            }

            // Criando demais dados de endereço
            if (logradouro.trim().equals("") || logradouro == null) {
                logradouro = "";
            }

            if (numeroEndereco.trim().equals("") || numeroEndereco == null) {
                numeroEndereco = "";
            }

            if (cep.trim().equals("") || cep == null) {
                cep = "";
            }

            if (bairro.trim().equals("") || bairro == null) {
                bairro = "";
            }

            if (cidade.trim().equals("") || cidade == null) {
                cidade = "";
            }

            if (siglaEstado.trim().equals("S") || siglaEstado == null) {
                siglaEstado = "";
            }

            Endereco endereco = new Endereco(identificacao, logradouro, numeroEndereco, complemento, bairro, cidade, siglaEstado, cep, cliente);
            cliente.setId(Integer.valueOf(request.getParameter("idCliente")));
            endereco.setCliente(cliente);
            endereco.setId(0);
            return endereco;
        }

        // Construindo Endereço para alterar
        if (request.getParameter("OPERACAO").trim().equals("ALTERAR")) {
            // Criando Endereço 
            String identificacao = request.getParameter("txtIdentificacao");
            String logradouro = request.getParameter("txtLogradouro");
            String numeroEndereco = request.getParameter("txtNumero");
            String cep = request.getParameter("txtCep");
            String complemento = request.getParameter("txtComplemento");
            String bairro = request.getParameter("txtBairro");
            String cidade = request.getParameter("txtCidade");
            String siglaEstado = request.getParameter("selectEstado");

            Cliente cliente = new Cliente();

            // Criando Endereço 
            // Cria Pais
            if (identificacao.trim().equals("") || identificacao == null) {
                identificacao = "";
            }

            // Criando demais dados de endereço
            if (logradouro.trim().equals("") || logradouro == null) {
                logradouro = "";
            }

            if (numeroEndereco.trim().equals("") || numeroEndereco == null) {
                numeroEndereco = "";
            }

            if (cep.trim().equals("") || cep == null) {
                cep = "";
            }

            if (bairro.trim().equals("") || bairro == null) {
                bairro = "";
            }

            if (cidade.trim().equals("") || cidade == null) {
                cidade = "";
            }

            if (siglaEstado.trim().equals("S") || siglaEstado == null) {
                siglaEstado = "";
            }

            Endereco endereco = new Endereco(identificacao, logradouro, numeroEndereco, complemento, bairro, cidade, siglaEstado, cep, cliente);
            cliente.setId(Integer.valueOf(request.getParameter("idCliente")));
            endereco.setCliente(cliente);
            endereco.setId(Integer.valueOf(request.getParameter("idEndereco")));
            return endereco;
        }

        // Contruindo Endereço para visualizar
        if (request.getParameter("OPERACAO").trim().equals("VISUALIZAR")) {
            String pagina = request.getParameter("PAGINA");
            // Criando Endereço 
            Endereco endereco = new Endereco();
            Cliente cliente = new Cliente();
            cliente.setId(0);
            endereco.setCliente(cliente);
            if (pagina == null) {
                endereco.setId(Integer.valueOf(request.getParameter("idEndereco")));
            } else {
                String id = request.getParameter("radioEndereco");
                if (id == null) {
                    endereco.setId(0);
                } else {
                    endereco.setId(Integer.valueOf(id));
                }
            }
            return endereco;
        }

        // Contruindo Endereço para visualizar
        if (request.getParameter("OPERACAO").trim().equals("EXCLUIR")) {
            // Criando Endereço 
            Endereco endereco = new Endereco();
            Cliente cliente = new Cliente();
            cliente.setId(Integer.valueOf(request.getParameter("idCliente")));
            endereco.setCliente(cliente);
            endereco.setId(Integer.valueOf(request.getParameter("idEndereco")));
            return endereco;
        }

        return null;
    }

    @Override
    public void setView(Resultado resultado, HttpServletRequest request, HttpServletResponse response) throws IOException {
        Endereco endereco = (Endereco) resultado.getEntidades().get(0);
        RequestDispatcher d = null;
        PrintWriter out = response.getWriter();
        String operacao = request.getParameter("OPERACAO");

        if (operacao.equals("SALVAR")) {
            request.setAttribute("resultado", resultado);
            request.setAttribute("operacao", "SALVAR");
            request.setAttribute("entidade", "ENDERECO");

            if (resultado.getMsg() == null) {
                if (request.getParameter("PAGINA").equals("MINHACONTA")) {
                    resultado.setMsg("<h1> Endereco com CEP: " + endereco.getCep() + " salvo com sucesso!!!</h1>");
                    d = request.getRequestDispatcher("resultado.jsp");
                }

                if (request.getParameter("PAGINA").equals("FINALIZARCOMPRA")) {
                    d = request.getRequestDispatcher("logado/finalizar2.jsp");
                }
            } else {
                resultado.setMsg("<h1>" + resultado.getMsg() + "</h1>");
                d = request.getRequestDispatcher("resultado.jsp");
            }

            try {
                d.forward(request, response);
            } catch (ServletException ex) {
                Logger.getLogger(EnderecoVH.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        if (operacao.equals("ALTERAR")) {
            request.setAttribute("resultado", resultado);
            request.setAttribute("entidade", "ENDERECO");

            if (resultado.getMsg() == null) {
                resultado.setMsg("<h1> Endereco com CEP: " + endereco.getCep() + " alterado com sucesso!!!</h1>");
            } else {
                resultado.setMsg("<h1>" + resultado.getMsg() + "</h1>");
            }

            d = request.getRequestDispatcher("resultado.jsp");
            try {
                d.forward(request, response);
            } catch (ServletException ex) {
                Logger.getLogger(EnderecoVH.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        if (resultado.getMsg() == null && operacao.equals("VISUALIZAR")) {
            request.setAttribute("resultado", resultado);
            
            String pagina = request.getParameter("PAGINA");
            
            if (pagina == null) {
                d = request.getRequestDispatcher("logado/alterar-endereco.jsp");
            } else {
                d = request.getRequestDispatcher("logado/finalizar2.jsp");
            }
            try {
                d.forward(request, response);
            } catch (ServletException ex) {
                Logger.getLogger(EnderecoVH.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        if (operacao.equals("EXCLUIR")) {
            resultado.setMsg("Exclusão realizada com sucesso.");
            request.setAttribute("resultado", resultado);
            request.setAttribute("operacao", "EXCLUIR");
            request.setAttribute("entidade", "ENDERECO");
            d = request.getRequestDispatcher("resultado.jsp");
            try {
                d.forward(request, response);
            } catch (ServletException ex) {
                Logger.getLogger(EnderecoVH.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
