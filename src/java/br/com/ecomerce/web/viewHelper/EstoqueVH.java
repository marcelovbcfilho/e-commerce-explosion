package br.com.ecomerce.web.viewHelper;

import br.com.ecomerce.dominio.Categoria;
import br.com.ecomerce.dominio.EntidadeDominio;
import br.com.ecomerce.dominio.Estoque;
import br.com.ecomerce.dominio.Produto;
import br.com.ecomerce.dominio.Resultado;
import br.com.ecomerce.dominio.SubCategoria;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class EstoqueVH implements IViewHelper {

    @Override
    public EntidadeDominio getEntidade(HttpServletRequest request) {
        if (request.getParameter("OPERACAO").trim().equals("SALVAR")) {

        }

        if (request.getParameter("OPERACAO").trim().equals("ALTERAR")) {
            Estoque estoque = new Estoque();            
            if (null != request.getParameter("txtQtd")) {
                int id = Integer.valueOf(request.getParameter("idEstoque"));
                int quantidadeEstoque = 0;
                if (request.getParameter("txtQtd").trim().equals("")) {
                    quantidadeEstoque = 0;
                }else
                {
                    quantidadeEstoque = Integer.valueOf(request.getParameter("txtQtd"));
                }
                
                estoque.setId(id);
                estoque.setQuantidadeEstoque(quantidadeEstoque);
                estoque.setProduto(new Produto());                
                estoque.getProduto().setNome("");
                return estoque;
            } else if (null != request.getParameter("ACAO")) {
                int id = Integer.valueOf(request.getParameter("idEstoque"));
                estoque.setId(id);
                if (request.getParameter("ACAO").equals("INATIVAR")) {
                    estoque.setStatus(false);
                }else
                {
                    estoque.setStatus(true);
                }
                estoque.setProduto(new Produto());
                estoque.getProduto().setNome("STATUS");
                return estoque;
            }
        }

        if (request.getParameter("OPERACAO").trim().equals("EXCLUIR")) {

        }

        if (request.getParameter("OPERACAO").trim().equals("VISUALIZAR")) {
            // Criando o produto
            Estoque estoque = new Estoque();

            int id;

            if (null != request.getParameter("idSubCategoria")) {
                id = Integer.valueOf(request.getParameter("idSubCategoria"));

                estoque.setProduto(new Produto());
                estoque.getProduto().setSubCategoria(new SubCategoria());
                estoque.getProduto().getSubCategoria().setCategoria(new Categoria());
                estoque.getProduto().getSubCategoria().setId(id);

                return estoque;
            } else if (null != request.getParameter("idCategoria")) {
                id = Integer.valueOf(request.getParameter("idCategoria"));

                estoque.setProduto(new Produto());
                estoque.getProduto().setSubCategoria(new SubCategoria());
                estoque.getProduto().getSubCategoria().setCategoria(new Categoria());
                estoque.getProduto().getSubCategoria().getCategoria().setId(id);

                return estoque;
            } else if (request.getParameter("PAGINA").equals("ESTOQUE")) {
                estoque.setProduto(new Produto());
                estoque.getProduto().setSubCategoria(new SubCategoria());
                estoque.getProduto().getSubCategoria().setCategoria(new Categoria());
                return estoque;
            }

        }

        if (request.getParameter("OPERACAO").trim().equals("CONSULTAR")) {

        }

        return null;
    }

    @Override
    public void setView(Resultado resultado, HttpServletRequest request, HttpServletResponse response) throws IOException {
        Estoque estoque;
        if (null == resultado.getEntidades().get(0)) {
            estoque = null;
        } else {
            estoque = (Estoque) resultado.getEntidades().get(0);
        }
        RequestDispatcher d = null;
        PrintWriter out = response.getWriter();
        String operacao = request.getParameter("OPERACAO");

        if (operacao.equals("ALTERAR")) {
            request.setAttribute("resultado", resultado);
            request.setAttribute("entidade", "ESTOQUE");
            request.setAttribute("operacao", "ALTERAR");

            if (resultado.getMsg() == null) {
                resultado.setMsg("<h1> Estoque: " + estoque.getId() + " alterado com sucesso!!!</h1>");
            } else {
                resultado.setMsg("<h1>" + resultado.getMsg() + "</h1>");
            }

            d = request.getRequestDispatcher("resultado.jsp");
            try {
                d.forward(request, response);
            } catch (ServletException ex) {
                Logger.getLogger(EnderecoVH.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        if (operacao.equals("VISUALIZAR")) {
            if (request.getParameter("PAGINA").equals("CATALOGO")) {
                request.setAttribute("resultado", resultado);
                d = request.getRequestDispatcher("produtos/catalogo-produtos.jsp");
                try {
                    d.forward(request, response);

                } catch (ServletException ex) {
                    Logger.getLogger(EstoqueVH.class
                            .getName()).log(Level.SEVERE, null, ex);
                }
            } else if (request.getParameter("PAGINA").equals("ESTOQUE")) {
                request.setAttribute("resultado", resultado);
                d = request.getRequestDispatcher("logado/logadoAdmin/consultar-estoque.jsp");
                try {
                    d.forward(request, response);

                } catch (ServletException ex) {
                    Logger.getLogger(EstoqueVH.class
                            .getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

}
