package br.com.ecomerce.web.viewHelper;

import br.com.ecomerce.dominio.EntidadeDominio;
import br.com.ecomerce.dominio.MetodoEntrega;
import br.com.ecomerce.dominio.Resultado;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class MetodoEntregaVH implements IViewHelper{

    @Override
    public EntidadeDominio getEntidade(HttpServletRequest request) {
        if (request.getParameter("OPERACAO").trim().equals("VISUALIZAR")) {
            MetodoEntrega metodoEntrega = new MetodoEntrega();
            metodoEntrega.setId(Integer.valueOf(request.getParameter("metodo_entrega")));
            return metodoEntrega;
        }
        
        return null;
    }

    @Override
    public void setView(Resultado resultado, HttpServletRequest request, HttpServletResponse response) throws IOException {
        MetodoEntrega metodoEntrega = (MetodoEntrega) resultado.getEntidades().get(0);
        RequestDispatcher d = null;
        PrintWriter out = response.getWriter();
        String operacao = request.getParameter("OPERACAO");
        
        if (operacao.equals("VISUALIZAR")) {
                request.setAttribute("resultado", resultado);
                d = request.getRequestDispatcher("/logado/finalizar3.jsp");
                try {
                    d.forward(request, response);
                } catch (ServletException ex) {
                    Logger.getLogger(CategoriaVH.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
    }
    
}
