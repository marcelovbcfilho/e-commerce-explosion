package br.com.ecomerce.web.viewHelper;

import br.com.ecomerce.dominio.Carrinho;
import br.com.ecomerce.dominio.CartaoCredito;
import br.com.ecomerce.dominio.CartaoCreditoPedido;
import br.com.ecomerce.dominio.Cliente;
import br.com.ecomerce.dominio.Endereco;
import br.com.ecomerce.dominio.EntidadeDominio;
import br.com.ecomerce.dominio.MetodoEntrega;
import br.com.ecomerce.dominio.Pedido;
import br.com.ecomerce.dominio.Produto;
import br.com.ecomerce.dominio.ProdutosPedido;
import br.com.ecomerce.dominio.Resultado;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Array;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class PedidoVH implements IViewHelper {

    public EntidadeDominio getEntidade(HttpServletRequest request) {
        // Recupera a session para verificar se existe um cliente logado
        HttpSession session = request.getSession();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

        // Construindo Pedido para salvar
        if (request.getParameter("OPERACAO").trim().equals("SALVAR")) {
            Pedido pedidoSession = (Pedido) session.getAttribute("pedido");
            Cliente clienteSession = (Cliente) session.getAttribute("cliente");
            Carrinho carrinho = (Carrinho) session.getAttribute("carrinho");
            ProdutosPedido produtoPedido = new ProdutosPedido();
            ArrayList<Produto> produtos = new ArrayList<Produto>();
            // Criando Data do pedido
            Date dataPedido = new Date();
            Date dataPagamento = new Date();

            // Criando o status do pedido
            String status = "Processando";

            pedidoSession.setStatus(status);
            pedidoSession.setDataPagamento(dataPagamento);
            pedidoSession.setDataPedido(dataPedido);
            if(pedidoSession.getValorTotal() - clienteSession.getCarteira() < 0){
                clienteSession.setCarteira(clienteSession.getCarteira() - pedidoSession.getValorTotal());
                pedidoSession.setValorTotal(0);
            }else{
                clienteSession.setCarteira(0);
                pedidoSession.setValorTotal(pedidoSession.getValorTotal() - clienteSession.getCarteira());
            }
            pedidoSession.setCliente(clienteSession);

            for (Produto produto : carrinho.getProdutos()) {
                produtos.add(produto);
            }
            produtoPedido.setProdutos(produtos);
            pedidoSession.setProdutos(produtoPedido);

            return pedidoSession;
        }

        // Construindo Pedido para alterar
        if (request.getParameter("OPERACAO").trim().equals("ALTERAR")) {
            Pedido pedido = new Pedido();
            pedido.setId(Integer.valueOf(request.getParameter("idPedido")));
            pedido.setStatus(request.getParameter("statusPedido"));
            if (pedido.getStatus().equals("Processando")) {
                pedido.setStatus("Autorizado");
            } else if (pedido.getStatus().equals("Autorizado")) {
                pedido.setStatus("Enviando");
            } else if (pedido.getStatus().equals("Enviando")) {
                pedido.setStatus("Entregue");
            }
            return pedido;
        }

        // Contruindo Pedido para visualizar
        if (request.getParameter("OPERACAO").trim().equals("VISUALIZAR")) {
            Pedido pedido = new Pedido();
            if (request.getParameter("PESQUISA").equals("MEUSPEDIDOS")) {
                int idCliente = Integer.valueOf(request.getParameter("idCliente"));
                Cliente cliente = new Cliente();
                cliente.setId(idCliente);
                cliente.setNomeCompleto("meuspedidos");
                pedido.setCliente(cliente);
                return pedido;
            }
            if (request.getParameter("PESQUISA").equals("EXIBIRPEDIDO")) {
                Cliente cliente = new Cliente();
                int idPedido = Integer.valueOf(request.getParameter("idPedido"));
                cliente.setNomeCompleto("");
                pedido.setId(idPedido);
                pedido.setCliente(cliente);
                return pedido;
            }
            if (request.getParameter("PESQUISA").equals("PEDIDOSADM")) {
                Cliente cliente = new Cliente();
                cliente.setNomeCompleto("");
                pedido.setCliente(cliente);
                pedido.setId(0);
                return pedido;
            }
        }

        // Contruindo Pedido para Excluir
        if (request.getParameter("OPERACAO").trim().equals("EXCLUIR")) {

        }

        return null;
    }

    @Override
    public void setView(Resultado resultado, HttpServletRequest request, HttpServletResponse response) throws IOException {
        Pedido pedido;
        if (null == resultado.getEntidades().get(0)) {
            pedido = null;
        } else {
            pedido = (Pedido) resultado.getEntidades().get(0);
        }
        RequestDispatcher d = null;
        PrintWriter out = response.getWriter();
        String operacao = request.getParameter("OPERACAO");

        if (operacao.equals("SALVAR")) {
            request.setAttribute("resultado", resultado);
            request.setAttribute("operacao", "SALVAR");
            request.setAttribute("entidade", "PEDIDO");

            if (resultado.getMsg() == null) {
                resultado.setMsg("<h1> Pedido número: " + pedido.getId() + " efetuado com sucesso!!!</h1>");
                d = request.getRequestDispatcher("resultado.jsp");
                HttpSession session = request.getSession();
                Carrinho carrinho = (Carrinho) session.getAttribute("carrinho");
                carrinho.setProdutos(new ArrayList<Produto>());
                session.setAttribute("carrinho", carrinho);
                session.removeAttribute("pedido");
            } else {
                resultado.setMsg("<h1>" + resultado.getMsg() + "</h1>");
                d = request.getRequestDispatcher("resultado.jsp");
            }

            try {
                d.forward(request, response);
            } catch (ServletException ex) {
                Logger.getLogger(EnderecoVH.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        if (operacao.equals("ALTERAR")) {
            request.setAttribute("resultado", resultado);
            request.setAttribute("operacao", "ALTERAR");
            request.setAttribute("entidade", "PEDIDO");

            if (resultado.getMsg() == null) {
                resultado.setMsg("<h1> Status do Pedido: " + pedido.getId() + " alterado para " + pedido.getStatus() + " com sucesso!!!</h1>");
                d = request.getRequestDispatcher("resultado.jsp");
            } else {
                resultado.setMsg("<h1>" + resultado.getMsg() + "</h1>");
                d = request.getRequestDispatcher("resultado.jsp");
            }

            try {
                d.forward(request, response);
            } catch (ServletException ex) {
                Logger.getLogger(EnderecoVH.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        if (resultado.getMsg() == null && operacao.equals("VISUALIZAR")) {
            request.setAttribute("resultado", resultado);
//            
            String pagina = request.getParameter("PAGINA");
//            
            if (pagina == null) {
                d = request.getRequestDispatcher("logado/minha-conta.jsp");
            } else if (pagina.equals("PEDIDOSCLIENTE")) {
                request.setAttribute("resultado", resultado);
                d = request.getRequestDispatcher("logado/meus-pedidos.jsp");
            } else if (pagina.equals("EXIBIRPEDIDO")) {
                request.setAttribute("resultado", resultado);
                d = request.getRequestDispatcher("logado/exibir-pedido.jsp");                
            }             
            else if (pagina.equals("DEVOLUCAO")) {
                request.setAttribute("resultado", resultado);
                d = request.getRequestDispatcher("logado/visualizar-pedido-devolucao.jsp");
            } else if (pagina.equals("PEDIDOS")) {
                request.setAttribute("resultado", resultado);
                d = request.getRequestDispatcher("logado/logadoAdmin/pedidos-realizados.jsp");
            }
            try {
                d.forward(request, response);
            } catch (ServletException ex) {
                Logger.getLogger(EnderecoVH.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        if (operacao.equals("EXCLUIR")) {

        }
    }
}
