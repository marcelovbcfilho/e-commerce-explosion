package br.com.ecomerce.web.viewHelper;

import br.com.ecomerce.dominio.Categoria;
import br.com.ecomerce.dominio.EntidadeDominio;
import br.com.ecomerce.dominio.Produto;
import br.com.ecomerce.dominio.Resultado;
import br.com.ecomerce.dominio.SubCategoria;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.convert.ConverterException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class ProdutoVH implements IViewHelper {

    @Override
    public EntidadeDominio getEntidade(HttpServletRequest request) {

        if (request.getParameter("OPERACAO").trim().equals("SALVAR")) {
            String nome = request.getParameter("txtNome");
            String descricao = request.getParameter("txtDescricao");
            String endereco_imagem = request.getParameter("imagemPrincipal");
            String endereco_imagem_lado = request.getParameter("imagemLado");
            String endereco_imagem_costas = request.getParameter("imagemCostas");
            SubCategoria subCategoria = new SubCategoria();
            Categoria categoria = new Categoria();

            int estoque = Integer.valueOf(request.getParameter("txtEstoque"));
            double peso;
            double preco;

            // Criando peso
            try {
                peso = Double.valueOf(request.getParameter("txtPeso"));
            } catch (NumberFormatException e) {
                peso = 0.0;
            }

            // Criando preco
            try {
                preco = Double.valueOf(request.getParameter("txtPreco"));
            } catch (NumberFormatException e) {
                preco = 0.0;
            }

            // Validando dados de Produto
            if (nome.trim().equals("") || nome == null) {
                nome = "";
            }

            if (descricao.trim().equals("") || descricao == null) {
                descricao = "";
            }

            if (endereco_imagem.trim().equals("") || endereco_imagem == null) {
                endereco_imagem = "";
            }

            if (endereco_imagem_lado.trim().equals("") || endereco_imagem_lado == null) {
                endereco_imagem_lado = "";
            }

            if (endereco_imagem_costas.trim().equals("") || endereco_imagem_costas == null) {
                endereco_imagem_costas = "";
            }

            // Criando sub-categoria
            try {
                subCategoria.setId(Integer.valueOf(request.getParameter("selectSubCategoria")));
            } catch (NumberFormatException e) {
                subCategoria.setId(0);
            }

            // Criando categoria
            try {
                categoria.setId(Integer.valueOf(request.getParameter("selectCategoria")));
            } catch (NumberFormatException e) {
                categoria.setId(0);
            }

            subCategoria.setCategoria(categoria);

            Produto produto = new Produto(nome, descricao, peso, preco, endereco_imagem, endereco_imagem_lado, endereco_imagem_costas, subCategoria);
            produto.setQuantidadeEstoque(estoque);

            return produto;
        }

        if (request.getParameter("OPERACAO").trim().equals("ALTERAR")) {
            String nome = request.getParameter("txtNome");
            String descricao = request.getParameter("txtDescricao");
            String endereco_imagem = request.getParameter("imagemPrincipal");
            String endereco_imagem_lado = request.getParameter("imagemLado");
            String endereco_imagem_costas = request.getParameter("imagemCostas");
            SubCategoria subCategoria = new SubCategoria();
            Categoria categoria = new Categoria();

            int id;
            double peso;
            double preco;

            // Criando id de produto
            try {
                id = Integer.valueOf(request.getParameter("idProduto"));
            } catch (NumberFormatException e) {
                id = 0;
            }

            // Criando peso
            try {
                peso = Double.valueOf(request.getParameter("txtPeso"));
            } catch (NumberFormatException e) {
                peso = 0.0;
            }

            // Criando preco
            try {
                preco = Double.valueOf(request.getParameter("txtPreco"));
            } catch (ConverterException e) {
                preco = 0.0;
            }

            // Validando dados de Produto
            if (nome.trim().equals("") || nome == null) {
                nome = "";
            }

            if (descricao.trim().equals("") || descricao == null) {
                descricao = "";
            }

            if (endereco_imagem.trim().equals("") || endereco_imagem == null) {
                endereco_imagem = "";
            }

            if (endereco_imagem_lado.trim().equals("") || endereco_imagem_lado == null) {
                endereco_imagem_lado = "";
            }

            if (endereco_imagem_costas.trim().equals("") || endereco_imagem_costas == null) {
                endereco_imagem_costas = "";
            }

            // Criando sub-categoria
            try {
                subCategoria.setId(Integer.valueOf(request.getParameter("selectSubCategoria")));
            } catch (NumberFormatException e) {
                subCategoria.setId(0);
            }

            // Criando categoria
            try {
                categoria.setId(Integer.valueOf(request.getParameter("selectCategoria")));
            } catch (NumberFormatException e) {
                categoria.setId(0);
            }

            subCategoria.setCategoria(categoria);

            int quantidadeEstoque = 10;

            Produto produto = new Produto(nome, descricao, peso, preco, endereco_imagem, endereco_imagem_lado, endereco_imagem_costas, subCategoria);

            return produto;
        }

        if (request.getParameter("OPERACAO").trim().equals("EXCLUIR")) {
            int id;

            // Criando id de produto
            try {
                id = Integer.valueOf(request.getParameter("idProduto"));
            } catch (NumberFormatException e) {
                id = 0;
            }
        }

        if (request.getParameter("OPERACAO").trim().equals("VISUALIZAR")) {
            // Criando o produto
            Produto produto = new Produto();
            int id;

            if (null != request.getParameter("idProduto")) {
                id = Integer.valueOf(request.getParameter("idProduto"));
                produto.setId(id);
                return produto;
            }
            if (null != request.getParameter("idSubCategoria")) {
                id = Integer.valueOf(request.getParameter("idSubCategoria"));
                SubCategoria sub = new SubCategoria();
                sub.setId(id);
                produto.setSubCategoria(sub);
                return produto;
            }

            if (null != request.getParameter("idCategoria")) {
                id = Integer.valueOf(request.getParameter("idCategoria"));
                SubCategoria sub = new SubCategoria();
                Categoria cat = new Categoria();
                cat.setId(id);
                sub.setCategoria(cat);
                produto.setSubCategoria(sub);
                return produto;
            }
            if (null != request.getParameter("pesquisar") || request.getParameter("PAGINA").equals("PESQUISA")) {
                String pesquisa = request.getParameter("pesquisar");

                produto.setNome(pesquisa);
                produto.setDescricao("pesquisa");

                return produto;
            }

            if (request.getParameter("PAGINA").equals("PRODUTOS")) {
                produto.setId(0);
                produto.setSubCategoria(new SubCategoria());
                produto.getSubCategoria().setCategoria(new Categoria());
                return produto;
            }

            return produto;
        }

        if (request.getParameter("OPERACAO").trim().equals("CONSULTAR")) {
            // Criando o produto
            Produto produto = new Produto();

            // recuperando id da subcategoria
            int idSubCategoria = Integer.valueOf(request.getParameter("idSubCategoria"));
            SubCategoria sub = new SubCategoria();
            sub.setId(idSubCategoria);
            produto.setSubCategoria(sub);
            return produto;
        }

        return null;
    }

    @Override
    public void setView(Resultado resultado, HttpServletRequest request, HttpServletResponse response) throws IOException {
        Produto produto;
        if (null == resultado.getEntidades().get(0)) {
            produto = null;
        } else {
            produto = (Produto) resultado.getEntidades().get(0);
        }

        RequestDispatcher d = null;
        PrintWriter out = response.getWriter();
        String operacao = request.getParameter("OPERACAO");

        if (operacao.equals("SALVAR")) {
            request.setAttribute("resultado", resultado);
            request.setAttribute("operacao", "SALVAR");
            request.setAttribute("entidade", "PRODUTO");

            if (resultado.getMsg() == null) {
                resultado.setMsg("<h1> Produto: " + produto.getNome() + " salvo com sucesso!!!</h1>");
            } else {
                resultado.setMsg("<h1>" + resultado.getMsg() + "</h1>");
            }

            d = request.getRequestDispatcher("resultado.jsp");
            try {
                d.forward(request, response);

            } catch (ServletException ex) {
                Logger.getLogger(ProdutoVH.class
                        .getName()).log(Level.SEVERE, null, ex);
            }
        }

        if (operacao.equals("ALTERAR")) {
            request.setAttribute("resultado", resultado);
            request.setAttribute("operacao", "ALTERAR");
            request.setAttribute("entidade", "PRODUTO");

            if (resultado.getMsg() == null) {
                resultado.setMsg("<h1> Cliente: " + produto.getNome() + " alterado com sucesso!!!</h1>");
            } else {
                resultado.setMsg("<h1>" + resultado.getMsg() + "</h1>");
            }

            d = request.getRequestDispatcher("resultado.jsp");
            try {
                d.forward(request, response);

            } catch (ServletException ex) {
                Logger.getLogger(ProdutoVH.class
                        .getName()).log(Level.SEVERE, null, ex);
            }
        }

        if (operacao.equals("EXCLUIR")) {
            request.setAttribute("resultado", resultado);
            request.setAttribute("operacao", "EXCLUIR");
            request.setAttribute("entidade", "PRODUTO");

            if (resultado.getMsg() == null) {
                resultado.setMsg("<h1> Cliente: " + produto.getNome() + " excluido com sucesso!!!</h1>");
            } else {
                resultado.setMsg("<h1>" + resultado.getMsg() + "</h1>");
            }

            d = request.getRequestDispatcher("resultado.jsp");
            try {
                d.forward(request, response);

            } catch (ServletException ex) {
                Logger.getLogger(ProdutoVH.class
                        .getName()).log(Level.SEVERE, null, ex);
            }
        }

        if (operacao.equals("VISUALIZAR")) {

            if (request.getParameter("PAGINA").equals("DETALHES")) {
                request.setAttribute("resultado", resultado);
                d = request.getRequestDispatcher("produtos/pagina-produto.jsp");
                try {
                    d.forward(request, response);

                } catch (ServletException ex) {
                    Logger.getLogger(ProdutoVH.class
                            .getName()).log(Level.SEVERE, null, ex);
                }
            }

            if (request.getParameter("PAGINA").equals("CARRINHO")) {
                request.setAttribute("resultado", resultado);
                d = request.getRequestDispatcher("produtos/resultado-carrinho.jsp");
                try {
                    d.forward(request, response);

                } catch (ServletException ex) {
                    Logger.getLogger(ProdutoVH.class
                            .getName()).log(Level.SEVERE, null, ex);
                }
            }

            if (request.getParameter("PAGINA").equals("CATALOGO")) {
                request.setAttribute("resultado", resultado);
                d = request.getRequestDispatcher("produtos/catalogo-produtos.jsp");
                try {
                    d.forward(request, response);

                } catch (ServletException ex) {
                    Logger.getLogger(ProdutoVH.class
                            .getName()).log(Level.SEVERE, null, ex);
                }
            }

            if (request.getParameter("PAGINA").equals("PESQUISA")) {
                request.setAttribute("resultado", resultado);
                d = request.getRequestDispatcher("produtos/pesquisa-produto.jsp");
                try {
                    d.forward(request, response);

                } catch (ServletException ex) {
                    Logger.getLogger(ProdutoVH.class
                            .getName()).log(Level.SEVERE, null, ex);
                }
            }

            if (request.getParameter("PAGINA").equals("PRODUTOS")) {
                request.setAttribute("resultado", resultado);
                d = request.getRequestDispatcher("logado/logadoAdmin/produtos.jsp");
                try {
                    d.forward(request, response);

                } catch (ServletException ex) {
                    Logger.getLogger(ProdutoVH.class
                            .getName()).log(Level.SEVERE, null, ex);
                }
            }

        }
    }
}
