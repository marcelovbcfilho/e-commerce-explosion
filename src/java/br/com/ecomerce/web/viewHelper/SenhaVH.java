package br.com.ecomerce.web.viewHelper;

import br.com.ecomerce.dominio.Cliente;
import br.com.ecomerce.dominio.EntidadeDominio;
import br.com.ecomerce.dominio.Resultado;
import br.com.ecomerce.dominio.Senha;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class SenhaVH implements IViewHelper {

    @Override
    public EntidadeDominio getEntidade(HttpServletRequest request) {
        if (request.getParameter("OPERACAO").trim().equals("ALTERAR")) {
            // Instancia as variáveis a serem usadas
            int idCliente = Integer.valueOf(request.getParameter("idCliente"));
            String senhaTemp = request.getParameter("txtSenhaAtual");
            String senha = request.getParameter("txtNovaSenha");
            String confirmaSenha = request.getParameter("txtConfirmaNovaSenha");

            Senha senhaCliente = new Senha(senhaTemp, senha, confirmaSenha);
            senhaCliente.setId(idCliente);

            return senhaCliente;
        }
        return null;
    }

    @Override
    public void setView(Resultado resultado, HttpServletRequest request, HttpServletResponse response) throws IOException {
        if (request.getParameter("OPERACAO").equals("ALTERAR")) {
            if (resultado.getMsg() == null) {
                resultado.setMsg("Troca de senha bem sucedida!");
            }
            request.setAttribute("resultado", resultado);
            RequestDispatcher d = request.getRequestDispatcher("logado/alterar-senha.jsp");
            try {
                d.forward(request, response);
            } catch (ServletException ex) {
                Logger.getLogger(ClienteVH.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

}
