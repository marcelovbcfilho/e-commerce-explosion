package br.com.ecomerce.web.viewHelper;

import br.com.ecomerce.dominio.Categoria;
import br.com.ecomerce.dominio.EntidadeDominio;
import br.com.ecomerce.dominio.Resultado;
import br.com.ecomerce.dominio.SubCategoria;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class SubCategoriaVH implements IViewHelper {

    @Override
    public EntidadeDominio getEntidade(HttpServletRequest request) {
        if (request.getParameter("OPERACAO") == null) {
            SubCategoria subcategoria = new SubCategoria();
            subcategoria.setId(0);
            return subcategoria;
        } else {
            if (request.getParameter("OPERACAO").trim().equals("SALVAR")) {
                String nome = request.getParameter("txtNome");

                if (nome.trim().equals("") || nome == null) {
                    nome = "";
                }

                SubCategoria subCategoria = new SubCategoria(nome);
                Categoria categoria = new Categoria();
                categoria.setId(Integer.valueOf(request.getParameter("selectIdCategoria")));
                subCategoria.setCategoria(categoria);
                subCategoria.setId(0);
                return subCategoria;
            }

            if (request.getParameter("OPERACAO").trim().equals("ALTERAR")) {
                String nome = request.getParameter("txtNome");
                
                if (nome.trim().equals("") || nome == null) {
                    nome = "";
                }
                
                // Criando Sub
                SubCategoria sub = new SubCategoria(nome);
                sub.setId(Integer.valueOf(request.getParameter("idSubCategoria")));
                sub.setCategoria(new Categoria());
                sub.getCategoria().setId(Integer.valueOf(request.getParameter("selectIdCategoria")));

                return sub;
            }

            if (request.getParameter("OPERACAO").trim().equals("EXCLUIR")) {
                SubCategoria subCategoria = new SubCategoria();
                subCategoria.setId(Integer.valueOf(request.getParameter("idSubCategoria")));
                subCategoria.setNome("");
                return subCategoria;
            }

            if (request.getParameter("OPERACAO").trim().equals("VISUALIZAR")) {
                // Criando o produto
                SubCategoria produto = new SubCategoria();
                produto.setId(Integer.valueOf(request.getParameter("idSubCategoria")));

                return produto;
            }

            if (request.getParameter("OPERACAO").trim().equals("CONSULTAR")) {
                SubCategoria sub = new SubCategoria();

                sub.setId(0);

                return sub;
            }

        }
        return null;
    }

    @Override
    public void setView(Resultado resultado, HttpServletRequest request, HttpServletResponse response) throws IOException {
        SubCategoria subCategoria = (SubCategoria) resultado.getEntidades().get(0);
        RequestDispatcher d = null;
        PrintWriter out = response.getWriter();
        String operacao = request.getParameter("OPERACAO");

        if (operacao == null) {
            HttpSession session = request.getSession();
            session.setAttribute("subcategorias", resultado);
            d = request.getRequestDispatcher("index.jsp");
            try {
                d.forward(request, response);
            } catch (ServletException ex) {
                Logger.getLogger(SubCategoriaVH.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            if (operacao.equals("SALVAR")) {
                request.setAttribute("resultado", resultado);
                request.setAttribute("operacao", "SALVAR");
                request.setAttribute("entidade", "SUBCATEGORIA");

                if (resultado.getMsg() == null) {

                    resultado.setMsg("<h1> SubCategoria: " + subCategoria.getNome() + " salvo com sucesso!!!</h1>");
                } else {
                    resultado.setMsg("<h1>" + resultado.getMsg() + "</h1>");
                }

                d = request.getRequestDispatcher("resultado.jsp");
                try {
                    d.forward(request, response);
                } catch (ServletException ex) {
                    Logger.getLogger(SubCategoriaVH.class.getName()).log(Level.SEVERE, null, ex);
                }
            }

            if (operacao.equals("CONSULTAR")) {
                request.setAttribute("resultado", resultado);

                d = request.getRequestDispatcher("logado/logadoAdmin/sub-categorias.jsp");
                try {
                    d.forward(request, response);
                } catch (ServletException ex) {
                    Logger.getLogger(SubCategoriaVH.class.getName()).log(Level.SEVERE, null, ex);
                }
            }

            if (operacao.equals("VISUALIZAR")) {
                request.setAttribute("resultado", resultado);
                d = request.getRequestDispatcher("/logado/logadoAdmin/alterar-sub-categoria.jsp");
                try {
                    d.forward(request, response);
                } catch (ServletException ex) {
                    Logger.getLogger(SubCategoriaVH.class.getName()).log(Level.SEVERE, null, ex);
                }
            }

            if (resultado.getMsg() == null && operacao.equals("EXCLUIR")) {
                request.setAttribute("resultado", resultado);
                request.setAttribute("operacao", "EXCLUIR");
                request.setAttribute("entidade", "SUBCATEGORIA");

                if (resultado.getMsg() == null) {
                    resultado.setMsg("<h1> SubCategoria: " + subCategoria.getNome() + " excluida com sucesso!</h1>");
                } else {
                    resultado.setMsg("<h1>" + resultado.getMsg() + "</h1>");
                }

                d = request.getRequestDispatcher("resultado.jsp");
                try {
                    d.forward(request, response);
                } catch (ServletException ex) {
                    Logger.getLogger(SubCategoriaVH.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            
            if (operacao.equals("ALTERAR")) {
                request.setAttribute("resultado", resultado);
                request.setAttribute("entidade", "SUBCATEGORIA");

                if (resultado.getMsg() == null) {
                    resultado.setMsg("<h1> SubCategoria: " + subCategoria.getNome() + " alterado com sucesso!!!</h1>");
                } else {
                    resultado.setMsg("<h1>" + resultado.getMsg() + "</h1>");
                }

                d = request.getRequestDispatcher("resultado.jsp");
                try {
                    d.forward(request, response);
                } catch (ServletException ex) {
                    Logger.getLogger(SubCategoriaVH.class.getName()).log(Level.SEVERE, null, ex);
                }
            }

        }
    }

}
