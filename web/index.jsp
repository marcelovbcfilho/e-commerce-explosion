<%@page import="java.text.DecimalFormat"%>
<%@page import="br.com.ecomerce.dominio.SubCategoria"%>
<%@page import="br.com.ecomerce.dominio.EntidadeDominio"%>
<%@page import="br.com.ecomerce.dominio.Categoria"%>
<%@page import="br.com.ecomerce.dominio.Carrinho"%>
<%@page import="br.com.ecomerce.dominio.Produto"%>
<%@page import="java.util.ArrayList"%>
<%@page import="br.com.ecomerce.dominio.Cliente"%>
<%@page import="br.com.ecomerce.dominio.Resultado"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Explosion | E-Commerce de Eletrônicos</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">

        <link rel="shortcut icon" href="images/logo/explosionV1.png">

        <!-- CSS -->
        <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="css/flexslider.css" rel="stylesheet" type="text/css" />
        <link href="css/fancySelect.css" rel="stylesheet" media="screen, projection" />
        <link href="css/animate.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/style.css" rel="stylesheet" type="text/css" />

        <!-- FONTS -->
        <link href='http://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
        <link href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">

    </head>
    <body>

        <!-- PRELOADER -->
        <div id="preloader"><img src="images/preloader.gif" alt="" /></div>
        <!-- //PRELOADER -->
        <div class="preloader_hide">

            <!-- PAGE -->
            <div id="page">

                <!-- HEADER -->
                <header>

                    <!-- TOP INFO -->
                    <div class="top_info">

                        <!-- CONTAINER -->
                        <div class="container clearfix">
                            <ul class="secondary_menu">
                                <%
                                    DecimalFormat df = new DecimalFormat("0.##");
                                    if (session.getAttribute("cliente") != null) {
                                        Cliente cliente = (Cliente) session.getAttribute("cliente");
                                        if (cliente.getAdmin() == 1) {
                                            out.println("<li><a href='minha-conta-admin.jsp' >Minha Conta</a></li>");
                                            out.println("<li><a>Bem Vindo " + cliente.getNomeCompleto() + "</a></li>");
                                            out.println("<li><a href='Logout?logout=logout'>Logout</a></li>");
                                        } else {
                                            out.println("<li><a href='minha-conta.jsp' >Minha Conta</a></li>");
                                            out.println("<li><a>Bem Vindo " + cliente.getNomeCompleto() + "</a></li>");
                                            out.println("<li><a>Carteira: R$" + df.format(cliente.getCarteira()) + "</a></li>");
                                            out.println("<li><a href='Logout?logout=logout'>Logout</a></li>");
                                        }

                                    } else {
                                        out.println("<li><a href='login.jsp' >Login</a></li>");
                                        out.println("<li><a href='cadastro-cliente.jsp' >Registrar</a></li>");
                                    }
                                %>
                            </ul>
                        </div><!-- //CONTAINER -->
                    </div><!-- TOP INFO -->


                    <!-- MENU BLOCK -->
                    <div class="menu_block">

                        <!-- CONTAINER -->
                        <div class="container clearfix">

                            <!-- LOGO -->
                            <div class="logo">
                                <a href="index.jsp" ><img src="images/logo/explosionV2.png" alt="" /></a>
                            </div><!-- //LOGO -->


                            <!-- SEARCH FORM -->
                            <div class="top_search_form">
                                <a class="top_search_btn" href="javascript:void(0);" ><i class="fa fa-search"></i></a>
                                <form action="${pageContext.request.contextPath}/VisualizarProduto" method='POST'>
                                    <input type="text" name="pesquisar" value="Pesquisar" onFocus="if (this.value == 'Pesquisar')
                                                this.value = '';" onBlur="if (this.value == '')
                                                            this.value = 'Pesquisar';" />
                                    <input type='hidden' id='OPERACAO' name='OPERACAO' value='VISUALIZAR'/>
                                    <input type='hidden' id='PAGINA' name='PAGINA' value='PESQUISA'/>
                                </form>
                            </div><!-- SEARCH FORM -->

                            <%
                                double total = 0;
                                if (null == session.getAttribute("carrinho")) {
                                    ArrayList<Produto> produtos = new ArrayList<Produto>();
                                    Carrinho carrinho = new Carrinho();
                                    carrinho.setProdutos(produtos);
                                    session.setAttribute("carrinho", carrinho);
                            %>
                            <div class="shopping_bag">
                                <a class="shopping_bag_btn" href = "javascript:void(0);" > 
                                    <i class="fa fa-shopping-cart"></i > <p>Carrinho</p > <span> <% out.print(carrinho.getProdutos().size());%></span></a>
                                <div class="cart">
                                    <div class="cart_total">
                                        <div class="clearfix">
                                            <span class="cart_subtotal">Total: <b><% out.print("R$" + total);%></b></span>
                                        </div>
                                        <a class="btn active" href="logado/carrinho-compra.jsp">Ir ao Carrinho</a>
                                    </div>
                                </div>
                            </div>
                            <%
                            } else {

                                Carrinho carrinho = (Carrinho) session.getAttribute("carrinho");

                            %>
                            <!-- SHOPPING BAG -->
                            <div class="shopping_bag">
                                <a class="shopping_bag_btn" href="javascript:void(0);" ><i class="fa fa-shopping-cart"></i><p>Carrinho</p><span><%out.print(carrinho.getProdutos().size());%></span></a>
                                <div class="cart">
                                    <ul class="cart-items">
                                        <%
                                            for (Produto pro : carrinho.getProdutos()) {
                                                out.print("<li class='clearfix'>");
                                                out.print("<img class='cart_item_product' src='" + pro.getEndereco_imagem() + "'/>");
                                                out.print(pro.getQuantidadeProdutoCarrinho() + " x " + pro.getNome());
                                                out.print("<span class='cart_item_price'> R$" + pro.getPreco() + "</span>");
                                                out.print("</li>");
                                                total += pro.getPreco() * pro.getQuantidadeProdutoCarrinho();
                                            }
                                        %>
                                    </ul>
                                    <div class="cart_total">
                                        <div class="clearfix"><span class="cart_subtotal">Total: <b><% out.print("R$" + total);%></b></span></div>
                                        <a class="btn active" href="logado/carrinho-compra.jsp">Ir ao Carrinho</a>
                                    </div>
                                </div>
                            </div>
                            <%
                                }
                            %>

                            <!-- MENU -->
                            <ul class="navmenu center">
                                <li>
                                    <a href="index.jsp" >Home</a>
                                </li>
                                <%
                                    Resultado resultadoCategorias = (Resultado) session.getAttribute("categorias");
                                    Categoria catg;
                                    SubCategoria sub;
                                    for (EntidadeDominio entity : resultadoCategorias.getEntidades()) {
                                        catg = (Categoria) entity;
                                %>
                                <li class='sub-menu'>
                                    <form name='categoria<%out.print(catg.getId());%>' action='${pageContext.request.contextPath}/VisualizarEstoque' method='POST' hidden>
                                        <input type='hidden' id='idCategoria' name='idCategoria' value='<% out.print(catg.getId());%>'/>
                                        <input type='hidden' id='OPERACAO' name='OPERACAO' value='VISUALIZAR'/>
                                        <input type='hidden' id='PAGINA' name='PAGINA' value='CATALOGO'/>                                        
                                    </form>
                                    <a href='javascript:categoria<%out.print(catg.getId());%>.submit();'>
                                        <%out.print(catg.getNome()); %>
                                    </a>
                                    <%
                                        if (catg.getSubCategorias().size() == 0) {
                                            out.println("</li>");
                                        } else {
                                    %>
                                    <ul class='mega_menu megamenu_col1 clearfix'>
                                        <li class='col'>
                                            <ol>
                                                <%
                                                    for (EntidadeDominio entitySub : catg.getSubCategorias()) {
                                                        sub = (SubCategoria) entitySub;
                                                %>
                                                <li>
                                                    <form name='sub<% out.print(sub.getId());%>' action='${pageContext.request.contextPath}/VisualizarEstoque' method='POST'>                                                        
                                                        <input type='hidden' id='OPERACAO' name='OPERACAO' value='VISUALIZAR'/>
                                                        <input type='hidden' id='PAGINA' name='PAGINA' value='CATALOGO'/>
                                                        <input type='hidden' id='idSubCategoria' name='idSubCategoria' value='<%out.print(sub.getId());%>'/>
                                                    </form>
                                                    <a href='javascript:sub<% out.print(sub.getId());%>.submit();'><%out.print(sub.getNome());%></a>
                                                </li>
                                                <%}%>
                                            </ol>
                                        </li>
                                    </ul>
                                    <%}%>
                                </li>
                                <%
                                    }
                                %>                                                                
                            </ul><!-- //MENU -->
                        </div><!-- //CONTAINER -->
                    </div><!-- //MENU BLOCK -->
                </header><!-- //HEADER -->


                <!-- HOME -->
                <section id="home" class="padbot0">

                    <!-- TOP SLIDER -->
                    <div class="flexslider top_slider">
                        <ul class="slides">
                            <li class="slide1">

                                <!-- CONTAINER -->
                                <div class="container">
                                    <div class="flex_caption1">
                                        <p class="title1 captionDelay2 FromTop">Explosão de Preços</p>
                                        <p class="title2 captionDelay3 FromTop">Promoções</p>
                                    </div>
                                    <a class="flex_caption2" href="javascript:void(0);" ><div class="middle"><span>Compre</span>Agora</div></a>
                                    <div class="flex_caption3 slide_banner_wrapper">

                                        <form name='slide' action='${pageContext.request.contextPath}/VisualizarProduto' method='POST'>
                                            <a class="slide_banner slide1_banner1 captionDelay4 FromBottom" href="" ><img src="images/banners/banner6.1.png" alt="" /></a>
                                            <a class="slide_banner slide1_banner2 captionDelay5 FromBottom" href="javascript:slide.submit()" ><img src="images/banners/banner5.1.png" alt="" /></a>
                                            <a class="slide_banner slide1_banner3 captionDelay6 FromBottom" name='idProduto' id='idProduto' value='1' href="javascript:slide.submit()" ><img src="images/banners/banner4.1.png" alt="" /></a>                                            
                                            <input type='hidden' id='OPERACAO' name='OPERACAO' value='VISUALIZAR'/>
                                            <input type='hidden' id='PAGINA' name='PAGINA' value='DETALHES'/>

                                        </form>

                                    </div>
                                </div><!-- //CONTAINER -->
                            </li>

                            <li class="slide2">

                                <!-- CONTAINER -->
                                <div class="container">
                                    <div class="flex_caption1">
                                        <p class="title1 captionDelay2 FromTop">Explosão de Preços</p>
                                        <p class="title2 captionDelay3 FromTop">Promoções</p>
                                    </div>
                                    <a class="flex_caption2" href="javascript:void(0);" ><div class="middle"><span>Compre</span>Agora</div></a>
                                    <div class="flex_caption3 slide_banner_wrapper">
                                        <a class="slide_banner slide1_banner1 captionDelay4 FromBottom" href="javascript:void(0);" ><img src="images/banners/banner8.1.png" alt="" /></a>
                                        <a class="slide_banner slide1_banner3 captionDelay5 FromBottom" href="javascript:void(0);" ><img src="images/banners/banner7.1.png" alt="" /></a>
                                        <a class="slide_banner slide1_banner2 captionDelay6 FromBottom" href="javascript:void(0);" ><img src="images/banners/banner9.1.png" alt="" /></a>
                                    </div>
                                </div><!-- //CONTAINER -->
                            </li>

                            <li class="slide3">

                                <!-- CONTAINER -->
                                <div class="container">
                                    <div class="flex_caption1">
                                        <p class="title1 captionDelay2 FromTop">Explosão de Preços</p>
                                        <p class="title2 captionDelay3 FromTop">Promoções</p>
                                    </div>
                                    <a class="flex_caption2" href="javascript:void(0);" ><div class="middle"><span>Compre</span>Agora</div></a>
                                    <div class="flex_caption3 slide_banner_wrapper">
                                        <a class="slide_banner slide1_banner3 captionDelay4 FromBottom" href="javascript:void(0);" ><img src="images/banners/banner10_1.1.png" alt="" /></a>
                                        <a class="slide_banner slide1_banner1 captionDelay5 FromBottom" href="javascript:void(0);" ><img src="images/banners/banner11.1.png" alt="" /></a>
                                        <a class="slide_banner slide1_banner2 captionDelay6 FromBottom" href="javascript:void(0);" ><img src="images/banners/banner12.1.png" alt="" /></a>
                                    </div>
                                </div><!-- //CONTAINER -->
                            </li>
                        </ul>
                    </div><!-- //TOP SLIDER -->
                </section><!-- //HOME -->


                <!-- TOVAR SECTION -->
                <section class="tovar_section">

                    <!-- CONTAINER -->
                    <div class="container">
                        <h2>Produtos Novos</h2>

                        <!-- ROW -->
                        <div class="row">

                            <!-- TOVAR WRAPPER -->
                            <div class="tovar_wrapper" data-appear-top-offset='-100' data-animated='fadeInUp'>

                                <!-- TOVAR1 -->
                                <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6 col-ss-12 padbot40">
                                    <form name="produto" action="${pageContext.request.contextPath}/VisualizarProduto" method="POST">
                                        <div class="tovar_item">
                                            <div class="tovar_img">
                                                <div class="tovar_img_wrapper">
                                                    <img class="img" src="images/produtos/informatica/image1.png" alt="" />
                                                    <img class="img_h" src="images/produtos/informatica/image1.png" alt="" />
                                                </div>

                                                <div class="tovar_item_btns">
                                                    <a class="open-project tovar_view" href="javascript:produto.submit()" >Visualizar</a>
                                                    <a class="add_bag" href="javascript:produto.submit()" ><i class="fa fa-shopping-cart"></i></a>
                                                </div>
                                            </div>

                                            <div class="tovar_description clearfix">
                                                <a class="tovar_title" href="javascript:produto.submit()">
                                                    Core I5 8° Geração
                                                </a>
                                                <span class="tovar_price">R$ 200,00</span>
                                            </div>
                                            <input type='hidden' name='idProduto' id='idProduto' value='2'/>
                                            <input type='hidden' id='OPERACAO' name='OPERACAO' value='VISUALIZAR'/>
                                            <input type='hidden' id='PAGINA' name='PAGINA' value='DETALHES'/>

                                        </div>
                                    </form>
                                </div><!-- //TOVAR1 -->

                                <!-- TOVAR2 -->
                                <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6 col-ss-12 padbot40">
                                    <div class="tovar_item">
                                        <div class="tovar_img">
                                            <div class="tovar_img_wrapper">
                                                <img class="img" src="images/produtos/informatica/image11.png" alt="" />
                                                <img class="img_h" src="images/produtos/informatica/image11.png" alt="" />
                                            </div>
                                            <div class="tovar_item_btns">
                                                <div class="open-project-link"><a class="open-project tovar_view" href="javascript:void(0);" >Visualizar</a></div>
                                                <a class="add_bag" href="javascript:void(0);" ><i class="fa fa-shopping-cart"></i></a>
                                                <a class="add_lovelist" href="javascript:void(0);" ><i class="fa fa-heart"></i></a>
                                            </div>
                                        </div>
                                        <div class="tovar_description clearfix">
                                            <a class="tovar_title" href="product-page.html" >Mouse + Teclado Logitech</a>
                                            <span class="tovar_price">R$ 100,00</span>
                                        </div>
                                    </div>
                                </div><!-- //TOVAR2 -->

                                <!-- TOVAR3 -->
                                <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6 col-ss-12 padbot40">
                                    <div class="tovar_item">
                                        <div class="tovar_img">
                                            <div class="tovar_img_wrapper">
                                                <img class="img" src="images/produtos/informatica/image12.png" alt="" />
                                                <img class="img_h" src="images/produtos/informatica/image12.png" alt="" />
                                            </div>
                                            <div class="tovar_item_btns">
                                                <div class="open-project-link"><a class="open-project tovar_view" href="javascript:void(0);" data-url="${pageContext.request.contextPath}/exibir-pedido.jsp" >Visualizar</a></div>
                                                <a class="add_bag" href="javascript:void(0);" ><i class="fa fa-shopping-cart"></i></a>
                                                <a class="add_lovelist" href="javascript:void(0);" ><i class="fa fa-heart"></i></a>
                                            </div>
                                        </div>
                                        <div class="tovar_description clearfix">
                                            <a class="tovar_title" href="product-page.html" >Micro SD 16 GB + Adaptador</a>
                                            <span class="tovar_price">R$ 30,00</span>
                                        </div>
                                    </div>
                                </div><!-- //TOVAR3 -->

                                <div class="respond_clear_768"></div>

                                <!-- BANNER -->
                                <div class="col-lg-3 col-md-3 col-xs-6 col-ss-12">
                                    <a class="banner type1 margbot30" href="javascript:void(0);" ><img src="images/banners/banner2.png" alt="" /></a>
                                    <a class="banner type2 margbot40" href="javascript:void(0);" ><img src="images/banners/banner3.png" alt="" /></a>
                                </div><!-- //BANNER -->
                            </div><!-- //TOVAR WRAPPER -->
                        </div><!-- //ROW -->


                        <!-- ROW -->
                        <div class="row">

                            <!-- TOVAR WRAPPER -->
                            <div class="tovar_wrapper" data-appear-top-offset='-100' data-animated='fadeInUp'>

                                <!-- BANNER -->
                                <div class="col-lg-3 col-md-3 col-xs-6 col-ss-12">
                                    <a class="banner type3 margbot40" href="javascript:void(0);" ><img src="images/banners/banner1_1.png" alt="" /></a>
                                </div><!-- //BANNER -->

                                <div class="respond_clear_768"></div>

                                <!-- TOVAR4 -->
                                <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6 col-ss-12 padbot40">
                                    <div class="tovar_item">
                                        <div class="tovar_img">
                                            <div class="tovar_img_wrapper">
                                                <img class="img" src="images/produtos/informatica/image16.png" alt="" />
                                                <img class="img_h" src="images/produtos/informatica/image16.png" alt="" />
                                            </div>
                                            <div class="tovar_item_btns">
                                                <div class="open-project-link"><a class="open-project tovar_view" href="javascript:void(0);" data-url="!projects/women/4.html" >Visualizar</a></div>
                                                <a class="add_bag" href="javascript:void(0);" ><i class="fa fa-shopping-cart"></i></a>
                                                <a class="add_lovelist" href="javascript:void(0);" ><i class="fa fa-heart"></i></a>
                                            </div>
                                        </div>
                                        <div class="tovar_description clearfix">
                                            <a class="tovar_title" href="" >RTX 2070 Galax OC</a>
                                            <span class="tovar_price">R$4000.00</span>
                                        </div>
                                    </div>
                                </div><!-- //TOVAR4 -->

                                <!-- TOVAR5 -->
                                <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6 col-ss-12 padbot40">
                                    <div class="tovar_item tovar_sale">
                                        <div class="tovar_img">
                                            <div class="tovar_img_wrapper">
                                                <img class="img" src="images/produtos/informatica/image17.png" alt="" />
                                                <img class="img_h" src="images/produtos/informatica/image17.png" alt="" />
                                            </div>
                                            <div class="tovar_item_btns">
                                                <div class="open-project-link"><a class="open-project tovar_view" href="javascript:void(0);" data-url="!projects/women/5.html" >Visualizar</a></div>
                                                <a class="add_bag" href="javascript:void(0);" ><i class="fa fa-shopping-cart"></i></a>
                                                <a class="add_lovelist" href="javascript:void(0);" ><i class="fa fa-heart"></i></a>
                                            </div>
                                        </div>
                                        <div class="tovar_description clearfix">
                                            <a class="tovar_title" href="product-page.html" >RTX 2070 Galax</a>
                                            <span class="tovar_price">R$3800.00</span>
                                        </div>
                                    </div>
                                </div><!-- //TOVAR5 -->

                                <!-- TOVAR6 -->
                                <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6 col-ss-12 padbot40">
                                    <div class="tovar_item">
                                        <div class="tovar_img">
                                            <div class="tovar_img_wrapper">
                                                <img class="img" src="images/produtos/informatica/image18.png" alt="" />
                                                <img class="img_h" src="images/produtos/informatica/image18.png" alt="" />
                                            </div>
                                            <div class="tovar_item_btns">
                                                <div class="open-project-link"><a class="open-project tovar_view" href="javascript:void(0);" data-url="!projects/women/6.html" >Visualizar</a></div>
                                                <a class="add_bag" href="javascript:void(0);" ><i class="fa fa-shopping-cart"></i></a>
                                                <a class="add_lovelist" href="javascript:void(0);" ><i class="fa fa-heart"></i></a>
                                            </div>
                                        </div>
                                        <div class="tovar_description clearfix">
                                            <a class="tovar_title" href="product-page.html" >RTX 2070 Zotac</a>
                                            <span class="tovar_price">R$4500.00</span>
                                        </div>
                                    </div>
                                </div><!-- //TOVAR6 -->
                            </div><!-- //TOVAR WRAPPER -->
                        </div><!-- //ROW -->


                        <!-- ROW -->
                        <div class="row">

                            <!-- BANNER WRAPPER -->
                            <div class="banner_wrapper" data-appear-top-offset='-100' data-animated='fadeInUp'>
                                <!-- BANNER -->
                                <div class="col-lg-9 col-md-9">
                                    <a class="banner type4 margbot40" href="javascript:void(0);" ><img src="images/banners/faixaBanner.png" alt="" /></a>
                                </div><!-- //BANNER -->

                                <!-- BANNER -->
                                <div class="col-lg-3 col-md-3">
                                    <a class="banner nobord margbot40" href="javascript:void(0);" ><img src="images/banners/fbBanner.png" alt="" /></a>
                                </div><!-- //BANNER -->
                            </div><!-- //BANNER WRAPPER -->
                        </div><!-- //ROW -->
                    </div><!-- //CONTAINER -->
                </section><!-- //TOVAR SECTION -->


                <!-- NEW ARRIVALS -->
                <section class="new_arrivals padbot50">

                    <!-- CONTAINER -->
                    <div class="container">
                        <h2>Novidades</h2>

                        <!-- JCAROUSEL -->
                        <div class="jcarousel-wrapper">

                            <!-- NAVIGATION -->
                            <div class="jCarousel_pagination">
                                <a href="javascript:void(0);" class="jcarousel-control-prev" ><i class="fa fa-angle-left"></i></a>
                                <a href="javascript:void(0);" class="jcarousel-control-next" ><i class="fa fa-angle-right"></i></a>
                            </div><!-- //NAVIGATION -->

                            <div class="jcarousel" data-appear-top-offset='-100' data-animated='fadeInUp'>
                                <ul>
                                    <li>
                                        <!-- TOVAR -->
                                        <div class="tovar_item_new">
                                            <div class="tovar_img">
                                                <img src="images/produtos/informatica/image1.png" alt="" />
                                                <div class="open-project-link">
                                                    <a class="open-project tovar_view" href="produtos/i5-8geracao.jsp">Visualizar</a>
                                                </div>
                                            </div>
                                            <div class="tovar_description clearfix">
                                                <a class="tovar_title" href="rtx2080.jsp" >Core i5 8° Geração</a>
                                                <span class="tovar_price">R$850.00</span>
                                            </div>
                                        </div><!-- //TOVAR -->
                                    </li>
                                    <li>
                                        <!-- TOVAR -->
                                        <div class="tovar_item_new">
                                            <div class="tovar_img">
                                                <img src="images/produtos/informatica/image6.png" alt="" />
                                                <div class="open-project-link">
                                                    <a class="open-project tovar_view" href="javascript:void(0);" data-url="!projects/women/1.html" >Visualizar</a>
                                                </div>
                                            </div>
                                            <div class="tovar_description clearfix">
                                                <a class="tovar_title" href="product-page.html" >HD Portátil 1TB</a>
                                                <span class="tovar_price">R$150.00</span>
                                            </div>
                                        </div><!-- //TOVAR -->
                                    </li>
                                    <li>
                                        <!-- TOVAR -->
                                        <div class="tovar_item_new">
                                            <div class="tovar_img">
                                                <img src="images/produtos/informatica/image7.png" alt="" />
                                                <div class="open-project-link">
                                                    <a class="open-project tovar_view" href="javascript:void(0);" data-url="!projects/women/1.html" >Visualizar</a>
                                                </div>
                                            </div>
                                            <div class="tovar_description clearfix">
                                                <a class="tovar_title" href="product-page.html" >GTX 1060 EVGA</a>
                                                <span class="tovar_price">R$1200.00</span>
                                            </div>
                                        </div><!-- //TOVAR -->
                                    </li>
                                    <li>
                                        <!-- TOVAR -->
                                        <div class="tovar_item_new">
                                            <div class="tovar_img">
                                                <img src="images/produtos/informatica/image8.png" alt="" />
                                                <div class="open-project-link">
                                                    <a class="open-project tovar_view" href="javascript:void(0);" data-url="!projects/women/1.html" >Visualizar</a>
                                                </div>
                                            </div>
                                            <div class="tovar_description clearfix">
                                                <a class="tovar_title" href="product-page.html" >ASUS Zen Fone</a>
                                                <span class="tovar_price">R$800.00</span>
                                            </div>
                                        </div><!-- //TOVAR -->
                                    </li>
                                    <li>
                                        <!-- TOVAR -->
                                        <div class="tovar_item_new">
                                            <div class="tovar_img">
                                                <img src="images/produtos/informatica/image9.png" alt="" />
                                                <div class="open-project-link">
                                                    <a class="open-project tovar_view" href="javascript:void(0);" data-url="!projects/women/1.html" >Visualizar</a>
                                                </div>
                                            </div>
                                            <div class="tovar_description clearfix">
                                                <a class="tovar_title" href="product-page.html" >Monitor Full HD LG</a>
                                                <span class="tovar_price">R$300.00</span>
                                            </div>
                                        </div><!-- //TOVAR -->
                                    </li>
                                    <li>
                                        <!-- TOVAR -->
                                        <div class="tovar_item_new">
                                            <div class="tovar_img">
                                                <img src="images/produtos/informatica/image10.png" alt="" />
                                                <div class="open-project-link">
                                                    <a class="open-project tovar_view" href="javascript:void(0);" data-url="!projects/women/1.html" >Visualizar</a>
                                                </div>
                                            </div>
                                            <div class="tovar_description clearfix">
                                                <a class="tovar_title" href="product-page.html" >Memória Ram Hyperx</a>
                                                <span class="tovar_price">$250.00</span>
                                            </div>
                                        </div><!-- //TOVAR -->
                                    </li>
                                    <li>
                                        <!-- TOVAR -->
                                        <div class="tovar_item_new">
                                            <div class="tovar_img">
                                                <img src="images/produtos/informatica/image11.png" alt="" />
                                                <div class="open-project-link">
                                                    <a class="open-project tovar_view" href="javascript:void(0);" data-url="!projects/women/1.html" >Visualizar</a>
                                                </div>
                                            </div>
                                            <div class="tovar_description clearfix">
                                                <a class="tovar_title" href="product-page.html" >Mouse e Teclado</a>
                                                <span class="tovar_price">R$180.00</span>
                                            </div>
                                        </div><!-- //TOVAR -->
                                    </li>
                                    <li>
                                        <!-- TOVAR -->
                                        <div class="tovar_item_new">
                                            <div class="tovar_img">
                                                <img src="images/produtos/informatica/image12.png" alt="" />
                                                <div class="open-project-link">
                                                    <a class="open-project tovar_view" href="javascript:void(0);" data-url="!projects/women/1.html" >Visualizar</a>
                                                </div>
                                            </div>
                                            <div class="tovar_description clearfix">
                                                <a class="tovar_title" href="product-page.html" >Micro SD 16 GB + Adaptador</a>
                                                <span class="tovar_price">R$100.00</span>
                                            </div>
                                        </div><!-- //TOVAR -->
                                    </li>


                                </ul>
                            </div>
                        </div><!-- //JCAROUSEL -->
                    </div><!-- //CONTAINER -->
                </section><!-- //NEW ARRIVALS -->




                <!-- FOOTER -->
                <footer>

                    <!-- CONTAINER -->
                    <div class="container" data-animated='fadeInUp'>

                        <!-- ROW -->
                        <div class="row">

                            <div class="col-lg-2 col-md-2 col-sm-3 col-xs-6 col-ss-12 padbot30">
                                <h4>Contato</h4>
                                <div class="foot_address"><span>Explosion E-Commerce</span>Rua Zé das Colves, Guiana City</div>
                                <div class="foot_phone"><a href="#" >(11) 4002-8922</a></div>
                                <div class="foot_mail"><a href="#" >explosionecommerce@explosion.com</a></div>
                            </div>

                            <div class="col-lg-2 col-md-2 col-sm-3 col-xs-6 col-ss-12 padbot30">
                                <h4>Information</h4>
                                <ul class="foot_menu">
                                    <li><a href="#" >Sobre Nós</a></li>
                                    <li><a href="javascript:void(0);" >Política de Privacidade</a></li>
                                    <li><a href="contacts.html" >Contato</a></li>
                                </ul>
                            </div>

                            <div class="respond_clear_480"></div>

                            <div class="col-lg-4 col-md-4 col-sm-6 padbot30">
                                <h4>Sobre a Loja</h4>
                                <p>Nós somos uma loja renomada no mercado, vendemos os produtos mais diversificados. Pode confiar!</p>
                            </div>

                            <div class="respond_clear_768"></div>

                            <div class="col-lg-4 col-md-4 padbot30">
                                <h4>Boletim de Notícias</h4>
                                <form class="newsletter_form clearfix" action="javascript:void(0);" method="get">
                                    <input type="text" name="newsletter" value="Informe seu e-mail" onFocus="if (this.value == 'Enter E-mail & Get 10% off')
                                                this.value = '';" onBlur="if (this.value == '')
                                                            this.value = 'Enter E-mail & Get 10% off';" />
                                    <input class="btn newsletter_btn" type="submit" value="SIGN UP">
                                </form>

                                <h4>Nossas Redes Sociais</h4>
                                <div class="social">
                                    <a href="javascript:void(0);" ><i class="fa fa-twitter"></i></a>
                                    <a href="javascript:void(0);" ><i class="fa fa-facebook"></i></a>
                                    <a href="javascript:void(0);" ><i class="fa fa-google-plus"></i></a>
                                    <a href="javascript:void(0);" ><i class="fa fa-pinterest-square"></i></a>
                                    <a href="javascript:void(0);" ><i class="fa fa-tumblr"></i></a>
                                    <a href="javascript:void(0);" ><i class="fa fa-instagram"></i></a>
                                </div>
                            </div>
                        </div><!-- //ROW -->
                    </div><!-- //CONTAINER -->

                    <!-- COPYRIGHT -->
                    <div class="copyright">

                        <!-- CONTAINER -->
                        <div class="container clearfix">
                            <div class="foot_logo"><a href="index.jsp" ><img src="images/foot_logo.png" alt="" /></a></div>

                            <div class="copyright_inf">
                                <span>Explosion E-Commerce © 1950</span> |                                            
                                <a class="back_top" href="javascript:void(0);" >Voltar ao topo <i class="fa fa-angle-up"></i></a>
                            </div>
                        </div><!-- //CONTAINER -->
                    </div><!-- //COPYRIGHT -->
                </footer><!-- //FOOTER -->
            </div><!-- //PAGE -->
        </div>


        <!-- SCRIPTS -->
        <!--[if IE]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
        <!--[if IE]><html class="ie" lang="en"> <![endif]-->

        <script src="${pageContext.request.contextPath}/js/jquery.min.js" type="text/javascript"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="${pageContext.request.contextPath}/js/jquery.sticky.js" type="text/javascript"></script>
        <script src="${pageContext.request.contextPath}/js/parallax.js" type="text/javascript"></script>
        <script src="${pageContext.request.contextPath}/js/jquery.flexslider-min.js" type="text/javascript"></script>
        <script src="${pageContext.request.contextPath}/js/jquery.jcarousel.js" type="text/javascript"></script>
        <script src="${pageContext.request.contextPath}/js/fancySelect.js"></script>
        <script src="${pageContext.request.contextPath}/js/animate.js" type="text/javascript"></script>
        <script src="${pageContext.request.contextPath}/js/myscript.js" type="text/javascript"></script>
        <script>
                                                    if (top != self)
                                                        top.location.replace(self.location.href);
        </script>

    </body>
</html>
