<%@page import="java.text.DecimalFormat"%>
<%@page import="br.com.ecomerce.dominio.CartaoCreditoPedido"%>
<%@page import="br.com.ecomerce.dominio.EntidadeDominio"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="br.com.ecomerce.dominio.Pedido"%>
<%@page import="br.com.ecomerce.dominio.Produto"%>
<%@page import="java.util.ArrayList"%>
<%@page import="br.com.ecomerce.dominio.Carrinho"%>
<%@page import="br.com.ecomerce.dominio.Cliente"%>
<%@page import="br.com.ecomerce.dominio.Resultado"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<html lang="pt">
    <head>

        <meta charset="utf-8">
        <title>Explosion | E-Commerce de Eletrônicos</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">

        <link rel="shortcut icon" href="images/logo/explosionV1.png">

        <!-- CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/css/flexslider.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/css/fancySelect.css" rel="stylesheet" media="screen, projection" />
        <link href="${pageContext.request.contextPath}/css/animate.css" rel="stylesheet" type="text/css" media="all" />
        <link href="${pageContext.request.contextPath}/css/style.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/css/estilo.css" rel="stylesheet" type="text/css" />

        <!-- FONTS -->
        <link href='http://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
        <link href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">

    </head>
    <body>

        <!-- PRELOADER -->
        <div id="preloader"><img src="${pageContext.request.contextPath}/images/preloader.gif" alt="" /></div>
        <!-- //PRELOADER -->
        <div class="preloader_hide">

            <!-- PAGE -->
            <div id="page">
                <br>

                <!-- TOVAR DETAILS -->
                <section class="tovar_details padbot70">
                    <div class="container">
                        <div class="row">  
                            <%
                                Resultado resultado = (Resultado) request.getAttribute("resultado");
                                Pedido pedido = (Pedido) resultado.getEntidades().get(0);
                                Cliente cliente = (Cliente) session.getAttribute("cliente");
                                DateFormat df = new SimpleDateFormat("dd-MM-yyyy");

                                if (cliente.getAdmin() == 1) {
                            %>
                            <form name="voltar" action="${pageContext.request.contextPath}/minha-conta-admin.jsp" class="checkout_form clearfix" method="POST">
                                <%} else {%>
                                <form name="voltar" action="${pageContext.request.contextPath}/minha-conta.jsp" class="checkout_form clearfix" method="POST">
                                    <%}%>
                                    <!-- SIDEBAR TOVAR DETAILS -->
                                    <div class="col-lg-2 col-md-1 sidebar_tovar_details">

                                    </div><!-- //SIDEBAR TOVAR DETAILS -->
                                    <div class="col-lg-9 col-md-9 tovar_details_wrapper clearfix">
                                        <br>
                                        <div class="col-xs-10">
                                            <h1 style="text-align:center;">Informações do Pedido</h1>                                
                                        </div>

                                        <%
                                        %>
                                        <div class="col-xs-10">
                                            <h2> Endereço de Entrega: </h2> 
                                        </div>
                                        <div class="col-xs-10">
                                            <h3><%out.print(pedido.getEndereco().getLogradouro()
                                                        + ", <b>Numero:</b> " + pedido.getEndereco().getNumeroEndereco()
                                                        + ", <b>CEP:</b>  " + pedido.getEndereco().getCep()
                                                        + ", <b>Bairro:</b>  " + pedido.getEndereco().getBairro()
                                                        + ", <b>Cidade:</b>  " + pedido.getEndereco().getCidade()
                                                        + ", <b>Estado:</b>   " + pedido.getEndereco().getSiglaEstado()
                                                        + ", <b>Complemento:</b>  " + pedido.getEndereco().getComplemento());%></h3>
                                            <hr>
                                        </div>

                                        <div class="col-xs-10">
                                            <h2> Forma de Pagamento:  
                                                <%
                                                    if (pedido.getCartoes().size() == 0) {
                                                        out.print(" Boleto</h2>");

                                                    } else {
                                                        out.print(" Cartão</h2>");
                                                        out.print("<h2> Cartão(ões) Utilizados:</h2>");
                                                    }
                                                %>
                                            </h2>
                                        </div>
                                        <div class="col-xs-10">
                                            <h3>
                                                <%
                                                    DecimalFormat def = new DecimalFormat("0.##");
                                                    if (pedido.getCartoes().size() == 0) {
                                                        out.print("Número do Boleto: " + pedido.getNumeroBoleto());
                                                    } else {
                                                        int contador = 1;
                                                        for (CartaoCreditoPedido cartaoPedido : pedido.getCartoes()) {
                                                            if (contador == 2) {
                                                                out.println("</br></br>");
                                                            }
                                                            out.println("<b>Cartao " + contador + "- </b> numero: " + cartaoPedido.getCartao().getNumeroCartao()
                                                                    + ", <b> Nome Impresso: </b>" + cartaoPedido.getCartao().getNomeImpresso()
                                                                    + ", <b> Data de Validade: </b>" + cartaoPedido.getCartao().getMes() + "/" + cartaoPedido.getCartao().getAno()
                                                                    + ", <b> Bandeira: </b>" + cartaoPedido.getCartao().getBandeira());
                                                            if (cartaoPedido.getValorPago()== 0) {
                                                                out.println("</br><b> Pagamento em : </b> Pago utilizando a carteira virtual");
                                                            } else {
                                                                out.println("</br><b> Pagamento em : </b>" + cartaoPedido.getParcelas()
                                                                        + " x " + def.format(cartaoPedido.getValorPago() / cartaoPedido.getParcelas()));
                                                                contador++;
                                                            }
                                                        }
                                                    }
                                                %> 
                                            </h3>
                                            <hr>
                                        </div>
                                        <div class="col-xs-10">
                                                <h2> Forma de Entrega: <%out.println(pedido.getMetodoEntrega().getNome()
                                                        + " (R$" + def.format(pedido.getMetodoEntrega().getValor()) + ")");%></h2>
                                            <hr>
                                        </div>

                                        <div class="col-xs-10">
                                            <h2> Produtos do Pedido: </h2> 
                                        </div>    

                                        <div class="col-lg-10 col-md-10 padbot40">

                                            <table class="shop_table">
                                                <thead>
                                                    <tr>
                                                        <th class="product-thumbnail" ></th>
                                                        <th class="product-name" style="text-align: center;">Item</th>
                                                        <th class="product-price" style="text-align: center;">Preço</th>
                                                        <th class="product-quantity" style="text-align: center;">Quantidade</th>
                                                        <th class="product-subtotal" style="text-align: center;">Total</th>
                                                    </tr>
                                                </thead>
                                                <tbody>

                                                    <%
                                                        for (Produto produto : pedido.getProdutos().getProdutos()) {

                                                    %>
                                                    <tr class="cart_item">
                                                        <td class="product-thumbnail"><img src="${pageContext.request.contextPath}/<%out.print(produto.getEndereco_imagem());%>" width="100px" alt="" /></td>
                                                        <td class="product-name" style="text-align: center;">
                                                            <a><%out.print(produto.getNome());%> </a>										
                                                        </td>

                                                        <td class="product-price" style="text-align: center;">
                                                            <a>R$ <% out.print(produto.getPreco());%></a>
                                                        </td>

                                                        <td class="product-quantity" style="text-align: center;">
                                                            <a><% out.print(produto.getQuantidadeProdutoCarrinho()); %></a>
                                                        </td>

                                                        <td class="product-subtotal" style="text-align: center;">
                                                            <a><%out.print("R$" + produto.getQuantidadeProdutoCarrinho() * produto.getPreco());%></a>
                                                        </td>

                                                    </tr>
                                                    <%
                                                        }
                                                    %>
                                                    <tr class="cart_item">
                                                        <td class="product-thumbnail"></td>
                                                        <td class="product-name" style="text-align: center;">

                                                        </td>

                                                        <td class="product-price" style="text-align: center;">

                                                        </td>

                                                        <td class="product-quantity" style="text-align: center;">

                                                        </td>

                                                        <td class="product-subtotal" style="text-align: center;">
                                                            <%out.println("R$" + pedido.getValorTotal());%>
                                                        </td>

                                                    </tr>
                                                </tbody>

                                            </table>
                                            <div class="col-xs-12">
                                                <a class="btn active" href="javascript:voltar.submit();" >Voltar</a>
                                            </div>
                                        </div>                                            
                                    </div>    



                                </form>
                        </div>
                </section>                
            </div><!-- //PAGE -->
        </div>


        <!-- SCRIPTS -->
        <!--[if IE]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
        <!--[if IE]><html class="ie" lang="en"> <![endif]-->

        <script src="${pageContext.request.contextPath}/js/jquery.min.js" type="text/javascript"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="${pageContext.request.contextPath}/js/jquery.sticky.js" type="text/javascript"></script>
        <script src="${pageContext.request.contextPath}/js/parallax.js" type="text/javascript"></script>
        <script src="${pageContext.request.contextPath}/js/jquery.flexslider-min.js" type="text/javascript"></script>
        <script src="${pageContext.request.contextPath}/js/jquery.jcarousel.js" type="text/javascript"></script>
        <script src="${pageContext.request.contextPath}/js/fancySelect.js"></script>
        <script src="${pageContext.request.contextPath}/js/animate.js" type="text/javascript"></script>
        <script src="${pageContext.request.contextPath}/js/myscript.js" type="text/javascript"></script>
        <script>
            if (top != self)
                top.location.replace(self.location.href);
        </script>
    </body>
</html>
