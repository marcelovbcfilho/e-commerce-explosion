<%@page import="br.com.ecomerce.dominio.EntidadeDominio"%>
<%@page import="br.com.ecomerce.dominio.SubCategoria"%>
<%@page import="br.com.ecomerce.dominio.Categoria"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="br.com.ecomerce.dominio.Pedido"%>
<%@page import="br.com.ecomerce.dominio.Endereco"%>
<%@page import="br.com.ecomerce.dominio.Resultado"%>
<%@page import="br.com.ecomerce.dominio.Produto"%>
<%@page import="br.com.ecomerce.dominio.Carrinho"%>
<%@page import="java.util.ArrayList"%>
<%@page import="br.com.ecomerce.dominio.Cliente"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>

        <meta charset="utf-8">
        <title>Explosion | E-Commerce de Eletrônicos</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">

        <link rel="shortcut icon" href="${pageContext.request.contextPath}/images/favicon.ico">

        <!-- CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/css/flexslider.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/css/fancySelect.css" rel="stylesheet" media="screen, projection" />
        <link href="${pageContext.request.contextPath}/css/animate.css" rel="stylesheet" type="text/css" media="all" />
        <link href="${pageContext.request.contextPath}/css/style.css" rel="stylesheet" type="text/css" />

        <!-- FONTS -->
        <link href="http://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900,900italic" rel="stylesheet" type="text/css">
        <link href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">

    </head>
    <body>

        <!-- PRELOADER -->
        <div id="preloader"><img src="${pageContext.request.contextPath}/images/preloader.gif" alt="" /></div>
        <!-- //PRELOADER -->
        <div class="preloader_hide">

            <!-- PAGE -->
            <div id="page">

                <!-- HEADER -->
                <header>

                    <!-- TOP INFO -->
                    <div class="top_info">

                        <!-- CONTAINER -->
                        <div class="container clearfix">
                            <ul class="secondary_menu">
                                <%
                                    DecimalFormat df = new DecimalFormat("0.##");
                                    Cliente cliente = null;
                                    if (session.getAttribute("cliente") != null) {
                                        cliente = (Cliente) session.getAttribute("cliente");
                                        if (cliente.getAdmin() == 1) {
                                            out.println("<li><a href='" + request.getContextPath() + "/minha-conta-admin.jsp' >Minha Conta</a></li>");
                                            out.println("<li><a>Bem Vindo " + cliente.getNomeCompleto() + "</a></li>");
                                            out.println("<li><a href='" + request.getContextPath() + "/Logout?logout=logout'>Logout</a></li>");
                                        } else {
                                            out.println("<li><a href='" + request.getContextPath() + "/minha-conta.jsp' >Minha Conta</a></li>");
                                            out.println("<li><a>Bem Vindo " + cliente.getNomeCompleto() + "</a></li>");
                                            out.println("<li><a>Carteira: R$" + df.format(cliente.getCarteira()) + "</a></li>");
                                            out.println("<li><a href='" + request.getContextPath() + "/Logout?logout=logout'>Logout</a></li>");
                                        }

                                    } else {
                                        out.println("<li><a href='" + request.getContextPath() + "/login.jsp' >Login</a></li>");
                                        out.println("<li><a href='" + request.getContextPath() + "/cadastro-cliente.jsp' >Registrar</a></li>");
                                    }

                                    Resultado resultado = (Resultado) request.getAttribute("resultado");
                                    Endereco endereco = (Endereco) resultado.getEntidades().get(0);
                                    Pedido pedido = new Pedido();
                                    pedido.setEndereco(endereco);
                                    pedido.setCliente(cliente);
                                    session.setAttribute("pedido", pedido);

                                %>
                            </ul>
                        </div><!-- //CONTAINER -->
                    </div><!-- TOP INFO -->


                    <!-- MENU BLOCK -->
                    <div class="menu_block">

                        <!-- CONTAINER -->
                        <div class="container clearfix">

                            <!-- LOGO -->
                            <div class="logo">
                                <a href="${pageContext.request.contextPath}/index.jsp" ><img src="${pageContext.request.contextPath}/images/logo/explosionV2.png" alt="" /></a>
                            </div><!-- //LOGO -->

                            <!-- SEARCH FORM -->
                            <div class="top_search_form">
                                <a class="top_search_btn" href="javascript:void(0);"><i class="fa fa-search"></i></a>
                                <form action="${pageContext.request.contextPath}/VisualizarProduto" method='POST'>
                                    <input type="text" name="pesquisar" value="Pesquisar" onFocus="if (this.value == 'Pesquisar')
                                                this.value = '';" onBlur="if (this.value == '')
                                                            this.value = 'Pesquisar';"/>
                                    <input type='hidden' id='OPERACAO' name='OPERACAO' value='VISUALIZAR'/>
                                    <input type='hidden' id='PAGINA' name='PAGINA' value='PESQUISA'/>
                                </form>
                            </div><!-- SEARCH FORM -->

                            <%                                double total = 0;
                                if (null == session.getAttribute("carrinho")) {
                                    ArrayList<Produto> produtos = new ArrayList<Produto>();
                                    Carrinho carrinho = new Carrinho();
                                    carrinho.setProdutos(produtos);
                                    session.setAttribute("carrinho", carrinho);
                            %>
                            <div class="shopping_bag">
                                <a class="shopping_bag_btn" href = "javascript:void(0);" > 
                                    <i class="fa fa-shopping-cart"></i > <p>Carrinho</p > <span> <% out.print(carrinho.getProdutos().size());%></span></a>
                                <div class="cart">
                                    <div class="cart_total">
                                        <div class="clearfix">
                                            <span class="cart_subtotal">Total: <b><% out.print("R$" + total);%></b></span>
                                        </div>
                                        <a class="btn active" href="logado/carrinho-compra.jsp">Ir ao Carrinho</a>
                                    </div>
                                </div>
                            </div>
                            <%
                            } else {

                                Carrinho carrinho = (Carrinho) session.getAttribute("carrinho");

                            %>
                            <!-- SHOPPING BAG -->
                            <div class="shopping_bag">
                                <a class="shopping_bag_btn" href="javascript:void(0);" ><i class="fa fa-shopping-cart"></i><p>Carrinho</p><span><%out.print(carrinho.getProdutos().size());%></span></a>
                                <div class="cart">
                                    <ul class="cart-items">
                                        <%
                                            for (Produto pro : carrinho.getProdutos()) {
                                                out.print("<li class='clearfix'>");
                                                out.print("<img class='cart_item_product' src='" + request.getContextPath()+"/"+ pro.getEndereco_imagem() + "'/>");
                                                out.print(pro.getQuantidadeProdutoCarrinho() + " x " + pro.getNome());
                                                out.print("<span class='cart_item_price'> R$" + pro.getPreco() + "</span>");
                                                out.print("</li>");
                                                total += pro.getPreco() * pro.getQuantidadeProdutoCarrinho();
                                            }
                                        %>
                                    </ul>
                                    <div class="cart_total">
                                        <div class="clearfix"><span class="cart_subtotal">Total: <b><% out.print("R$" + total);%></b></span></div>
                                        <a class="btn active" href="${pageContext.request.contextPath}/logado/carrinho-compra.jsp">Ir ao Carrinho</a>
                                    </div>
                                </div>
                            </div>
                            <%
                                }
                            %>

                            <!-- MENU -->
                            <ul class="navmenu center">
                                <li>
                                    <a href="${pageContext.request.contextPath}/index.jsp" >Home</a>
                                </li>
                                <%
                                    Resultado resultadoCategorias = (Resultado) session.getAttribute("categorias");
                                    Categoria catg;
                                    SubCategoria sub;
                                    for (EntidadeDominio entity : resultadoCategorias.getEntidades()) {
                                        catg = (Categoria) entity;
                                %>
                                <li class='sub-menu'>
                                    <form name='categoria<%out.print(catg.getId());%>' action='${pageContext.request.contextPath}/VisualizarEstoque' method='POST' hidden>
                                        <input type='hidden' id='idCategoria' name='idCategoria' value='<% out.print(catg.getId());%>'/>
                                        <input type='hidden' id='OPERACAO' name='OPERACAO' value='VISUALIZAR'/>
                                        <input type='hidden' id='PAGINA' name='PAGINA' value='CATALOGO'/>                                        
                                    </form>
                                    <a href='javascript:categoria<%out.print(catg.getId());%>.submit();'>
                                        <%out.print(catg.getNome()); %>
                                    </a>
                                    <%
                                        if (catg.getSubCategorias().size() == 0) {
                                            out.println("</li>");
                                        } else {
                                    %>
                                    <ul class='mega_menu megamenu_col1 clearfix'>
                                        <li class='col'>
                                            <ol>
                                                <%
                                                    for (EntidadeDominio entitySub : catg.getSubCategorias()) {
                                                        sub = (SubCategoria) entitySub;
                                                %>
                                                <li>
                                                    <form name='sub<% out.print(sub.getId());%>' action='${pageContext.request.contextPath}/VisualizarEstoque' method='POST' hidden>
                                                        <input type='hidden' id='OPERACAO' name='OPERACAO' value='VISUALIZAR'/>
                                                        <input type='hidden' id='PAGINA' name='PAGINA' value='CATALOGO'/>
                                                        <input type='hidden' id='idSubCategoria' name='idSubCategoria' value='<%out.print(sub.getId());%>'/>
                                                    </form>
                                                    <a href='javascript:sub<% out.print(sub.getId());%>.submit();'><%out.print(sub.getNome());%></a>
                                                </li>
                                                <%}%>
                                            </ol>
                                        </li>
                                    </ul>
                                    <%}%>
                                </li>
                                <%
                                    }
                                %>                                                                
                            </ul><!-- //MENU -->
                        </div><!-- //CONTAINER -->
                    </div><!-- //MENU BLOCK -->
                </header><!-- //HEADER -->


                <!-- BREADCRUMBS -->
                <section class="breadcrumb parallax margbot30"></section>
                <!-- //BREADCRUMBS -->


                <!-- PAGE HEADER -->
                <section class="page_header">

                    <!-- CONTAINER -->
                    <div class="container border0 margbot0">
                        <h3 class="pull-left"><b>Finalizando Compra</b></h3>

                        <div class="pull-right">
                            <a href="carrinho-compra.jsp" >Voltar ao Carrinho<i class="fa fa-angle-right"></i></a>
                        </div>
                    </div><!-- //CONTAINER -->
                </section><!-- //PAGE HEADER -->


                <!-- CHECKOUT PAGE -->
                <section class="checkout_page">

                    <!-- CONTAINER -->
                    <div class="container">

                        <!-- CHECKOUT BLOCK -->
                        <div class="checkout_block">
                            <ul class="checkout_nav">
                                <li class="done_step">1. Confirmar Endereço</li>
                                <li class="active_step">2. Método de Entrega</li>
                                <li>3. Forma de Pagamento</li>
                                <li class="last">4. Confirmar Compra</li>
                            </ul>

                            <div class="checkout_delivery clearfix">
                                <p class="checkout_title">Método de Entrega</p>
                                <form class="form" action="${pageContext.request.contextPath}/VisualizarMetodoEntrega" method="POST">
                                    <ul>                
                                        <li>
                                            <input id="sedex" type="radio" name="metodo_entrega" value ="1" checked hidden />
                                            <label for="sedex">Sedex <b>R$15,06 (1-5 dias)</b></label>
                                        </li>
                                        <li>
                                            <input id="ninja" type="radio" name="metodo_entrega" value ="2" hidden />
                                            <label for="ninja">Entrega Ninja<b>R$ 26,01 (1-3 dias)</b><img  alt="" /></label>
                                        </li>
                                        <li>
                                            <input id="tnt" type="radio" name="metodo_entrega" value ="3" hidden />
                                            <label for="tnt">TNT Express<b>R$ 50 (1-2 dias)</b><img alt="" /></label>
                                        </li>
                                        <li>
                                            <input id="agendado" type="radio" name="metodo_entrega" value ="4" hidden />
                                            <label for="agendado">Entrega Agendada <b>R$ 15,40 (Apartir de 23 dias)</b><img alt="" /></label>
                                        </li>
                                    </ul>
                                    <h4>Opções disponíveis para o endereço: <% out.print(endereco.getLogradouro()); %>
                                        Numero: <% out.print(endereco.getNumeroEndereco()); %>
                                        CEP: <% out.print(endereco.getCep()); %>
                                        Bairro: <% out.print(endereco.getBairro()); %>
                                        Cidade: <% out.print(endereco.getCidade()); %>
                                        Estado: <% out.print(endereco.getSiglaEstado());%></h4>
                                    <input type="hidden" id="OPERACAO" name="OPERACAO" value="VISUALIZAR"/>
                                    <div class="col-xs-12">
                                        <input type="submit" class="btn" value="CONTINUAR"/>
                                    </div>
                                </form>                                
                            </div>
                        </div><!-- //CHECKOUT BLOCK -->
                    </div><!-- //CONTAINER -->
                </section><!-- //CHECKOUT PAGE -->


                <!-- FOOTER -->
                <footer>

                    <!-- CONTAINER -->
                    <div class="container" data-animated='fadeInUp'>

                        <!-- ROW -->
                        <div class="row">

                            <div class="col-lg-2 col-md-2 col-sm-3 col-xs-6 col-ss-12 padbot30">
                                <h4>Contato</h4>
                                <div class="foot_address"><span>Explosion E-Commerce</span>Rua Zé das Colves, Guiana City</div>
                                <div class="foot_phone"><a href="#" >(11) 4002-8922</a></div>
                                <div class="foot_mail"><a href="#" >explosionecommerce@explosion.com</a></div>
                            </div>

                            <div class="col-lg-2 col-md-2 col-sm-3 col-xs-6 col-ss-12 padbot30">
                                <h4>Information</h4>
                                <ul class="foot_menu">
                                    <li><a href="#" >Sobre Nós</a></li>
                                    <li><a href="javascript:void(0);" >Política de Privacidade</a></li>
                                    <li><a href="contacts.html" >Contato</a></li>
                                </ul>
                            </div>

                            <div class="respond_clear_480"></div>

                            <div class="col-lg-4 col-md-4 col-sm-6 padbot30">
                                <h4>Sobre a Loja</h4>
                                <p>Nós somos uma loja renomada no mercado, vendemos os produtos mais diversificados. Pode confiar!</p>
                            </div>

                            <div class="respond_clear_768"></div>

                            <div class="col-lg-4 col-md-4 padbot30">
                                <h4>Boletim de Notícias</h4>
                                <form class="newsletter_form clearfix" action="javascript:void(0);" method="get">
                                    <input type="text" name="newsletter" value="Informe seu e-mail" onFocus="if (this.value == 'Enter E-mail & Get 10% off')
                                                this.value = '';" onBlur="if (this.value == '')
                                                            this.value = 'Enter E-mail & Get 10% off';" />
                                    <input class="btn newsletter_btn" type="submit" value="SIGN UP">
                                </form>

                                <h4>Nossas Redes Sociais</h4>
                                <div class="social">
                                    <a href="javascript:void(0);" ><i class="fa fa-twitter"></i></a>
                                    <a href="javascript:void(0);" ><i class="fa fa-facebook"></i></a>
                                    <a href="javascript:void(0);" ><i class="fa fa-google-plus"></i></a>
                                    <a href="javascript:void(0);" ><i class="fa fa-pinterest-square"></i></a>
                                    <a href="javascript:void(0);" ><i class="fa fa-tumblr"></i></a>
                                    <a href="javascript:void(0);" ><i class="fa fa-instagram"></i></a>
                                </div>
                            </div>
                        </div><!-- //ROW -->
                    </div><!-- //CONTAINER -->

                    <!-- COPYRIGHT -->
                    <div class="copyright">

                        <!-- CONTAINER -->
                        <div class="container clearfix">
                            <div class="foot_logo"><a href="${pageContext.request.contextPath}/index.jsp" ><img src="images/foot_logo.png" alt="" /></a></div>

                            <div class="copyright_inf">
                                <span>Explosion E-Commerce © 1950</span> |                                            
                                <a class="back_top" href="javascript:void(0);" >Voltar ao topo <i class="fa fa-angle-up"></i></a>
                            </div>
                        </div><!-- //CONTAINER -->
                    </div><!-- //COPYRIGHT -->
                </footer><!-- //FOOTER -->
            </div><!-- //PAGE -->
        </div>

        <!-- TOVAR MODAL CONTENT -->
        <div id="modal-body" class="clearfix">
            <div id="tovar_content"></div>
            <div class="close_block"></div>
        </div><!-- TOVAR MODAL CONTENT -->

        <!-- SCRIPTS -->
        <!--[if IE]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
        <!--[if IE]><html class="ie" lang="en"> <![endif]-->

        <script src="${pageContext.request.contextPath}/js/jquery.min.js" type="text/javascript"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="${pageContext.request.contextPath}/js/jquery.sticky.js" type="text/javascript"></script>
        <script src="${pageContext.request.contextPath}/js/parallax.js" type="text/javascript"></script>
        <script src="${pageContext.request.contextPath}/js/jquery.flexslider-min.js" type="text/javascript"></script>
        <script src="${pageContext.request.contextPath}/js/jquery.jcarousel.js" type="text/javascript"></script>
        <script src="${pageContext.request.contextPath}/js/jqueryui.custom.min.js" type="text/javascript"></script>
        <script src="${pageContext.request.contextPath}/js/fancySelect.js"></script>
        <script src="${pageContext.request.contextPath}/js/animate.js" type="text/javascript"></script>
        <script src="${pageContext.request.contextPath}/js/myscript.js" type="text/javascript"></script>

    </body>
</html>