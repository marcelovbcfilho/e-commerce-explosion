<%@page import="java.text.DecimalFormat"%>
<%@page import="br.com.ecomerce.dominio.SubCategoria"%>
<%@page import="br.com.ecomerce.dominio.Categoria"%>
<%@page import="br.com.ecomerce.dominio.CartaoCreditoPedido"%>
<%@page import="br.com.ecomerce.dominio.CartaoCredito"%>
<%@page import="br.com.ecomerce.dominio.EntidadeDominio"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="br.com.ecomerce.dominio.Pedido"%>
<%@page import="br.com.ecomerce.dominio.Produto"%>
<%@page import="java.util.ArrayList"%>
<%@page import="br.com.ecomerce.dominio.Carrinho"%>
<%@page import="br.com.ecomerce.dominio.Cliente"%>
<%@page import="br.com.ecomerce.dominio.Resultado"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<html lang="pt">
    <head>

        <meta charset="utf-8">
        <title>Explosion | E-Commerce de Eletrônicos</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">

        <link rel="shortcut icon" href="${pageContext.request.contextPath}/images/logo/explosionV1.png">

        <!-- CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/css/flexslider.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/css/fancySelect.css" rel="stylesheet" media="screen, projection" />
        <link href="${pageContext.request.contextPath}/css/animate.css" rel="stylesheet" type="text/css" media="all" />
        <link href="${pageContext.request.contextPath}/css/style.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/css/estilo.css" rel="stylesheet" type="text/css" />

        <!-- FONTS -->
        <link href='http://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
        <link href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">

    </head>
    <body>

        <!-- PRELOADER -->
        <div id="preloader"><img src="${pageContext.request.contextPath}/images/preloader.gif" alt="" /></div>
        <!-- //PRELOADER -->
        <div class="preloader_hide">

            <!-- PAGE -->
            <div id="page">

                <!-- HEADER -->
                <header>

                    <!-- TOP INFO -->
                    <div class="top_info">

                        <!-- CONTAINER -->
                        <div class="container clearfix">
                            <ul class="secondary_menu">
                                <%
                                    DecimalFormat def = new DecimalFormat("0.##");
                                    Cliente cliente = null;
                                    if (session.getAttribute("cliente") != null) {
                                        cliente = (Cliente) session.getAttribute("cliente");
                                        if (cliente.getAdmin() == 1) {
                                            out.println("<li><a href='" + request.getContextPath() + "/minha-conta-admin.jsp' >Minha Conta</a></li>");
                                            out.println("<li><a>Bem Vindo " + cliente.getNomeCompleto() + "</a></li>");
                                            out.println("<li><a href='" + request.getContextPath() + "/Logout?logout=logout'>Logout</a></li>");
                                        } else {
                                            out.println("<li><a href='" + request.getContextPath() + "/minha-conta.jsp' >Minha Conta</a></li>");
                                            out.println("<li><a>Bem Vindo " + cliente.getNomeCompleto() + "</a></li>");
                                            out.println("<li><a>Carteira: R$" + def.format(cliente.getCarteira()) + "</a></li>");
                                            out.println("<li><a href='" + request.getContextPath() + "/Logout?logout=logout'>Logout</a></li>");
                                        }

                                    } else {
                                        out.println("<li><a href='" + request.getContextPath() + "/login.jsp' >Login</a></li>");
                                        out.println("<li><a href='" + request.getContextPath() + "/cadastro-cliente.jsp' >Registrar</a></li>");
                                    }
                                %>
                            </ul>
                        </div><!-- //CONTAINER -->
                    </div><!-- TOP INFO -->


                    <!-- MENU BLOCK -->
                    <div class="menu_block">

                        <!-- CONTAINER -->
                        <div class="container clearfix">

                            <!-- LOGO -->
                            <div class="logo">
                                <a href="${pageContext.request.contextPath}/index.jsp" ><img src="${pageContext.request.contextPath}/images/logo/explosionV2.png" alt="" /></a>
                            </div><!-- //LOGO -->

                            <!-- SEARCH FORM -->
                            <div class="top_search_form">
                                <a class="top_search_btn" href="javascript:void(0);"><i class="fa fa-search"></i></a>
                                <form action="${pageContext.request.contextPath}/VisualizarProduto" method='POST'>
                                    <input type="text" name="pesquisar" value="Pesquisar" onFocus="if (this.value == 'Pesquisar')
                                                this.value = '';" onBlur="if (this.value == '')
                                                            this.value = 'Pesquisar';"/>
                                    <input type='hidden' id='OPERACAO' name='OPERACAO' value='VISUALIZAR'/>
                                    <input type='hidden' id='PAGINA' name='PAGINA' value='PESQUISA'/>
                                </form>
                            </div><!-- SEARCH FORM -->

                            <%
                                double total = 0;
                                if (null == session.getAttribute("carrinho")) {
                                    ArrayList<Produto> produtos = new ArrayList<Produto>();
                                    Carrinho carrinho = new Carrinho();
                                    carrinho.setProdutos(produtos);
                                    session.setAttribute("carrinho", carrinho);
                            %>
                            <div class="shopping_bag">
                                <a class="shopping_bag_btn" href = "javascript:void(0);" > 
                                    <i class="fa fa-shopping-cart"></i > <p>Carrinho</p > <span> <% out.print(carrinho.getProdutos().size());%></span></a>
                                <div class="cart">
                                    <div class="cart_total">
                                        <div class="clearfix">
                                            <span class="cart_subtotal">Total: <b><% out.print("R$" + total);%></b></span>
                                        </div>
                                        <a class="btn active" href="logado/carrinho-compra.jsp">Ir ao Carrinho</a>
                                    </div>
                                </div>
                            </div>
                            <%
                            } else {

                                Carrinho carrinho = (Carrinho) session.getAttribute("carrinho");

                            %>
                            <!-- SHOPPING BAG -->
                            <div class="shopping_bag">
                                <a class="shopping_bag_btn" href="javascript:void(0);" ><i class="fa fa-shopping-cart"></i><p>Carrinho</p><span><%out.print(carrinho.getProdutos().size());%></span></a>
                                <div class="cart">
                                    <ul class="cart-items">
                                        <%
                                            for (Produto pro : carrinho.getProdutos()) {
                                                out.print("<li class='clearfix'>");
                                                out.print("<img class='cart_item_product' src='" + request.getContextPath() + "/" + pro.getEndereco_imagem() + "'/>");
                                                out.print(pro.getQuantidadeProdutoCarrinho() + " x " + pro.getNome());
                                                out.print("<span class='cart_item_price'> R$" + pro.getPreco() + "</span>");
                                                out.print("</li>");
                                                total += pro.getPreco() * pro.getQuantidadeProdutoCarrinho();
                                            }
                                        %>
                                    </ul>
                                    <div class="cart_total">
                                        <div class="clearfix"><span class="cart_subtotal">Total: <b><% out.print("R$" + total);%></b></span></div>
                                        <a class="btn active" href="${pageContext.request.contextPath}/logado/carrinho-compra.jsp">Ir ao Carrinho</a>
                                    </div>
                                </div>
                            </div>
                            <%
                                }
                            %>

                            <!-- MENU -->
                            <ul class="navmenu center">
                                <li>
                                    <a href="${pageContext.request.contextPath}/index.jsp" >Home</a>
                                </li>
                                <%
                                    Resultado resultadoCategorias = (Resultado) session.getAttribute("categorias");
                                    Categoria catg;
                                    SubCategoria sub;
                                    for (EntidadeDominio entity : resultadoCategorias.getEntidades()) {
                                        catg = (Categoria) entity;
                                %>
                                <li class='sub-menu'>
                                    <form name='categoria<%out.print(catg.getId());%>' action='${pageContext.request.contextPath}/VisualizarEstoque' method='POST' hidden>
                                        <input type='hidden' id='idCategoria' name='idCategoria' value='<% out.print(catg.getId());%>'/>
                                        <input type='hidden' id='OPERACAO' name='OPERACAO' value='VISUALIZAR'/>
                                        <input type='hidden' id='PAGINA' name='PAGINA' value='CATALOGO'/>                                        
                                    </form>
                                    <a href='javascript:categoria<%out.print(catg.getId());%>.submit();'>
                                        <%out.print(catg.getNome()); %>
                                    </a>
                                    <%
                                        if (catg.getSubCategorias().size() == 0) {
                                            out.println("</li>");
                                        } else {
                                    %>
                                    <ul class='mega_menu megamenu_col1 clearfix'>
                                        <li class='col'>
                                            <ol>
                                                <%
                                                    for (EntidadeDominio entitySub : catg.getSubCategorias()) {
                                                        sub = (SubCategoria) entitySub;
                                                %>
                                                <li>
                                                    <form name='sub<% out.print(sub.getId());%>' action='${pageContext.request.contextPath}/VisualizarEstoque' method='POST' hidden>
                                                        <input type='hidden' id='OPERACAO' name='OPERACAO' value='VISUALIZAR'/>
                                                        <input type='hidden' id='PAGINA' name='PAGINA' value='CATALOGO'/>
                                                        <input type='hidden' id='idSubCategoria' name='idSubCategoria' value='<%out.print(sub.getId());%>'/>
                                                    </form>
                                                    <a href='javascript:sub<% out.print(sub.getId());%>.submit();'><%out.print(sub.getNome());%></a>
                                                </li>
                                                <%}%>
                                            </ol>
                                        </li>
                                    </ul>
                                    <%}%>
                                </li>
                                <%
                                    }
                                %>                                                                
                            </ul><!-- //MENU -->
                        </div><!-- //CONTAINER -->
                    </div><!-- //MENU BLOCK -->
                </header><!-- //HEADER -->

                <!-- BREADCRUMBS -->
                <section class="breadcrumb parallax margbot30"></section>
                <!-- //BREADCRUMBS -->

                <!-- TOVAR DETAILS -->
                <section class="tovar_details padbot70">
                    <div class="container">
                        <div class="row">  

                            <form action='${pageContext.request.contextPath}/SalvarDevolucao' class="checkout_form clearfix" name='devolucao' method='POST'>
                                <!-- SIDEBAR TOVAR DETAILS -->
                                <div class="col-lg-2 col-md-1 sidebar_tovar_details">

                                </div><!-- //SIDEBAR TOVAR DETAILS -->
                                <div class="col-lg-9 col-md-9 tovar_details_wrapper clearfix">                                    

                                    <%
                                        Resultado resultado = (Resultado) request.getAttribute("resultado");
                                        DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
                                        boolean flg = false;
                                        Pedido pedido = (Pedido) resultado.getEntidades().get(0);
                                        session.setAttribute("devolucao", pedido);

                                    %>
                                    <div class="col-xs-10">
                                        <h1>Informações do Pedido</h1>                                
                                    </div>
                                    <div class="col-xs-10">
                                        <h2> Endereço de Entrega: </h2> 
                                    </div>
                                    <div class="col-xs-10">
                                        <h3><%out.print(pedido.getEndereco().getLogradouro()
                                                    + ", <b>Numero:</b> " + pedido.getEndereco().getNumeroEndereco()
                                                    + ", <b>CEP:</b>  " + pedido.getEndereco().getCep()
                                                    + ", <b>Bairro:</b>  " + pedido.getEndereco().getBairro()
                                                    + ", <b>Cidade:</b>  " + pedido.getEndereco().getCidade()
                                                    + ", <b>Estado:</b>   " + pedido.getEndereco().getSiglaEstado()
                                                    + ", <b>Complemento:</b>  " + pedido.getEndereco().getComplemento());%></h3>
                                        <hr>
                                    </div>

                                    <div class="col-xs-10">
                                        <h2> Forma de Pagamento: </h2> 
                                    </div>
                                    <div class="col-xs-10">
                                        <h3>
                                            <%
                                                if (pedido.getCartoes().size() < 1) {
                                                    out.print("<b>Boleto - </b> numero: " + pedido.getNumeroBoleto());
                                                } else {
                                                    for (CartaoCreditoPedido crtPedido : pedido.getCartoes()) {
                                                        out.print("<b>Cartao - </b> numero: " + crtPedido.getCartao().getNumeroCartao()
                                                                + ", <b> Nome Impresso: </b>" + crtPedido.getCartao().getNomeImpresso()
                                                                + ", <b> Data de Validade: </b>" + crtPedido.getCartao().getMes()+"/"+crtPedido.getCartao().getAno()
                                                                + ", <b> Bandeira: </b>" + crtPedido.getCartao().getBandeira()
                                                                + ", <b> Parcelas:  </b>" + crtPedido.getParcelas()
                                                                + ", <b> Valor pago: </b>" + crtPedido.getValorPago()
                                                                + "<br />");
                                                    }
                                                }
                                            %> 
                                        </h3>
                                        <hr>
                                    </div>

                                    <div class="col-xs-10">
                                        <h2> Produtos do Pedido: </h2> 
                                    </div>    

                                    <div class="col-lg-10 col-md-10 padbot40">

                                        <table class="shop_table">
                                            <thead>
                                                <tr>
                                                    <th class="product-thumbnail" ></th>
                                                    <th class="product-name" style="text-align: center;">Item</th>
                                                    <th class="product-price" style="text-align: center;">Preço</th>
                                                    <th class="product-quantity" style="text-align: center;">Quantidade</th>
                                                    <th class="product-subtotal" style="text-align: center;">Total</th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                                <%
                                                    for (Produto produto : pedido.getProdutos().getProdutos()) {
                                                        if (produto.getQuantidadeProdutoCarrinho() != 0) {

                                                %>
                                                <tr class="cart_item">
                                                    <td class="product-thumbnail">
                                                        <input type="checkbox" id="checkProduto<% out.print(produto.getId()); %>" name="checkProduto" value = '<%out.print(produto.getId());%>'hidden>
                                                        <label for="checkProduto<% out.print(produto.getId()); %>">
                                                            <a>
                                                                <img src="${pageContext.request.contextPath}/<%out.print(produto.getEndereco_imagem());%>" width="100px" alt="" />
                                                            </a>
                                                        </label>    

                                                        <input type="number" name="qtdeProduto" id="qtdeProduto" min ="1" max="<%out.print(produto.getQuantidadeProdutoCarrinho());%>" value="<%out.print(produto.getQuantidadeProdutoCarrinho());%>" />
                                                    </td>
                                                    <td class="product-name" style="text-align: center;">
                                                        <a><%out.print(produto.getNome());%></a>
                                                    </td>

                                                    <td class="product-price" style="text-align: center;">
                                                        <a>R$ <% out.print(produto.getPreco());%></a>
                                                    </td>

                                                    <td class="product-quantity" style="text-align: center;">
                                                        <a id="qtdePedido" name="qtdePedido"><% out.print(produto.getQuantidadeProdutoCarrinho()); %></a>
                                                    </td>

                                                    <td class="product-subtotal" style="text-align: center;">
                                                        <a><%out.print(produto.getQuantidadeProdutoCarrinho() * produto.getPreco());%></a>
                                                    </td>

                                                </tr>
                                                <%
                                                } else {
                                                %>
                                                <tr class="cart_item">
                                                    <td class="product-thumbnail">  
                                                        DEVOLUÇÃO
                                                    </td>
                                                    <td class="product-name" style="text-align: center;">
                                                        <a><%out.print(produto.getNome());%></a>
                                                    </td>

                                                    <td class="product-price" style="text-align: center;">
                                                        <a>R$ <% out.print(produto.getPreco());%></a>
                                                    </td>

                                                    <td class="product-quantity" style="text-align: center;">
                                                        <a><% out.print(produto.getQuantidadeProdutoCarrinho()); %></a>
                                                    </td>

                                                    <td class="product-subtotal" style="text-align: center;">
                                                        <a><%out.print(produto.getQuantidadeProdutoCarrinho() * produto.getPreco());%></a>
                                                    </td>    
                                                    <%
                                                            }
                                                        }
                                                    %>
                                            </tbody>
                                        </table>
                                        <input type='hidden' id='OPERACAO' name='OPERACAO' value='SALVAR'/>
                                        <input type='hidden' id='idCliente' name='idCliente' value='<%out.print(cliente.getId());%>'/>
                                        <input type='hidden' id='idPedido' name='idPedido' value='<%out.print(pedido.getId());%>'/>                                        
                                        <div class="col-xs-10">
                                            <div class="checkout_form_note"><b><span id='aviso' class="color_red"></span></b></div>                                    
                                        </div>
                                        <div class='col-xs-10'>
                                            <input type="button" class='btn' value="Confirmar" onclick="verificaChecks()">
                                        </div>
                                    </div>
                                </div>

                            </form>
                        </div>
                </section>
                <br/>

                <!-- FOOTER -->
                <footer>

                    <!-- CONTAINER -->
                    <div class="container" data-animated='fadeInUp'>

                        <!-- ROW -->
                        <div class="row">

                            <div class="col-lg-2 col-md-2 col-sm-3 col-xs-6 col-ss-12 padbot30">
                                <h4>Contato</h4>
                                <div class="foot_address"><span>Explosion E-Commerce</span>Rua Zé das Colves, Guiana City</div>
                                <div class="foot_phone"><a href="#" >(11) 4002-8922</a></div>
                                <div class="foot_mail"><a href="#" >explosionecommerce@explosion.com</a></div>
                            </div>

                            <div class="col-lg-2 col-md-2 col-sm-3 col-xs-6 col-ss-12 padbot30">
                                <h4>Information</h4>
                                <ul class="foot_menu">
                                    <li><a href="#" >Sobre Nós</a></li>
                                    <li><a href="javascript:void(0);" >Política de Privacidade</a></li>
                                    <li><a href="contacts.html" >Contato</a></li>
                                </ul>
                            </div>

                            <div class="respond_clear_480"></div>

                            <div class="col-lg-4 col-md-4 col-sm-6 padbot30">
                                <h4>Sobre a Loja</h4>
                                <p>Nós somos uma loja renomada no mercado, vendemos os produtos mais diversificados. Pode confiar!</p>
                            </div>

                            <div class="respond_clear_768"></div>

                            <div class="col-lg-4 col-md-4 padbot30">
                                <h4>Boletim de Notícias</h4>
                                <form class="newsletter_form clearfix" action="javascript:void(0);" method="get">
                                    <input type="text" name="newsletter" value="Informe seu e-mail" onFocus="if (this.value == 'Enter E-mail & Get 10% off')
                                                this.value = '';" onBlur="if (this.value == '')
                                                            this.value = 'Enter E-mail & Get 10% off';" />
                                    <input class="btn newsletter_btn" type="submit" value="SIGN UP">
                                </form>

                                <h4>Nossas Redes Sociais</h4>
                                <div class="social">
                                    <a href="javascript:void(0);" ><i class="fa fa-twitter"></i></a>
                                    <a href="javascript:void(0);" ><i class="fa fa-facebook"></i></a>
                                    <a href="javascript:void(0);" ><i class="fa fa-google-plus"></i></a>
                                    <a href="javascript:void(0);" ><i class="fa fa-pinterest-square"></i></a>
                                    <a href="javascript:void(0);" ><i class="fa fa-tumblr"></i></a>
                                    <a href="javascript:void(0);" ><i class="fa fa-instagram"></i></a>
                                </div>
                            </div>
                        </div><!-- //ROW -->
                    </div><!-- //CONTAINER -->

                    <!-- COPYRIGHT -->
                    <div class="copyright">

                        <!-- CONTAINER -->
                        <div class="container clearfix">
                            <div class="foot_logo"><a href="index.jsp" ><img src="images/foot_logo.png" alt="" /></a></div>

                            <div class="copyright_inf">
                                <span>Explosion E-Commerce © 1950</span> |                                            
                                <a class="back_top" href="javascript:void(0);" >Voltar ao topo <i class="fa fa-angle-up"></i></a>
                            </div>
                        </div><!-- //CONTAINER -->
                    </div><!-- //COPYRIGHT -->
                </footer><!-- //FOOTER -->
            </div><!-- //PAGE -->
        </div>


        <!-- SCRIPTS -->
        <!--[if IE]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
        <!--[if IE]><html class="ie" lang="en"> <![endif]-->

        <script src="${pageContext.request.contextPath}/js/jquery.min.js" type="text/javascript"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="${pageContext.request.contextPath}/js/jquery.sticky.js" type="text/javascript"></script>
        <script src="${pageContext.request.contextPath}/js/parallax.js" type="text/javascript"></script>
        <script src="${pageContext.request.contextPath}/js/jquery.flexslider-min.js" type="text/javascript"></script>
        <script src="${pageContext.request.contextPath}/js/jquery.jcarousel.js" type="text/javascript"></script>
        <script src="${pageContext.request.contextPath}/js/fancySelect.js"></script>
        <script src="${pageContext.request.contextPath}/js/animate.js" type="text/javascript"></script>
        <script src="${pageContext.request.contextPath}/js/myscript.js" type="text/javascript"></script>
        <script>
                                                    if (top != self)
                                                        top.location.replace(self.location.href);

                                                    function verificaChecks() {
                                                        let aChk = document.getElementsByName("checkProduto");
                                                        let qtdeProduto = document.getElementsByName("qtdeProduto");
                                                        let qtdePedido = document.getElementsByName("qtdePedido");
                                                        let flgCheck = 0;
                                                        let flgQtde = 0;
                                                        for (let i = 0; i < aChk.length; i++) {
                                                            if (aChk[i].checked == true) {
                                                                flgCheck += 1;
                                                                if( qtdeProduto[i].value <= qtdePedido[i].value && qtdeProduto[i].value > 0){
                                                                    flgQtde += 1;
                                                                }
                                                            }
                                                        }
                                                        if (flgCheck == 0) {
                                                            document.getElementById("aviso").innerHTML = 'É necessário marcar um campo de produto';
                                                        } else if (flgCheck > 0){                                                                                                                       
                                                            document.devolucao.submit();
                                                        }else{
                                                            document.getElementById("aviso").innerHTML = 'As quantidades de cada produto nao podem exeder a quantidade comprada.';
                                                        }
                                                    }

        </script>
    </body>
</html>
