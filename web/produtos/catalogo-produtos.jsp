<%@page import="br.com.ecomerce.dominio.Estoque"%>
<%@page import="br.com.ecomerce.dominio.Categoria"%>
<%@page import="br.com.ecomerce.dominio.SubCategoria"%>
<%@page import="br.com.ecomerce.dominio.EntidadeDominio"%>
<%@page import="br.com.ecomerce.dominio.Resultado"%>
<%@page import="br.com.ecomerce.dominio.Produto"%>
<%@page import="br.com.ecomerce.dominio.Carrinho"%>
<%@page import="java.util.ArrayList"%>
<%@page import="br.com.ecomerce.dominio.Cliente"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<html>
    <head>
        <meta charset="utf-8"/>
        <title>Explosion | E-Commerce de Eletrônicos</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">

        <link rel="shortcut icon" href="${pageContext.request.contextPath}/images/logo/explosionV1.png">

        <!-- CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/css/flexslider.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/css/fancySelect.css" rel="stylesheet" media="screen, projection" />
        <link href="${pageContext.request.contextPath}/css/animate.css" rel="stylesheet" type="text/css" media="all" />
        <link href="${pageContext.request.contextPath}/css/style.css" rel="stylesheet" type="text/css" />
        <link href="${pageContext.request.contextPath}/css/estilo.css" rel="stylesheet" type="text/css" />

        <!-- FONTS -->
        <link href='http://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
        <link href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">

    </head>
    <body>

        <!-- PRELOADER -->

        <!-- //PRELOADER -->
        <div id="preloader"><img src="${pageContext.request.contextPath}/images/preloader.gif" alt="" /></div>
        <div class="preloader_hide">

            <!-- PAGE -->
            <div id="page">

                <!-- HEADER -->
                <header>

                    <!-- TOP INFO -->
                    <div class="top_info">

                        <!-- CONTAINER -->
                        <div class="container clearfix">
                            <ul class="secondary_menu">
                                <%

                                    if (session.getAttribute("cliente") != null) {
                                        Cliente cliente = (Cliente) session.getAttribute("cliente");
                                        if (cliente.getAdmin() == 1) {
                                            out.println("<li><a href='minha-conta-admin.jsp' >Minha Conta</a></li>");
                                            out.println("<li><a>Bem Vindo " + cliente.getNomeCompleto() + "</a></li>");
                                            out.println("<li><a href='Logout?logout=logout'>Logout</a></li>");
                                        } else {
                                            out.println("<li><a href='minha-conta.jsp' >Minha Conta</a></li>");
                                            out.println("<li><a>Bem Vindo " + cliente.getNomeCompleto() + "</a></li>");
                                            out.println("<li><a href='Logout?logout=logout'>Logout</a></li>");
                                        }

                                    } else {
                                        out.print("<li><a href='login.jsp' >Login</a></li>");
                                        out.println("<li><a href='cadastro-cliente.jsp' >Registrar</a></li>");
                                    }
                                %>
                            </ul>
                        </div><!-- //CONTAINER -->
                    </div><!-- TOP INFO -->


                    <!-- MENU BLOCK -->
                    <div class="menu_block">

                        <!-- CONTAINER -->
                        <div class="container clearfix">

                            <!-- LOGO -->
                            <div class="logo">
                                <a href="${pageContext.request.contextPath}/index.jsp" ><img src="${pageContext.request.contextPath}/images/logo/explosionV2.png" alt="" /></a>
                            </div><!-- //LOGO -->


                            <!-- SEARCH FORM -->
                            <div class="top_search_form">
                                <a class="top_search_btn" href="javascript:void(0);" ><i class="fa fa-search"></i></a>
                                <form action="${pageContext.request.contextPath}/VisualizarProduto" method='POST'>
                                    <input type="text" name="pesquisar" value="Pesquisar" onFocus="if (this.value == 'Pesquisar')
                                                this.value = '';" onBlur="if (this.value == '')
                                                            this.value = 'Pesquisar';" />
                                    <input type='hidden' id='OPERACAO' name='OPERACAO' value='VISUALIZAR'/>
                                    <input type='hidden' id='PAGINA' name='PAGINA' value='PESQUISA'/>
                                </form>

                            </div><!-- SEARCH FORM -->

                            <%
                                double total = 0;
                                if (null == session.getAttribute("carrinho")) {
                                    ArrayList<Produto> produtos = new ArrayList<Produto>();
                                    Carrinho carrinho = new Carrinho();
                                    carrinho.setProdutos(produtos);
                                    session.setAttribute("carrinho", carrinho);
                            %>
                            <div class="shopping_bag">
                                <a class="shopping_bag_btn" href = "javascript:void(0);" > 
                                    <i class="fa fa-shopping-cart"></i > <p>Carrinho</p > <span> <% out.print(carrinho.getProdutos().size());%></span></a>
                                <div class="cart">
                                    <div class="cart_total">
                                        <div class="clearfix">
                                            <span class="cart_subtotal">Total: <b><% out.print("R$" + total);%></b></span>
                                        </div>
                                        <a class="btn active" href="logado/carrinho-compra.jsp">Ir ao Carrinho</a>
                                    </div>
                                </div>
                            </div>
                            <%
                            } else {

                                Carrinho carrinho = (Carrinho) session.getAttribute("carrinho");

                            %>
                            <!-- SHOPPING BAG -->
                            <div class="shopping_bag">
                                <a class="shopping_bag_btn" href="javascript:void(0);" >
                                    <i class="fa fa-shopping-cart"></i><p>Carrinho</p><span><%out.print(carrinho.getProdutos().size());%></span></a>
                                <div class="cart">
                                    <ul class="cart-items">
                                        <%
                                            for (Produto pro : carrinho.getProdutos()) {
                                                out.print("<li class='clearfix'>");
                                                out.print("<img class='cart_item_product' src='" + pro.getEndereco_imagem() + "'/>");
                                                out.print(pro.getQuantidadeProdutoCarrinho() + " x " + pro.getNome());
                                                out.print("<span class='cart_item_price'> R$" + pro.getPreco() + "</span>");
                                                out.print("</li>");
                                                total += pro.getPreco() * pro.getQuantidadeProdutoCarrinho();
                                            }
                                        %>
                                    </ul>
                                    <div class="cart_total">
                                        <div class="clearfix"><span class="cart_subtotal">Total: <b><% out.print("R$" + total);%></b></span></div>
                                        <a class="btn active" href="logado/carrinho-compra.jsp">Ir ao Carrinho</a>
                                    </div>
                                </div>
                            </div>
                            <%
                                }
                            %>

                            <!-- MENU -->
                            <ul class="navmenu center">
                                <li>
                                    <a href="index.jsp" >Home</a>
                                </li>
                                <%
                                    Resultado resultadoCategorias = (Resultado) session.getAttribute("categorias");
                                    Categoria catg;
                                    SubCategoria sub;
                                    for (EntidadeDominio entity : resultadoCategorias.getEntidades()) {
                                        catg = (Categoria) entity;
                                %>
                                <li class='sub-menu'>
                                    <form name='categoria<%out.print(catg.getId());%>' action='${pageContext.request.contextPath}/VisualizarEstoque' method='POST' hidden>
                                        <input type='hidden' id='idCategoria' name='idCategoria' value='<% out.print(catg.getId());%>'/>
                                        <input type='hidden' id='OPERACAO' name='OPERACAO' value='VISUALIZAR'/>
                                        <input type='hidden' id='PAGINA' name='PAGINA' value='CATALOGO'/>                                        
                                    </form>
                                    <a href='javascript:categoria<%out.print(catg.getId());%>.submit();'>
                                        <%out.print(catg.getNome()); %>
                                    </a>
                                    <%
                                        if (catg.getSubCategorias().size() == 0) {
                                            out.println("</li>");
                                        } else {
                                    %>
                                    <ul class='mega_menu megamenu_col1 clearfix'>
                                        <li class='col'>
                                            <ol>
                                                <%
                                                    for (EntidadeDominio entitySub : catg.getSubCategorias()) {
                                                        sub = (SubCategoria) entitySub;
                                                %>
                                                <li>
                                                    <form name='sub<% out.print(sub.getId());%>' action='${pageContext.request.contextPath}/VisualizarEstoque' method='POST' hidden>
                                                        <input type='hidden' id='OPERACAO' name='OPERACAO' value='VISUALIZAR'/>
                                                        <input type='hidden' id='PAGINA' name='PAGINA' value='CATALOGO'/>
                                                        <input type='hidden' id='idSubCategoria' name='idSubCategoria' value='<%out.print(sub.getId());%>'/>
                                                    </form>
                                                    <a href='javascript:sub<% out.print(sub.getId());%>.submit();'><%out.print(sub.getNome());%></a>
                                                </li>
                                                <%}%>
                                            </ol>
                                        </li>
                                    </ul>
                                    <%}%>
                                </li>
                                <%
                                    }
                                %>                                                                
                            </ul><!-- //MENU -->                            
                        </div><!-- //CONTAINER -->
                    </div><!-- //MENU BLOCK -->
                </header><!-- //HEADER -->

                <%
                    Resultado resultado = (Resultado) request.getAttribute("resultado");
                    Estoque estoque;
                    ArrayList<Estoque> estoques = new ArrayList<Estoque>();
                    for (EntidadeDominio entity : resultado.getEntidades()) {
                        estoque = (Estoque) entity;
                        estoques.add(estoque);
                    }
                %>

                <!-- BREADCRUMBS -->
                <section class="breadcrumb men parallax margbot30">

                    <!-- CONTAINER -->
                    <div class="container">
                        <%
                            String sidePanel = "";

                            if (estoques.get(0).getProduto().getSubCategoria().getId() != 0) {
                                for (EntidadeDominio entity : resultadoCategorias.getEntidades()) {
                                    catg = (Categoria) entity;
                                    for (EntidadeDominio entitySub : catg.getSubCategorias()) {
                                        sub = (SubCategoria) entitySub;
                                        if (estoques.get(0).getProduto().getSubCategoria().getId() == entitySub.getId()) {
                                            sidePanel = catg.getNome()
                                                    + " - "
                                                    + sub.getNome();
                                        }
                                    }
                                }
                            } else {
                                for (EntidadeDominio entity : resultadoCategorias.getEntidades()) {
                                    catg = (Categoria) entity;

                                    if (catg.getId()
                                            == estoques.get(0).getProduto().getSubCategoria().getCategoria().getId()) {
                                        sidePanel = catg.getNome();
                                    }
                                }
                            }
                            estoques.remove(0);
                        %>
                        <h2 style="color">
                            <%out.print(sidePanel);%>
                        </h2>
                    </div><!-- //CONTAINER -->
                </section><!-- //BREADCRUMBS -->


                <!-- SHOP BLOCK -->
                <section class="shop">

                    <!-- CONTAINER -->
                    <div class="container">

                        <!-- ROW -->
                        <div class="row">
                            <!-- SIDEBAR -->
                            <div id="sidebar" class="col-lg-3 col-md-3 col-sm-3 padbot50">                                

                                <!-- CATEGORIES -->
                                <div class="sidepanel widget_categories">
                                    <%
                                        for (EntidadeDominio entity : resultadoCategorias.getEntidades()) {
                                            catg = (Categoria) entity;
                                    %>
                                    <form name='sideCategoria<%out.print(catg.getId());%>' action='${pageContext.request.contextPath}/VisualizarEstoque' method='POST' hidden >
                                        <input type='hidden' id='OPERACAO' name='OPERACAO' value='VISUALIZAR'/>
                                        <input type='hidden' id='PAGINA' name='PAGINA' value='CATALOGO'/>
                                        <input type='hidden' id='idCategoria' name='idCategoria' value='<%out.print(catg.getId());%>'/>                                        
                                    </form>                                
                                    <a href='javascript:sideCategoria<%out.print(catg.getId());%>.submit();'>
                                        <h3><%out.println(catg.getNome());%></h3>
                                    </a>    
                                    <%
                                        if (catg.getSubCategorias().size() > 0) {
                                    %>
                                    <ul>
                                        <%
                                            for (EntidadeDominio entitySub : catg.getSubCategorias()) {
                                                sub = (SubCategoria) entitySub;
                                        %>
                                        <li>
                                            <form name='sideSub<%out.print(sub.getId());%>' action='${pageContext.request.contextPath}/VisualizarEstoque' method='POST' hidden>                                                
                                                <input type='hidden' id='OPERACAO' name='OPERACAO' value='VISUALIZAR'/>
                                                <input type='hidden' id='PAGINA' name='PAGINA' value='CATALOGO'/>
                                                <input type='hidden' id='idSubCategoria' name='idSubCategoria' value='<%out.print(sub.getId());%>'/>
                                            </form>
                                            <a href='javascript:sideSub<%out.print(sub.getId());%>.submit();'>
                                                <%out.println(sub.getNome());%>
                                            </a>    
                                        </li>    
                                        <%
                                                    }
                                                    out.println("</ul>");
                                                }
                                            }
                                        %>
                                </div><!-- //CATEGORIES -->
                            </div><!-- //SIDEBAR -->

                            <!-- SHOP PRODUCTS -->
                            <div class="col-lg-9 col-sm-9 padbot20">

                                <!-- SORTING TOVAR PANEL -->
                                <div class="sorting_options clearfix">

                                    <!-- COUNT TOVAR ITEMS -->
                                    <div class="count_tovar_items">
                                        <p><%out.println(sidePanel);%></p>
                                        <span>
                                            <%
                                                out.print(estoques.size() + " Produtos");
                                            %> 
                                        </span>
                                    </div><!-- //COUNT TOVAR ITEMS -->                                                                    
                                </div><!-- //SORTING TOVAR PANEL -->

                                <!-- ROW -->
                                <div class="row shop_block">
                                    <!-- TOVARS -->
                                    <%
                                        for (Estoque estoqueProduto : estoques) {
                                            if (estoqueProduto.getQuantidadeEstoque() > 0) {
                                    %>
                                    <div class='col-lg-3 col-md-3 col-sm-4 col-xs-6 col-ss-12 padbot40'>
                                        <form name='produto<%out.print(estoqueProduto.getProduto().getId());%>' action='${pageContext.request.contextPath}/VisualizarProduto' method='POST'>
                                            <div class='tovar_item'>
                                                <div class='tovar_img'>
                                                    <div class='tovar_img_wrapper'>
                                                        <%
                                                            if (!estoqueProduto.getProduto().getEndereco_imagem().trim().equals("") || !estoqueProduto.getProduto().getEndereco_imagem_lado().trim().equals("")) {

                                                        %>                                                    
                                                        <img class='img' src='${pageContext.request.contextPath}/<%out.print(estoqueProduto.getProduto().getEndereco_imagem_lado());%>' alt='123' />
                                                        <img class='img_h' src='${pageContext.request.contextPath}/<%out.print(estoqueProduto.getProduto().getEndereco_imagem());%>' alt='123' />
                                                        <%
                                                        } else {
                                                        %>
                                                        <img class='img' src='<%out.print(request.getContextPath() + "/images/imagemIndisponivel.png");%>' alt='' />
                                                        <img class='img_h' src='<%out.print(request.getContextPath() + "/images/imagemIndisponivel.png");%>' alt='' />
                                                        <%
                                                            }
                                                        %>
                                                    </div>
                                                    <div class='tovar_item_btns'>
                                                        <a class="open-project tovar_view" href='javascript:produto<%out.print(estoqueProduto.getProduto().getId());%>.submit();'>Visualizar</a>
                                                    </div>
                                                </div>
                                                <div class='tovar_description clearfix'>
                                                    <a class='tovar_title' href='javascript:produto<%out.print(estoqueProduto.getProduto().getId());%>.submit();'>
                                                        <%out.print(estoqueProduto.getProduto().getNome());%>
                                                    </a>
                                                    <span class='tovar_price'>
                                                        R$<%out.print(estoqueProduto.getProduto().getPreco());%>
                                                    </span>
                                                </div>
                                                <input type='hidden' name='idProduto' id='idProduto' value='<%out.print(estoqueProduto.getProduto().getId());%>'/>
                                                <input type='hidden' id='OPERACAO' name='OPERACAO' value='VISUALIZAR'/>
                                                <input type='hidden' id='PAGINA' name='PAGINA' value='DETALHES'/>
                                            </div>
                                        </form>
                                    </div><!-- //TOVAR -->
                                    <%
                                            }
                                        }
                                        for (Estoque estoqueProduto : estoques) {
                                            if (estoqueProduto.getQuantidadeEstoque() == 0) {
                                    %>
                                    <div class='col-lg-3 col-md-3 col-sm-4 col-xs-6 col-ss-12 padbot40'>
                                        <div class='tovar_item'>
                                            <div class='tovar_img'>
                                                <div class='tovar_img_wrapper'>
                                                    <%
                                                        if (!estoqueProduto.getProduto().getEndereco_imagem().trim().equals("") || !estoqueProduto.getProduto().getEndereco_imagem_lado().trim().equals("")) {

                                                    %>                                                    
                                                    <img class='img' src='${pageContext.request.contextPath}/<%out.print(estoqueProduto.getProduto().getEndereco_imagem_lado());%>' alt='123' />
                                                    <img class='img_h' src='${pageContext.request.contextPath}/<%out.print(estoqueProduto.getProduto().getEndereco_imagem());%>' alt='123' />
                                                    <%
                                                    } else {
                                                    %>
                                                    <img class='img' src='<%out.print(request.getContextPath() + "/images/imagemIndisponivel.png");%>' alt='' />
                                                    <img class='img_h' src='<%out.print(request.getContextPath() + "/images/imagemIndisponivel.png");%>' alt='' />
                                                    <%
                                                        }
                                                    %>
                                                </div>
                                                <div class='tovar_item_btns'>
                                                    <div class="open-project-link"><a class="open-project tovar_view" href="javascript:void(0);">Indisponivel</a></div>
                                                </div>
                                            </div>
                                            <div class='tovar_description clearfix'>
                                                <a class='tovar_title' href="javascript:void(0);">
                                                    <%out.print(estoqueProduto.getProduto().getNome());%>
                                                </a>
                                                <span class='tovar_price'>
                                                    R$<%out.print(estoqueProduto.getProduto().getPreco());%>
                                                </span>
                                            </div>
                                        </div>
                                    </div><!-- //TOVAR -->
                                    <%
                                            }
                                        }
                                    %>
                                </div><!-- //ROW -->
                                <%
                                    if (estoques.size()
                                            == 0) {
                                        out.println("<h1>Nenhum Produto Foi Encontrado!</h1>");
                                        out.print("<hr>");
                                    } else {
                                        out.print("<hr>");
                                %>


                                <div class="clearfix">
                                    <!-- PAGINATION -->
                                    <ul class="pagination" style ="margin:auto;">
                                        <li><a href="javascript:void(0);" >1</a></li>
                                        <li><a href="javascript:void(0);" >2</a></li>
                                        <li class="active"><a href="javascript:void(0);" >3</a></li>
                                        <li><a href="javascript:void(0);" >4</a></li>
                                        <li><a href="javascript:void(0);" >5</a></li>
                                        <li><a href="javascript:void(0);" >6</a></li>
                                        <li><a href="javascript:void(0);" >...</a></li>
                                    </ul><!-- //PAGINATION -->                                    
                                </div>
                                <hr>
                                <%}%>
                            </div><!-- //SHOP PRODUCTS -->
                        </div><!-- //ROW -->                        
                    </div><!-- //CONTAINER -->                    
                </section><!-- //SHOP -->

                <!-- FOOTER -->
                <footer>

                    <!-- CONTAINER -->
                    <div class="container" data-animated='fadeInUp'>

                        <!-- ROW -->
                        <div class="row">

                            <div class="col-lg-2 col-md-2 col-sm-3 col-xs-6 col-ss-12 padbot30">
                                <h4>Contato</h4>
                                <div class="foot_address"><span>Explosion E-Commerce</span>Rua Zé das Colves, Guiana City</div>
                                <div class="foot_phone"><a href="${pageContext.request.contextPath}/#" >(11) 4002-8922</a></div>
                                <div class="foot_mail"><a href="${pageContext.request.contextPath}/#" >explosionecommerce@explosion.com</a></div>
                            </div>

                            <div class="col-lg-2 col-md-2 col-sm-3 col-xs-6 col-ss-12 padbot30">
                                <h4>Information</h4>
                                <ul class="foot_menu">
                                    <li><a href="${pageContext.request.contextPath}/#" >Sobre Nós</a></li>
                                    <li><a href="javascript:void(0);" >Política de Privacidade</a></li>
                                    <li><a href="${pageContext.request.contextPath}/contacts.html" >Contato</a></li>
                                </ul>
                            </div>

                            <div class="respond_clear_480"></div>

                            <div class="col-lg-4 col-md-4 col-sm-6 padbot30">
                                <h4>Sobre a Loja</h4>
                                <p>Nós somos uma loja renomada no mercado, vendemos os produtos mais diversificados. Pode confiar!</p>
                            </div>

                            <div class="respond_clear_768"></div>

                            <div class="col-lg-4 col-md-4 padbot30">
                                <h4>Boletim de Notícias</h4>
                                <form class="newsletter_form clearfix" action="javascript:void(0);" method="get">
                                    <input type="text" name="newsletter" value="Informe seu e-mail" onFocus="if (this.value == 'Enter E-mail & Get 10% off')
                                                this.value = '';" onBlur="if (this.value == '')
                                                            this.value = 'Enter E-mail & Get 10% off';" />
                                    <input class="btn newsletter_btn" type="submit" value="SIGN UP">
                                </form>

                                <h4>Nossas Redes Sociais</h4>
                                <div class="social">
                                    <a href="javascript:void(0);" ><i class="fa fa-twitter"></i></a>
                                    <a href="javascript:void(0);" ><i class="fa fa-facebook"></i></a>
                                    <a href="javascript:void(0);" ><i class="fa fa-google-plus"></i></a>
                                    <a href="javascript:void(0);" ><i class="fa fa-pinterest-square"></i></a>
                                    <a href="javascript:void(0);" ><i class="fa fa-tumblr"></i></a>
                                    <a href="javascript:void(0);" ><i class="fa fa-instagram"></i></a>
                                </div>
                            </div>
                        </div><!-- //ROW -->
                    </div><!-- //CONTAINER -->

                    <!-- COPYRIGHT -->
                    <div class="copyright">

                        <!-- CONTAINER -->
                        <div class="container clearfix">
                            <div class="foot_logo"><a href="${pageContext.request.contextPath}/index.jsp" ><img src="${pageContext.request.contextPath}/images/foot_logo.png" alt="" /></a></div>

                            <div class="copyright_inf">
                                <span>Explosion E-Commerce © 1950</span> |                                            
                                <a class="back_top" href="${pageContext.request.contextPath}/javascript:void(0);" >Voltar ao topo <i class="fa fa-angle-up"></i></a>
                            </div>
                        </div><!-- //CONTAINER -->
                    </div><!-- //COPYRIGHT -->
                </footer><!-- //FOOTER -->
            </div><!-- //PAGE -->
        </div>


        <!-- SCRIPTS -->
        <!--[if IE]><script src="${pageContext.request.contextPath}/http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
        <!--[if IE]><html class="ie" lang="en"> <![endif]-->

        <script src="${pageContext.request.contextPath}/js/jquery.min.js" type="text/javascript"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="${pageContext.request.contextPath}/js/jquery.sticky.js" type="text/javascript"></script>
        <script src="${pageContext.request.contextPath}/js/parallax.js" type="text/javascript"></script>
        <script src="${pageContext.request.contextPath}/js/jquery.flexslider-min.js" type="text/javascript"></script>
        <script src="${pageContext.request.contextPath}/js/jquery.jcarousel.js" type="text/javascript"></script>
        <script src="${pageContext.request.contextPath}/js/fancySelect.js"></script>
        <script src="${pageContext.request.contextPath}/js/animate.js" type="text/javascript"></script>
        <script src="${pageContext.request.contextPath}/js/myscript.js" type="text/javascript"></script>
        <script>
                                                    if (top != self)
                                                        top.location.replace(self.location.href);
        </script>

    </body>
</html>
