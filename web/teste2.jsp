<%@page import="java.text.DecimalFormat"%>
<%@page import="br.com.ecomerce.dominio.AnaliseGrafico"%>
<%@page import="br.com.ecomerce.dominio.EntidadeDominio"%>
<%@page import="br.com.ecomerce.dominio.Resultado"%>
<%@page import="java.util.ArrayList"%>
<%@page import="br.com.ecomerce.dominio.Produto"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.SQLException"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.Connection"%>
<html>

    <head>

        <link rel="stylesheet" type="text/css" href="http://ajax.aspnetcdn.com/ajax/jquery.dataTables/1.9.4/css/jquery.dataTables.css"/>
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    </head>

    <body>
        <div class="col-xs-11">
            <canvas id="myChart"></canvas>
        </div>


        <script src="http://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.8.3.min.js"></script>
        <script src="http://ajax.aspnetcdn.com/ajax/jquery.dataTables/1.9.4/jquery.dataTables.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>

        <script>
            if (top != self)
                top.location.replace(self.location.href);

            // Script que gera o grafico
            // Contadores para atribuir valores
            var contador = 0;
            var contadorData = 0;
            var contadorLabel = 0;
            
            // Cores possiveis para as linhas
            var borderColors = ["red", "green", "blue", "yellow", "black", "pink", "orange", "violet", "brown", "purple", "gold"];
            
            // variavel para o titulo
            var titulo = "";
            var labelPeriodo = [];

            <%
                Resultado resultado = (Resultado) request.getAttribute("resultado");
                ArrayList<AnaliseGrafico> analises = new ArrayList<AnaliseGrafico>();
                AnaliseGrafico analise = new AnaliseGrafico();
                DecimalFormat df = new DecimalFormat("0.##");
                boolean flg = false;
                ArrayList<String> periodos = new ArrayList<String>();
            %>
            var datasetValue = [<%=resultado.getEntidades().size()%>];
            <%
                for (EntidadeDominio entity : resultado.getEntidades()) {
                    analise = (AnaliseGrafico) entity;
            %>
            var valorAnalise = ['<%=analise.getDados().size()%>'];
            <%
                for (String dado : analise.getDados()) {
            %>
            valorAnalise[contadorData] = '<%=dado%>';
            contadorData++;
            <%
                }
            %>

            datasetValue[contador] = {
                borderColor: borderColors[contador],
                fill: false,
                label: '<%=analise.getProdutos().get(0).getNome()%>',
                data: valorAnalise
            };
            contadorData = 0;
            contador++;
            <%

                }
            %>
            <%
                for (String periodo : analise.getPeriodos()) {

            %>
            labelPeriodo[contadorLabel] = '<%=periodo%>';
            contadorLabel++;

            <%
                }
            %>

            let ctx = document.getElementById('myChart');
            let chart = new Chart(ctx, {
                type: 'line',
                data: {
                    labels: labelPeriodo,
                    datasets: datasetValue
                }, options: {
                    scales: {
                        yAxes: [{
                                ticks: {
                                    beginAtZero: true
                                }
                            }]
                    }
                }
            });
        </script>
    </body>
</html>